Gnome Basic

Michael Meeks

michael@helixcode.com

(C) Helix Code, Inc. 2000


Introduction

	Gnome Basic or GB is an embryonic attempt to provide
Visual Basic like functionality within the GNOME environment.
It is growing out a realization that currently Microsoft
software and development tools are, regrettably, a standard.
Unlike several comparable Basic like projects GB is committed
to attempting to achieve 100% compatibility, both syntactic and
functional, with Visual Basic. This is the primary and immutable
goal.

Rational

	The need for a Visual Basic equivalent is not perhaps
instantly apparent. The project sprang from the gnumeric project
and the ability to extract plain VBA text from excel files.
Clearly the higher quality the compatibility with Microsoft Office
the better. Many people appear to think that a superior
approach would be to convert the VBA code to python. This on the
face of it seems like a good idea. However when the problems of
backwards compatibility are raised, and by this I mean exporting
to Visual Basic, the proponents of python fall silent. Furthermore
I envisage serious problems adapting Python to some of the more
primitive issues that will have to be faced.

Design

	The project divides neatly into two parts: a parser
and an interpreter. It is intended in the fullness of time
to also create a Gnome Basic -> C converter using the parser
as a shared front end.

Progress

	So far progress has been good. Whilst being most interested
in getting the core solid there has been work on implementing more
functions than strictly needed to test, allowing small programs to
be written. The parser, whilst needing lots of work for the 'wealth'
of statements Gnome Basic has to provide, is in good shape. The
interpreter is evolving nicely, and a chunk of work is being put
into emulating Visual Basic Forms, to allow effortless GUI porting.

	Gnome Basic has also been paired up with the spreadsheet to
allow writing complex iterative functions for transparent use in the
spreadsheet.

	There are currently no plans to provide a development
environment akin to the VB IDE.

Conclusion

	There is a long way to go, however whilst the project is
large, many hands chipping away at it as necessary will I think
allow GB to snowball as more and more people use it.

      For more information see http://www.gnome.org/gb