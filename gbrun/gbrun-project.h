/*
 * GNOME Basic project runtime
 *
 * Author:
 *    Michael Meeks <michael@helixcode.com>
 *
 * Copyright 2000, Helix Code, Inc.
 */

#ifndef GBRUN_PROJECT_H
#define GBRUN_PROJECT_H

#include <gbrun/gbrun.h>
#include <gb/gb-project.h>

#define GBRUN_TYPE_PROJECT            (gbrun_project_get_type ())
#define GBRUN_PROJECT(obj)            (GTK_CHECK_CAST ((obj), GBRUN_TYPE_PROJECT, GBRunProject))
#define GBRUN_PROJECT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GBRUN_TYPE_PROJECT, GBRunProjectClass))
#define GBRUN_IS_PROJECT(obj)	      (GTK_CHECK_TYPE ((obj), GBRUN_TYPE_PROJECT))
#define GBRUN_IS_PROJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GBRUN_TYPE_PROJECT))

typedef struct _GBRunProjectPriv GBRunProjectPriv;

struct _GBRunProject {
	GBObject          object;

	GBRunProjectPriv *priv;
};

struct _GBRunProjectClass {
	GBObjectClass     klass;
};

GtkType           gbrun_project_get_type          (void);

void              gbrun_project_register_module   (GBRunProject *project,
						   GBObject     *object);

void              gbrun_project_deregister_module (GBRunProject *project,
						   GBObject     *object);

void              gbrun_project_register_object   (GBRunProject *project,
						   const char   *name,
						   GBRunObject  *object);

void              gbrun_project_deregister_object (GBRunProject *project,
						   const char   *name);

#endif /* GBRUN_PROJECT_H */
