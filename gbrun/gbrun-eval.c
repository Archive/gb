/*
 * GNOME Expression evaluation
 *
 * Author:
 *    Michael Meeks <michael@imaginator.com>
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <math.h>
#include <setjmp.h>

#include <gbrun/gbrun.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-stack.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-object.h>
#include <gbrun/gbrun-file.h>

static GBEvalContextClass *gbrun_eval_context_parent = NULL;

static GBValue *
eval_expr (GBEvalContext  *ec,
	   const GBExpr   *e)
{
	GBValue *val = NULL;

	g_return_val_if_fail (e != NULL, NULL);

	if (e->type == GB_EXPR_VALUE)
		val = gb_value_copy (ec, e->parm.value);

	else if (e->type == GB_EXPR_OBJREF)
		val = gbrun_eval_objref (GBRUN_EVAL_CONTEXT (ec), e);

	else
		g_warning ("Unhandled expr type in eval %d", e->type);

	return val;
}

GBRunEvalContext *
gbrun_eval_context_construct (GBRunEvalContext  *ec,
			      const char        *module_name,
			      GBRunSecurityFlag  flags)
{
	g_return_val_if_fail (module_name != NULL, NULL);

	ec->stack = gtk_type_new (GBRUN_TYPE_STACK);
	gbrun_stack_call (ec, module_name);

	ec->flags = flags;

	ec->on_error.type = GB_ON_ERROR_FLAG;

	ec->me = NULL;

	ec->random.reseed = FALSE;
	ec->random.randseed = 0;

	gbrun_files_init (ec);

	gbrun_eval_context_set_module (ec, module_name);

	return ec;
}

GBRunEvalContext *
gbrun_eval_context_new (const char        *module_name,
			GBRunSecurityFlag  flags)
{
	GBRunEvalContext *ec;

	ec = gtk_type_new (GBRUN_TYPE_EVAL_CONTEXT);

	return gbrun_eval_context_construct (ec, module_name, flags);
}

static void
gbrun_eval_context_destroy (GtkObject *o)
{
	GBRunEvalContext *ec;

	g_return_if_fail (GBRUN_IS_EVAL_CONTEXT (o));

	ec = GBRUN_EVAL_CONTEXT (o);
	if (ec) {
		GBObject *object;
		
		gtk_object_unref (GTK_OBJECT (ec->stack));
		ec->stack = NULL;
		gbrun_files_clean (ec);

		if (ec->me)
			gb_value_destroy (ec->me);

		g_free (ec->module);

		while (gbrun_eval_context_proj_pop (ec));

		while ((object = gbrun_eval_context_with_pop (ec)))
			gb_object_unref (object);
	}

	GTK_OBJECT_CLASS (gbrun_eval_context_parent)->destroy (o);
}

static void
fire (GBEvalContext *ec, const char *txt)
{
	char        **stack;
	char         *msg;
	int           i;
	
	g_return_if_fail (txt != NULL);
	g_return_if_fail (GBRUN_IS_EVAL_CONTEXT (ec));

	stack = gbrun_stack_dump (GBRUN_EVAL_CONTEXT (ec)->stack);
	msg = g_strconcat (txt, " in module ",
			   GBRUN_EVAL_CONTEXT (ec)->module, NULL);

	for (i = 1; stack [i]; i++) {
		char *tmp = msg;
		msg = g_strconcat (tmp, "\n called from ", stack [i], NULL); 
		g_free (tmp);
	}
	
	g_strfreev (stack);

	gbrun_eval_context_parent->fire (ec, msg);

	g_free (msg);
}

GBValue *
gbrun_exception_fire (GBRunEvalContext *ec,
		      const char *txt)
{
	GBEvalContextClass *klass;

	klass = GB_EVAL_CONTEXT_CLASS (GTK_OBJECT (ec)->klass);
	g_return_val_if_fail (klass != NULL, NULL);

	klass->fire (GB_EVAL_CONTEXT (ec), txt);

	return NULL;
}

GBValue *
gbrun_exception_firev (GBRunEvalContext *ec,
		       const char *format, ...)
{
	va_list args;
	char   *txt;
  
	va_start (args, format);

	txt = g_strdup_vprintf (format, args);
	gbrun_exception_fire (ec, txt);
	g_free (txt);

	va_end (args);

	return NULL;
}

GBObject *
gbrun_eval_context_me_get (GBRunEvalContext *ec)
{
	g_return_val_if_fail (ec != NULL, NULL);

	if (!ec->me)
		return NULL;

	g_return_val_if_fail (GB_IS_AN_OBJECT (ec->me->gtk_type), NULL);

	return GB_OBJECT (ec->me->v.obj);
}

void
gbrun_eval_context_me_set (GBRunEvalContext *ec,
			   GBObject         *obj)
{
	g_return_if_fail (ec != NULL);

	if (ec->me)
		gb_value_destroy (ec->me);

	if (obj)
		ec->me = gb_value_new_object (
			gb_object_ref (GB_OBJECT (obj)));
	else
		ec->me = NULL;
}

gboolean
gbrun_eval_context_exception (GBRunEvalContext *ec)
{
	g_return_val_if_fail (ec != NULL, TRUE);

	if (GB_EVAL_CONTEXT (ec)->exception)
		return TRUE;

	return FALSE;
}

static void
gbrun_eval_context_class_init (GBRunEvalContextClass *klass)
{
	GtkObjectClass     *object_class;
	GBEvalContextClass *eval_context_class;

	gbrun_eval_context_parent = gtk_type_class (GB_TYPE_EVAL_CONTEXT);

	object_class          = (GtkObjectClass*) klass;
	object_class->destroy = gbrun_eval_context_destroy;

	eval_context_class       = (GBEvalContextClass *)klass;
	eval_context_class->eval = eval_expr;
	eval_context_class->fire = fire;
}

static void
gbrun_eval_context_init (GBRunEvalContext *ec)
{
	/* FIXME: should setup the stack here etc. */
	ec->me = NULL;
}

GtkType
gbrun_eval_context_get_type (void)
{
	static GtkType eval_type = 0;

	if (!eval_type) {
		static const GtkTypeInfo eval_info = {
			"GBRunEvalContext",
			sizeof (GBRunEvalContext),
			sizeof (GBRunEvalContextClass),
			(GtkClassInitFunc) gbrun_eval_context_class_init,
			(GtkObjectInitFunc) gbrun_eval_context_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		eval_type = gtk_type_unique (GB_TYPE_EVAL_CONTEXT, &eval_info);
	}

	return eval_type;	
}

GBRunProject *
gbrun_eval_context_proj_get (GBRunEvalContext *ec)
{
	if (ec && ec->projs)
		return ec->projs->data;
	else
		return NULL;
}

void
gbrun_eval_context_proj_push (GBRunEvalContext *ec,
			      GBRunProject     *proj)
{
	g_return_if_fail (ec != NULL);

	gtk_object_ref (GTK_OBJECT (proj));
	ec->projs = g_slist_prepend (ec->projs, proj);
}
      
GBRunProject *
gbrun_eval_context_proj_pop (GBRunEvalContext *ec)
{
	GBRunProject *proj;

	if (!ec || !ec->projs)
		return NULL;

	proj = ec->projs->data;
	ec->projs = g_slist_remove (ec->projs, proj);
	gtk_object_unref (GTK_OBJECT (proj));

	return proj;
}

void
gbrun_eval_context_with_push (GBRunEvalContext *ec,
			      GBObject         *object)
{
	g_return_if_fail (ec != NULL);
	g_return_if_fail (object != NULL);

	ec->with = g_slist_prepend (ec->with, gb_object_ref (object));
}

GBObject *
gbrun_eval_context_with_pop  (GBRunEvalContext *ec)
{
	GBObject *object = NULL;
	
	if (!ec || !ec->with)
		return NULL;

	object = ec->with->data;
	ec->with = g_slist_remove (ec->with, object);

	return object;
}

GBValue *
gbrun_eval_as (GBRunEvalContext  *ec,
	       const GBExpr      *expr,
	       GBValueType        ret_type)
{
	GBValue *tmp, *ans;

	tmp = gb_eval_context_eval (GB_EVAL_CONTEXT (ec), expr);
	if (!tmp || gbrun_eval_context_exception (ec))
		return tmp;

	ans = gb_value_promote (GB_EVAL_CONTEXT (ec),
				gb_gtk_type_from_value (ret_type), tmp);
	gb_value_destroy (tmp);

	return ans;
}

char *
gbrun_eval_context_get_module (GBRunEvalContext *ec)
{
	g_return_val_if_fail (GBRUN_IS_EVAL_CONTEXT (ec), NULL);

	return g_strdup (ec->module);
}

void
gbrun_eval_context_set_module (GBRunEvalContext *ec,
			       const char       *module)
{
	g_return_if_fail (GBRUN_IS_EVAL_CONTEXT (ec));

	g_free (ec->module);
	ec->module = g_strdup (module);
}
