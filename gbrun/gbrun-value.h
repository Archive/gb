/*
 * GNOME Basic Values manipulation.
 *
 * Authors:
 *    Michael Meeks <mmeeks@gnu.org>
 *    Ariel Rios    <ariel@arcavia.com>
 *
 * Copyright 2000, Helix Code, Inc.
 */

#ifndef GBE_VALUE_H
#define GBE_VALUE_H

#include <gbrun/gbrun.h>
#include <gb/gb-value.h>
#include <gbrun/gbrun-eval.h>

/*
 * NB. if you accidentaly use GB_VALUE_INTEGER instead of
 * GB_VALUE_INT you're on a sticky wicket.
 */
#define GB_IS_VALUE(e,v,t)     if (!(v) || ((v)->gtk_type != gb_gtk_type_from_value (t))) \
      return gbrun_exception_firev ((e),"... Incorrect argument type: %s should be %s", \
				    (v)?gtk_type_name ((v)->gtk_type):"Unknown",	\
				    gtk_type_name (gb_gtk_type_from_value (t)))

#define GB_IS_VALUE_OPT(e,v,t) if ( (v) && ((v)->gtk_type != gb_gtk_type_from_value (t))) \
      return gbrun_exception_firev ((e),"... Incorrect argument type: %s should be %s", \
				    (v)?gtk_type_name ((v)->gtk_type):"Unknown",	\
				    gtk_type_name (gb_gtk_type_from_value (t)))

#define  gbrun_value_promote(ec,v,to)    (gb_value_promote (GB_EVAL_CONTEXT (ec),	\
							    gb_gtk_type_from_value (to), (v)))

GBValue *gbrun_value_promote_name        (GBRunEvalContext *ec,
					  GBValue          *v,
					  const char       *type_name);

#define  gbrun_value_copy(ec,v)          (gb_value_copy ((GBEvalContext *) (ec), (v)))

struct _GBEArray {
	GSList   *indicees;
	gpointer  data;
};

GBValue *gbrun_value_new_array        (GBRunEvalContext *ec,
				       GSList   *indicees,
				       GBValue  *def);

GBValue *gbrun_value_array_lookup     (GBRunEvalContext *ec,
				       GBValue          *array,
				       const GBObjRef   *ref);

GBValue *gbrun_objref_deref           (GBRunEvalContext *ec,
				       GBObject         *object,
				       const GBObjRef   *ref,
				       gboolean          first_deref);

GBValue *gbrun_eval_objref            (GBRunEvalContext *ec,
				       const GBExpr     *objref);

gboolean gbrun_eval_assign            (GBRunEvalContext *ec,
				       const GSList     *objref,
				       GBValue          *value);

GBValue *gbrun_value_default_from_var (GBRunEvalContext *ec,
				       GBVar            *var);

#endif
