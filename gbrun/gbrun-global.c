#include "gbrun-global.h"

struct _GBRunGlobalPrivate {
	GSList *children;
};

typedef struct {
	char     *name;
	GBObject *object;
} GlobalObj;

static GlobalObj *
global_obj_new (GBObject   *object,
		const char *name)
{
	GlobalObj *ret = g_new0 (GlobalObj, 1);

	ret->name   = g_strdup (name);
	ret->object = object;
	gtk_object_ref (GTK_OBJECT (object));

	return ret;
}

static void
global_obj_destroy (GlobalObj *obj)
{
	if (obj) {
		g_free (obj->name);
		gtk_object_unref (GTK_OBJECT (obj->object));
		g_free (obj);
	}
}

static void
gbrun_global_destroy (GtkObject *global)
{
	GSList *l;
	GBRunGlobal *obj = (GBRunGlobal *) global;
	GBRunGlobalPrivate *priv = obj->priv;

	for (l = priv->children; l; l = l->next)
		global_obj_destroy (l->data);

	g_slist_free (priv->children);
	priv->children = NULL;
	g_free (priv);
}

static void
gbrun_global_copy (GBEvalContext  *ec,
		   GBObject       *src,
		   GBObject       *dest)
{
	g_warning ("Copying all global functions is not implemented");
}

static gboolean
gbrun_global_assign (GBEvalContext  *ec,
		     GBObject       *object,
		     const GBObjRef *ref,
		     GBValue        *value,
		     gboolean        try_assign)
{
	GSList  *l;
	GBRunGlobal *obj = GBRUN_GLOBAL (object);
	GBRunGlobalPrivate *priv = obj->priv;

	for (l = priv->children; l; l = l->next) {
		GlobalObj *obj = l->data;
		if (gb_object_assign (ec, obj->object, ref,
				      value, try_assign))
			return TRUE;
	}

	return FALSE;
}

static GBValue *
gbrun_global_deref (GBEvalContext  *ec,
		    GBObject       *object,
		    const GBObjRef *ref,
		    gboolean        try_deref)
{
	GSList  *l;
	GBValue *ret;
	GBRunGlobal *obj = GBRUN_GLOBAL (object);
	GBRunGlobalPrivate *priv = obj->priv;

	for (l = priv->children; l; l = l->next) {
		GlobalObj *obj = l->data;

		if ((ret = gb_object_deref (ec, obj->object,
					    ref, try_deref)))
			return ret;
	}

	if (!ref->parms) {
		for (l = priv->children; l; l = l->next) {
			GlobalObj *obj = l->data;
			
			if (!g_strcasecmp (obj->name, ref->name)) {
				ret = gb_value_new_object (obj->object);
				return ret;
			}
		}
	}

	return NULL;
}

static void
gbrun_global_class_init (GBRunGlobalClass *klass)
{
	GBObjectClass  *gb_class = (GBObjectClass *) klass;
	GtkObjectClass *gtk_class = (GtkObjectClass *) klass;

	gb_class->copy   = gbrun_global_copy;
	gb_class->assign = gbrun_global_assign;
	gb_class->deref  = gbrun_global_deref;

	gtk_class->destroy = gbrun_global_destroy;
}

static void
gbrun_global_init (GtkObject *object)
{
	GBRunGlobal *global = (GBRunGlobal *) object;

	global->priv = g_new0 (GBRunGlobalPrivate, 1);
}

GtkType
gbrun_global_get_type (void)
{
	static GtkType global_type = 0;

	if (!global_type) {
		static const GtkTypeInfo global_info = {
			"GBRunGlobal",
			sizeof (GBRunGlobal),
			sizeof (GBRunGlobalClass),
			(GtkClassInitFunc)  gbrun_global_class_init,
			(GtkObjectInitFunc) gbrun_global_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		global_type = gtk_type_unique (GB_TYPE_OBJECT, &global_info);
		gtk_type_class (global_type);

		/* FIXME: need a g_atexit (unref_global) */
	}

	return global_type;	
}

GBObject *
gbrun_global_get (void)
{
	static GBObject *global = NULL;

	if (!global)
		global = gtk_type_new (gbrun_global_get_type ());

	return global;
}

void
gbrun_global_add (GBObject *object,
		  const char *name)
{
	GBRunGlobal *global = GBRUN_GLOBAL (gbrun_global_get ());
	GBRunGlobalPrivate *priv = global->priv;

	g_return_if_fail (name != NULL);
	g_return_if_fail (GB_IS_OBJECT (object));

	priv->children = g_slist_prepend (
		priv->children, global_obj_new (object, name));
}
