/*
 * GNOME Basic Array support
 *
 * Authors:
 *    Michael Meeks (mmeeks@gnu.org)
 *
 * Copyright 2000, Helix Code Inc.
 */
#include <math.h>

#include <gbrun/gbrun.h>
#include <gbrun/gbrun-object.h>  /* fjc */
#include <gbrun/gbrun-array.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-stack.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-statement.h>

#undef ARRAY_DEBUG

typedef struct {
	GBLong min, max;
} GBRunARange;

static GBRunObjectClass *parent_class = NULL;

static guint
array_count (GBRunEvalContext *ec, GBRunArray *array)
{
	guint	 length = 0;

	GSList	*i;

	i = array->indices;
	while (i) {
		GBRunARange	*r = i->data;
		
		length += (r->max - r->min + 1);
		
		i = i->next;
	}
	
	return length;
}


static GBValue *
gbrun_array_count_func (GBRunEvalContext *ec,
			GBRunObject      *object,
			GBValue         **args)
{
	long	         length;
	GBRunArray      *array = GBRUN_ARRAY (object);

	length = array_count (ec, array);

	if (gbrun_eval_context_exception (ec)) 
		return NULL;
	else
		return gb_value_new_long (length);
}

static GBValue *
array_deref (GBRunEvalContext *ec,
	     GBRunArray       *a,
	     const GBObjRef   *ref,
	     const GBValue    *assign,
	     gboolean          try_only)
{
	gpointer   *data;
	GBValue   **pos = NULL;
	GSList     *i, *offset;

	if (g_slist_length (a->indices) != g_slist_length (ref->parms) &&
	    !ref->name)
		return gbrun_exception_firev (ec, _("Too many / few array indices"));
	else if (ref->name) {
		/* Now we need to call the base class's deref implementation */
		GBValue *ret;

		ret = parent_class->parent.deref (GB_EVAL_CONTEXT(ec), GB_OBJECT (a), ref, FALSE);
		if (!gbrun_eval_context_exception (ec))
			return ret;
		else
			return NULL;
	}

	i      = a->indices;
	data   = a->data;
	offset = ref->parms;

#ifdef ARRAY_DEBUG
	fprintf (stderr, "      Array index : %s (", (ref->name)?(ref->name):"[Unknown]");
#endif

	while (i && offset) {
		GBRunARange *r = i->data;
		GBValue *v;
		int      into;
		
		v = gb_eval_context_eval (GB_EVAL_CONTEXT (ec), offset->data);
		if (!v)
			return NULL;

		into = gb_value_get_as_int (v);
		gb_value_destroy (v);
		
		if (into < r->min ||
		    into > r->max)
			return gbrun_exception_firev (ec, _("Out of bounds array index "
						      "%d !<= %d !<= %d"), r->min, into, r->max);

		pos    = (GBValue **)&(data [into - r->min]);
		data   = data [into - r->min];
#ifdef ARRAY_DEBUG
		fprintf (stderr, "      into = %d, i->next = [%s]", into, i->next?", ":"");
#endif
		offset = offset->next;
		i      = i->next;
	}

#ifdef ARRAY_DEBUG
	if (assign)
		fprintf (stderr, ") := '%s'\n",
			 gb_value_get_as_string (assign)->str);
	else
		fprintf (stderr, ") == '%s'\n",
			 gb_value_get_as_string ((GBValue *)data)->str);
#endif

	if (assign) {
		if (!pos)
			return gbrun_exception_firev (ec, _("Wierd, nowhere  to assign"));
		if (*pos)
			gb_value_destroy (*pos);

		*pos = gb_value_copy (GB_EVAL_CONTEXT (ec), assign);

		return gb_value_new_empty ();
	}

	if (!data || (!GB_IS_AN_OBJECT (((GBValue *)data)->gtk_type) &&
		      !GB_IS_A_FUNDAMENTAL (((GBValue *)data)->gtk_type)))
		return gbrun_exception_firev (ec, _("Unknown type in array dereference '%p' '%s'"),
					      data, data ? gtk_type_name (((GBValue *)data)->gtk_type): "no data");

	return gb_value_copy (GB_EVAL_CONTEXT (ec), (GBValue *)data);
}

static GBValue *
gbrun_array_deref (GBEvalContext  *ec,
		   GBObject       *object,
		   const GBObjRef *ref,
		   gboolean        try_deref)
{
	GBValue *ans;

	g_return_val_if_fail (GBRUN_IS_ARRAY (object), NULL);

	ans = array_deref (GBRUN_EVAL_CONTEXT (ec), GBRUN_ARRAY (object),
			   ref, NULL, try_deref);

	return ans;
}

static GBValue *
gbrun_array_item_func (GBRunEvalContext *ec,
		       GBRunObject      *object,
		       GBValue          **args)
{
	GBObjRef        ref;
	const GBExpr   *pos;
	GBValue        *ret, *i;

	GB_IS_VALUE (ec, args[0], GB_VALUE_INT);
	
	ref.name   = NULL;
	ref.method = TRUE;
	
	i   = gb_value_new_int (args [0]->v.i);
	pos = gb_expr_new_value (i);
	
	ref.parms  = g_slist_prepend (NULL, (gpointer) pos);

	ret = gbrun_array_deref (GB_EVAL_CONTEXT (ec), GB_OBJECT (object),
				 &ref, TRUE);

	if (!gbrun_eval_context_exception (ec))
		return ret;
	else 
		return NULL;
}

gboolean
gbrun_array_assign (GBEvalContext  *ec,
		    GBObject       *object,
		    const GBObjRef *ref,
		    GBValue        *value,
		    gboolean        try_assign)
{
	GBValue    *ans;

	g_return_val_if_fail (GBRUN_IS_ARRAY (object), FALSE);

	ans = array_deref (GBRUN_EVAL_CONTEXT (ec), GBRUN_ARRAY (object),
			   ref, value, try_assign);

	if (try_assign) /* FIXME: slow */
		gb_eval_context_reset (ec);

	if (!ans)
		return FALSE;

	gb_value_destroy (ans);

	return TRUE;
}

static gboolean
get_as_long (GBRunEvalContext *ec,
	     const GBExpr     *expr,
	     GBLong           *ans)
{
	gboolean ret;
	GBValue *v, *i;

	v = gb_eval_context_eval (GB_EVAL_CONTEXT (ec), expr);
	if (!v) {
		gb_value_destroy (v);
		return FALSE;
	}
	
	i = gbrun_value_promote (ec, v, GB_VALUE_LONG);
	if (!i || i->gtk_type != gb_gtk_type_from_value (GB_VALUE_LONG))
		ret = FALSE;
	else
		ret = TRUE;

	*ans = i->v.l;

	gb_value_destroy (v);
	gb_value_destroy (i);

	return ret;
}

static GBRunARange *
range_create (GBRunEvalContext *ec,
	      GBIndex          *idx)
{
	GBRunARange *r = g_new (GBRunARange, 1);

	if (!get_as_long (ec, idx->min, &r->min) ||
	    !get_as_long (ec, idx->max, &r->max)) {
		g_free (r);
		return NULL;
	}

	if (r->min > r->max) {
		GBLong t = r->min;
		r->min = r->max;
		r->max = t;
		g_warning ("Testme: swapping indices max %d min %d",
			   r->max, r->min);
	}

	return r;
}

static void
indices_free (GSList *indices)
{
	GSList *l;

	for (l = indices; l; l = l->next)
		g_free (l->data);
	
	g_slist_free (indices);
}

static gpointer
alloc_array (GBRunEvalContext *ec,
	     GSList           *l,
	     GBRunArray       *a)
{
	GBRunARange *r;
	GBLong       size, i;
	gpointer    *data;

	if (!l) {
		if (gtk_type_is_a (a->content_type, GBRUN_TYPE_ARRAY))
			return gb_value_new_empty ();
		else
			return gb_value_new_default (
				GB_EVAL_CONTEXT (ec), a->content_type);
	}
	r = l->data;

	size = r->max - r->min + 1;
	if (size < 0)
		size = -size;

	data = g_new (gpointer, size);

	for (i = 0; i < size; i++)
		data [i] = alloc_array (ec, l->next, a);

	return data;
}

static void
data_free (GSList *l, gpointer *data)
{
	GBRunARange *r;
	GBLong       size, i;

	if (!l) {
		if (data)
			gb_value_destroy ((GBValue *) data);
		return;
	}

	r = l->data;

	size = r->max - r->min + 1;
	if (size < 0)
		size = -size;

	for (i = 0; i < size; i++)
		data_free (l->next, data [i]);

	g_free (data);
}

static GBRunArray *
array_new_for_type (GBRunEvalContext *ec, const char *type)
{
	GBRunArray *a;

	a = gtk_type_new (GBRUN_TYPE_ARRAY);

	a->content_type = gb_gtk_type_from_name (type);

	return a;
}

GBObject *
gbrun_array_new (GBRunEvalContext *ec,
		 const GBVar      *var)
{
	GBRunArray *a;
	GSList     *l;

	g_return_val_if_fail (ec != NULL, NULL);
	g_return_val_if_fail (var != NULL, NULL);
	g_return_val_if_fail (GB_IS_EVAL_CONTEXT (ec), NULL);

	a = array_new_for_type (ec, var->type);
	
	g_return_val_if_fail (a != NULL, NULL);

	/* Evaluate indices */
	a->indices = NULL;
	for (l = var->indices; l; l = l->next) {
		GBRunARange *r = range_create (ec, l->data);

		if (!r) /* FIXME: leak */
			return NULL;

		a->indices = g_slist_append (a->indices, r);
	}

	a->data = alloc_array (ec, a->indices, a);

	return GB_OBJECT (a);
}

GBObject *
gbrun_array_new_vals (GBRunEvalContext *ec,
		      GSList           *values)
{
	GBRunArray  *a;
	GBRunARange *r;
	GBValue     *v;
	GBValue    **data;
	int          i;

	g_return_val_if_fail (ec != NULL, NULL);
	g_return_val_if_fail (values != NULL, NULL);
	g_return_val_if_fail (values->data != NULL, NULL);

	a = gtk_type_new (GBRUN_TYPE_ARRAY);

	r = g_new0 (GBRunARange, 1);
	r->min = 0;
	r->max = g_slist_length (values) - 1;

	a->indices = g_slist_append (NULL, r);

	v = values->data;
	a->content_type = v->gtk_type;

	data = g_new (GBValue *, r->max + 1);
	a->data = (gpointer)data;

	for (i = 0; i < r->max + 1; i++) {
		data [i] = gb_value_promote (GB_EVAL_CONTEXT (ec),
					     a->content_type, values->data);
		values = values->next;
	}

	return GB_OBJECT (a);
}

gboolean
gbrun_array_redim (GBRunEvalContext  *ec,
		   GBRunArray        *a,
		   const GSList      *new_indices,
		   gboolean           preserve)
{
	GSList *indices;
	const GSList *l;

	g_return_val_if_fail (GBRUN_IS_ARRAY (a), FALSE);

	/* Evaluate indices */
	indices = NULL;
	for (l = new_indices; l; l = l->next) {
		GBRunARange *r = range_create (ec, l->data);
		
		if (!r) /* FIXME: leak */
			return FALSE;
		
		indices = g_slist_append (indices, r);
	}

	if (preserve) {
		g_warning ("preserving redim unimplemented");
		return FALSE;
	} else {
		data_free (a->indices, a->data);
		indices_free (a->indices);

		a->indices = indices;
		a->data = alloc_array (ec, a->indices, a);

		return TRUE;
	}
}

gboolean
gbrun_array_erase (GBRunEvalContext  *ec,
		   GBRunArray        *a)
{
	g_return_val_if_fail (GBRUN_IS_ARRAY (a), FALSE);

	data_free (a->indices, a->data);
	a->data = alloc_array (ec, a->indices, a);

	/* FIXME: add value_free? free indices (only dynamic)? */

	return TRUE;
}

static void
gbrun_array_destroy (GtkObject *object)
{
	GBRunArray *a = GBRUN_ARRAY (object);

	g_warning ("Do array destroy");

	data_free (a->indices, a->data);
	a->data = NULL;

	indices_free (a->indices);
	a->indices = NULL;
}

static void
gbrun_array_copy (GBEvalContext  *ec,
		  GBObject       *src,
		  GBObject       *dest)
{
	g_warning ("Array copy unimplemented");
}

static void
gbrun_array_class_init (GBRunArrayClass *klass)
{
	GBRunObjectClass *gbrun_class = (GBRunObjectClass *) klass;
	GBObjectClass    *gb_class    = (GBObjectClass *) klass;
	GtkObjectClass   *gtk_class   = (GtkObjectClass *) klass;

	gb_class->copy     = gbrun_array_copy;
	gb_class->assign   = gbrun_array_assign;
	gb_class->deref    = gbrun_array_deref;

	gtk_class->destroy = gbrun_array_destroy;

	parent_class = gtk_type_parent_class (GBRUN_TYPE_ARRAY);

	gbrun_object_add_method_arg (gbrun_class, "func;count;.;long;n",
				     gbrun_array_count_func);
	gbrun_object_add_method_arg (gbrun_class, "func;item;index,integer;object;n",
				     gbrun_array_item_func);
}

GtkType
gbrun_array_get_type (void)
{
	static GtkType array_type = 0;

	if (!array_type) {
		static const GtkTypeInfo array_info = {
			"GBRunArray",
			sizeof (GBRunArray),
			sizeof (GBRunArrayClass),
			(GtkClassInitFunc)  gbrun_array_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		array_type = gtk_type_unique (GBRUN_TYPE_OBJECT, &array_info);
		gtk_type_class (array_type);
	}

	return array_type;	
}

gboolean
gbrun_array_initialized (GBRunArray *a)
{
	g_return_val_if_fail (GBRUN_IS_ARRAY (a), FALSE);

	if (!a->content_type || !a->indices || !a->data)
		return FALSE;
	
	return TRUE;
}

int
gbrun_array_dimensions (GBRunArray *a)
{
	g_return_val_if_fail (GBRUN_IS_ARRAY (a), 0);

	return g_slist_length (a->indices);
}

GBLong
gbrun_array_min_bound (GBRunArray *a,
		       int dimension)
{
	GSList *l;

	g_return_val_if_fail (GBRUN_IS_ARRAY (a), 0);

	l = g_slist_nth (a->indices, dimension - 1);
	if (!l || !l->data)
		return -1;

	return ((GBRunARange *) a->indices->data)->min;
}

GBLong
gbrun_array_max_bound (GBRunArray *a,
		       int dimension)
{
        GSList *l;

        g_return_val_if_fail (GBRUN_IS_ARRAY (a), 0);

        l = g_slist_nth (a->indices, dimension - 1);
        if (!l || !l->data)
                return -1;

        return ((GBRunARange *) a->indices->data)->max;
}

GBValue *
gbrun_array_lbound (GBRunEvalContext *ec,
		    GBRunArray       *a,
		    int               dimension)
{
	GBLong i;

	g_return_val_if_fail (GBRUN_IS_ARRAY (a), NULL);

	i = gbrun_array_min_bound (a, dimension);
	
	if (i == -1)
		return gbrun_exception_firev (
			ec, _("dimension %d out of range"), dimension);
	
	return gb_value_new_long (i);
}

GBValue *
gbrun_array_ubound (GBRunEvalContext *ec,
		    GBRunArray       *a,
		    int               dimension)
{
	GBLong i;

	g_return_val_if_fail (GBRUN_IS_ARRAY (a), NULL);

	i = gbrun_array_max_bound (a, dimension);

	if (i == -1)
		return gbrun_exception_firev (
			ec, _("dimension %d out of range"), dimension);
	
	return gb_value_new_long (i);
}
