
/*
 * GNOME Expression evaluation
 *
 * Author:
 *    Michael Meeks <michael@imaginator.com>
 *
 * Copyright 2000, Helix Code, Inc.
 */

#ifndef GBRUN_EVAL_H
#define GBRUN_EVAL_H

#include <gbrun/gbrun.h>
#include <gb/gb-value.h>
#include <gb/gb-expr.h>
#include <gb/gb-eval.h>
#include <gb/gb-main.h>
#include <gbrun/gbrun-file.h>

#define GBRUN_TYPE_EVAL_CONTEXT            (gbrun_eval_context_get_type ())
#define GBRUN_EVAL_CONTEXT(obj)            (GTK_CHECK_CAST ((obj), GBRUN_TYPE_EVAL_CONTEXT, GBRunEvalContext))
#define GBRUN_EVAL_CONTEXT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GBRUN_TYPE_EVAL_CONTEXT, GBRunEvalContextClass))
#define GBRUN_IS_EVAL_CONTEXT(obj)         (GTK_CHECK_TYPE ((obj), GBRUN_TYPE_EVAL_CONTEXT))
#define GBRUN_IS_EVAL_CONTEXT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GBRUN_TYPE_EVAL_CONTEXT))

struct _GBRunEvalContext {
	GBEvalContext     context;

	GSList           *projs;
	GSList           *with;

	GBRunStack       *stack;
	GBRunSecurityFlag flags;

	GBOnError         on_error;

	GBValue          *me;

	char             *module;

	GBOptions         options;

	/* Runtime execution state storage */
	GBRunFileHandle  *file_handles;

        struct {
		gint     randseed;
		gboolean reseed;
	} random;
};

struct _GBRunEvalContextClass {
	GBEvalContextClass klass;
};

GtkType           gbrun_eval_context_get_type   (void);

GBRunEvalContext *gbrun_eval_context_construct  (GBRunEvalContext  *ec,
						 const char        *module_name,
						 GBRunSecurityFlag  flags);

GBRunEvalContext *gbrun_eval_context_new        (const char        *module_name,
						 GBRunSecurityFlag  flags);

char             *gbrun_eval_context_get_module (GBRunEvalContext  *ec);
void              gbrun_eval_context_set_module (GBRunEvalContext  *ec,
						 const char        *module);

GBValue          *gbrun_eval_as                 (GBRunEvalContext  *ec,
						 const GBExpr      *expr,
						 GBValueType        ret_type);

GBObject         *gbrun_eval_context_me_get     (GBRunEvalContext *ec);
void              gbrun_eval_context_me_set     (GBRunEvalContext *ec,
						 GBObject         *obj);

GBRunProject     *gbrun_eval_context_proj_get   (GBRunEvalContext *ec);
void              gbrun_eval_context_proj_push  (GBRunEvalContext *ec,
						 GBRunProject     *obj);
GBRunProject     *gbrun_eval_context_proj_pop   (GBRunEvalContext *ec);

void              gbrun_eval_context_with_push  (GBRunEvalContext *ec,
						 GBObject         *object);
GBObject         *gbrun_eval_context_with_pop   (GBRunEvalContext *ec);

/* convenience wrappers */
gboolean          gbrun_eval_context_exception  (GBRunEvalContext *ec);
#define           gbrun_eval_context_get_text(ec) (gb_eval_context_get_text (GB_EVAL_CONTEXT (ec)))

GBValue          *gbrun_exception_fire          (GBRunEvalContext *ec,
						 const char       *txt);
GBValue          *gbrun_exception_firev         (GBRunEvalContext *ec,
						 const char       *format, ...);

#endif /* GBRUN_EVAL_H */
