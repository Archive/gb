/*
 * Gnome Basic Arrays
 *
 * Author:
 *    Michael Meeks <michael@imaginator.com>
 *
 * Copyright 2000, Helix Code, Inc.
 */

#ifndef GBRUN_ARRAY_H
#define GBRUN_ARRAY_H

#include <gbrun/gbrun-object.h>

#define GBRUN_TYPE_ARRAY            (gbrun_array_get_type ())
#define GBRUN_ARRAY(obj)            (GTK_CHECK_CAST ((obj), GBRUN_TYPE_ARRAY, GBRunArray))
#define GBRUN_ARRAY_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GBRUN_TYPE_ARRAY, GBRunArrayClass))
#define GBRUN_IS_ARRAY(obj)         (GTK_CHECK_TYPE ((obj), GBRUN_TYPE_ARRAY))
#define GBRUN_IS_ARRAY_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GBRUN_TYPE_ARRAY))
#define GBRUN_ARRAY_GET_CLASS(obj)  (GBRUN_ARRAY_CLASS (GTK_OBJECT (obj)->klass))

struct _GBRunArray {
	GBRunObject object;

	GtkType     content_type;

	GSList     *indices;
	gpointer    data;
};

struct _GBRunArrayClass {
	GBRunObjectClass  klass;
};

GtkType        gbrun_array_get_type    (void);

GBObject      *gbrun_array_new         (GBRunEvalContext  *ec,
					const GBVar       *var);

GBObject      *gbrun_array_new_vals    (GBRunEvalContext  *ec,
					GSList            *values);

gboolean       gbrun_array_redim       (GBRunEvalContext  *ec,
					GBRunArray        *a,
					const GSList      *new_indices,
					gboolean           preserve);

gboolean       gbrun_array_assign      (GBEvalContext  *ec,
					GBObject          *obj,
					const GBObjRef    *ref,
					GBValue           *val,
					gboolean           try_assign);


gboolean       gbrun_array_initialized (GBRunArray        *a);

int            gbrun_array_dimensions  (GBRunArray        *a);

GBValue       *gbrun_array_lbound      (GBRunEvalContext  *ec,
					GBRunArray        *a,
					int                dimension);

GBValue       *gbrun_array_ubound      (GBRunEvalContext  *ec,
					GBRunArray        *a,
					int                dimension);

GBLong         gbrun_array_min_bound   (GBRunArray        *a,
				        int                dimension);

GBLong         gbrun_array_max_bound   (GBRunArray        *a,
                                        int                dimension);

gboolean       gbrun_array_erase       (GBRunEvalContext  *ec,
					GBRunArray        *a);

#endif /* GBRUN_ARRAY_H */
