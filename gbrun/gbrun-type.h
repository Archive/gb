/**
 * GNOME Basic User defined type bits.
 *
 *   A 'Type' is ( confusingly ) the
 * analogue of a C structure.
 *
 * Author:
 *    Michael Meeks <mmeeks@gnu.org>
 *
 * Copyright 2000, Helix Code, Inc.
 */
#ifndef GBRUN_TYPE_H
#define GBRUN_TYPE_H

#include <gbrun/gbrun.h>
#include <gb/gb-value.h>
#include <gb/gb-object.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-stack.h>

#define GBRUN_TYPE_TYPE            (gbrun_type_get_type ())
#define GBRUN_TYPE(obj)            (GTK_CHECK_CAST ((obj), GBRUN_TYPE_TYPE, GBRunType))
#define GBRUN_TYPE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GBRUN_TYPE_TYPE, GBRunTypeClass))
#define GBRUN_IS_TYPE(obj)         (GTK_CHECK_TYPE ((obj), GBRUN_TYPE_TYPE))
#define GBRUN_IS_TYPE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GBRUN_TYPE_TYPE))

struct _GBRunType {
	GBObject object;

	GBRunStackLevel *values;
};

struct _GBRunTypeClass {
	GBObjectClass klass;

	GBType  *type;
};

GtkType gbrun_type_get_type  (void);

void    gbrun_register_types (GBRunEvalContext *ec,
			      GBRunObjectClass *klass,
			      GSList           *types);

#endif
