/*
 * GNOME Basic Collection support
 *
 * Authors:
 *    Michael Meeks (mmeeks@gnu.org)
 *
 * Copyright 2000, Helix Code Inc.
 */
#include <math.h>
#include <stdlib.h>

#include <gbrun/gbrun.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-collection.h>

#undef COLLECTION_DEBUG

GBObjectClass *parent_class = NULL;

GSList *
gbrun_collection_enumerate (GBRunEvalContext *ec,
			    GBRunCollection  *collection)
{
	g_error ("This is an abstract base method, do not use");
	return NULL;
}

static guint
gbrun_collection_count (GBRunEvalContext *ec,
			GBRunCollection  *collection)
{
	guint length;
	GBRunCollectionClass *klass =
		GBRUN_COLLECTION_GET_CLASS (collection);

	if (klass->enumerate == gbrun_collection_enumerate)
		length = g_slist_length (collection->private);

	else {
		GSList *elems = klass->enumerate (ec, collection);

		if (gbrun_eval_context_exception (ec))
			length = 0;
		else 
			length = g_slist_length (elems);

		while (elems) {
			gbrun_collection_element_destroy (elems->data);
			elems = g_slist_remove (elems, elems->data);
		}
	}

	return length;
}

static GBValue *
gbrun_collection_count_fn (GBRunEvalContext *ec,
			   GBRunObject      *object,
			   GBValue         **args)
{
	int length;
	GBRunCollection *collection = GBRUN_COLLECTION (object);
	GBRunCollectionClass *klass = GBRUN_COLLECTION_GET_CLASS (object);

	length = klass->count (ec, collection);

	if (gbrun_eval_context_exception (ec))
		return NULL;
	else
		return gb_value_new_long (length);
}

static GBRunCollectionElement *
do_lookup (GBRunEvalContext *ec, GSList *elements, const char *name)
{
	GSList *l;
	char   *end;
	long    idx;

	for (l = elements; l; l = l->next) {
		GBRunCollectionElement *e = l->data;

		if (e->name && !g_strcasecmp (e->name, name))
			return e;
	}

	/* Might be a numeric index */

	idx = strtol (name, &end, 10);
			
	if (*name == '\0' || *end != '\0') {
		gbrun_exception_firev (
			ec, "Cannot find element '%s'", name);

		return NULL;
 	} else {
		GBRunCollectionElement *e = NULL;
		
		l = g_slist_nth (elements, idx - 1);
		if (!l)
			gbrun_exception_firev (
				ec, "index %d out of bounds", idx);
		else
			e = l->data;

		return e;
	}
}

static GBValue *
gbrun_collection_lookup (GBRunEvalContext *ec,
			 GBRunCollection  *collection,
			 const char       *name)
{
	GSList *elems;
	GBValue *ret = NULL;
	GBRunCollectionClass *klass =
		GBRUN_COLLECTION_GET_CLASS (collection);

	if (klass->enumerate == gbrun_collection_enumerate)
		elems = collection->private;
	else
		elems = klass->enumerate (ec, collection);

	if (!gbrun_eval_context_exception (ec)) {
		GBRunCollectionElement *e;

		e = do_lookup (ec, elems, name);
		if (e)
			ret = gb_value_copy (GB_EVAL_CONTEXT (ec),
					     e->value);
	}

	if (elems != collection->private) {
		while (elems) {
			gbrun_collection_element_destroy (elems->data);
			elems = g_slist_remove (elems, elems->data);
		}
	}

	return ret;
}

static GBValue *
gbrun_collection_lookup_fn (GBRunEvalContext *ec,
			    GBRunObject      *object,
			    GBValue         **args)
{
	const char *name;
	GBRunCollection *collection = GBRUN_COLLECTION (object);
	GBRunCollectionClass *klass = GBRUN_COLLECTION_GET_CLASS (object);

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);

	name = args [0]->v.s->str;

	return klass->lookup (ec, collection, name);
}

static GBValue *
gbrun_collection_add (GBRunEvalContext *ec,
		      GBRunCollection  *collection,
		      const char       *name,
		      GBValue          *value)

{
	char *end;
	long index;
	GSList *l;
	GBRunCollectionElement *e;

	for (l = collection->private; l; l = l->next) {
		GBRunCollectionElement *e = l->data;
		
		if (e->name && !g_strcasecmp (e->name, name)) {
			gb_value_destroy (e->value);
			e->value = gb_value_copy (
				GB_EVAL_CONTEXT (ec), value);

			return gb_value_copy (
				GB_EVAL_CONTEXT (ec), e->value);
		}
	}
	
	e = gbrun_collection_element_new_val (
		GB_EVAL_CONTEXT (ec), name, value);
	
	/* Might be a numeric index */
	index = strtol (name, &end, 10);
	
	if (*name != '\0' &&
	    *end == '\0'  &&
	    index <= g_slist_length (
		    collection->private)) {
		
		g_free (e->name);
		e->name = NULL;
		
		collection->private = g_slist_insert (
			collection->private, e, index - 1);
		
	} else
		collection->private = g_slist_append (
			collection->private, e);

	return gb_value_copy (GB_EVAL_CONTEXT (ec), e->value);
}

static GBValue *
gbrun_collection_add_fn (GBRunEvalContext *ec,
			 GBRunObject      *object,
			 GBValue         **args)
{
	const char *name;
	GBRunCollection *collection = GBRUN_COLLECTION (object);
	GBRunCollectionClass *klass = GBRUN_COLLECTION_GET_CLASS (object);

	GB_IS_VALUE (ec, args [1], GB_VALUE_STRING);

	name = args [1]->v.s->str;

	return klass->add (ec, collection, name, args [0]);
}

static void
gbrun_collection_remove (GBRunEvalContext *ec,
			 GBRunCollection  *collection,
			 const char       *name)
{
	GBRunCollectionElement *e;

	e = do_lookup (ec, collection->private, name);
	if (e) {
		collection->private = g_slist_remove (
			collection->private, e);
		gbrun_collection_element_destroy (e);
	}
}

static GBValue *
gbrun_collection_remove_fn (GBRunEvalContext *ec,
			    GBRunObject      *object,
			    GBValue         **args)
{
	const char *name;
	GBRunCollection *collection = GBRUN_COLLECTION (object);
	GBRunCollectionClass *klass = GBRUN_COLLECTION_GET_CLASS (object);

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);

	name = args [0]->v.s->str;

	klass->remove (ec, collection, name);

	if (gbrun_eval_context_exception (ec))
		return NULL;
	else
		return gb_value_new_empty ();
}

GBRunCollection *
gbrun_collection_construct (GBRunCollection *collection)
{
	/* Ok so this does nothing yet :-) */
	return collection;
}

static void
gbrun_collection_destroy (GtkObject *object)
{
	GSList *l;
	GBRunCollection *collection = (GBRunCollection *) object;

	for (l = collection->private; l; l = l->next)
		gbrun_collection_element_destroy (l->data);

	g_slist_free (collection->private);
	collection->private = NULL;
}

static void
gbrun_collection_copy (GBEvalContext  *ec,
		       GBObject       *src,
		       GBObject       *dest)
{
	g_warning ("Collection copy unimplemented");
}

static void
gbrun_collection_class_init (GBObjectClass *klass)
{
	GtkObjectClass *gtk_class = (GtkObjectClass *) klass;
	GBRunObjectClass *gbrun_class = (GBRunObjectClass *) klass;
	GBRunCollectionClass *collection_class = (GBRunCollectionClass *) klass;

	parent_class = gtk_type_class (GBRUN_TYPE_OBJECT);

	klass->copy   = gbrun_collection_copy;
/*	klass->assign = gbrun_collection_assign;
	klass->deref  = gbrun_collection_deref;*/

	gtk_class->destroy = gbrun_collection_destroy;

	collection_class->add    = gbrun_collection_add;
	collection_class->count  = gbrun_collection_count;
	collection_class->lookup = gbrun_collection_lookup;
	collection_class->remove = gbrun_collection_remove;
	collection_class->enumerate = gbrun_collection_enumerate;

	gbrun_object_add_method_arg (
		gbrun_class, "func;count;.;long;n",
		gbrun_collection_count_fn);

	gbrun_object_add_method_arg (
		gbrun_class, "sub;remove;name,string;n",
		gbrun_collection_remove_fn);

	gbrun_object_add_method_arg (
		gbrun_class, "func;item;name,string;variant;n",
		gbrun_collection_lookup_fn);

	gbrun_object_set_default_method (gbrun_class, "item");

	gbrun_object_add_method_arg (
		gbrun_class, "sub;add;object,variant;name,string;n",
		gbrun_collection_add_fn);
}

GtkType
gbrun_collection_get_type (void)
{
	static GtkType collection_type = 0;

	if (!collection_type) {
		static const GtkTypeInfo collection_info = {
			"gb-collection",
			sizeof (GBRunCollection),
			sizeof (GBRunCollectionClass),
			(GtkClassInitFunc)  gbrun_collection_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		collection_type = gtk_type_unique (
			GBRUN_TYPE_OBJECT, &collection_info);
		gtk_type_class (collection_type);
	}

	return collection_type;	
}

GBRunCollectionElement *
gbrun_collection_element_new (GBEvalContext *ec,
			      const char    *name,
			      GBObject      *object)
{
	GBRunCollectionElement *elem = g_new0 (GBRunCollectionElement, 1);

	elem->name  = name ? g_strdup (name) : NULL;
	gb_object_ref (object);
	elem->value = gb_value_new_object (object);

	return elem;
}

GBRunCollectionElement *
gbrun_collection_element_new_val (GBEvalContext *ec,
				  const char    *name,
				  const GBValue *value)
{
	GBRunCollectionElement *elem = g_new0 (GBRunCollectionElement, 1);

	elem->name  = name ? g_strdup (name) : NULL;
	elem->value = gb_value_copy (ec, value);

	return elem;
}

void
gbrun_collection_element_destroy (GBRunCollectionElement *elem)
{
	if (elem) {
		g_free (elem->name);
		gb_value_destroy (elem->value);
		g_free (elem);
	}
}
