/*
 * Gnome Basic Global Object
 *
 * Author:
 *    Michael Meeks <michael@helixcode.com>
 *
 * Copyright 2000, Helix Code, Inc.
 */

#ifndef GBRUN_GLOBAL_H
#define GBRUN_GLOBAL_H

#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-object.h>

#define GBRUN_TYPE_GLOBAL            (gbrun_global_get_type ())
#define GBRUN_GLOBAL(obj)            (GTK_CHECK_CAST ((obj), GBRUN_TYPE_GLOBAL, GBRunGlobal))
#define GBRUN_GLOBAL_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GBRUN_TYPE_GLOBAL, GBRunGlobalClass))
#define GBRUN_IS_GLOBAL(obj)         (GTK_CHECK_TYPE ((obj), GBRUN_TYPE_GLOBAL))
#define GBRUN_IS_GLOBAL_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GBRUN_TYPE_GLOBAL))
#define GBRUN_GLOBAL_GET_CLASS(obj)  (GBRUN_GLOBAL_CLASS (GTK_GLOBAL (obj)->klass))

typedef struct _GBRunGlobalPrivate      GBRunGlobalPrivate;

struct _GBRunGlobal {
	GBRunObject parent;

	GBRunGlobalPrivate *priv;
};

struct _GBRunGlobalClass {
	GBRunObjectClass klass;
};

GtkType   gbrun_global_get_type (void);
GBObject *gbrun_global_get      (void);
void      gbrun_global_add      (GBObject   *object,
				 const char *name);

#endif /* GBRUN_GLOBAL_H */
