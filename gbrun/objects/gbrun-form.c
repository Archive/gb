/*
 * gbrun-form.c
 *
 * Gnome Basic Interpreter Form functions.
 *
 * Authors:
 *	Frank Chiulli  <fc-linux@home.com>
 *      Michael Meeks  <michael@helixcode.com>
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <gbrun/gbrun-stack.h>
#include <gbrun/gbrun-array.h>

#include "gbrun-form.h"
#include "gbrun-form-item.h"

#define ITEM_NAME "gb-vb.form"


/*
 * Prototypes
 */
gboolean
gbrun_form_invoke (GBRunEvalContext *ec,
		   GBRunForm        *form,
		   const char       *method,
		   GBRunFormItem    *fi);

/*
 *  temporary.
 */
static gint
delete_event_cb (GtkWidget *window, GdkEventAny *e, gpointer data)
{
	gtk_main_quit ();
	return TRUE;
}


/*
 * A type/structure to pass all the necessary information to form_focus_in.
 */
typedef struct ec_form {
	GBRunEvalContext *ec;
	GBRunForm       *form;
} focus_data;


/*
 * form_focus_in
 *  @window
 *  @event
 *  @data
 *
 * Process/handle the focus_in_event and call Form_Click.
 *
 * Note:
 * Since there is no "clicked" event for a window or a gtkfixed, it is as close as it gets.
 *
 */
static void
form_focus_in (GtkWidget *window, GdkEvent *event, focus_data *data)
{
	gbrun_form_invoke (data->ec, data->form, "Form_Click", NULL);
}

static void
form_click (GtkWidget *window, GdkEvent *event, focus_data *data)
{
	gbrun_form_invoke (data->ec, data->form, "Form_Click", NULL);
}

static void
form_resize (GtkWidget *window, 
		GdkEvent *event, 
		focus_data *data)
{
	gbrun_form_invoke (data->ec, data->form, "Form_Resize", NULL);
}

static void 
form_paint (GtkWidget *item,
		GdkEvent *event,
		focus_data *data)
{
	gbrun_form_invoke (data->ec, data->form, "Form_Paint", NULL);
}

/*
 * _GBEFormFont defines a Microsoft font.
 */
/*
struct _GBEFormFont {
	gchar		*name;
	gfloat		 size;
	guint8		 charset;
	guint16		 weight;
	gboolean	 underline;
	gboolean	 italic;
	gboolean	 strike_thru;
};
*/

/*
 * _GBEForm defines a VB Form
 *
 * appearance
 * auto_redraw
 * back_color		Specifies the forms's background color.
 * border_style		Determines whether a single-line border appears
 *			around the form.
 * caption		The text that appears on the form.
 * clip_controls
 * control_box
 * draw_mode
 * draw_style
 * draw_width
 * enabled
 * fill_color
 * fill_style
 * font		 	Caption's font name, style and size.
 * font_transparent
 * fore_color
 * has_dc
 * height		The height of button in twips.
 * help_context_id
 * icon
 * key_preview
 * left			The distance between the internal left edge of an 
 *			object and the left edge of its container.
 * link_mode
 * link_topic
 * max_button
 * mdi_child
 * min_button
 * mouse_icon
 * mouse_ptr		The shape of the mouse cursor when the user moves the 
 *			mouse over the command button.
 * moveable
 * negotiate_menus
 * ole_drop_mode
 * palette
 * palette_mode
 * picture		The name of icon graphic image that appears on the 
 *			command button as long as the Style property is set to
 *			1-Graphical.
 * right_to_left
 * scale_height		The number of units for the vertical measurement of an
 *			object's interior
 * scale_left		The horizontal coordinates for the left edge of an 
 *			object.
 * scale_mode
 * scale_top
 * scale_width
 * show_in_taskbar
 * start_up_position
 * tag
 * top			Holds the number of twips from the command button's
 *			top edge to the Form window's top edge.
 * visible		Determines whether the command button appears or is
 *			hidden from the user.
 * whats_this_button
 * whats_this_help
 * width		The width of an object
 * window_state
 *
 * cmd_buttons
 * labels
 * text_boxes
 *
 * window
 * fixed
 *
 * Definitions:
 * twip		1,440th of an inch (the smallest screen measurement)
 */

/*
 * Form properties
 * p_caption		The text that appears on the form.
 * p_height		The height of a form.
 * p_left		The distance between the internal left edge of an
 *			object and the left edge of its container.
 * p_scale_height	The number of units for the vertical measurement of an
 *			object's interior.
 * p_scale_width	The number of units for the horizontal measurement of
 *			an object's interior.
 * p_top		The distance between the internal top edge of an object
 *			and the top edge of its container.
 * p_width		The width of a form.
 *
 */

enum {
	ARG_FIRST = 0,
	CAPTION,
	HEIGHT,
	LEFT,
	SCALE_HEIGHT,
	SCALE_WIDTH,
	SCALE_MODE,
	TOP,
	WIDTH,
	LINK_TOPIC,
	STARTUP_POSITION,
	BORDER_STYLE,
	MAX_BUTTON,
	MIN_BUTTON,
	SHOW_IN_TASKBAR,
	TAG,
	CONTROLBOX
};

static const char* p_name[] = {
	"Arg_First",
	"Caption",
	"Height",
	"Left",
	"Scale_Height",
	"Scale_Width",
	"Scale_Mode",
	"Top",
	"Width",
	"Link_Topic",
	"Startup_Position",
	"Border_Style",
	"Max_Button",
	"Min_Button",
	"Show_in_taskbar",
	"Tag",
	"Controlbox"
};

/**
 * form_setarg
 *   @ec
 *   @object
 *   @property
 *   @val
 *
 *   Internal function.
 *   This function processes the form properties and calls the appropriate
 * GTK function to set the appropriate properties of a GTK object.
 **/
static gboolean
form_setarg (GBRunEvalContext *ec,
	     GBRunObject      *object,
	     int               property,
	     GBValue          *val)
{
	GBRunForm *form = GBRUN_FORM (object);
	GtkWidget *w = GTK_WIDGET (form->window);

	g_return_val_if_fail (w != NULL, FALSE);
	g_return_val_if_fail (form != NULL, FALSE);

	switch (property) {
	case CAPTION:
		gtk_window_set_title (form->window, val->v.s->str);
		return TRUE;

	case HEIGHT:
		w->requisition.height = GBRUN_FORM_TWIPS_TO_Y (val->v.i);
		gtk_window_set_default_size (form->window, w->requisition.width,
				             w->requisition.height);
		return TRUE;

	case LEFT:
		w->allocation.x = GBRUN_FORM_TWIPS_TO_X (val->v.i);
		gtk_widget_set_uposition (w, w->allocation.x, w->allocation.y);
		return TRUE;

	case TOP:
		w->allocation.y = GBRUN_FORM_TWIPS_TO_Y (val->v.i);
		gtk_widget_set_uposition (w, w->allocation.x, w->allocation.y);
		return TRUE;
	
	case WIDTH:
		w->requisition.width = GBRUN_FORM_TWIPS_TO_X (val->v.i);
 		gtk_window_set_default_size (form->window, w->requisition.width, 
		                             w->requisition.height);
		return TRUE;

	case TAG:
		form->tag = g_strdup (val->v.s->str);
		return TRUE;
	
	case SCALE_HEIGHT:
		form->scaleheight = val->v.i;
		return TRUE;
	
	case SCALE_WIDTH:
		form->scalewidth = val->v.i;
		return TRUE;

	case SCALE_MODE:
		form->scalemode = val->v.i;
		return TRUE;

	case LINK_TOPIC:
		form->linktopic = g_strdup (val->v.s->str);
		return TRUE;
	
	case STARTUP_POSITION:
		form->startup_pos = val->v.i;
                switch (form->startup_pos) {
                case 0:
                        gtk_window_set_position (GTK_WINDOW (form->window), GTK_WIN_POS_NONE);
		return TRUE;
                case 1:
                        /* FIXME : add vbStartUpOwner here */
                        return TRUE;
                case 2:
                        gtk_window_set_position (GTK_WINDOW (form->window), GTK_WIN_POS_CENTER);
                        return TRUE;
                case 3:
                        /* FIXME : add vbStartUpWindowsDefault here */
                        return TRUE;
                default:
                        return TRUE;
                }

        case MAX_BUTTON:
		form->maxbutton = (val->v.i == VB_TRUE);
		return TRUE;
	      
	case MIN_BUTTON:
		form->minbutton = (val->v.i == VB_TRUE);
		return TRUE;

	case SHOW_IN_TASKBAR:
		form->show_in_taskbar = (val->v.i == VB_TRUE);
		return TRUE;
			
	case BORDER_STYLE:
		form->borderstyle = val->v.i;
		return TRUE;

	case CONTROLBOX:
		form->controlbox = (val->v.i == VB_TRUE);
		return TRUE;

			
	default:
		g_warning ("form: Set of unhandled property '%s'", p_name[property]);
		return FALSE;
	}
}

/**
 * form_getarg
 *   @ec
 *   @object
 *   @property
 *
 *   Internal function.
 **/
static GBValue *
form_getarg (GBRunEvalContext *ec,
	     GBRunObject      *object,
	     int               property)
{
	GBRunForm *form = GBRUN_FORM (object);

	g_return_val_if_fail (form != NULL, NULL);
	g_return_val_if_fail (form->window != NULL, NULL);
	
	switch (property) {
	case CAPTION:
		return gb_value_new_string_chars (form->window->title);

	case WIDTH: {
		guint i = GTK_WIDGET (form->window)->allocation.width;
		return gb_value_new_int (GBRUN_FORM_X_TO_TWIPS (i));
	}

	case HEIGHT: {
		guint i = GTK_WIDGET (form->window)->allocation.height;
		return gb_value_new_int (GBRUN_FORM_Y_TO_TWIPS (i));
	}

	case SCALE_HEIGHT:
		return gb_value_new_int (form->scaleheight);
	
	case SCALE_WIDTH:
		return gb_value_new_int (form->scalewidth);

	case SCALE_MODE:
		return gb_value_new_int (form->scalemode);

	case TAG: 
		return gb_value_new_string_chars (form->tag);
		
        case LINK_TOPIC:
		return gb_value_new_string_chars (form->linktopic);
	     
        case STARTUP_POSITION:
		return gb_value_new_int (form->startup_pos);
		
	case MAX_BUTTON:
		return gb_value_new_boolean (form->maxbutton);

        case MIN_BUTTON:
		return gb_value_new_boolean (form->minbutton);

	case SHOW_IN_TASKBAR:
		return gb_value_new_boolean (form->show_in_taskbar);

	case BORDER_STYLE:
		return gb_value_new_int (form->borderstyle);

	case CONTROLBOX:
		return gb_value_new_boolean (form->controlbox);


	default:
		g_warning ("form: get of unhandled property '%s'", p_name[property]);
		break;
	}

	return NULL;
}

static GBValue *
form_show (GBRunEvalContext *ec,
	   GBRunObject      *form,
	   GBValue         **args)
{
	/* FIXME: loads if not already loaded !? */
	if (args [0] || args [1])
		g_warning ("form_show: Modality & owner unimplemented");

	gtk_widget_show (GTK_WIDGET (GBRUN_FORM (form)->window));

	return gb_value_new_empty ();
}

static GBValue *
form_hide (GBRunEvalContext *ec,
	   GBRunObject      *form,
	   GBValue         **args)
{
	gtk_widget_hide (GTK_WIDGET (GBRUN_FORM (form)->window));

	return gb_value_new_empty ();
}

/**
 * gbrun_form_register
 **/
void
gbrun_form_register (void)
{
	gbrun_form_get_type ();
}


/**
 * gbrun_form_shutdown
 **/
void
gbrun_form_shutdown (void)
{	
}


/**
 * gbrun_form_pass_properties:
 *   @ec: 
 *   @obj: 
 *   @item: 
 * 
 *   This function takes the properties described by the form description,
 * promotes their values to the correct types as expected by gbrun_object_set_arg
 * etc.  It then calls gbrun_object_set_arg. Ideally we must move the promotion 
 * into gbrun_object_set_arg.
 **/
static void
gbrun_form_pass_properties (GBRunEvalContext *ec,
			    GBRunObject      *obj,
			    GBFormItem       *item)
{
	GSList *l;

	g_return_if_fail (item != NULL);
	g_return_if_fail (GBRUN_IS_OBJECT (obj));
	g_return_if_fail (GBRUN_IS_EVAL_CONTEXT (ec));
	
	for (l = item->properties; l; l = l->next) {
		GBFormProperty *prop = l->data;
		GBObjRef ref;
		
		ref.method = FALSE;
		ref.name   = prop->name;
		ref.parms  = NULL;

		if (gbrun_object_has_property (
			GBRUN_OBJECT_GET_CLASS (obj),
			prop->name, GBRUN_PROPERTY_WRITEABLE))
			gb_object_assign (GB_EVAL_CONTEXT (ec), GB_OBJECT (obj),
					  &ref, prop->value, FALSE);
		else
			g_warning ("Missing property '%s' on '%s' named '%s'", 
			           prop->name, gbrun_object_name (GBRUN_OBJECT (obj)),
				   item->name);
	}
}


/**
 * gbrun_form_show:
 *   @obj: 
 **/
void
gbrun_form_show (GBRunForm *form)
{
	g_return_if_fail (GBRUN_IS_FORM (form));

	gtk_widget_show (GTK_WIDGET (form->window));	
}


void
gbrun_form_add (GBRunForm         *form,
		GBRunFormItem     *item,
		const char        *name)
{
	g_return_if_fail (item != NULL);
	g_return_if_fail (name != NULL);
	g_return_if_fail (form != NULL);

	item->parent = GBRUN_OBJECT (form);
	item->name   = g_strdup (name);

	gtk_fixed_put (form->fixed, item->widget, 0, 0);
}

gboolean
gbrun_form_invoke (GBRunEvalContext *ec,
		   GBRunForm        *form,
		   const char       *method,
		   GBRunFormItem    *fi)
{
	const GBExpr	*expr = NULL;
	GBValue  	*ignore;
	gboolean	 ret;
	
	g_return_val_if_fail (form != NULL, FALSE);

	if (gbrun_object_has_method (GBRUN_OBJECT_GET_CLASS (form), method)) {
		GBObjRef ref;

		ref.method = TRUE;
		ref.name   = method;
		ref.parms  = NULL;

		if (fi) {
			if (fi->index >= 0) {
				expr = gb_expr_new_int (fi->index);
				ref.parms = g_slist_append (ref.parms, (gpointer) expr);
			}
		}

		if ((ignore = gbrun_objref_deref (ec, GB_OBJECT (form),
						  &ref, TRUE))) {
			gb_value_destroy (ignore);
			if (expr)
				gb_expr_destroy (expr);
			ret = TRUE;
		} else {
			if (gb_eval_exception (GB_EVAL_CONTEXT (ec))) {
				g_warning ("Error invoking '%s' : '%s", method,
					   gb_eval_context_get_text (GB_EVAL_CONTEXT (ec)));
				gb_eval_context_reset (GB_EVAL_CONTEXT (ec));
				if (expr)
					gb_expr_destroy (expr);
				ret = FALSE;
			}
		}
	}

	return ret;
}


/**
 * setup_carray
 *  @key	key from hash table; "type/name"
 *  @value	value from hash table; address of GBRunFormItem
 *  @user_data	address of GBRunEvalContext
 *
 * Builds a control array
 **/
static void
setup_carray (gpointer key, gpointer value, gpointer user_data)
{
	
	char            *name;
	char		*type;
	gchar		**keys;
	GBRunFormItem   *fi1 = (GBRunFormItem *) value;
	GBValue         *objval, *ndx, *array;
	const GBExpr    *pos;
	GBObjRef         ref;
	const GBExpr    *tmp;
	GBVar           *var;
	GBIndex         *index;
	GBValue         *v; 
	GBObject        *a;
	GSList		*i;


	/*
	 * Parse key to get type and name.
	 */
	keys = g_strsplit ((gchar *)key, "/", 3);
	type = keys[0];
	name = keys[1];
	
	/*
	 * Create the control array.
	 */
	v     = gb_value_new_int ( g_slist_length (fi1->carray) );
	tmp   = gb_expr_new_value (v);
	index = gb_index_new (NULL, tmp);
	
	var   = gb_var_new (name, TRUE, TRUE, g_slist_append (NULL, index),
			    type);
	
	a = gbrun_array_new (user_data, var);

	gbrun_object_var_add (user_data, fi1->parent,
			      name,
			      gb_value_new_object (GB_OBJECT (a)));

	
	gb_expr_destroy (tmp);
	gb_var_destroy (var);
	
	/*
	 * Populate the array
	 * Do the top level one first
	 */
	objval = gb_value_new_object (GB_OBJECT (fi1));
	
	ndx = gb_value_new_int (fi1->index);
	pos = gb_expr_new_value (ndx);

	ref.method = TRUE;
	ref.name   = NULL;
	ref.parms  = g_slist_prepend (NULL, (gpointer) pos);
	
	array = gbrun_object_var_get (user_data, fi1->parent, name);

	if (!gbrun_array_assign (user_data, GB_OBJECT (array->v.obj),
				 &ref, objval, TRUE)) 
		g_warning ("Could not initialize first control array member on %s", name);
	
	gb_expr_destroy (pos);

	for (i = fi1->carray; i; i = i->next) {
		GBRunFormItem *fi2 = i->data;

		objval = gb_value_new_object (GB_OBJECT (fi2));
	
		ndx = gb_value_new_int (fi2->index);
		pos = gb_expr_new_value (ndx);

		ref.method = TRUE;
		ref.name   = NULL;
		ref.parms  = g_slist_prepend (NULL, (gpointer) pos);
	
		array = gbrun_object_var_get (user_data, fi1->parent, name);

		if (!gbrun_array_assign (user_data, GB_OBJECT (array->v.obj),
				         &ref, objval, TRUE)) 
			g_warning ("Could not initialize control array members on %s", 
			           name);
	
		gb_expr_destroy (pos);
	}

	g_strfreev (keys);
}

		
/**
 * gbrun_form_init:
 *   @ec: 
 *   @form: 
 *   @pd:
 * 
 **/
void
gbrun_form_init (GBRunEvalContext  *ec,
		 GBRunForm         *form,
		 const GBParseData *pd)
{
	GHashTable    *siblings;
	GSList        *l;
	GBFormItem    *item;
	focus_data    *data;
	
       	g_return_if_fail (ec != NULL);
	g_return_if_fail (pd != NULL);
	g_return_if_fail (form != NULL);

	data = g_new (focus_data, 1);
	g_return_if_fail (data != NULL);
	
	data->ec   = ec;
	data->form = form;
	gtk_signal_connect (GTK_OBJECT (form->window), "focus_in_event",
		            GTK_SIGNAL_FUNC (form_focus_in),
		            data);
	
	gtk_signal_connect (GTK_OBJECT (form->window), "clicked",
					GTK_SIGNAL_FUNC (form_click), data);
					
	gtk_signal_connect (GTK_OBJECT (form->window), "size-request",
					GTK_SIGNAL_FUNC (form_resize), data);
		
	gtk_signal_connect (GTK_OBJECT (form->window), "draw",
					GTK_SIGNAL_FUNC (form_paint), data);

	/* Process parent form first  */
	item = pd->form;
	g_return_if_fail (item != NULL);

	siblings = g_hash_table_new (g_str_hash, g_str_equal);

	form->carray = g_hash_table_new (g_str_hash, g_str_equal);

	gbrun_form_pass_properties (ec, GBRUN_OBJECT (form), item);
	
	/* Now let's process the form items and their children */

	for (l = item->children; l; l = l->next) {
		GBFormItem       *i = l->data;
		GtkType           type;
		GBRunFormItem    *item2;
		gchar	         *hash_key;
		gpointer         *hash_value;

		type = gb_gtk_type_from_name (i->type);
		if (!type) {
			g_warning ("Unknown sub-form type '%s'", i->type);
			continue;
		}

		item2 = gbrun_form_item_new (ec, type);

		/*
		 * We first let the item add itself to the form
		 * and after that pass properties. FIXME: Does this make sense?
		 */			
		
		gbrun_form_item_add (ec, item2, form, i->name);
		gbrun_form_pass_properties (ec, GBRUN_OBJECT (item2), i);
		
		/*
		 * Check for arrays by looking for duplicates (siblings)..
		 */
		hash_key   = g_strconcat (i->type, "/", i->name, NULL);
		hash_value = g_hash_table_lookup (siblings, hash_key);
		
		if (!hash_value) {
			/*
			 * Not a duplicate.
			 * Record this new sibling
			 */
			g_hash_table_insert (siblings, hash_key, item2);

		} else {
			GBRunFormItem *item3 = (GBRunFormItem *)hash_value;
			
			item3->carray = g_slist_append (item3->carray, item2);
				
			if (!g_hash_table_lookup (form->carray, hash_key)) 
				g_hash_table_insert (form->carray, hash_key,
				                     item3);
		}
		
		if (i->children) {
			GSList		*c;
			
			/*if (i->children->data)
			  g_warning ("Processing children on item '%s' of type '%s'",
			  i->name, i->type);*/

			for (c = i->children; c; c = c->next) {
				GBFormItem	*ii = c->data;
				GtkType		 type_sub;
				GBRunFormItem	*subitem;

				if (ii) {
					type_sub = gb_gtk_type_from_name (ii->type);
					if (!type_sub) {
						g_warning ("Unknown sub-form type '%s'", ii->type);
						continue;
					}
					
					subitem = gbrun_form_item_new (ec, type_sub);

					gbrun_form_subitem_add (ec, subitem, item2, form, ii->name);

					gbrun_form_pass_properties (ec, GBRUN_OBJECT (subitem), ii);

				}
			}
		}
	}

	/* Add non-carray controls to the stack */
	for (l = item->children; l; l = l->next) {
		GBFormItem       *i = l->data;
		GBRunFormItem    *item;
		gchar            *hash_key;
		gpointer         *hash_value;

		hash_key = g_strconcat (i->type, "/", i->name, NULL);
		hash_value = g_hash_table_lookup (siblings, hash_key);

		if (g_hash_table_lookup (form->carray, hash_key))
			continue;
		
		item = (GBRunFormItem *) hash_value;

		gbrun_stack_add  (ec, i->name,
				  gb_value_new_object (GB_OBJECT (item)),
				  GBRUN_STACK_MODULE);
	}
	
		
	/* 
	 * Now we fill in the control array elements with the appropriate
	 * form item references
	 */
	g_hash_table_foreach (form->carray, setup_carray, ec);

	gbrun_form_show (form);
}

void
gbrun_form_widget_set_color (GtkWidget         *widget,
			     GBRunFormColorType type,
			     GBLong             color)
{
	GdkColor  col;
	GtkStyle *style;
	GdkColor *array = NULL;

	col.red   = ((color >>  0) & 0xff) * 255;
	col.green = ((color >>  8) & 0xff) * 255;
	col.blue  = ((color >> 16) & 0xff) * 255;

	style = gtk_style_copy (widget->style);

/*	g_warning ("Setting %d color on widget to 0x%x (= %d, %d, %d)", type,
	color, col.red, col.green, col.blue);*/
	/*
	 * FIXME: should recurse down containment hierarchy perhaps ?
	 * at least until we hit a widget with the FormItemKey
	 */

	/* FIXME: these need testing / rationalizing */
	switch (type) {

	case GBRUN_FORM_COLOR_BACK:
		array = style->bg;
		break;

	case GBRUN_FORM_COLOR_BORDER:
		array = style->base;
		break;

	case GBRUN_FORM_COLOR_FILL:
		array = style->bg;
		break;

	case GBRUN_FORM_COLOR_FORE:
		array = style->fg;
		break;

	case GBRUN_FORM_COLOR_MASK:
	default:
		g_warning ("Unknown color type");
		break;
	};

	if (array) {
		int i;
		for (i = GTK_STATE_NORMAL; i <= GTK_STATE_INSENSITIVE; i++)
			array [i] = col;
	}

	gtk_widget_set_style  (widget, style);
	gtk_widget_queue_draw (widget);
}

GBLong
gbrun_form_widget_get_color (GtkWidget         *widget,
			     GBRunFormColorType type)
{
	GdkColor	*wcolor = NULL;
	GBLong		 color = 0;
	
	switch (type) {

	case GBRUN_FORM_COLOR_BACK:
		wcolor = widget->style->bg;
		break;

	case GBRUN_FORM_COLOR_BORDER:
		wcolor = widget->style->base;
		break;

	case GBRUN_FORM_COLOR_FILL:
		wcolor = widget->style->bg;
		break;

	case GBRUN_FORM_COLOR_FORE:
		wcolor = widget->style->fg;
		break;

	case GBRUN_FORM_COLOR_MASK:
	default:
		g_warning ("gbrun_form_widget_get_color: Unknown color type");
		break;
	};

	if (wcolor)
		color = (((wcolor->blue  / 255) & 0xff) << 16) +
		        (((wcolor->green / 255) & 0xff) <<  8) +
		        (((wcolor->red   / 255) & 0xff) <<  0);

	return color;
}

char *
gbrun_form_un_shortcutify (const char *txt, char *shortcut)
{
	char *ans;
	int   i;

	g_return_val_if_fail (txt != NULL, NULL);

	ans = g_strdup (txt);

	/* FIXME: some sort of escaping must happen */
	for (i = 0; ans [i]; i++)
		if (ans [i] == '&') {
			if (shortcut)
				*shortcut = ans [i + 1];
			ans [i] = '_';
		}

	return ans;
}


static void
gbrun_form_destroy (GtkObject *object)
{
	GBRunForm *form = GBRUN_FORM (object);

	if (form->window)
		gtk_widget_destroy (GTK_WIDGET (form->window));
	form->window = NULL;
}

static void
gbrun_form_instance_init (GBRunForm *form)
{
	GtkVBox *vbox;
	
	form->window = GTK_WINDOW (gtk_window_new (GTK_WINDOW_TOPLEVEL));
	gtk_signal_connect (GTK_OBJECT (form->window),
  		            "delete_event",
		            GTK_SIGNAL_FUNC (delete_event_cb),
		            form);

	/*
	 * We add an vbox so we can add the menu on top
	 * and the fixed form itself on the bottom
	 */
	vbox = GTK_VBOX (gtk_vbox_new (FALSE, 5));
	gtk_container_add (GTK_CONTAINER (form->window),
			   GTK_WIDGET (vbox));

	form->menubar = GTK_MENU_BAR (gtk_menu_bar_new ());
	gtk_box_pack_start (GTK_BOX (vbox),
			    GTK_WIDGET (form->menubar),
			    FALSE, FALSE, 0);
	gtk_widget_hide (GTK_WIDGET (form->menubar));

	form->fixed = GTK_FIXED (gtk_fixed_new ());
	gtk_box_pack_end (GTK_BOX (vbox),
			  GTK_WIDGET (form->fixed),
			  FALSE, FALSE, 0);

	form->accel_group = gtk_accel_group_new ();

	gtk_window_add_accel_group (GTK_WINDOW (form->window),
				    form->accel_group);

	form->radio_btn = NULL;
	
	gtk_widget_show (GTK_WIDGET (vbox));
	gtk_widget_show (GTK_WIDGET (form->fixed));
}

static void
gbrun_form_class_init (GBRunObjectClass *klass)
{
	GtkObjectClass *gtk_class = (GtkObjectClass *) klass;

	klass->set_arg = form_setarg;
	klass->get_arg = form_getarg;

	gbrun_object_add_property (klass, "caption",	  gb_type_string, CAPTION);
	gbrun_object_add_property (klass, "height", gb_type_int, HEIGHT);
	gbrun_object_add_property (klass, "clientheight", gb_type_int, HEIGHT);
	gbrun_object_add_property (klass, "left",   gb_type_int, LEFT);
	gbrun_object_add_property (klass, "clientleft", gb_type_int, LEFT);
	gbrun_object_add_property (klass, "scaleheight",  gb_type_int, SCALE_HEIGHT);
	gbrun_object_add_property (klass, "scalewidth",   gb_type_int, SCALE_WIDTH);
	gbrun_object_add_property (klass, "scalemode",    gb_type_int, SCALE_MODE);
	gbrun_object_add_property (klass, "top",    gb_type_int, TOP);
	gbrun_object_add_property (klass, "clienttop", gb_type_int, TOP);
	gbrun_object_add_property (klass, "width",  gb_type_int, WIDTH);
	gbrun_object_add_property (klass, "clientwidth", gb_type_int, WIDTH);
	gbrun_object_add_property (klass, "linktopic",    gb_type_string, LINK_TOPIC);
	gbrun_object_add_property (klass, "startupposition", gb_type_int, STARTUP_POSITION);
	gbrun_object_add_property (klass, "borderstyle",     gb_type_int, BORDER_STYLE);
	gbrun_object_add_property (klass, "maxbutton",       gb_type_int, MAX_BUTTON);
	gbrun_object_add_property (klass, "minbutton",       gb_type_int, MIN_BUTTON);
	gbrun_object_add_property (klass, "showintaskbar",   gb_type_int, SHOW_IN_TASKBAR);
	gbrun_object_add_property (klass, "tag",             gb_type_string, TAG);
	gbrun_object_add_property (klass, "controlbox",      gb_type_int, CONTROLBOX);

	/*
	 * We nee the Objects collection here;
	 */
/*
	This line:

		Form1!Ctrl1.Text = "Hello"

	translates into this code:

		Form1.Controls.Item("Ctrl1").Text = "Hello"
*/

	gbrun_object_add_method_arg (
		klass, "sub;show;modal,integer,byval,0;ownerform,variant,byref,0;g", form_show);

	gbrun_object_add_method_arg (
		klass, "sub;hide;g", form_hide);

	gtk_class->destroy = gbrun_form_destroy;
}

GtkType
gbrun_form_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunForm),
			sizeof (GBRunFormClass),
			(GtkClassInitFunc)  gbrun_form_class_init,
			(GtkObjectInitFunc) gbrun_form_instance_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_OBJECT, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}
