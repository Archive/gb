
/*
 * GNOME Basic Enterpreter Function registration
 *
 * Authors:
 *    Michael Meeks (mmeeks@gnu.org)
 *    Thomas Meeks  (meekte95@christs-hospital.org.uk)
 *    Ariel Rios    (ariel@arcavia.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <gbrun/gbrun.h>
#include <gbrun/libgbrun.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-stack.h>
#include <gbrun/gbrun-statement.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-object.h>

#include "libgba.h"
#include "gba-collection.h"
#include "gba-conversion.h"
#include "gba-datetime.h"
#include "gba-interaction.h"
#include "gba-math.h"
#include "gba-string.h"
#include "gba-func.h"

void
libgba_register (GBEvalContext *ec)
{
	gba_collection_register  (ec);
	gba_conversion_register  (ec);
        gba_datetime_register    (ec);
        gba_interaction_register (ec);
        gba_math_register        (ec);
        gba_string_register      (ec);
        gba_func_register        (ec);
}

void
libgba_shutdown ()
{
        gba_func_shutdown        ();
        gba_string_shutdown      ();
        gba_math_shutdown        ();
        gba_interaction_shutdown ();
        gba_datetime_shutdown    ();
        gba_conversion_shutdown  ();
        gba_collection_shutdown  ();
}
