
#ifndef VBA_INTERACTION_H
#define VBA_INTERACTION_H

#define GBRUN_STOCK_BUTTON_ABORT  "Abort"
#define GBRUN_STOCK_BUTTON_RETRY  "Retry"
#define GBRUN_STOCK_BUTTON_IGNORE "Ignore"

void gba_interaction_register (GBEvalContext *ec);
void gba_interaction_shutdown (void);


#endif
