
/*
 * GNOME Basic Enterpreter Function registration
 *
 * Authors:
 *    Michael Meeks (mmeeks@gnu.org)
 *    Thomas Meeks  (meekte95@christs-hospital.org.uk)
 *    Ariel Rios    (ariel@arcavia.com)
 *    Julian Froment (julian@jfroment.uklinux.net)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <gnome.h>

#include <gbrun/gbrun.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-array.h>
#include <gbrun/gbrun-object.h>
#include <gbrun/gbrun-global.h>

#include <gb/gb-constants.h>

#include "gba-interaction.h"

#define ITEM_NAME "VBA.Interaction"

#if 0
static GBValue *
gbrun_func_choose (GBRunEvalContext *ec,
                 GBRunObject      *object,
		 GBValue         **args)
{
	GBInt i = gb_value_get_as_int (args [0]);

	return args [i];
}
#endif

static GBValue *
gbrun_sub_msgbox (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	GtkWidget   *dialog;
	char        *type;
	const char **buttons;
	const char  *buttons_0[] = { GNOME_STOCK_BUTTON_OK, NULL };
	const char  *buttons_1[] = { GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL };
	const char  *buttons_2[] = { GBRUN_STOCK_BUTTON_ABORT, GBRUN_STOCK_BUTTON_RETRY,
				     GNOME_STOCK_BUTTON_CANCEL, NULL };
	const char  *buttons_3[] = { GNOME_STOCK_BUTTON_YES, GNOME_STOCK_BUTTON_NO,
				     GNOME_STOCK_BUTTON_CANCEL, NULL };
	const char  *buttons_4[] = { GNOME_STOCK_BUTTON_YES, GNOME_STOCK_BUTTON_NO, NULL };
	const char  *buttons_5[] = { GBRUN_STOCK_BUTTON_RETRY, GNOME_STOCK_BUTTON_CANCEL, NULL };
	long         b;
	int          buts, i;
	
	GB_IS_VALUE     (ec, args [0], GB_VALUE_STRING);
	GB_IS_VALUE_OPT (ec, args [1], GB_VALUE_LONG);

	if (args [1])
		b = args [1]->v.l;
	else
		b = 0;

	if (b & GB_C_Critical)
		type = GNOME_MESSAGE_BOX_ERROR;
	else if (b & GB_C_Question)
		type = GNOME_MESSAGE_BOX_QUESTION;
	else if (b & GB_C_Exclamation)
		type = GNOME_MESSAGE_BOX_WARNING;
	else if (b & GB_C_Information)
		type = GNOME_MESSAGE_BOX_INFO;
	else
		type = GNOME_MESSAGE_BOX_GENERIC;

	buts = b & 0xf;
	switch (buts) {

	case GB_C_OKCancel:
		buttons = buttons_1;
		break;

	case GB_C_AbortRetryIgnore:
		buttons = buttons_2;
		break;

	case GB_C_YesNoCancel:
		buttons = buttons_3;
		break;

	case GB_C_YesNo:
		buttons = buttons_4;
		break;

	case GB_C_RetryCancel:
		buttons = buttons_5;
		break;

	case GB_C_OKOnly:
	default:
		buttons = buttons_0;
		break;
	};

	dialog = gnome_message_box_newv (args [0]->v.s->str, type, buttons);

	i = gnome_dialog_run (GNOME_DIALOG (dialog));

	if (i < 0 || !buttons [i] ||
	    !strcmp (buttons [i], GNOME_STOCK_BUTTON_CANCEL))
		return gb_value_new_int (GB_C_Cancel);

	if (!strcmp (buttons [i], GBRUN_STOCK_BUTTON_ABORT))
		return gb_value_new_int (GB_C_Abort);

	if (!strcmp (buttons [i], GBRUN_STOCK_BUTTON_RETRY))
		return gb_value_new_int (GB_C_Retry);

	if (!strcmp (buttons [i], GBRUN_STOCK_BUTTON_IGNORE))
		return gb_value_new_int (GB_C_Ignore);

	if (!strcmp (buttons [i], GNOME_STOCK_BUTTON_YES))
		return gb_value_new_int (GB_C_Yes);

	if (!strcmp (buttons [i], GNOME_STOCK_BUTTON_NO))
		return gb_value_new_int (GB_C_No);

	return gb_value_new_int (GB_C_OK);
}

static GBValue *
gbrun_sub_inputbox (GBRunEvalContext *ec,
		    GBRunObject      *object,
		    GBValue         **args)
{
	GtkWidget *dialog;
        GtkWidget *label;
        GtkWidget *entry;
	GBValue   *ret;
        gint       i;

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	GB_IS_VALUE (ec, args [1], GB_VALUE_STRING);
	GB_IS_VALUE (ec, args [2], GB_VALUE_STRING);

	dialog = gnome_dialog_new (args[1]->v.s->str, 
				   GNOME_STOCK_BUTTON_OK, 
				   GNOME_STOCK_BUTTON_CANCEL,
				   NULL);

	label = gtk_label_new (args[0]->v.s->str);
        gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox), label,
			    TRUE, TRUE, 0);

	entry = gtk_entry_new ();

	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox), entry,
			    TRUE, TRUE, 0);

	gtk_widget_grab_focus (entry);

	gnome_dialog_set_default (GNOME_DIALOG (dialog), 0);
        gnome_dialog_editable_enters (GNOME_DIALOG (dialog),
				      GTK_EDITABLE (entry));
	gtk_entry_set_text (GTK_ENTRY (entry), args[2]->v.s->str);

        gtk_widget_show_all (dialog);
      
	i = gnome_dialog_run (GNOME_DIALOG (dialog));

	if (i == 0) {
		char *result;

		result = gtk_editable_get_chars (
			GTK_EDITABLE (entry), 0, -1);
		ret = gb_value_new_string_chars (result);
		g_free (result);
	} else /* FIXME: is this correct for cancel ? */
		ret = gb_value_new_string_chars ("");

	gnome_dialog_close (GNOME_DIALOG (dialog));

	return ret;
}

static GBValue *
gbrun_func_iif (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      GBValue         **args)
{
	GB_IS_VALUE (ec, args [0], GB_VALUE_BOOLEAN);

	return gb_value_copy (GB_EVAL_CONTEXT (ec),
			      args [args [0]->v.bool ? 1: 2]);
}


static GBValue *
gbrun_func_shell (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	GBString str = gb_value_get_as_string (args [0]);

	/*
	 * FIXME: We need to include the optional argument.
	 */
	
	return gb_value_new_int (gnome_execute_shell (NULL, str->str));	
}
static GBValue *
gbrun_func_partition (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	long interval;
	long start_int;
	long int i;
	gchar *result;
	GBValue *ret;

	GB_IS_VALUE (ec, args [0], GB_VALUE_LONG);
	GB_IS_VALUE (ec, args [1], GB_VALUE_LONG);
	GB_IS_VALUE (ec, args [2], GB_VALUE_LONG);
	GB_IS_VALUE (ec, args [3], GB_VALUE_LONG);

	g_return_val_if_fail ((args [1]->v.l) >= 0, NULL);
	g_return_val_if_fail ((args [2]->v.l) >= 0, NULL);
	g_return_val_if_fail ((args [3]->v.l) >= 0, NULL);

	if ((args [0]->v.l) < 0) {
		result = g_strdup_printf (":%d", -1);
		ret = gb_value_new_string_chars (result);
		g_free (result);
		return ret;
	}

	if ((args [0]->v.l) < (args [1]->v.l)) {
		result = g_strdup_printf (":%ld", (long)((args [1]->v.l) - 1));
		ret = gb_value_new_string_chars (result);
		g_free (result);
		return ret;
	}

	if ((args [0]->v.l) > (args [2]->v.l)) { 
		result = g_strdup_printf ("%ld:", (long)((args [2]->v.l) + 1));
		ret = gb_value_new_string_chars (result);
		g_free (result);
		return ret;
	}

	start_int = args [1]->v.l;
	interval = args [3]->v.l;

	for (i = start_int; i <= (args [2]->v.l); i += interval) {
		if ((i + interval - 1) >= args [2]->v.l) {
			if (((args [0]->v.l) >= i) && ((args [0]->v.l) <= (i + interval - 1))) 
				result = g_strdup_printf ("%ld:%ld", i, (long)args [2]->v.l);		
		} else {	
			if (((args [0]->v.l) >= i) && ((args [0]->v.l) <= (i + interval - 1))) 
				result = g_strdup_printf ("%ld:%ld", i, (long)(i + interval - 1));	
		}
	}

	ret = gb_value_new_string_chars (result);
	g_free (result);

	/* FIXME: needs preceding spaces to be added to numbers shorter than the length of the stop number */

	return ret;
}

void
gba_interaction_register (GBEvalContext *ec)
{
	GBRunObject      *gba_object;
	GBRunObjectClass *gba;

	gba_object = gtk_type_new (
		gbrun_object_subclass_simple (GBRUN_TYPE_OBJECT, ITEM_NAME));
	gbrun_global_add (GB_OBJECT (gba_object), "interaction");

	gba = GBRUN_OBJECT_GET_CLASS (gba_object);

	gbrun_object_add_method_arg (gba, "func;iif;truth,boolean;truepart,string;falsepart,string;string;n",
				     gbrun_func_iif);

        gbrun_object_add_method_arg (gba, "func;shell;a,string;integer;x",
                                     gbrun_func_shell);

	gbrun_object_add_method_arg (gba, "func;msgbox;prompt,string;"
				     "buttons,long,byval,0;"
				     "title,string,byref, ;"
				     "helpfile,string,byref, ;"
				     "context,integer,byval,0;"
				     "integer;g", gbrun_sub_msgbox);

	gbrun_object_add_method_arg (gba, "func;inputbox;prompt,string;"
                                     "title,string,byref, ;"
                                     "default,string,byref, ;"
                                     "xpos,integer,byval,0;"
				     "ypos,integer,byval,0;"
                                     "helpfile,string,byref, ;"
                                     "context,integer,byval,0;"
                                     "string;g", gbrun_sub_inputbox);

	gbrun_object_add_method_arg (gba, "func;partition;number,long;"
				     "start,long,byval,0;"
				     "stop,long,byval,0;"
				     "interval,long,byval,0;"
				     "string;x", gbrun_func_partition);
}

void
gba_interaction_shutdown (void)
{

}
