
/*
 * GNOME Basic Enterpreter Function registration
 *
 * Authors:
 *    Michael Meeks (mmeeks@gnu.org)
 *    Thomas Meeks  (meekte95@christs-hospital.org.uk)
 *    Ariel Rios    (ariel@arcavia.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */


#include <gbrun/gbrun.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-array.h>
#include <gbrun/gbrun-object.h>
#include <gbrun/gbrun-global.h>

#include <gb/gb-constants.h>

#include "gba-conversion.h"

#define ITEM_NAME "VBA.Conversion"

/* FIXME: this function is guessed; check it */
static GBValue * 
gbrun_func_cstr (GBRunEvalContext *ec,
	       GBRunObject      *object,
	       GBValue         **args)
{
	return gb_value_promote (GB_EVAL_CONTEXT (ec),
				 gb_gtk_type_from_value (GB_VALUE_STRING),
				 args [0]);
}

/* FIXME: this function is guessed; check it */
static GBValue * 
gbrun_func_clng (GBRunEvalContext *ec,
	       GBRunObject      *object,
	       GBValue         **args)
{
	return gb_value_promote (GB_EVAL_CONTEXT (ec),
				 gb_gtk_type_from_value (GB_VALUE_LONG),
				 args [0]);
}

static GBValue *
gbrun_func_hex (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      GBValue         **args)
{
	GBValue *v   = args [0];
	GBValue *ret = NULL;

	switch (gb_value_from_gtk_type (v->gtk_type)) {

	case GB_VALUE_STRING: /* FIXME: perhaps special case this */
	case GB_VALUE_NUMBER:
	{
		GBLong     l = gb_value_get_as_long (v);
		GString *str = g_string_sized_new (sizeof (GBLong) + 1);

		g_string_sprintf (str, "%x", l);

		ret = gb_value_new_string_chars (str->str);

		g_string_free (str, TRUE);
		break;
	}

	case GB_VALUE_NULL:
		ret = gb_value_new_null ();
		break;

	case GB_VALUE_EMPTY:
		ret = gb_value_new_string_chars ("0");
		break;

	default:
		g_warning ("Unhandled hex argument type");
		return gb_value_new_empty ();
	}

	return ret;
}


void
gba_conversion_register (GBEvalContext *ec)
{
	GBRunObject      *gba_object;
	GBRunObjectClass *gba;

	gba_object = gtk_type_new (
		gbrun_object_subclass_simple (GBRUN_TYPE_OBJECT, ITEM_NAME));
	gbrun_global_add (GB_OBJECT (gba_object), "conversion");

	gba = GBRUN_OBJECT_GET_CLASS (gba_object);

	gbrun_object_add_method_arg (gba, "func;cstr;num,variant;string;n",
				     gbrun_func_cstr);

	gbrun_object_add_method_arg (gba, "func;clng;num,variant;long;n",
				     gbrun_func_clng);

	gbrun_object_add_method_arg (gba, "func;hex;a,variant;string;n",
				     gbrun_func_hex);

	gbrun_object_add_method_arg (gba, "func;hex$;a,variant;string;n",
				     gbrun_func_hex);

/*
        gbrun_object_add_method_arg (gba, "func;val;a,string;double;n",
				     gbrun_func_val);
	
	gbrun_object_add_method_arg (gba, "func;string;num,integer;"
				     "char,string;string;n", gbrun_func_string);
*/
}



void
gba_conversion_shutdown (void)
{

}
