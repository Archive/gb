
/*
 * GNOME Basic Function registration
 *
 * Author:
 *    Per Winkvist (nd96pwt@student.hig.se)
 *
 * Copyright 2000, Helix Code, Inc
 */

#ifndef LIBGBA_H
#define LIBGBA_H

void libgba_register (GBEvalContext *ec);
void libgba_shutdown (void);

#endif
