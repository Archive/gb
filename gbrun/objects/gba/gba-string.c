/*
 * GNOME Basic Enterpreter Function registration
 *
 * Authors:
 *    Michael Meeks (mmeeks@gnu.org)
 *    Thomas Meeks  (meekte95@christs-hospital.org.uk)
 *    Ariel Rios    (ariel@arcavia.com)
 *    Sean Atkinson (sca20@cam.ac.uk)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

#include <gbrun/gbrun.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-array.h>
#include <gbrun/gbrun-object.h>
#include <gbrun/gbrun-global.h>

#include <gb/gb-constants.h>

#include "gba-string.h"

#define ITEM_NAME "VBA.String"

static GBValue *
gbrun_func_asc  (GBRunEvalContext *ec,
	       GBRunObject      *object,
	       GBValue         **args)
{
	int v;
	GString *str;

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	str = args [0]->v.s;
	v = (int) (str->str)[0];
	
	return gb_value_new_int (v);
}

static GBValue *
gbrun_func_chr (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      GBValue         **args)
{
	char tmp[2] = " ";

	GB_IS_VALUE(ec, args[0], GB_VALUE_INT);
	tmp[0] = (char) args[0]->v.i;

	return gb_value_new_string_chars (tmp);
}

static GBValue *
gbrun_func_chrb (GBRunEvalContext *ec,
	       GBRunObject      *object,
	       GBValue         **args)
{
	char tmp[2] = " ";

	GB_IS_VALUE(ec, args[0], GB_VALUE_INT);
	tmp[0] = (guint8) args[0]->v.i;

	return gb_value_new_string_chars(tmp);
}

static GBValue *
gbrun_func_instr (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	unsigned start;
	char *pos;
	GString *hayst, *needle;

	start = gb_value_get_as_int(args[0]);
	GB_IS_VALUE (ec, args[1], GB_VALUE_STRING);
	hayst = args [1]->v.s;
	GB_IS_VALUE (ec, args[2], GB_VALUE_STRING);
	needle = args[2]->v.s;
	switch (gb_value_get_as_int(args[3])) {
	case 0: /* binary */
		break;
	case 1: /* case insensitive */
		g_strdown(hayst->str);
		g_strdown(needle->str);
		break;
	case 2: /* Access, from Option Compare */
	default:
		gbrun_exception_fire(ec, _("instr: unsupported comparison"));
	}
	if (start == 0 || start > 1 + hayst->len - needle->len)
		return gb_value_new_int(0);

	pos = strstr (&hayst->str[start-1], needle->str);

	return gb_value_new_int((pos) ? 1 + pos - hayst->str : 0);
}

static GBValue *
gbrun_func_instrb (GBRunEvalContext *ec,
                 GBRunObject      *object,
                 GBValue         **args)
{
	unsigned start;
	char *pos;
	GString *hayst, *needle;

	start = gb_value_get_as_int(args[0]);
	GB_IS_VALUE (ec, args[1], GB_VALUE_STRING);
	hayst = args [1]->v.s;
	GB_IS_VALUE (ec, args[2], GB_VALUE_STRING);
	needle = args[2]->v.s;
	switch (gb_value_get_as_int(args[3])) {
	case 0: /* binary */
		break;
	case 1: /* case insensitive */
		g_strdown(hayst->str);
		g_strdown(needle->str);
		break;
	case 2: /* Access, from Option Compare */
	default:
		gbrun_exception_fire(ec, _("instr: unsupported comparison"));
	}
	if (start == 0 || start > 1 + hayst->len - needle->len)
		return gb_value_new_int(0);

	pos = strstr (&hayst->str[start-1], needle->str);

	return gb_value_new_int((pos) ? sizeof(*hayst->str) *
				(1 + pos - hayst->str) : 0);
}

static GBValue *
gbrun_func_instrrev (GBRunEvalContext *ec,
                     GBRunObject      *object,
                     GBValue         **args)
{
	GString *haystack, *needle;
	gint start;
	GB_IS_VALUE (ec, args[0], GB_VALUE_STRING);
	haystack = args[0]->v.s;
	GB_IS_VALUE (ec, args[1], GB_VALUE_STRING);
	needle = args[1]->v.s;
	start = gb_value_get_as_int (args[2]);

	switch (gb_value_get_as_int (args[3])) {
	case 1:
		g_string_down (haystack);
		g_string_down (needle);
		break;
	case 2:
		gbrun_exception_fire (ec, _("instrrev: unsupported comparison"));
		break;
	}

	if (!haystack->len)
		return gb_value_new_int (0);
	if (!needle->len)
		return gb_value_new_int (start);
	if (start < needle->len && start != -1)
		return gb_value_new_int (0);
	{
		gchar *loc, *temp_h,
                      *temp_n = g_strdup (needle->str);
		gint index;
		if (start < 0)
			start = strlen (haystack->str);
		temp_h = malloc (start + 1);
		strncpy (temp_h, haystack->str, start);
		temp_h[start] = '\0';
		g_strreverse (temp_h);
		g_strreverse (temp_n);
		loc = strstr (temp_h, temp_n);
		if (!loc) {
			free (temp_h);
			free (temp_n);
			return gb_value_new_int (0);
		}
		index = loc - temp_h;
		index = strlen (temp_h) - index - strlen (temp_n) + 1;
		free (temp_h);
		free (temp_n);
		return gb_value_new_int (index);
	}
}

static GBValue *
gbrun_func_join (GBRunEvalContext *ec,
                 GBRunObject      *object,
                 GBValue         **args)
{
	GBLong max;
	GBRunArray *array;
	GBValue **data, *res;
	int i = 0;
	char *delimeter, *str = NULL;

	if (!args[0] ||
            !gtk_type_is_a (args[0]->gtk_type, GBRUN_TYPE_ARRAY)) {
		gbrun_exception_firev (ec, _("Join: First argument "
                  "must be an array"));
		return NULL;
	}

	delimeter = args[1]->v.s->str;
	array = GBRUN_ARRAY (args[0]->v.obj);

	if (gbrun_array_dimensions (array) > 1) {
		gbrun_exception_firev (ec, _("Join: First argument "
                  "must be one-dimensional array"));
		return NULL;
	}
	max = gbrun_array_max_bound (array, 1) - gbrun_array_min_bound (array, 1);
	data = (GBValue **) array->data;

	for (i = 0; i < max + 1; i++) {
		GBValue *val;
		char *temp;

		if (!data[i]) {
			printf ("i = %d\n", i);
			continue;
		}

		val = gb_value_promote (GB_EVAL_CONTEXT (ec),
		        gb_gtk_type_from_value (GB_VALUE_STRING), data[i]);

		if (!val) {
			gbrun_exception_firev (ec, _("Join: Array "
			  "elements must evaluate to string."));
			return NULL;
		}

		temp = val->v.s->str;

		if (!str) {
			str = malloc (strlen (temp) + strlen (delimeter) + 1);
			strcpy (str, temp);
			strcat (str, delimeter);
		} else if (i == max) {
			str = realloc (str, strlen (str) + strlen (temp) + 1);
			strcat (str, temp);
		} else {
			str = realloc (str, strlen (str) + strlen (temp) +
			  strlen (delimeter) + 1);
			strcat (str, temp);
			strcat (str, delimeter);
		}
	}
	res = gb_value_new_string_chars (str);
	free (str);
	return res;
}

static GBValue *
gbrun_func_lcase (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	GString *str;

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	str = args [0]->v.s;
	g_strdown (str->str);
	
	return gb_value_new_string (str);
}

static GBValue *
gbrun_func_left (GBRunEvalContext *ec,
	       GBRunObject      *object,
	       GBValue         **args)
{
	GBValue *ans;
       	GString *str;
	GBInt length = gb_value_get_as_int (args [1]);
     
	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	str = args [0]->v.s;

	if (length > str->len)
		length = str->len;
	
	str->str[length]='\0';
	ans = gb_value_new_string_chars (str->str);

	return ans;
}

static GBValue *
gbrun_func_leftb (GBRunEvalContext *ec,
                GBRunObject      *object,
                GBValue         **args)
{
	GBValue *ans;
       	GString *str;
	GBInt length = gb_value_get_as_int (args [1]);
     
	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	str = args [0]->v.s;

	if (length > str->len)
		length = str->len;
	
	ans = gb_value_new_int (sizeof (*str->str) * length);

	return ans;
}


static GBValue *
gbrun_func_len (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	GString *str;
	
	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);

	str = args [0]->v.s;
	return gb_value_new_int (str->len);
}


static GBValue *
gbrun_func_lenb (GBRunEvalContext *ec,
		 GBRunObject      *object,
		 GBValue         **args)
{
	GBValue *ans;
	GString *str;
	
	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	str = args [0]->v.s;

	ans = gb_value_new_int (sizeof (*str->str) * str->len);

        return ans;
}

static GBValue *
gbrun_func_ltrim (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	GString *str;

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	str = args [0]->v.s;

	str->str = g_strchug (str->str);

	return gb_value_new_string (str);
}


static GBValue *
gbrun_func_mid (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	int last;
	GBValue *ans;
       	GString *str;	
	GBInt start  = gb_value_get_as_int (args [1]) - 1;
	GBInt length = gb_value_get_as_int (args [2]);

	if (start == -1)
		start = 0; /* Wierd, but hey */
	else if (start < 0)
		return gbrun_exception_firev (ec, _("invalid string offset %d"), start);

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	str = args [0]->v.s;

	last = start + length;
		
	if (start > str->len)
		start = str->len;

	if (last > str->len)
		last = str->len;

	str->str [last]='\0';
	ans = gb_value_new_string_chars (str->str + start);

	return ans;
}


static GBValue *
gbrun_func_midb (GBRunEvalContext *ec,
               GBRunObject      *object,
               GBValue         **args)
{
	int last;
	GBValue *ans;
       	GString *str;	
	GBInt start  = gb_value_get_as_int (args [1]) - 1;
	GBInt length = gb_value_get_as_int (args [2]);

	if (start == -1)
		start = 0; /* Wierd, but hey */
	else if (start < 0)
		return gbrun_exception_firev (ec, _("invalid string offset %d"), start);

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	str = args [0]->v.s;

	last = start + length;
		
	if (length > str->len)
		return gbrun_exception_fire (ec, _("offset larger than string"));

	if (last > str->len)
		return gbrun_exception_fire (ec, _("last beyond end of string"));

	ans = gb_value_new_int (sizeof (*str->str) * length);

	return ans;
}


static GBValue *
gbrun_func_right (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	int      first;
	GBValue *ans;
       	GString *str;
	GBInt    length = gb_value_get_as_int    (args [1]);

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	str = args [0]->v.s;

	first = str->len - length;

	if (length > str->len)
		first = 0;
		
	ans = gb_value_new_string_chars (str->str + first);
	
	return ans;
}

static GBValue *
gbrun_func_rightb (GBRunEvalContext *ec,
                 GBRunObject      *object,
                 GBValue         **args)
{
	int      first;
	GBValue *ans;
       	GString *str;
	GBInt    length = gb_value_get_as_int    (args [1]);

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	str = args [0]->v.s;

	first = str->len - length;

	if (length > str->len)
		first = 0;
		
	ans = gb_value_new_int (sizeof (*str->str) * length);
	
	return ans;
}


static GBValue *
gbrun_func_rtrim (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	GBValue *res;
	GString *str;

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	str = args [0]->v.s;

	str->str = g_strchomp (str->str);
	
	res = gb_value_new_string (str);

	return res;
}


static GBValue * 
gbrun_func_space (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	GBValue *res;
	int num = gb_value_get_as_int (args [0]);
	char *tmp = g_strnfill (num, ' ');
	
	res = gb_value_new_string_chars (tmp);
	g_free (tmp);

	return res;
}

static GBValue *
gbrun_func_split (GBRunEvalContext *ec,
		GBRunObject	 *object,
		GBValue		**args)
{
	GString *expr, *delimeter;
	int count, compare;

	GB_IS_VALUE (ec, args[1], GB_VALUE_STRING);
	delimeter = args[1]->v.s;

	if (delimeter->len == 0) {
		GBRunArray *a = gtk_type_new (GBRUN_TYPE_ARRAY);
		a->indices = NULL;
		a->data = NULL;
		return gb_value_new_object (GB_OBJECT (a));
	}

	GB_IS_VALUE (ec, args[0], GB_VALUE_STRING);
	expr = args[0]->v.s;

	count = gb_value_get_as_int (args[2]);
	compare = gb_value_get_as_int (args[3]);

	if (compare == 1) {
		g_string_down (expr);
		g_string_down (delimeter);
	} else if (compare == 2) {
		return gbrun_exception_firev(ec, _("instr: unsupported comparison"));
	}

	{
		int i;
		GSList *tokenised = NULL;
		char *haystack = expr->str, *needle = delimeter->str;

		for (i = 0; i < count || count == -1; i++) {
			int len;
			char *dest, *loc = strstr (haystack, needle); 
			if (!loc) {
				tokenised = g_slist_append (tokenised,
                                                            gb_value_new_string_chars (haystack));
				break;
			}
			len = loc - haystack;
			dest = malloc (len + 1);
			if (len)
				memcpy (dest, haystack, len);
			dest[len] = '\0';
			haystack = loc + strlen (needle); /* adjust haystack */
			tokenised = g_slist_append (tokenised, gb_value_new_string_chars (dest));
			free (dest);
		}
		return gb_value_new_object (gbrun_array_new_vals (ec, tokenised));
	}
}


static GBValue * 
gbrun_func_str (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
        char *p;
	GBValue *ans;

	p = g_strdup_printf("% g", gb_value_get_as_double(args[0]));
	ans = gb_value_new_string_chars(p);
	g_free(p);
	return ans;
}


static GBValue * 
gbrun_func_string (GBRunEvalContext *ec,
		   GBRunObject      *object,
		   GBValue         **args)
{
	GBValue *res;
	int num = gb_value_get_as_int (args [0]);
	GString *c;
	char *tmp;
	
	GB_IS_VALUE (ec, args [1], GB_VALUE_STRING);
	c = args [1]->v.s;

	tmp = g_strnfill (num, c->str [0]);

	res = gb_value_new_string_chars (tmp);
	g_free (tmp);

	return res;
}


static GBValue *
gbrun_func_strcomp (GBRunEvalContext *ec,
		  GBRunObject      *object,
		  GBValue         **args)
{
	int cmp = 0, val;
	
	/* FIXME: return NULL if either string is NULL */
	GB_IS_VALUE (ec, args[0], GB_VALUE_STRING);
	GB_IS_VALUE (ec, args[1], GB_VALUE_STRING);

	if (args[2] != NULL)
		cmp = gb_value_get_as_int(args[2]);
	else
		; /* FIXME: use Option Compare setting */

	switch (cmp) {
	case 0: /* binary */
		val = strcmp(args[0]->v.s->str, args[1]->v.s->str);
		break;
	case 2: /* Access */
		g_warning("gbrun_func_strcomp: FIXME: Access comparison\n");
	case 1: /* textual */
		val = strcasecmp(args[0]->v.s->str, args[1]->v.s->str);
		break;
	default:
		g_warning("gbrun_func_strcomp: unknown comparison %d\n", cmp);
		return NULL;
	}

	if (val > 0)
		val = +1;
	else if (val < 0)
		val = -1;
	else
		val = 0;
	return gb_value_new_int(val);
}


static GBValue * gbrun_func_ucase (GBRunEvalContext *, GBRunObject *, GBValue **);				   
				  
static GBValue *
gbrun_func_strconv (GBRunEvalContext *ec,
		  GBRunObject      *object,
		  GBValue         **args)
{
	gboolean w = TRUE;
	char *p;
	int cnv;

	GB_IS_VALUE(ec, args[0], GB_VALUE_STRING);
	cnv = gb_value_get_as_int(args[1]);

	switch (cnv) {
	case 1: /* upper */
		return gbrun_func_ucase(ec, object, args);
	case 2: /* lower */
		return gbrun_func_lcase(ec, object, args);
	case 3: /* proper */
		for (p = args[0]->v.s->str; *p; p++) {
			*p = w ? toupper(*p) : tolower(*p);
			w = isalpha(*p) ? FALSE : TRUE;
		}
		return gb_value_new_string(args[0]->v.s);
	default:
		g_warning("gbrun_func_strconv: unsupported conversion %d\n",cnv);
	}
	return NULL;
}


static GBValue *
gbrun_func_strreverse (GBRunEvalContext *ec,
		     GBRunObject      *object,
		     GBValue         **args)
{
	GString *str;

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	str = args [0]->v.s;
	
	g_strreverse (str->str);
	return gb_value_new_string (str);
}


static GBValue *
gbrun_func_trim (GBRunEvalContext *ec,
	       GBRunObject      *object,
	       GBValue         **args)
{
	GBValue *res;
	GString *str;

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	str = args [0]->v.s;

	str->str = g_strstrip (str->str);
	res = gb_value_new_string (str);

	return res;
}


static GBValue *
gbrun_func_ucase (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	GString *str;
	
	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	str = args [0]->v.s;

	g_strup (str->str);
	return gb_value_new_string (str);
}


static GBValue *
gbrun_func_val (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      GBValue         **args)
{
	return gb_value_new_double(atof(gb_value_get_as_string(args[0])->str));
}


void
gba_string_register (GBEvalContext *ec)
{
	GBRunObject      *gba_object;
	GBRunObjectClass *gba;

	gba_object = gtk_type_new (
		gbrun_object_subclass_simple (GBRUN_TYPE_OBJECT, ITEM_NAME));
	gbrun_global_add (GB_OBJECT (gba_object), "string");

	gba = GBRUN_OBJECT_GET_CLASS (gba_object);

	gbrun_object_add_method_arg (gba, "func;asc;char,string;integer;n",
				     gbrun_func_asc);

	gbrun_object_add_method_arg (gba, "func;chr;a,integer;string;n",
				     gbrun_func_chr);

	gbrun_object_add_method_arg (gba, "func;chr$;a,integer;string;n",
				     gbrun_func_chr);

	gbrun_object_add_method_arg (gba, "func;chrb;a,integer;string;n",
				     gbrun_func_chrb);

	gbrun_object_add_method_arg (gba, "func;chrb$;a,integer;string;n",
				     gbrun_func_chrb);

	gbrun_object_add_method_arg (gba, "func;chrw;a,integer;string;n",
				     gbrun_func_chr); /* FIXME: unicode */

	/* FIXME: format(expression, format, firstdayofweek, firstweekofyear)*/

	gbrun_object_add_method_arg (gba, "func;instr;start,integer,byval,1;"
				     "haystack,string;needle,string;"
				     "compare,integer,byval,0;"
				     "integer;n", gbrun_func_instr);

/* FIXME: these should be like InStr(1, haystack, needle, 0)
	gbrun_object_add_method_arg (gba, "func;instr;haystack,string;"
				     "needle,string;integer;n",
				     gbrun_func_instr);

	gbrun_object_add_method_arg (gba, "func;instrb;haystack,string;"
				     "needle,string;integer;n",
				     gbrun_func_instrb);
*/

	gbrun_object_add_method_arg (gba, "func;instrb;start,integer,byval,1;"
				     "haystack,string;needle,string;"
				     "compare,integer,byval,0;"
				     "integer;n", gbrun_func_instrb);

	gbrun_object_add_method_arg (gba, "func;instrrev;haystack,string;"
                                     "needle,string;start,integer,byval,-1;"
                                     "compare,integer,byval,0;integer;n",
                                     gbrun_func_instrrev);

	gbrun_object_add_method_arg (gba, "func;join;list,variant;"
				     "delimeter,string,byval, ;"
                                     "string;n",
                                     gbrun_func_join);

	gbrun_object_add_method_arg (gba, "func;left;a,string;b,integer;"
				     "string;n", gbrun_func_left);

	gbrun_object_add_method_arg (gba, "func;left$;a,string;b,integer;"
				     "string;n", gbrun_func_left);

	gbrun_object_add_method_arg (gba, "func;leftb;a,string;b,integer;"
				     "string;n", gbrun_func_leftb);

	gbrun_object_add_method_arg (gba, "func;leftb$;a,string;b,integer;"
				     "string;n", gbrun_func_leftb);

	gbrun_object_add_method_arg (gba, "func;lcase;str,string;string;n",
				     gbrun_func_lcase);

	gbrun_object_add_method_arg (gba, "func;lcase$;str,string;string;n",
				     gbrun_func_lcase);

	gbrun_object_add_method_arg (gba, "func;len;a,string;integer;n",
				     gbrun_func_len);

	gbrun_object_add_method_arg (gba, "func;lenb;a,string;integer;n",
				     gbrun_func_lenb);

	gbrun_object_add_method_arg (gba, "func;ltrim;str,string;string;n",
				     gbrun_func_ltrim);

	gbrun_object_add_method_arg (gba, "func;ltrim$;str,string;string;n",
				     gbrun_func_ltrim);

	gbrun_object_add_method_arg (gba, "func;mid;a,string;b,integer;"
				     "c,integer,byval,32767;string;n", gbrun_func_mid);

	gbrun_object_add_method_arg (gba, "func;mid$;a,string;b,integer;"
				     "c,integer,byval,32767;string;n", gbrun_func_mid);

	gbrun_object_add_method_arg (gba, "func;midb;a,string;b,integer;"
				     "c,integer,byval,32767;string;n", gbrun_func_midb);

	gbrun_object_add_method_arg (gba, "func;midb$;a,string;b,integer;"
				     "c,integer,byval,32767;string;n", gbrun_func_midb);

	gbrun_object_add_method_arg (gba, "func;right;a,string;b,integer;"
				     "string;n", gbrun_func_right);

	gbrun_object_add_method_arg (gba, "func;right$;a,string;b,integer;"
				     "string;n", gbrun_func_right);

	gbrun_object_add_method_arg (gba, "func;rightb;a,string;b,integer;"
				     "string;n", gbrun_func_rightb);

	gbrun_object_add_method_arg (gba, "func;rightb$;a,string;b,integer;"
				     "string;n", gbrun_func_rightb);

	gbrun_object_add_method_arg (gba, "func;rtrim;str,string;string;n",
				     gbrun_func_rtrim);

	gbrun_object_add_method_arg (gba, "func;rtrim$;str,string;string;n",
				     gbrun_func_rtrim);

	gbrun_object_add_method_arg (gba, "func;str;a,double;string;n",
				     gbrun_func_str);

	gbrun_object_add_method_arg (gba, "func;str$;a,double;string;n",
				     gbrun_func_str);

	gbrun_object_add_method_arg (gba, "func;strcomp;a,string;b,string;"
				     "compare,integer,byval,0;integer;n",
				     gbrun_func_strcomp);

	gbrun_object_add_method_arg (gba, "func;strconv;a,string;conv,integer;"
				     "string;n", gbrun_func_strconv);

	gbrun_object_add_method_arg (gba, "func;space;num,integer;string;n",
				     gbrun_func_space);

	gbrun_object_add_method_arg (gba, "func;space$;num,integer;string;n",
				     gbrun_func_space);
	
	gbrun_object_add_method_arg (gba, "func;split;expr,string;"
				     "delimeter,string,byval, ;count,integer,byval,-1;"
				     "compare,integer,byval,-1;variant;n",
				     gbrun_func_split);

	gbrun_object_add_method_arg (gba, "func;string;num,integer;"
				     "char,string;string;n", gbrun_func_string);

	gbrun_object_add_method_arg (gba, "func;strreverse;a,string;string;n",
				     gbrun_func_strreverse);

	gbrun_object_add_method_arg (gba, "func;trim;str,string;string;n",
				     gbrun_func_trim);

	gbrun_object_add_method_arg (gba, "func;trim$;str,string;string;n",
				     gbrun_func_trim);

	gbrun_object_add_method_arg (gba, "func;ucase;str,string;string;n",
				     gbrun_func_ucase);

	gbrun_object_add_method_arg (gba, "func;ucase$;str,string;string;n",
				     gbrun_func_ucase);

	gbrun_object_add_method_arg (gba, "func;val;a,string;double;n",
				     gbrun_func_val);

}

void
gba_string_shutdown (void)
{

}
