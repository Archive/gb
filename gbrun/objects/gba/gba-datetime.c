/*
 * GNOME Basic Enterpreter Function registration
 *
 * Authors:
 *    Sean Atkinson (sca20@cam.ac.uk)
 *    Ariel Rios    (ariel@arcavia.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <sys/time.h>
#include <unistd.h>
#include <time.h>
#include <ctype.h>
#include <math.h>

#include <gbrun/gbrun.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-array.h>
#include <gbrun/gbrun-object.h>
#include <gbrun/gbrun-global.h>

#include <gb/gb-constants.h>

#include "gba-datetime.h"

#define ITEM_NAME "VBA.DateTime"

static GBValue *
gbrun_func_date (GBRunEvalContext *ec,
	       GBRunObject      *object,
	       GBValue         **args) 
{	
	GBValue   *res;
	GDate     *d;
	struct tm *tmp;
	time_t     t = time (NULL);

	tmp = localtime (&t);
	d   = g_date_new_dmy (tmp->tm_mday, tmp->tm_mon, tmp->tm_year + 1900);
	res = gb_value_new_date_gdate (d);

	g_date_free (d);

	return res;
}

static GBValue *
gbrun_func_dateadd (GBRunEvalContext *ec,
		    GBRunObject      *object,
		    GBValue         **args)
{
	GString *str;
	int      num;
	GDate   *d;

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	str = args [0]->v.s;
	num = gb_value_get_as_int (args [1]);
	d = gb_value_get_as_gdate (args [2]);

	if (str->len == 4 && !g_strncasecmp ("yyyy", str->str, 4)) { /* year */
		if (num >= 0)
			g_date_add_years (d, num);
		else
			g_date_subtract_years (d, -num);

	} else if (str->len == 2 && !g_strncasecmp ("ww", str->str, 2)) { /* week */
		if (num >= 0)
			g_date_add_days (d, 7 * num);
		else
			g_date_subtract_days (d, -7 * num);

	} else if (str->len == 1) {
		int secs = 0;

		switch (tolower ((guchar) *str->str)) {
		case 'q': /* quarter */
			num *= 3;
		case 'm': /* month */
			if (num >= 0)
				g_date_add_months (d, num);
			else
				g_date_subtract_months (d, num);
			break;
		case 'y': /* day of year */
		case 'd': /* day */
		case 'w': /* weekday */
			if (num >= 0)
				g_date_add_days (d, num);
			else
				g_date_subtract_days (d, num);
			break;
		case 'h': /* hour */
			secs = 60 * 60 * num;
			break;
		case 'n': /* minute */
			secs = 60 * num;
			break;
		case 's': /* second */
			secs = num;
			break;
		default:
			gbrun_exception_fire (ec, _("datepart: invalid interval"));
		}
		if (secs) {
			GBValue *res;
			double i, f = modf (args [1]->v.date, &i);

			f += 1 + secs / ((double) 24 * 60 * 60);
			if (f >= 1.0) {
				g_date_add_days (d, 1);
				f -= 1.0;
			} else if (f <= -1.0) {
				g_date_subtract_days (d, 1);
				f += 1.0;
			}
			res = gb_value_new_date_gdate (d);
			res->v.date += f;
			return res;
		}
	} else {
		gbrun_exception_fire (ec, _("datepart: invalid interval"));
	}
	return gb_value_new_date_gdate (d);
}

static GBValue *gbrun_func_datepart (GBRunEvalContext *, GBRunObject *, GBValue **);

static GBValue *
gbrun_func_datediff (GBRunEvalContext *ec,
		     GBRunObject      *object,
		     GBValue         **args)
{
	GBValue *date, *ans;
	GBValue *pargs [4] = { args [0], NULL, args [3], args [4]};

	GB_IS_VALUE (ec, args [1], GB_VALUE_DATE);
	GB_IS_VALUE (ec, args [2], GB_VALUE_DATE);

	date = gb_value_new_date (args [2]->v.date - args [1]->v.date);
	pargs [1] = date;
	ans = gbrun_func_datepart (ec, object, pargs);
	gb_value_destroy (date);

	return ans;
}

static GBValue *
gbrun_func_datepart (GBRunEvalContext *ec,
		     GBRunObject      *object,
		     GBValue         **args)
{
	GDate *d;
	int ans = 0, firstdayofweek = 1;
	GString *str;

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);
	str = args [0]->v.s;
	d = gb_value_get_as_gdate (args [1]);
	firstdayofweek = gb_value_get_as_int (args [2]);
	/* FIXME: use fistdayofuear argument in args [3] */

	if (str->len == 4 && !g_strncasecmp ("yyyy", str->str, 4)) /* year */
		ans = g_date_year (d);

	else if (str->len == 2 && !g_strncasecmp("ww", str->str, 2)) /* week */
		switch (firstdayofweek) {
		case 0: /* default (what's an NLS API?) */
		case 1: /* Sunday */
			ans = g_date_sunday_week_of_year (d);
			break;
		case 2: /* Monday */
			ans = g_date_monday_week_of_year (d);
			break;
		case 3: case 4: case 5: case 6: case 7:
			g_warning ("datepart FIXME: support Tuesday-Saturday");
		default:
			gbrun_exception_fire(ec,_("bad first day of week"));
		}
	else if (str->len == 1)
		switch (tolower ((guchar) *str->str)) {
		case 'q': /* quarter */
			ans = 1 + g_date_month (d) / 3;
			break;
		case 'm': /* month */
			ans = g_date_month (d);
			break;
		case 'y': /* day of year */
			ans = g_date_day_of_year (d);
			break;
		case 'd': /* day */
			ans = g_date_day (d);
			break;
		case 'w': /* weekday (FIXME: use firstdayofweek) */
			ans = g_date_weekday (d);
			break;
		case 'h': /* hour */
			ans = gb_value_time_get_hr(args [1]);
			break;
		case 'n': /* minute */
			ans = gb_value_time_get_min(args [1]);
			break;
		case 's': /* second */
			ans = gb_value_time_get_sec(args [1]);
			break;
		default:
			gbrun_exception_fire (ec, _("invalid interval character"));
		}
	else
		gbrun_exception_fire (ec, _("invalid interval string"));

	return gb_value_new_int (ans);
}

static GBValue *
gbrun_func_dateserial (GBRunEvalContext *ec,
		     GBRunObject      *object,
		     GBValue         **args)
{
	GDate   *date; 
	GBValue *res;
	int      year  = gb_value_get_as_int (args [0]);
	int      month = gb_value_get_as_int (args [1]);
	int      day   = gb_value_get_as_int (args [2]);
        
	if (g_date_valid_dmy (day, month, year)) {
		date = g_date_new_dmy (day, month, year);
		res = gb_value_new_date_gdate (date);
		g_date_free (date);
	} else
		return gbrun_exception_fire (ec, _("invalid date"));

	return res;
}

static GBValue *
gbrun_func_day (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      GBValue         **args)
{
	GBValue *res;
	GDate   *date = gb_value_get_as_gdate (args [0]);

        res = gb_value_new_int (g_date_day (date));
	g_date_free (date);

	return res;
}

static GBValue *
gbrun_func_hour (GBRunEvalContext *ec,
	       GBRunObject      *object,
	       GBValue         **args) 
{
	GBValue *res;
	int      hr  = gb_value_time_get_hr (args [0]);

	res = gb_value_new_int (hr);
	
	return res;
}	

static GBValue *
gbrun_func_minute (GBRunEvalContext *ec,
		 GBRunObject      *object,
		 GBValue         **args) 
{
	GBValue *res;
	int      min = gb_value_time_get_min (args [0]);

	res = gb_value_new_int (min);
	
	return res;
}

static GBValue *
gbrun_func_month (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	GBValue *res;	
	GDate   *date = gb_value_get_as_gdate (args [0]);
	
	res = gb_value_new_int (g_date_month (date));
	g_date_free (date);

	return res;
}

static GBValue *
gbrun_func_monthname (GBRunEvalContext *ec,
		      GBRunObject      *object,
		      GBValue         **args)
{
	int arg;
	static const char *month_names[24] = 
          { N_("January"), N_("February"), N_("March"), N_("April"),
	    N_("May"), N_("June"), N_("July"), N_("August"),
	    N_("September"), N_("October"), N_("November"),
	    N_("December"),
	    N_("Jan"), N_("Feb"), N_("Mar"), N_("Apr"), N_("May"),
	    N_("Jun"), N_("Jul"), N_("Aug"), N_("Sep"), N_("Oct"),
	    N_("Nov"), N_("Dec") };

	if ((arg = args[0]->v.i) < 1 || arg > 12) {
		gbrun_exception_firev (ec, _("Monthname: month out of range"));
		return NULL;
	}
	if (!args[1]->v.bool)
		return gb_value_new_string_chars (_(month_names[arg - 1]));
	else
		return gb_value_new_string_chars (_(month_names[arg + 11]));
}

static GBValue *
gbrun_func_now (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      GBValue         **args) 
{
	GBValue *res;
	GDate   *d;
	struct tm *tmp;
	time_t     t = time (NULL);

	tmp = localtime (&t);
	d   = g_date_new_dmy (tmp->tm_mday, (tmp->tm_mon) + 1, tmp->tm_year + 1900);
	res = gb_value_new_date_time (d, tmp->tm_hour, tmp->tm_min,
				      tmp->tm_sec);

	g_date_free (d);

	return res;
}

static GBValue *
gbrun_func_second (GBRunEvalContext *ec,
		 GBRunObject      *object,
		 GBValue        **args) 
{
	GBValue *res;
	int      sec  = gb_value_time_get_sec (args [0]);

	res = gb_value_new_int (sec);
	
	return res;
}

static GBValue *
gbrun_func_time (GBRunEvalContext *ec,
	       GBRunObject      *object,
	       GBValue         **args)
{
	GBValue *res;
	time_t t = time (NULL);
	struct tm *tmp;

	tmp = localtime (&t);

	res = gb_value_new_time (tmp->tm_hour, tmp->tm_min, tmp->tm_sec);

	return res;
}

static GBValue *
gbrun_func_timer (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	struct timeval tv;
	struct tm *tmp;

	gettimeofday(&tv, NULL);
	tmp = localtime(&tv.tv_sec);
	return gb_value_new_single((tmp->tm_hour * 60 + tmp->tm_min) * 60 +
				   tmp->tm_sec + tv.tv_usec / 1E6);
}

static GBValue *
gbrun_func_timeserial (GBRunEvalContext *ec,
		     GBRunObject      *object,
		     GBValue        **args) 
{
	GBValue *res;
	int      hr  = gb_value_get_as_int (args [0]);
	int      min = gb_value_get_as_int (args [1]);
	int      sec = gb_value_get_as_int (args [2]);

	res = gb_value_new_time (hr, min, sec);

	return res;
}

static GBValue *
gbrun_func_weekday (GBRunEvalContext *ec,
		  GBRunObject      *object,
		  GBValue         **args)
{
	GBValue *res;
	GDate *date = gb_value_get_as_gdate (args [0]);
        
	res = gb_value_new_int (g_date_weekday (date)); 
	g_date_free (date);
	
	return res; 
}

static GBValue *
gbrun_func_year (GBRunEvalContext *ec,
               GBRunObject      *object,
               GBValue         **args)
{
	GBValue *res;	
	GDate *date = gb_value_get_as_gdate (args [0]);
	
	res = gb_value_new_int (g_date_year (date));
	g_date_free (date);
	
	return res; 
}

void
gba_datetime_register (GBEvalContext *ec)
{
	GBRunObject      *gba_object;
	GBRunObjectClass *gba;

	gba_object = gtk_type_new (
		gbrun_object_subclass_simple (GBRUN_TYPE_OBJECT, ITEM_NAME));
	gbrun_global_add (GB_OBJECT (gba_object), "datetime");

	gba = GBRUN_OBJECT_GET_CLASS (gba_object);
	
	gbrun_object_add_method_arg (gba, "func;date;.;date;n",
				     gbrun_func_date);

	gbrun_object_add_method_arg (gba, "func;dateadd;interval,string;"
				     "number,integer;date,date;date;n",
				     gbrun_func_dateadd);

	gbrun_object_add_method_arg (gba, "func;datediff;interval,string;"
				     "date1,date;date2,date;"
				     "firstdayofweek,integer,byval,1;"
				     "firstweekofyear,integer,byval,1;date;n",
				     gbrun_func_datediff);

	gbrun_object_add_method_arg (gba, "func;datepart;interval,string;"
				     "date,date;"
				     "firstdayofweek,integer,byval,1;"
				     "firstweekofyear,integer,byval,1;"
				     "datepart;n", gbrun_func_datepart);

 	gbrun_object_add_method_arg (gba, "func;dateserial;a,integer;"
				     "b,integer;c,integer;date;n",
				     gbrun_func_dateserial);
/*
	gbrun_object_add_method_arg (gba, "func;datevalue;date,date;date;n",
				     gbrun_func_datevalue);
*/
	gbrun_object_add_method_arg (gba, "func;day;a,date;integer;n",
				     gbrun_func_day);

	gbrun_object_add_method_arg (gba, "func;hour;a,date;integer;n",
				     gbrun_func_hour);
	
	gbrun_object_add_method_arg (gba, "func;minute;a,date;integer;n",
				     gbrun_func_minute);

	gbrun_object_add_method_arg (gba, "func;month;a,date;integer;n",
				     gbrun_func_month);

	gbrun_object_add_method_arg (gba, "func;monthname;a,integer;"
                                          "abbr,boolean,byval,False;"
				          "string;n",
				     gbrun_func_monthname);
	
	gbrun_object_add_method_arg (gba, "func;now;.;date;n",
				     gbrun_func_now);

	gbrun_object_add_method_arg (gba, "func;second;a,date;integer;n",
				     gbrun_func_second);

	gbrun_object_add_method_arg (gba, "func;time;.;date;n",
				     gbrun_func_time);

	gbrun_object_add_method_arg (gba, "func;timer;.;single;n",
				     gbrun_func_timer);

	gbrun_object_add_method_arg (gba, "func;timeserial;a,integer;"
				     "b,integer;c,integer;date;n",
				     gbrun_func_timeserial);
/*
	gbrun_object_add_method_arg (gba, "func;timevalue;time,date;date;n",
				     gbrun_func_timevalue);
*/
	gbrun_object_add_method_arg (gba, "func;weekday;a,date;integer;n",
				     gbrun_func_weekday);
	
	gbrun_object_add_method_arg (gba, "func;year;a,date;integer;n",
				     gbrun_func_year);

}

void gba_datetime_shutdown (void)
{

}
