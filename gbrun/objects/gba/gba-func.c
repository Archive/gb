/*
 * GNOME Basic Enterpreter Function registration
 *
 * Authors:
 *    Michael Meeks (mmeeks@gnu.org)
 *    Thomas Meeks  (meekte95@christs-hospital.org.uk)
 *    Ariel Rios    (ariel@arcavia.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include <config.h>

#include <gbrun/libgbrun.h>
#include <gbrun/gbrun.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-array.h>
#include <gbrun/gbrun-object.h>
#include <gbrun/gbrun-file.h>
#include <gbrun/gbrun-global.h>

#include <gb/gb-constants.h>

#include "gba-func.h"

static GBValue *
gbrun_func_vartype (GBRunEvalContext *ec,
		    GBRunObject      *object,
		    GBValue         **args)
{
	if (!args [0])
		return gbrun_exception_firev (ec, _("No argument"));

	return gb_value_new_int (gb_value_from_gtk_type (args [0]->gtk_type));
}


static GBValue *
gbrun_func_filelen (GBRunEvalContext *ec,
		    GBRunObject      *object,
		    GBValue         **args)
{
	char       *name;
	struct stat buf;
	
	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);

	name = args [0]->v.s->str;

	if (stat (name, &buf))
		return gbrun_exception_firev (ec, _("FileLen error on %s"), name);
	
	return gb_value_new_long (buf.st_size);
}

static GBValue *
gbrun_func_fileattr (GBRunEvalContext *ec,
		    GBRunObject      *object,
		    GBValue         **args)
{
	long res;
	GBRunFileHandle *h;

	GB_IS_VALUE (ec, args [0], GB_VALUE_INT);
	GB_IS_VALUE (ec, args [1], GB_VALUE_INT);

	if (args [1]->v.i != 1)
		return gbrun_exception_firev (ec, _("The only valid value for arg 2 on non 16 bit systems is 1 -")); 
	
	h = internal_handle_from_gb_no (ec, args [0]->v.i);
	
	if (!h) 
		return gbrun_exception_fire (ec, _("Bad file handle"));
       
	if (h->mode == GB_OPEN_INPUT)
		res = 1;
	else if (h->mode == GB_OPEN_OUTPUT)
		res = 2;
	else if (h->mode == GB_OPEN_RANDOM)
		res = 4;
	else if (h->mode == GB_OPEN_APPEND)
		res = 8;		
	else if (h->mode == GB_OPEN_BINARY)
		res = 32;

	g_free (h);

	return gb_value_new_long (res);
}

static GBValue *
gbrun_func_cint (GBRunEvalContext *ec,
		 GBRunObject      *object,
		 GBValue         **args)
{
	GBValue *ret;

	ret = gb_value_promote (GB_EVAL_CONTEXT (ec), gb_gtk_type_from_value (GB_VALUE_INT), args [0]);

	return ret;

}

static GBValue *
gbrun_sub_print (GBRunEvalContext *ec,
		 GBRunObject      *object,
		 GSList           *expr)
{
	for (; expr; expr = expr->next) {
		GBValue *v;

		v = gbrun_eval_as (ec, expr->data, GB_VALUE_STRING);
		if (!v)
			return NULL;

		printf ("%s", v->v.s->str);

		gb_value_destroy (v);
	}
	printf ("\n");

	return gb_value_new_empty ();
}

static GBValue *
gbrun_sub_array (GBRunEvalContext *ec,
		 GBRunObject      *object,
		 GSList           *expr)
{
	GBObject *a;
	GSList   *vals = NULL;

	for (; expr; expr = expr->next) {
		GBValue *v;

		v = gb_eval_context_eval (GB_EVAL_CONTEXT (ec), expr->data);
		if (!v)
			return NULL;

		vals = g_slist_append (vals, v);
	}

	a = gbrun_array_new_vals (ec, vals);

	while (vals) {
		gb_value_destroy (vals->data);
		vals = g_slist_remove (vals, vals->data);
	}
	
	return gb_value_new_object (a);
}


static GBValue *
gbrun_func_isnumeric (GBRunEvalContext *ec,
		      GBRunObject      *object,
		      GBValue        **args)
{
	if (!args [0])
		return gb_value_new_boolean (FALSE);

	switch (gb_value_from_gtk_type (args [0]->gtk_type)) {

	case GB_VALUE_CURRENCY:
	case GB_VALUE_NUMBER:
		return gb_value_new_boolean (TRUE);

	default:
		return gb_value_new_boolean (FALSE);
	}
}

static GBValue *
gbrun_func_isnull (GBRunEvalContext *ec,
		   GBRunObject      *object,
		   GBValue        **args)
{
	if (!args [0] ||
	    args [0]->gtk_type != gb_gtk_type_from_value (GB_VALUE_NULL))
		return gb_value_new_boolean (FALSE);

	return gb_value_new_boolean (TRUE);
}

static GBValue *
gbrun_func_lbound (GBRunEvalContext *ec,
		   GBRunObject      *object,
		   GBValue        **args)
{
	GB_IS_VALUE (ec, args [1], GB_VALUE_LONG);

	if (!args [0] ||
	    !gtk_type_is_a (args [0]->gtk_type, GBRUN_TYPE_ARRAY))
		return gbrun_exception_firev (ec, _("LBound takes an array argument"));

	return gbrun_array_lbound (
		ec, GBRUN_ARRAY (args [0]->v.obj),
		gb_value_get_as_long (args [1]));
}

static GBValue *
gbrun_func_ubound (GBRunEvalContext *ec,
		   GBRunObject      *object,
		   GBValue        **args)
{
	GB_IS_VALUE (ec, args [1], GB_VALUE_LONG);

	if (!args [0] ||
	    !gtk_type_is_a (args [0]->gtk_type, GBRUN_TYPE_ARRAY))
		return gbrun_exception_firev (ec, _("UBound takes an array argument"));

	return gbrun_array_ubound (
		ec, GBRUN_ARRAY (args [0]->v.obj),
		gb_value_get_as_long (args [1]));
}

static GBValue *
gbrun_func_isobject (GBRunEvalContext *ec,
		     GBRunObject      *object,
		     GBValue        **args)
{
	if (!args [0] ||
	    args [0]->gtk_type != gb_gtk_type_from_value (GB_VALUE_OBJECT))
		return gb_value_new_boolean (FALSE);
	
	return gb_value_new_boolean (TRUE);
}

static GBValue *
gbrun_sub_call_by_name (GBRunEvalContext *ec,
			GBRunObject      *object,
			GBValue        **args)
{
	GBObjRef ref;

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);

	ref.method = TRUE;
	ref.name   = args [0]->v.s->str;
	ref.parms  = NULL;

	return gbrun_objref_deref (ec, GB_OBJECT (object), &ref, TRUE);
}

#ifdef HAVE_ENVIRON
extern char **environ;
#endif

static GBValue *
gbrun_func_environ (GBRunEvalContext *ec,
		    GBRunObject      *object,
		    GBValue        **args)
{
	long l;
	char *name, *end;
	const char *str;

	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);

	name = args [0]->v.s->str;

        l = strtol (name, &end, 10);
	if (*name != '\0' && *end == '\0') { /* A number */
#ifdef HAVE_ENVIRON
		int    i;
		char **s;

		for (i = 0, s = environ; s && *s && i < l; i++)
			s++;
		
		str = *s;
#else
		gbrun_exception_firev (
			ec, "Cannot dump environ by index portably yet");
		return NULL;
#endif
	} else /* name is valid already */
		str = getenv (name);

	if (!str)
		return gb_value_new_string_chars ("");
	else
		return gb_value_new_string_chars (str);
}

static GBValue *
gbrun_func_eval (GBRunEvalContext *ec,
		 GBRunObject      *object,
		 GBValue         **args)
{
	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);

	return gbrun_eval_str (ec, object, args [0]->v.s->str);
}

static GBValue *
gbrun_sub_execute (GBRunEvalContext *ec,
		   GBRunObject      *object,
		   GBValue         **args)
{
	GB_IS_VALUE (ec, args [0], GB_VALUE_STRING);

	gbrun_exec_str (ec, object, args [0]->v.s->str);

	if (gbrun_eval_context_exception (ec))
		return NULL;

	return gb_value_new_empty ();
}

void
gba_func_register (GBEvalContext *ec)
{
	GBRunObject      *gba_object;
	GBRunObjectClass *gba;

	gba_object = gtk_type_new (
		gbrun_object_subclass_simple (GBRUN_TYPE_OBJECT, "vba"));
	gbrun_global_add (GB_OBJECT (gba_object), "vba");

	gba = GBRUN_OBJECT_GET_CLASS (gba_object);

	gbrun_object_add_method_arg (gba, "func;vartype;a,variant;integer;n",
				     gbrun_func_vartype);
        
	gbrun_object_add_method_arg (gba, "func;filelen;pathname,string;long;i",
				     gbrun_func_filelen);

	gbrun_object_add_method_arg (gba, "func;fileattr;filenumber,integer;"
				     "returntype,integer,byval,1;"
				     "long;i",gbrun_func_fileattr);

	gbrun_object_add_method_arg (gba, "func;eof;filenumber,integer;boolean;i",
				     gbrun_func_eof);

	gbrun_object_add_method_arg (gba, "func;seek;filenumber,integer;long;i",
				     gbrun_func_seek);

	gbrun_object_add_method_arg (gba, "func;loc;filenumber,integer;long;i",
				     gbrun_func_loc);

	gbrun_object_add_method_arg (gba, "func;lof;filenumber,integer;long;i",
				     gbrun_func_lof);

	gbrun_object_add_method_arg (gba, "func;freefile;rangenumber,integer,byval,NULL;integer;i",
				     gbrun_func_freefile);

        gbrun_object_add_method_arg (gba, "func;isnumeric;expression,variant;boolean;n",
				     gbrun_func_isnumeric);

	gbrun_object_add_method_arg (gba, "func;isnull;expression,variant;boolean;n",
				     gbrun_func_isnull);

	gbrun_object_add_method_arg (gba, "func;isobject;expression,variant;boolean;n",
				     gbrun_func_isobject);

        gbrun_object_add_method_var (gba, "sub;array;...;n",
				     gbrun_sub_array);

        gbrun_object_add_method_arg (gba, "sub;callbyname;name,string;n",
				     gbrun_sub_call_by_name);

	gbrun_object_add_method_arg (gba, "func;lbound;arrayname,variant;dimension,long,byval,1;long;n",
				     gbrun_func_lbound);

	gbrun_object_add_method_arg (gba, "func;ubound;arrayname,variant;dimension,long,byval,1;long;n",
				     gbrun_func_ubound);

	gbrun_object_add_method_arg (gba, "func;cint;expression,variant;integer;n",
				     gbrun_func_cint);

	/* FIXME: is this a security thing ? */
	gbrun_object_add_method_arg (gba, "func;environ;var,string;string;n",
				     gbrun_func_environ);
	gbrun_object_add_method_arg (gba, "func;environ$;var,string;string;n",
				     gbrun_func_environ);

	/* Possibly only valid for vbscript etc. */
        gbrun_object_add_method_arg (gba, "func;eval;basic,string;variant;n",
				     gbrun_func_eval);

        gbrun_object_add_method_arg (gba, "sub;execute;name,string;n",
				     gbrun_sub_execute);

	/* Debug. */
	gba_object = gtk_type_new (
		gbrun_object_subclass_simple (GBRUN_TYPE_OBJECT, "Debug"));
	gbrun_global_add (GB_OBJECT (gba_object), "debug");

	gba = GBRUN_OBJECT_GET_CLASS (gba_object);

	gbrun_object_add_method_var (gba, "sub;print;...;n", gbrun_sub_print);
}

void
gba_func_shutdown (void)
{
}
