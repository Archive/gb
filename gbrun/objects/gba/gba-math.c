
/*
 * GNOME Basic Enterpreter Function registration
 *
 * Authors:
 *    Michael Meeks (mmeeks@gnu.org)
 *    Thomas Meeks  (meekte95@christs-hospital.org.uk)
 *    Ariel Rios    (ariel@arcavia.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <math.h>
#include <stdlib.h>

#include <gbrun/gbrun.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-array.h>
#include <gbrun/gbrun-object.h>
#include <gbrun/gbrun-global.h>

#include <gb/gb-constants.h>

#include "gba-math.h"

#define ITEM_NAME "VBA.Math"

static GBValue *
gbrun_func_abs (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      GBValue         **args)
{
	GB_IS_VALUE (ec, args [0], GB_VALUE_DOUBLE);
	
	return gb_value_new_double (fabs (args [0]->v.d));
}


static GBValue *
gbrun_func_atan (GBRunEvalContext *ec,
	       GBRunObject      *object,
	       GBValue          **args)
{
	GB_IS_VALUE (ec, args [0], GB_VALUE_DOUBLE);

	return gb_value_new_double (atan (args [0]->v.d));
}


static GBValue *
gbrun_func_cos (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      GBValue         **args)
{
	GB_IS_VALUE (ec, args [0], GB_VALUE_DOUBLE);

	return gb_value_new_double (cos (args [0]->v.d));
}


static GBValue *
gbrun_func_exp (GBRunEvalContext *ec,
	      GBRunObject      *object,
              GBValue         **args)
{
	GB_IS_VALUE (ec, args [0], GB_VALUE_DOUBLE);

	return gb_value_new_double (exp (args [0]->v.d));
}


static GBValue *
gbrun_func_fix (GBRunEvalContext *ec,
	      GBRunObject      *object,
              GBValue         **args)
{
	GB_IS_VALUE (ec, args [0], GB_VALUE_DOUBLE);

	return gb_value_new_int(args[0]->v.d);
}


static GBValue *
gbrun_func_int (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	double x;

	GB_IS_VALUE (ec, args [0], GB_VALUE_DOUBLE);

	x = args [0]->v.d;
	if (x < 0.0)
		x -= 1.0;
	return gb_value_new_int ((int) x);
}


static GBValue *
gbrun_func_log (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      GBValue         **args)
{
	GBDouble d = gb_value_get_as_double (args [0]);

	if (!d)
		return gbrun_exception_fire  (ec, _("Log error: infinite result"));
	else if (d <= 1)
		return gbrun_exception_fire  (ec, _("Log error: NAN"));
	else
		return gb_value_new_double (log (d));
}


static GBValue *
gbrun_func_rnd (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue        **args)
{
        gint value = -1;

        if (args [0]) {
		GB_IS_VALUE (ec, args [0], GB_VALUE_SINGLE);

                if (args [0]->v.f < 0.0) {
                        value = -(args [0]->v.f);
                        ec->random.reseed   = TRUE;
                        ec->random.randseed = value;
                }

                if (args [0]->v.f == 0.0)
                        value = ec->random.randseed;
        }

        if (value == -1) {
                srand (ec->random.randseed);
                value = rand();
                ec->random.randseed = value;
	}

	return gb_value_new_single (value / (RAND_MAX + 1.0));
}


static GBValue *
gbrun_func_sgn (GBRunEvalContext  *ec,
	      GBRunObject       *object,
	      GBValue          **args)
{
	int sgn = 0;

	GBDouble d = gb_value_get_as_double (args [0]);

	if (d < 0)
		sgn = -1;
	else if (d > 0)
		sgn = +1;
	return gb_value_new_int (sgn);
}


static GBValue *
gbrun_func_sin (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      GBValue         **args)
{
	GB_IS_VALUE (ec, args [0], GB_VALUE_DOUBLE);

	return gb_value_new_double (sin (args [0]->v.d));
}


static GBValue *
gbrun_func_sqr (GBRunEvalContext *ec,
	      GBRunObject      *object,
              GBValue         **args)
{
	return gb_value_new_double (sqrt (gb_value_get_as_double (args [0])));
}



static GBValue *
gbrun_func_tan (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      GBValue         **args)
{
	GB_IS_VALUE (ec, args [0], GB_VALUE_DOUBLE);

	return gb_value_new_double (tan (args [0]->v.d));
}


void
gba_math_register (GBEvalContext *ec)
{
	GBRunObject      *gba_object;
	GBRunObjectClass *gba;

	gba_object = gtk_type_new (
		gbrun_object_subclass_simple (GBRUN_TYPE_OBJECT, ITEM_NAME));
	gbrun_global_add (GB_OBJECT (gba_object), "math");

	gba = GBRUN_OBJECT_GET_CLASS (gba_object);

	gbrun_object_add_method_arg (gba, "func;abs;a,double;double;n",
				     gbrun_func_abs);

	gbrun_object_add_method_arg (gba, "func;atn;a,double;double;n",
				     gbrun_func_atan);

	gbrun_object_add_method_arg (gba, "func;cos;a,double;double;n",
				     gbrun_func_cos);

	gbrun_object_add_method_arg (gba, "func;exp;a,double;double;n",
				     gbrun_func_exp);

	gbrun_object_add_method_arg (gba, "func;fix;a,double;double;n",
				     gbrun_func_fix);

	gbrun_object_add_method_arg (gba, "func;int;a,double;double;n",
				     gbrun_func_int);

	gbrun_object_add_method_arg (gba, "func;log;a,double;double;n",
				     gbrun_func_log);

	gbrun_object_add_method_arg (gba, "func;rnd;seed,single,byval,NULL;"
				     "single;n", gbrun_func_rnd);
	
	gbrun_object_add_method_arg (gba, "func;sgn;a,integer;double;n",
				     gbrun_func_sgn);

	gbrun_object_add_method_arg (gba, "func;sin;a,double;double;n",
				     gbrun_func_sin);
	
	gbrun_object_add_method_arg (gba, "func;sqr;a,double;double;n",
				     gbrun_func_sqr);

	gbrun_object_add_method_arg (gba, "func;tan;a,double;double;n",
				     gbrun_func_tan);

}


void
gba_math_shutdown (void)
{

}
