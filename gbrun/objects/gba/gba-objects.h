
/*
 * GNOME Basic Function registration
 *
 * Author:
 *    Per Winkvist (nd96pwt@student.hig.se)
 *
 * Copyright 2000, Helix Code, Inc
 */

#ifndef GBA_OBJECTS_H
#define GBA_OBJECTS_H

void gbrun_gba_register (GBEvalContext *ec);
void gbrun_gba_shutdown (void);

#endif
