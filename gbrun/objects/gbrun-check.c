/*
 * gbrun-check.c
 *
 * Gnome Basic Interpreter Check functions.
 *
 * Author:
 *	Thomas Meeks (thomas@imaginator.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include "gbrun-form-item.h"

#define ITEM_NAME "gb-vb.checkbox"

enum {
	ARG_FIRST = 0,
	CAPTION,
	VALUE
};

#define GBRUN_CHECKBOX(obj) (GTK_CHECK_CAST ((obj), gbrun_checkbox_get_type (), GBRunCheckBox))

typedef struct {
	GBRunFormItem    item;

	GtkJustification alignment;
} GBRunCheckBox;

/**
 * checkbox_setarg:
 *   @ec
 *   @object
 *   @property
 *   @val
 **/
static gboolean
checkbox_setarg (GBRunEvalContext *ec,
		 GBRunObject      *object,
		 int               property,
		 GBValue          *val)
{
	GBRunCheckBox *checkbox = GBRUN_CHECKBOX (object);
	GtkWidget     *w = gbrun_form_item_get_widget (GBRUN_FORM_ITEM (object));

	g_return_val_if_fail (checkbox != NULL, FALSE);

	switch (property) {
	case CAPTION:
		gtk_label_set_text (GTK_LABEL (GTK_BIN (w)->child),
				    val->v.s->str);
		return TRUE;

	case VALUE:
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w),
					      val->v.i);
		return TRUE;

	default:
		g_warning ("check: Unhandled property '%d'", property);
		return FALSE;
	}
}


/**
 * checkbox_getarg:
 *   @ec
 *   @object
 *   @property
 **/
static GBValue *
checkbox_getarg (GBRunEvalContext *ec,
		  GBRunObject      *object,
		  int               property)
{
	GBRunCheckBox *checkbox = GBRUN_CHECKBOX (object);
	GtkWidget     *w = gbrun_form_item_get_widget (
		GBRUN_FORM_ITEM (object));

	g_return_val_if_fail (checkbox != NULL, NULL);

	switch (property) {
	case CAPTION:
		return gb_value_new_string_chars (
			GTK_LABEL (GTK_BIN (w)->child)->label);

	case VALUE:
		return gb_value_new_int (
			GTK_TOGGLE_BUTTON (w)->active);

	default:
		g_warning ("check: Unhandled property '%d'", property);
		break;
	}

	return NULL;
}

static void
gbrun_checkbox_destroy (GtkObject *object)
{
}

static void
gbrun_checkbox_construct (GBRunEvalContext *ec, GBRunFormItem *item)
{
	GtkWidget  *w;

	w = gtk_check_button_new_with_label ("");
	gtk_widget_show_all (w);
	gbrun_form_item_set_widget (item, w);
}

static void
gbrun_checkbox_class_init (GBRunObjectClass *klass)
{
	GtkObjectClass     *gtk_class    = (GtkObjectClass *) klass;
	GBRunFormItemClass *form_class   = (GBRunFormItemClass *) klass;

	klass->set_arg = checkbox_setarg;
	klass->get_arg = checkbox_getarg;

	gbrun_object_add_property (klass, "caption", 
				   gb_type_string, CAPTION);

	gbrun_object_add_property (klass, "value",
				   gb_type_int, VALUE);

	form_class->construct = gbrun_checkbox_construct;

	gtk_class->destroy    = gbrun_checkbox_destroy;
}

GtkType
gbrun_checkbox_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunCheckBox),
			sizeof (GBRunFormItemClass),
			(GtkClassInitFunc)  gbrun_checkbox_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_FORM_ITEM, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}

/**
 * gbrun_checkbox_register:
 *
 **/
void
gbrun_checkbox_register ()
{
	gbrun_checkbox_get_type ();
}


/**
 *  gbrun_checkbox_shutdown:
 **/
void
gbrun_checkbox_shutdown ()
{
}

