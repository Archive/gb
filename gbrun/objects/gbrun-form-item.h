
/*
 * GNOME Abstract Form item base class
 *
 * Authors:
 *   Michael Meeks (michael@helixcode.com)
 */

#ifndef GBRUN_FORM_ITEM_H
#define GBRUN_FORM_ITEM_H

/*
 * Alignment values
 */
#define VB_LEFT_JUSTIFY		0
#define VB_RIGHT_JUSTIFY	1
#define VB_CENTER_JUSTIFY	2

/*
 * Fill Style values
 */
#define VB_FILL_SOLID		0		/* Solid */
#define VB_FILL_TRANS		1		/* Transparent */
#define VB_FILL_HLINE		2		/* Horizontal Line */
#define VB_FILL_VLINE		3		/* Vertical Line */
#define VB_FILL_UP_DIAG		4		/* Upward Diagonal */
#define VB_FILL_DOWN_DIAG	5		/* Downward Diagonal */
#define VB_FILL_CROSS		6		/* Cross */
#define VB_FILL_DIAG_CROSS	7		/* Diagonal Cross */

/*
 * Shape values
 */
#define VB_SHAPE_RECTANGLE	0		/* Rectangle */
#define VB_SHAPE_SQUARE		1		/* Square */
#define VB_SHAPE_OVAL		2		/* Oval */
#define VB_SHAPE_CIRCLE		3		/* Circle */
#define VB_SHAPE_RRECT		4		/* Rounded Rectangle */
#define VB_SHAPE_RSQUARE	5		/* Rounded Square */

/*
 */
#define VB_TRUE		       -1
#define VB_FALSE		0

#include "gbrun-form.h"
#include "gbrun-objects.h"

#define GBRUN_TYPE_FORM_ITEM            (gbrun_form_item_get_type ())
#define GBRUN_FORM_ITEM(obj)            (GTK_CHECK_CAST ((obj), GBRUN_TYPE_FORM_ITEM, GBRunFormItem))
#define GBRUN_FORM_ITEM_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GBRUN_TYPE_FORM_ITEM, GBRunFormItemClass))
#define GBRUN_IS_FORM_ITEM(obj)         (GTK_CHECK_TYPE ((obj), GBRUN_TYPE_FORM_ITEM))
#define GBRUN_IS_FORM_ITEM_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GBRUN_TYPE_FORM_ITEM))
/* #define GBRUN_FORM_ITEM_GET_CLASS(obj)  (GBRUN_FORM_ITEM_CLASS (GTK_OBJECT (obj)->klass)) */

struct _GBRunFormItem {
	GBRunObject     object;

	GBRunObject    *parent;
	int             x, y;
	GtkWidget      *widget;

	char           *name;
	int             tabindex;
	char           *tag;
	int		index;

	int		scaleheight;
	int		scalewidth;
	int		scalemode;
	int             dragmode;

	GSList         *carray;
};

typedef struct {
	GBRunObjectClass klass;
	
	void (*construct) (GBRunEvalContext *ec,
			   GBRunFormItem    *item);

	void (*add) (GBRunEvalContext *ec,
		     GBRunFormItem *item,
		     GBRunForm *to_form,
		     const char *name);

	void (*addsub) (GBRunEvalContext *ec,
		        GBRunFormItem    *subitem,
		        GBRunFormItem    *item,
		        GBRunForm        *to_form,
		        const char       *name);

	GtkFixed *(*get_fixed) (GBRunEvalContext *ec,
		                 GBRunFormItem    *item);

	gboolean (*get_radio_btn) (GBRunEvalContext *ec,
		                   GBRunFormItem    *item,
		                   GtkWidget        **radio_btn);

	void (*set_radio_btn) (GBRunEvalContext *ec,
		               GBRunFormItem    *item,
		               GtkWidget        *radio_btn);

} GBRunFormItemClass;

#define GBRUN_FORM_ITEM_KEY "GBRunFormItemWidgetKey"

GtkType        gbrun_form_item_get_type    (void);

void           gbrun_form_item_add         (GBRunEvalContext *ec,
					    GBRunFormItem    *item,
					    GBRunForm        *to_form,
					    const char       *name);

void           gbrun_form_subitem_add      (GBRunEvalContext *ec,
					    GBRunFormItem    *subitem,
					    GBRunFormItem    *item,
					    GBRunForm        *to_form,
					    const char       *name);

GBRunFormItem *gbrun_form_item_new         (GBRunEvalContext *ec,
					    GtkType           type);

void           gbrun_form_item_set_widget  (GBRunFormItem *item,
					    GtkWidget     *widget);

GtkWidget     *gbrun_form_item_get_widget  (GBRunFormItem *item);

GBRunForm     *gbrun_form_item_get_form    (GBRunFormItem *item);

void           gbrun_form_item_setup       (GBRunFormItem *item,
					    GtkFixed      *parent,
					    const char    *name);

void           gbrun_form_item_invoke      (GBRunEvalContext *ec,
					    GtkWidget        *widget,
					    char             *suffix);

void           gbrun_form_item_register    (void);

void           gbrun_form_item_shutdown    (void);

#endif
