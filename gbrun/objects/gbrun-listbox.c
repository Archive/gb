
/*
 * Gnome Basic ListBox implementation.
 *
 * Author:
 *	Frank Chiulli	(fc-linux@home.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */
#include "gbrun-form-item.h"
#include <string.h>

#define ITEM_NAME "gb-vb.listbox"

#undef DEBUG_LB

/*
 * ListBox Properties:
 * COLUMNS		Specifies the number of columns
 * ITEMDATA		Specifies a specific number for each item in the ListBox
 *			window's left edge.
 * LIST			The items contained in the control's list portion.
 * SORTED
 * WIDTH
 */
enum {
	ARG_FIRST = 0,
	COLUMNS,
	ITEMDATA,
	LIST,
	LISTCOUNT,
	LISTINDEX,
	SORTED,
	WIDTH
};

static const char* p_name[] = {
	"Arg_First",
	"Columns",
	"Itemdata",
	"List",
	"ListCount",
	"ListIndex",
	"Sorted",
	"Width"
};

#define GBRUN_LISTBOX(obj) (GTK_CHECK_CAST ((obj), gbrun_listbox_get_type (), GBRunListBox))

/*
 *  Function Prototypes.
 */
static GBValue *listbox_getarg (GBRunEvalContext *ec,
				GBRunObject      *object,
				int		  property);

static gboolean listbox_setarg (GBRunEvalContext *ec,
				GBRunObject      *object,
				int		  property,
				GBValue          *val);


typedef struct  {
	GBRunFormItem    item;

	GtkWidget	*sw;		/* scrolled window */

	GtkCList	*clist;

	GSList		*list;
	GSList		*itemdata;

	gint		 current_column;
	gint		 current_row;
	gint		 height;
	gint		 width;
	gint		 rows;
	gint		 columns;	/* number of columns */
	gint		 real_columns;
	GBBoolean	 sorted;

	GBRunEvalContext *ec;

} GBRunListBox;


static void
gbrun_listbox_destroy (GtkObject *object)
{
	g_warning ("Unimplemented %s destroy", ITEM_NAME);
}


static void
listbox_click_signals (GBRunListBox *listbox,
			GdkEventButton *button)
{
	switch (button->type) {

	case GDK_2BUTTON_PRESS:
		gbrun_form_item_invoke (listbox->ec, listbox->item.widget, "_DblClick");
		break;

	case GDK_3BUTTON_PRESS:
	default:
		gbrun_form_item_invoke (listbox->ec, listbox->item.widget, "_Click");
		break;
	}
}


static void
listbox_click (GtkWidget *dummy,
		gint row,
		gint column,
		GdkEventButton *button,
		GBRunListBox *listbox)
{
	listbox->current_row = row;
	listbox->current_column = column;
	listbox_click_signals (listbox, button);
}


static void
listbox_unclick (GtkWidget *dummy,
		gint row,
		gint column,
		GdkEventButton *button,
		GBRunListBox *listbox)
{
	listbox->current_row = -1;
	listbox_click_signals (listbox, button);
} 


static void
connect_signals (GBRunListBox *dest)
{
	gtk_signal_connect (GTK_OBJECT (dest->clist), "select-row",
		GTK_SIGNAL_FUNC (listbox_click), dest);
	gtk_signal_connect (GTK_OBJECT (dest->clist), "unselect-row",
		GTK_SIGNAL_FUNC (listbox_unclick), dest);
}


static void
listbox_recreate (GBRunListBox *listbox)
{
	gint i;
	GtkCList *clist = listbox->clist;
	GtkWidget *widget = GTK_WIDGET (clist);
	gint row_height = widget->style->font->ascent + 1 + widget->style->font->descent;
	gint rows_per_column = listbox->height / row_height;
	gint real_columns;
	GSList *data = listbox->list;
	gint count = g_slist_length (data);

	real_columns = ceil ((gdouble) count / (gdouble) rows_per_column);

	if (real_columns < listbox->columns)
		real_columns = listbox->columns;

	listbox->rows = rows_per_column;
	listbox->real_columns = real_columns;
	/* FIXME: leak */
	gtk_container_remove (GTK_CONTAINER (listbox->sw), GTK_WIDGET (clist));
	clist = listbox->clist = GTK_CLIST (gtk_clist_new (real_columns));
	gtk_container_add (GTK_CONTAINER (listbox->sw), GTK_WIDGET (listbox->clist));
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (listbox->sw),
		GTK_POLICY_AUTOMATIC, GTK_POLICY_NEVER);
	gtk_widget_show (GTK_WIDGET (clist));
	gtk_clist_freeze (clist);

	for (i = 0; i < real_columns; i++) {
		gtk_clist_set_column_width (clist, i,
			listbox->width / listbox->columns);
	}

	{
		gchar *blank[real_columns];
		for (i = 0; i < real_columns; i++) {
			blank[i] = "";
		}
		for (i = 0; i < rows_per_column && i < count; i++) {
			gtk_clist_insert (clist, i, blank);
		}
	}

	{
		gint row = 0;
		gint column = 0;
		for (i = 0; i < count; i++, row++) {
			gchar *text = g_slist_nth (data, i)->data;

			if (row == rows_per_column) {
				column++;
				row = 0;
			}

			gtk_clist_set_text (clist, row, column, g_strdup (text));
		}
	}

/* TODO: add adjustment code */
	connect_signals (listbox);
	gtk_clist_thaw (clist);
}


static GBValue *
listbox_clear (GBRunEvalContext *ec,
		GBRunObject *object,
		GBValue **args)
{
	GBRunListBox *listbox = GBRUN_LISTBOX (object);

	gtk_clist_clear (listbox->clist);
	listbox->rows = 0;
	listbox->columns = 1;	
	listbox->current_row = -1;
	listbox->list = NULL;
	listbox->itemdata = NULL;

	return gb_value_new_empty ();
}


static GBValue *
listbox_additem (GBRunEvalContext *ec,
		GBRunObject *object,
		GBValue **args)
{
	gint position;
	gchar *entry;
	GBRunListBox *listbox = GBRUN_LISTBOX (object);

	entry = args[0]->v.s->str;

	if (args[1]->v.i == -1) {
		if (listbox->sorted)
			g_slist_insert_sorted (listbox->list, g_strdup (entry), (GCompareFunc)strcmp);
		else
			listbox->list = g_slist_append (listbox->list, g_strdup (entry));
	} else {
		position = args[1]->v.i;
		g_slist_insert (listbox->list, g_strdup (entry), position);
	}

	if (GTK_WIDGET (listbox->clist)->allocation.height != 1)
		listbox->height = GTK_WIDGET (listbox->clist)->allocation.height - 2;
	if (GTK_WIDGET (listbox->clist)->allocation.width != 1)
		listbox->width = GTK_WIDGET (listbox->clist)->allocation.width - 2;

	listbox_recreate (listbox);
	return gb_value_new_empty ();
}


static GBValue *
listbox_removeitem (GBRunEvalContext *ec,
			GBRunObject *object,
			GBValue **args)
{
	gint position;
	GBRunListBox *listbox = GBRUN_LISTBOX (object);
	GSList *data = listbox->list;
	GSList *temp;

	position = args[0]->v.i;

	if (position < 0 || position >= g_slist_length (data)) {
		gbrun_exception_firev (ec, _("Listbox.RemoveItem: Index out of range"));
		return gb_value_new_empty ();
	}

	temp = g_slist_nth (data, position);
	listbox->list = g_slist_remove_link (data, temp);
	g_slist_free_1 (temp);
	listbox_recreate (listbox);

	return gb_value_new_empty ();
}


/**
 * gbrun_listbox_construct:
 *   @ec
 *   @item
 **/
static void
gbrun_listbox_construct (GBRunEvalContext *ec,
			 GBRunFormItem *item)
{
	GBRunListBox *dest = GBRUN_LISTBOX (item);

	/* Create a scrolled window to pack the CList widget into */
	dest->sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (dest->sw),
				    	GTK_POLICY_AUTOMATIC, GTK_POLICY_NEVER);
	gbrun_form_item_set_widget (GBRUN_FORM_ITEM (dest), dest->sw);

	dest->clist = GTK_CLIST (gtk_clist_new (1));

	gtk_container_add (GTK_CONTAINER (dest->sw), GTK_WIDGET (dest->clist));
	gtk_widget_show (GTK_WIDGET (dest->clist));

	dest->columns  = 1;
	dest->rows     = 0;
	dest->list     = NULL;
	dest->itemdata = NULL;
	dest->ec       = ec;

	connect_signals (dest);
}


/**
 * gbrun_listbox_class_init
 *   @klass
 **/
static void
gbrun_listbox_class_init (GBRunObjectClass *klass)
{
	GtkObjectClass		 *gtk_class = (GtkObjectClass *) klass;
	GBRunFormItemClass	*form_class = (GBRunFormItemClass *) klass;

#ifdef DEBUG_LB
	printf ("gbrun_listbox_class_init entered.\n");	
#endif

	klass->set_arg = listbox_setarg;
	klass->get_arg = listbox_getarg;

	gbrun_object_add_property (klass, "columns",  gb_type_int, COLUMNS);

	gbrun_object_add_property (klass, "itemdata", gb_type_list, ITEMDATA);

	gbrun_object_add_property (klass, "list",     gb_type_list, LIST);

	gbrun_object_add_property_full (klass, "listcount", gb_type_int, LISTCOUNT,
		GBRUN_PROPERTY_READABLE);

	gbrun_object_add_property (klass, "listindex", gb_type_int, LISTINDEX);

	gbrun_object_add_property (klass, "sorted",   gb_type_int, SORTED);

	gbrun_object_add_property (klass, "width",    gb_type_int, WIDTH);

	gbrun_object_add_method_arg (klass, "sub;additem;item,string;index,integer,byval,-1;g",
		listbox_additem);

	gbrun_object_add_method_arg (klass, "sub;removeitem;index,integer;g",
		listbox_removeitem);

	gbrun_object_add_method_arg (klass, "sub;clear;g", listbox_clear);

	form_class->construct = gbrun_listbox_construct;

	gtk_class->destroy    = gbrun_listbox_destroy;
}


/**
 * gbrun_listbox_get_type
 **/
GtkType
gbrun_listbox_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunListBox),
			sizeof (GBRunFormItemClass),
			(GtkClassInitFunc)  gbrun_listbox_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_FORM_ITEM, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}


/**
 * listbox_setarg:
 *   @ec
 *   @object
 *   @property
 *   @val
 **/
static gboolean
listbox_setarg (GBRunEvalContext *ec,
		GBRunObject      *object,
		int		  property,
		GBValue          *val)
{
	gchar		*cptr;

	gint		 col_width;
	gint		 i;
	
	guint		 no_ptrs;

	GBRunListBox 	*listbox = GBRUN_LISTBOX (object);;

	g_return_val_if_fail (listbox != NULL, FALSE);
	g_return_val_if_fail (listbox->clist != NULL, FALSE);

	switch (property) {
	case COLUMNS:
	{
		if ((val->v.i == 0) || (val->v.i == 1))
			listbox->columns = 1;
		else if (val->v.i < 0) {
			gbrun_exception_firev (ec, _("listbox: Negative columns"));
			return FALSE;
		} else
			listbox->columns = val->v.i;

		listbox_recreate (listbox);
		return TRUE;
	}
	
	case ITEMDATA:
	{
		/*
		 * Each item in the list has an additional, hidden value 
		 * associated with it.  This long value is contained in the
		 * ItemData array, each element of which corresponds to an 
		 * element of the List array.
		 */
		long	 *iptr;

		no_ptrs = val->v.list->len;
		for (i = 0; i < no_ptrs; i++) {
			cptr = (char *)g_ptr_array_index (val->v.list, i);
			iptr = g_new (long, 1);
			if (sscanf (cptr, "%ld", iptr) != 1) {
				g_warning ("listbox: Error converting itemdata to long '%s'",
				           cptr);
				return FALSE;
			}
				
			listbox->itemdata = g_slist_append (listbox->itemdata, iptr);
		}

		return TRUE;
	}

	case LIST:
	{
		/*
		 *  The contents of the list box are contained in this array.
		 */
#ifdef DEBUG_LB
		printf ("listbox_setarg: LIST\n");
		printf ("                columns = %d.\n", listbox->columns);
#endif
		no_ptrs       = val->v.list->len;

		/*
		 *  Copy the data to the list..
		 */
		for (i = 0; i < no_ptrs; i++) {
			cptr = (char *)g_ptr_array_index (val->v.list, i);
			listbox->list = g_slist_append (listbox->list, cptr);
		}

		listbox_recreate (listbox);

		return TRUE;
	}

	case SORTED:
	{
		listbox->sorted = (!!val->v.i);

		if (listbox->sorted)
			g_slist_sort (listbox->list, (GCompareFunc) strcmp);

		listbox_recreate (listbox);
		return TRUE;
	}
	
	case WIDTH:
	{
#ifdef DEBUG_LB
		printf ("listbox_setarg: WIDTH\n");
#endif
		listbox->sw->allocation.width = GBRUN_FORM_TWIPS_TO_X (val->v.i);
		gtk_widget_set_usize (listbox->sw, listbox->sw->allocation.width, 
		                      listbox->sw->allocation.height);

		for (i = 0; i < listbox->real_columns; i++) {
			col_width = listbox->sw->allocation.width / listbox->columns;
			gtk_clist_set_column_width (listbox->clist, i, col_width);
		}
		return TRUE;
	}
	
	default:
		g_warning ("listbox: Set of unhandled property '%s'", p_name[property]);
		return FALSE;
	}

}


/**
 * listbox_getarg:
 *   @ec
 *   @object
 *   @property
 **/
static GBValue *
listbox_getarg (GBRunEvalContext *ec,
		GBRunObject      *object,
		int		  property)
{
	GBRunListBox *listbox = GBRUN_LISTBOX (object);
/*	GtkList      *l       = GTK_LIST (gbrun_form_item_get_widget (object));  */

	g_return_val_if_fail (listbox != NULL, NULL);

	switch (property) {
	case COLUMNS:
		return gb_value_new_int (listbox->columns);
	
	case ITEMDATA:
		g_warning ("listbox: Get of unhandled property '%s'", p_name[property]);
	
	case LIST:
		g_warning ("listbox: Get of unhandled property '%s'", p_name[property]);

	case LISTCOUNT:
		return gb_value_new_int (g_slist_length (listbox->list));

	case LISTINDEX:
	{
		gint temp;

		if (listbox->current_row == -1 || listbox->current_column == -1 ||
			!listbox->list) {
			return gb_value_new_int (-1);
		}

		temp = listbox->current_column * listbox->rows;
		return gb_value_new_int (temp + listbox->current_row);
	}
	
	case SORTED:
		return gb_value_new_boolean (listbox->sorted);

	default:
		g_warning ("listbox: Get of unhandled property '%s'", p_name[property]);
		break;
	}

	return NULL;
}


/**
 * gbrun_listbox_register
 **/
void
gbrun_listbox_register ()
{
	gbrun_listbox_get_type ();
}


/**
 * gbrun_listbox_shutdown
 **/
void
gbrun_listbox_shutdown ()
{
}
