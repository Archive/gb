/*
 * gbrun-shape.c
 *
 * Gnome Basic Interpreter Shape functions.
 *
 * Author:
 *	Frank Chiulli  <fc-linux@home.com>
 *
 * Copyright 2001, Helix Code, Inc.
 */
#include <gnome.h>
#include "gbrun-form-item.h"

#define ITEM_NAME "gb-vb.shape"


enum {
	ARG_FIRST = 0,
	FILLSTYLE,
	SHAPE
};

static const char* p_name[] = {
	"Arg_First",
	"FillStyle",
	"Shape"
};

#define GBRUN_SHAPE(obj) (GTK_CHECK_CAST ((obj), gbrun_shape_get_type (), GBRunShape))

typedef struct {
	GBRunFormItem    item;
	
	gint		 fillstyle;
	gint		 shape;

	GdkPixmap	*pixmap;
} GBRunShape;


/**
 * shape_config_event
 *   @widget,
 *   @event
 *   @item
 *
 **/
static gint
shape_config_event  (GtkWidget *widget, GdkEventConfigure *event, GBRunFormItem *item)
{
	GBRunShape	*shape = GBRUN_SHAPE (item);

	if (shape->pixmap)
		gdk_pixmap_unref (shape->pixmap);

	shape->pixmap = gdk_pixmap_new (widget->window,
	                                widget->allocation.width,
				        widget->allocation.height,
				        -1);
	switch (shape->shape) {
		case VB_SHAPE_RECTANGLE:
			gdk_draw_rectangle (shape->pixmap, widget->style->white_gc,
			                    TRUE, 0, 0, widget->allocation.width,
					    widget->allocation.height);
			break;

		case VB_SHAPE_CIRCLE:
		{
			int	 radius;
			radius = (widget->allocation.width < widget->allocation.height) 
			          ? widget->allocation.width
				  : widget->allocation.height;

			/*
			 * If this invisible rectangle isn't drawn, then the area
			 * around the square does not look right.
			 */
			gdk_draw_rectangle (shape->pixmap, 
			                    widget->style->bg_gc[GTK_STATE_NORMAL],
			                    TRUE, 0, 0, widget->allocation.width,
					    widget->allocation.height);

			gdk_draw_arc (shape->pixmap, widget->style->white_gc,
			              TRUE, 0, 0, radius, radius, 0, 360*64);
			break;
		}

		default:
			break;
	}

	return TRUE;
}


/**
 * shape_expose_event
 *   @widget
 *   @event
 *   @item
 *
 **/
static gint
shape_expose_event (GtkWidget *widget, GdkEventExpose *event, GBRunFormItem *item)
{
	GBRunShape	*shape = GBRUN_SHAPE (item);

	gdk_draw_pixmap (widget->window,
	                 widget->style->fg_gc [GTK_WIDGET_STATE  (widget)],
			 shape->pixmap,
			 event->area.x,     event->area.y,
			 event->area.x,     event->area.y,
			 event->area.width, event->area.height);

	return FALSE;
}


/**
 * shape_setarg
 *  @ec
 *  @object
 *  @property
 *  @val
 *
 * Sets the value of a property.  Returns TRUE is successful.
 **/
static gboolean
shape_setarg (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      int		property,
	      GBValue	       *val)
{
	GBRunShape *shape = GBRUN_SHAPE (object);

	g_return_val_if_fail (shape != NULL, FALSE);

	switch (property) {

	case FILLSTYLE:
		switch (val->v.i) {
		case VB_FILL_SOLID:
			shape->fillstyle = val->v.i;
			return TRUE;

		case VB_FILL_TRANS:
			shape->fillstyle = val->v.i;
			return TRUE;

		default:
			g_warning ("shape: Unhandled FILLSTYLE value '%d'", val->v.i);
			return FALSE;
		}

	case SHAPE:
		switch (val->v.i) {
		case VB_SHAPE_RECTANGLE:
			shape->shape = val->v.i;
			return TRUE;

		case VB_SHAPE_SQUARE:
			shape->shape = val->v.i;
			return TRUE;

		case VB_SHAPE_CIRCLE:
			shape->shape = val->v.i;
			return TRUE;

		default:
			g_warning ("shape: Unhandled SHAPE value '%d'", val->v.i);
			return FALSE;
		}
	default:
		g_warning ("shape: Unhandled property '%s'", p_name[property]);
		return FALSE;
	}
}


/**
 * shape_getarg:
 *   @ec
 *   @object
 *   @property
 *
 * Return the value of a property.
 **/
static GBValue *
shape_getarg (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      int               property)
{
	GBRunShape *shape = GBRUN_SHAPE (object);

	g_return_val_if_fail (shape != NULL, NULL);

	switch (property) {
	case FILLSTYLE:
		return gb_value_new_int (shape->fillstyle);
	
	case SHAPE:
		return gb_value_new_int (shape->shape);

	default:
		g_warning ("shape: Unhandled property '%s'", p_name[property]);
		return NULL;
	}
}


/**
 * gbrun_shape_construct
 *  @gbrun_shape_construct
 *
 * Build/construct a new shape.
 **/
static void
gbrun_shape_construct (GBRunEvalContext *ec, GBRunFormItem *item)
{
	GtkWidget	*draw_area;
	GBRunShape	*dest = GBRUN_SHAPE (item);

	draw_area = gtk_drawing_area_new ();
	gbrun_form_item_set_widget (GBRUN_FORM_ITEM (dest), draw_area);

	gtk_signal_connect (GTK_OBJECT (draw_area), "expose_event", 
	                    (GtkSignalFunc) shape_expose_event, item);
	
	gtk_signal_connect (GTK_OBJECT (draw_area), "configure_event", 
	                    (GtkSignalFunc) shape_config_event, item);
	
	gtk_widget_set_events (draw_area, GDK_EXPOSURE_MASK);
			       
	dest->fillstyle = -1;
	dest->shape     = VB_SHAPE_RECTANGLE;
	dest->pixmap    = NULL;

}


/**
 * gbrun_shape_class_init
 *  @klass
 *
 * Initialize the Shape class.
 **/
static void
gbrun_shape_class_init (GBRunObjectClass *klass)
{
	GBRunFormItemClass *form_class   = (GBRunFormItemClass *) klass;

	klass->set_arg = shape_setarg;
	klass->get_arg = shape_getarg;

	gbrun_object_add_property (klass, "fillstyle",  gb_type_int, FILLSTYLE);
	gbrun_object_add_property (klass, "shape",      gb_type_int, SHAPE);

	form_class->construct = gbrun_shape_construct;
}


/**
 * gbrun_shape_get_type
 *
 * Returnsthe GtkType of the Shape.
 *
 **/
GtkType
gbrun_shape_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunShape),
			sizeof (GBRunFormItemClass),
			(GtkClassInitFunc)  gbrun_shape_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_FORM_ITEM, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}


/**
 * gbrun_shape_register
 *
 * Register the shape as a GtkType.
 *
 **/
void
gbrun_shape_register ()
{
	gbrun_shape_get_type ();	
}


/**
 * gbrun_shape_shutdown
 *
 **/
void
gbrun_shape_shutdown ()
{
}
