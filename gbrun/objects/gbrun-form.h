
/*
 * GNOME Basic Interpreter Form Definitions
 *
 * Authors:
 *   Frank Chiulli (fc-linux@home.com)
 *   Michael Meeks (michael@helixcode.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#ifndef GBRUN_FORM_H
#define GBRUN_FORM_H

#include <math.h>

#include <gbrun/gbrun.h>
#include <gb/gb-form.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-object.h>
#include "libgbobj.h"

#define TWIPS_PER_INCH		1440
/*
#define PIXELS_PER_INCH_X 	  96
#define PIXELS_PER_INCH_Y 	  96
*/
/* This works better for me */
#define PIXELS_PER_INCH_X 	 96
#define PIXELS_PER_INCH_Y 	 96

#define GBRUN_FORM_TWIPS_TO_X(i) ((int) (((float)i / (float)TWIPS_PER_INCH) * PIXELS_PER_INCH_X))
#define GBRUN_FORM_X_TO_TWIPS(i) (floor ((i / PIXELS_PER_INCH_X) * TWIPS_PER_INCH))

#define GBRUN_FORM_TWIPS_TO_Y(i) ((int) (((float)i / (float)TWIPS_PER_INCH) * PIXELS_PER_INCH_Y))
#define GBRUN_FORM_Y_TO_TWIPS(i) (floor ((i / PIXELS_PER_INCH_Y) * TWIPS_PER_INCH))

#define GBRUN_FORM(obj)    (GTK_CHECK_CAST ((obj), gbrun_form_get_type (), GBRunForm))
#define GBRUN_IS_FORM(obj) (GTK_CHECK_TYPE ((obj), gbrun_form_get_type ()))

struct _GBRunForm {
	GBRunObject     object;

	GtkWindow      *window;
	GtkMenuBar     *menubar;
	GtkFixed       *fixed;
	GtkAccelGroup  *accel_group;
	GtkWidget      *radio_btn;

	GHashTable     *carray;

	/* Form properties */
	char           *tag;
	char           *linktopic;
	int             scalemode;
	int             scaleheight;
	int             scalewidth;
	int             startup_pos;
	int             borderstyle;
	gboolean        maxbutton;
	gboolean        minbutton;
	gboolean        show_in_taskbar;
	gboolean        controlbox;

};

typedef struct {
	GBRunObjectClass klass;
} GBRunFormClass;

GtkType      gbrun_form_get_type         (void);

char        *gbrun_form_un_shortcutify   (const char *txt, char *shortcut);

void         gbrun_form_add              (GBRunForm     *form,
					  GBRunFormItem *item,
					  const char    *name);

gboolean     gbrun_form_invoke           (GBRunEvalContext *ec,
					  GBRunForm        *form,
					  const char       *method,
					  GBRunFormItem    *fi);

void         gbrun_form_init             (GBRunEvalContext  *ec,
					  GBRunForm         *form,
					  const GBParseData *pd);

void         gbrun_form_show             (GBRunForm         *form);

void         gbrun_form_register         (void);
void         gbrun_form_shutdown         (void);

typedef enum {
	GBRUN_FORM_COLOR_BACK,
	GBRUN_FORM_COLOR_BORDER,
	GBRUN_FORM_COLOR_FILL,
	GBRUN_FORM_COLOR_FORE,
	GBRUN_FORM_COLOR_MASK
} GBRunFormColorType;

void         gbrun_form_widget_set_color (GtkWidget         *widget,
					  GBRunFormColorType type,
					  GBLong             color);
GBLong       gbrun_form_widget_get_color (GtkWidget         *widget,
					  GBRunFormColorType type);

#endif
