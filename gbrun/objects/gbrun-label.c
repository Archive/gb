/*
 * gbrun-label.c
 *
 * Gnome Basic Interpreter Label functions.
 *
 * Author:
 *	Michael Meeks	(michael@helixcode.com)
 *	Frank Chiulli	(fc-linux@home.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */
#include "gbrun-form-item.h"

#define ITEM_NAME "gb-vb.label"


enum {
	ARG_FIRST = 0,
	CAPTION,
	ALIGNMENT
};

#define GBRUN_LABEL(obj) (GTK_CHECK_CAST ((obj), gbrun_label_get_type (), GBRunLabel))

typedef struct {
	GBRunFormItem   item;

	GtkJustification alignment;
} GBRunLabel;

/*
 * _GBELabel defines a VB Text Box
 *
 * alignment		Determines whether the text box's text appears left-
 *			justified, centered, or right-justified with the
 *			text box's boundaries.
 * back_color		Specifies the text box's background color.
 * border_style		Determines whether a single-line border appears
 *			around the text box.
 * enabled		Determines whether the text box is active.
 * font		 	Text's font name, style and size.
 * height		The height of the text box's outline in twips.
 * left			The number of twips from the text box's left edge to
 *			the Form window's left edge.
 * locked		Determines whether the user can edit the text inside 
 *			the text box.
 * max_length		Specifies the number of characters the user can type
 *			into the text box.
 * mouse_ptr		The shape of the mouse cursor when the user moves the 
 *			mouse over the text box.
 * multi_line		Lets the text box hold multiple lines of text or sets
 *			the text box to hold only a single line of text.
 * pwd_char		Determines the character that appears in the text box
 *			when the user enters a password, which keeps prying
 *			eyes from knowing what the user enters into the box.
 * scroll_bars		Determines whether scrollbars appear on the edges of a
 *			multiline text box.
 * tab_index		Specifies the order of the text box in the focus order.
 * tab_stop		Determines whether the text box can receive the focus.
 * text			Holds the value of the text inside the text box.
 * tool_tip_text		Holds the text that appears as a ToolTip at runtime.
 * top			Holds the number of twips from the text box's top edge
 *			to the Form window's top edge.
 * visible		Determines whether the text box appears or is hidden
 *			from the user.
 * width		The width of the text box in twips.
 *
 * text_box
 *
 * Definitions:
 * twip		1,440th of an inch (the smallest screen measurement)
 */

struct _GBELabel {
/*	guint8		 alignment;
	guint32		 back_color;
	guint8		 border_style;
	gboolean	 enabled;
	GBEFormFont	 font;
	guint32		 fore_color;
	guint16		 height;
	guint16		 left;
	gboolean	 locked;
	guint16		 max_length;
	guint8		 mouse_ptr;
	gboolean	 multi_line;
	gchar		 pwd_vhar;
	gboolean	 scroll_bars;
	guint8		 tab_index;
	guint8		 tab_stop;
	gchar		*text;
	gchar		*tool_tip_text;
	guint16		 top;
	gboolean	 visible;
	guint16		 width;

	GtkWidget	*text_box;*/
};



/**
 * label_setarg
 *
 **/
static gboolean
label_setarg (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      int               property,
	      GBValue          *val)
{
	GBRunLabel *label = GBRUN_LABEL (object);
	GtkLabel   *w = GTK_LABEL (
		gbrun_form_item_get_widget (GBRUN_FORM_ITEM (object)));

	g_return_val_if_fail (label != NULL, FALSE);

	switch (property) {
	case CAPTION:
		gtk_label_set_text (w, val->v.s->str);
		return TRUE;

	case ALIGNMENT: {
		switch (val->v.i) {
		case VB_LEFT_JUSTIFY:
			label->alignment = GTK_JUSTIFY_LEFT;
			gtk_label_set_justify (w, GTK_JUSTIFY_LEFT);
			break;

		case VB_RIGHT_JUSTIFY:
			label->alignment = GTK_JUSTIFY_RIGHT;
			gtk_label_set_justify (w, GTK_JUSTIFY_RIGHT);
			break;

		case VB_CENTER_JUSTIFY:
			label->alignment = GTK_JUSTIFY_CENTER;
			gtk_label_set_justify (w, GTK_JUSTIFY_CENTER);
			break;

		default:
			g_warning ("label: Unhandled alignment: %d", val->v.i);
		}

		return TRUE;
	}

	default:
		g_warning ("label: Unhandled property '%d'", property);
		return FALSE;
	}
}


/**
 * label_getarg:
 *   @ec
 *   @object
 *   @property
 **/
static GBValue *
label_getarg (GBRunEvalContext *ec,
		GBRunObject      *object,
		int               property)
{
	GBRunLabel *label = GBRUN_LABEL (object);
	GtkLabel   *w = GTK_LABEL (
		gbrun_form_item_get_widget (GBRUN_FORM_ITEM (object)));

	g_return_val_if_fail (label != NULL, NULL);

	switch (property) {
	case CAPTION:
		return gb_value_new_string_chars (w->label);

	case ALIGNMENT:
		return gb_value_new_int (label->alignment);

	default:
		g_warning ("label: Unhandled property '%d'", property);
		return NULL;
	}
}

static void
gbrun_label_construct (GBRunEvalContext *ec, GBRunFormItem *item)
{
	GtkWidget  *w;
	GBRunLabel *dest = GBRUN_LABEL (item);

	w = gtk_label_new ("");
	gbrun_form_item_set_widget (item, w);

	dest->alignment = GTK_JUSTIFY_LEFT;
	gtk_label_set_justify (GTK_LABEL (w), GTK_JUSTIFY_LEFT);

}

static void
gbrun_label_class_init (GBRunObjectClass *klass)
{
	GBRunFormItemClass *form_class   = (GBRunFormItemClass *) klass;

	klass->set_arg = label_setarg;
	klass->get_arg = label_getarg;

	gbrun_object_add_property (klass, "caption",
				   gb_type_string, CAPTION);

	gbrun_object_add_property (klass, "alignment", 
				   gb_type_int, ALIGNMENT);

	form_class->construct = gbrun_label_construct;
}

GtkType
gbrun_label_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunLabel),
			sizeof (GBRunFormItemClass),
			(GtkClassInitFunc)  gbrun_label_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_FORM_ITEM, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}

/**
 * gbrun_label_register
 *
 **/
void
gbrun_label_register ()
{
	gbrun_label_get_type ();	
}


/**
 * gbrun_label_shutdown
 *
 **/
void
gbrun_label_shutdown ()
{
}
