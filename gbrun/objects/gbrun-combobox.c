
/*
 * Gnome Basic ComboBox implementation.
 *
 * Authors:
 *	Frank Chiulli	(fc-linux@home.com)
 *      Julian Froment  (julian@jfroment.uklinux.net)
 *
 * Copyright 2000, Helix Code, Inc.
 */
#include "gbrun-form-item.h"
#include <string.h>

#define ITEM_NAME "gb-vb.combobox"

/*
 * ComboBox Properties:
 * LIST			The items contained in the control's list portion.
 * SORTED
 */
enum {
	ARG_FIRST = 0,
	SORTED,
	TEXT
};

static const char* p_name[] = {
	"Arg_First",
	"Sorted",
	"Text"
};

#define GBRUN_COMBOBOX(obj) (GTK_CHECK_CAST ((obj), gbrun_combobox_get_type (), GBRunComboBox))

/*
 *  Function Prototypes.
 */
static GBValue *combobox_getarg (GBRunEvalContext *ec,
				 GBRunObject      *object,
				 int		   property);

static gboolean combobox_setarg (GBRunEvalContext *ec,
				 GBRunObject      *object,
				 int		   property,
				 GBValue          *val);


typedef struct  {
	GBRunFormItem    item;

	GtkWidget       *combo;

	GList		*list;

	GBBoolean	 sorted;

} GBRunComboBox;


static void
gbrun_combobox_destroy (GtkObject *object)
{
	g_warning ("Unimplemented %s destroy", ITEM_NAME);
}


/**
 * gbrun_combobox_construct:
 *   @ec
 *   @item
 **/
static void
gbrun_combobox_construct (GBRunEvalContext *ec, GBRunFormItem *item)
{
	GBRunComboBox	*dest = GBRUN_COMBOBOX (item);

	dest->combo = gtk_combo_new ();
	gbrun_form_item_set_widget (GBRUN_FORM_ITEM (dest), dest->combo);
	
	dest->list   = NULL;
	dest->sorted = FALSE;
}


static GBValue *
combobox_clear (GBRunEvalContext *ec,
                GBRunObject *object,
                GBValue **args)
{
	GBRunComboBox *combobox = GBRUN_COMBOBOX (object);
	GtkList *list;

	list = GTK_LIST (GTK_COMBO (combobox->combo)->list); 

	gtk_list_clear_items (list, 0, -1);

	combobox->list = NULL;

        return gb_value_new_empty ();
}


static GBValue *
combobox_removeitem (GBRunEvalContext *ec,
                        GBRunObject *object,
                        GBValue **args)
{
	gint position;
        GBRunComboBox *combobox = GBRUN_COMBOBOX (object);
        GList *data = combobox->list;
        GList *temp;

        position = args[0]->v.i;

        if (position < 0 || position >= g_list_length (data)) {
                gbrun_exception_firev (ec, _("Combobox.RemoveItem: Index out of range"));
                return gb_value_new_empty ();
        }

	temp = g_list_nth (data, position);
        combobox->list = g_list_remove_link (data, temp);
        g_list_free_1 (temp);

	gtk_combo_set_popdown_strings (GTK_COMBO (combobox->combo), combobox->list);

        return gb_value_new_empty ();
}


static GBValue *
combobox_additem (GBRunEvalContext *ec,
                GBRunObject *object,
                GBValue **args)
{
	gint position;
        gchar *entry;
	GBRunComboBox *combobox = GBRUN_COMBOBOX (object);

        entry = args[0]->v.s->str;

        if (args[1]->v.i == -1) {
                if (combobox->sorted)
                        g_list_insert_sorted (combobox->list, g_strdup(entry), (GCompareFunc)strcmp);
                else
                        combobox->list = g_list_append (combobox->list, g_strdup (entry));
        } else {
                position = args[1]->v.i;
                g_list_insert (combobox->list, g_strdup (entry), position);
        }

	gtk_combo_set_popdown_strings (GTK_COMBO (combobox->combo), combobox->list);

	return gb_value_new_empty ();
}


/**
 * gbrun_combobox_class_init
 *   @klass
 **/
static void
gbrun_combobox_class_init (GBRunObjectClass *klass)
{
	GtkObjectClass		*gtk_class  = (GtkObjectClass *) klass;
	GBRunFormItemClass	*form_class = (GBRunFormItemClass *) klass;

#ifdef DEBUG_CB
	printf ("gbrun_combobox_class_init entered.\n");	
#endif

	klass->set_arg = combobox_setarg;
	klass->get_arg = combobox_getarg;

	gbrun_object_add_property (klass, "sorted", gb_type_int,    SORTED);

	gbrun_object_add_property (klass, "text",   gb_type_string, TEXT);

	gbrun_object_add_method_arg (klass, "sub;additem;item,string;index,integer,byval,-1;g",
                combobox_additem);

	gbrun_object_add_method_arg (klass, "sub;removeitem;index,integer;g",
                combobox_removeitem);

        gbrun_object_add_method_arg (klass, "sub;clear;g", combobox_clear);

	form_class->construct = gbrun_combobox_construct;

	gtk_class->destroy    = gbrun_combobox_destroy;
}


/**
 * gbrun_combobox_get_type
 **/
GtkType
gbrun_combobox_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunComboBox),
			sizeof (GBRunFormItemClass),
			(GtkClassInitFunc)  gbrun_combobox_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_FORM_ITEM, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}


/**
 * combobox_setarg:
 *   @ec
 *   @object
 *   @property
 *   @val
 **/
static gboolean
combobox_setarg (GBRunEvalContext *ec,
		 GBRunObject      *object,
		 int		   property,
		 GBValue          *val)
{
	GBRunComboBox 	*combobox = GBRUN_COMBOBOX (object);

	g_return_val_if_fail (combobox != NULL, FALSE);
/*	g_return_if_fail (combobox->clist != NULL); */

	switch (property) {
	case SORTED:
	{
		combobox->sorted = (val->v.i == VB_FALSE);
		return TRUE;
	}

	case TEXT:
	{
		GtkWidget     *combo;
		
		combo = gbrun_form_item_get_widget (GBRUN_FORM_ITEM (object));

		combobox->list = g_list_append(combobox->list, g_strdup (val->v.s->str));
		gtk_combo_set_popdown_strings (GTK_COMBO(combo), combobox->list) ;

		return TRUE;
	}
	
	default:
		g_warning ("combobox: Set of unhandled property '%s'", p_name[property]);
		return FALSE;
	}

}


/**
 * combobox_getarg:
 *   @ec
 *   @object
 *   @property
 **/
static GBValue *
combobox_getarg (GBRunEvalContext *ec,
		 GBRunObject      *object,
		 int		   property)
{
	GBRunComboBox *combobox = GBRUN_COMBOBOX (object);
/*	GtkList      *l       = GTK_LIST (gbrun_form_item_get_widget (object));  */

	g_return_val_if_fail (combobox != NULL, NULL);

	switch (property) {
	case SORTED:
		return gb_value_new_boolean (combobox->sorted);

	default:
		g_warning ("combobox: Get of unhandled property '%s'", p_name[property]);
		break;
	}

	return NULL;
}


/**
 * gbrun_combobox_register
 **/
void
gbrun_combobox_register ()
{
	gbrun_combobox_get_type ();
}


/**
 * gbrun_combobox_shutdown
 **/
void
gbrun_combobox_shutdown ()
{
}
