
/*
 * Gnome Basic OptionButton implementation.
 *
 * Author:
 *	Frank Chiulli	(fc-linux@home.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */
#include <gdk/gdkkeysyms.h>
#include "gbrun-form-item.h"

#define ITEM_NAME "gb-vb.optionbutton"

#undef DEBUG_OB

/*
 * OptionButton Properties:
 */
enum {
	ARG_FIRST = 0,
	CAPTION,
	VALUE
};

static const char* p_name[] = {
	"Arg_First",
	"Caption",
	"Value"
};

#define GBRUN_OPTBUTTON(obj) (GTK_CHECK_CAST ((obj), gbrun_optbutton_get_type (), GBRunOptButton))


/*
 *  Function Prototypes.
 */
static GBValue *optbutton_getarg (GBRunEvalContext *ec,
				  GBRunObject      *object,
				  int		    property);

static gboolean optbutton_setarg (GBRunEvalContext *ec,
				  GBRunObject      *object,
				  int		    property,
				  GBValue          *val);

static void gbrun_optbutton_add (GBRunEvalContext *ec,
		                 GBRunFormItem    *item,
		                 GBRunForm        *to_form,
		                 const char       *name);

static void gbrun_optbutton_addsub (GBRunEvalContext *ec,
		                    GBRunFormItem    *subitem,
		                    GBRunFormItem    *item,
		                    GBRunForm        *to_form,
		                    const char       *name);


typedef struct  {
	GBRunFormItem    item;
	
	GtkLabel	*label;
	char		*real_caption;
} GBRunOptButton;


static void
gbrun_optbutton_destroy (GtkObject *object)
{
	GBRunOptButton *optbutton = GBRUN_OPTBUTTON (object);

	if (optbutton->real_caption)
		g_free (optbutton->real_caption);

	g_warning ("Unimplemented %s destroy", ITEM_NAME);
}


/**
 * gbrun_optbutton_construct:
 *   @ec
 *   @item
 **/
static void
gbrun_optbutton_construct (GBRunEvalContext *ec,
			   GBRunFormItem *item)
{
	GBRunOptButton	*dest = GBRUN_OPTBUTTON (item);
	GtkWidget	*radio;

	/* Create a radio button with a GtkLabel widget */
	radio = gtk_radio_button_new (NULL);
	gbrun_form_item_set_widget (GBRUN_FORM_ITEM (dest), radio);

	dest->label = GTK_LABEL (gtk_accel_label_new ("WIBBLE"));
	gtk_widget_show (GTK_WIDGET (dest->label));
	
	gtk_container_add (GTK_CONTAINER (radio), GTK_WIDGET (dest->label));
}


/**
 * gbrun_optbutton_class_init
 *   @klass
 **/
static void
gbrun_optbutton_class_init (GBRunObjectClass *klass)
{
	GtkObjectClass		 *gtk_class = (GtkObjectClass *) klass;
	GBRunFormItemClass	*form_class = (GBRunFormItemClass *) klass;

#ifdef DEBUG_OB
	printf ("Enter gbrun_optbutton_class_init.\n");	
#endif

	klass->set_arg = optbutton_setarg;
	klass->get_arg = optbutton_getarg;

	gbrun_object_add_property (klass, "caption", gb_type_string, CAPTION);
	
	gbrun_object_add_property (klass, "value",   gb_type_boolean, VALUE);

	form_class->construct     = gbrun_optbutton_construct;
	form_class->add           = gbrun_optbutton_add;
	form_class->addsub        = gbrun_optbutton_addsub;

	gtk_class->destroy        = gbrun_optbutton_destroy;
}


/**
 * gbrun_optbutton_get_type
 **/
GtkType
gbrun_optbutton_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunOptButton),
			sizeof (GBRunFormItemClass),
			(GtkClassInitFunc)  gbrun_optbutton_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_FORM_ITEM, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}


/**
 * optbutton_setarg:
 *   @ec
 *   @object
 *   @property
 *   @val
 **/
static gboolean
optbutton_setarg (GBRunEvalContext *ec,
		  GBRunObject      *object,
		  int		    property,
		  GBValue          *val)
{
	GBRunOptButton 	*optbutton = GBRUN_OPTBUTTON (object);
	GtkWidget	*rwidget = gbrun_form_item_get_widget (GBRUN_FORM_ITEM (object));

#ifdef DEBUG_OB
	printf ("Enter optbutton_setarg.\n");
#endif
	g_return_val_if_fail (optbutton != NULL, FALSE);

	switch (property) {
	case CAPTION:
	{
		char *txt;
		guint keyval;

#ifdef DEBUG_OB
		printf ("      Caption = [%s]\n", val->v.s->str);
#endif
		optbutton->real_caption = g_strdup (val->v.s->str);

		txt = gbrun_form_un_shortcutify (optbutton->real_caption, NULL);
		gtk_label_set_text (optbutton->label, txt);

		gtk_accel_label_set_accel_widget (
			GTK_ACCEL_LABEL (optbutton->label), rwidget);

		keyval = gtk_label_parse_uline (GTK_LABEL (optbutton->label), txt);

		if (keyval != GDK_VoidSymbol &&
		    optbutton->item.parent)
			gtk_widget_add_accelerator (rwidget, "clicked",
				                    GBRUN_FORM (optbutton->item.parent)->accel_group,
				                    keyval, GDK_MOD1_MASK,
				                    (GtkAccelFlags) 0);
		
		g_free (txt);

		return TRUE;
	} 

	case VALUE:
		gtk_toggle_button_set_active (
			GTK_TOGGLE_BUTTON (rwidget), val->v.bool);
		return TRUE;

	default:
		g_warning ("optbutton: Set of unhandled property '%s'", p_name[property]);
		return FALSE;
	}

}


/**
 * optbutton_getarg:
 *   @ec
 *   @object
 *   @property
 **/
static GBValue *
optbutton_getarg (GBRunEvalContext *ec,
		  GBRunObject      *object,
		  int		    property)
{
	GBRunOptButton	*optbutton = GBRUN_OPTBUTTON (object);
	GtkWidget	*rwidget   = gbrun_form_item_get_widget (GBRUN_FORM_ITEM (object));

	g_return_val_if_fail (optbutton != NULL, NULL);

	switch (property) {
	case CAPTION:
		return gb_value_new_string_chars (optbutton->real_caption);

	case VALUE: 
		return gb_value_new_boolean (
			gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (rwidget) ) );

	default:
		g_warning ("optbutton: Get of unhandled property '%s'", p_name[property]);
		break;
	}

	return NULL;
}


/**
 * gbrun_optbutton_add
 *   @ec
 *   @item
 *   @to_form
 *   @name
 **/
static void
gbrun_optbutton_add (GBRunEvalContext *ec,
		     GBRunFormItem    *item,
		     GBRunForm        *to_form,
		     const char       *name)
{
	GtkWidget	*radio_btn = item->widget;
	GSList		*radio_grp = NULL;

	item->parent = GBRUN_OBJECT (to_form);
	item->name   = g_strdup (name);
	
	if (to_form->radio_btn) {
		radio_grp = gtk_radio_button_group (GTK_RADIO_BUTTON (to_form->radio_btn));
		gtk_radio_button_set_group (GTK_RADIO_BUTTON (radio_btn), radio_grp);
	} else {
		to_form->radio_btn = radio_btn;
	}

	gtk_fixed_put (to_form->fixed, item->widget, 0, 0);
	
	return;
}


/**
 * gbrun_optbutton_addsub
 *   @ec
 *   @subitem
 *   @item
 *   @to_form
 *   @name
 **/
static void
gbrun_optbutton_addsub (GBRunEvalContext *ec,
		        GBRunFormItem    *subitem,
		        GBRunFormItem    *item,
		        GBRunForm        *to_form,
		        const char       *name)
{
	GtkFixed	   *fixed;
	GtkWidget	   *radio     = NULL;
	GSList	           *radio_grp = NULL;
	
	GtkWidget          *radio_btn = subitem->widget;
	GBRunFormItemClass *klass     = GBRUN_FORM_ITEM_CLASS (GTK_OBJECT (item)->klass);

	subitem->parent = GBRUN_OBJECT (item);
	subitem->name   = g_strdup (name);

	if (klass->get_fixed) {
		fixed = klass->get_fixed (ec, item);
		if (fixed) {
			if (klass->get_radio_btn (ec, item, &radio)) {
				if (radio) {
					radio_grp = gtk_radio_button_group (GTK_RADIO_BUTTON (radio));
					gtk_radio_button_set_group (GTK_RADIO_BUTTON (radio_btn), 
				                                    radio_grp);
				} else {
					radio_grp = gtk_radio_button_group (GTK_RADIO_BUTTON (radio_btn));
					klass->set_radio_btn (ec, 
					                      item,
							      radio_btn);
				}
			} else {
				g_warning ("gbrun_optbutton_addsub: Item %s missing fixed widget",
				           item->name);
				return;
			}

			gtk_fixed_put (fixed, subitem->widget, 0, 0);
		}
	}

	return;	
}

/**
 * gbrun_optbutton_register
 **/
void
gbrun_optbutton_register ()
{
	gbrun_optbutton_get_type ();
}


/**
 * gbrun_optbutton_shutdown
 **/
void
gbrun_optbutton_shutdown ()
{
}
