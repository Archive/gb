/*
 * gbrun-hscrollbar.c
 *
 * Gnome Basic Interpreter Command HScrollbar functions.
 *
 * Authors:
 * 	Matthew Mei <mei@fas.harvard.edu>
 *
 */

#include <gdk/gdkkeysyms.h>
#include "gbrun-form-item.h"

#define ITEM_NAME "gb-vb.hscrollbar"

enum {
	ARG_FIRST = 0,
	LARGECHANGE,
	SMALLCHANGE,
	MIN,
	MAX,
	VALUE,
};

#define GBRUN_HSCROLLBAR(obj) (GTK_CHECK_CAST ((obj), gbrun_hscrollbar_get_type (), GBRunHScrollbar))

typedef struct {
	GBRunFormItem   item;

	GtkObject  *adjustment;
	GBRunEvalContext *ec;

} GBRunHScrollbar;

static void
hscrollbar_change (GtkObject *adj, GBRunHScrollbar *hscrollbar)
{
	gbrun_form_item_invoke (hscrollbar->ec, hscrollbar->item.widget, "_Change");
}

/**
 * hscrollbar_setarg:
 *   @ec
 *   @object
 *   @property
 *   @val
 **/
static gboolean
hscrollbar_setarg (GBRunEvalContext *ec,
		  GBRunObject      *object,
		  int               property,
		  GBValue          *val)
{
	GBRunHScrollbar *hscrollbar = GBRUN_HSCROLLBAR (object);
	GtkAdjustment *adjust = GTK_ADJUSTMENT (hscrollbar->adjustment);

	g_return_val_if_fail (hscrollbar != NULL, FALSE);

	switch (property) {

	case LARGECHANGE:
		if (val->v.i > 0)
			adjust->page_increment = val->v.i;
		break;

	case SMALLCHANGE:
		if (val->v.i > 0)
			adjust->step_increment = val->v.i;
		break;

	case MIN:
		if (-32767 <= val->v.i && 32767 <= val->v.i)
			adjust->lower = val->v.i;

	case MAX:
		if (-32767 <= val->v.i && 32767 <= val->v.i)
			adjust->upper = val->v.i;
		break;

	case VALUE:
		if (adjust->lower <= val->v.i && val->v.i <= adjust->upper)
			adjust->value = val->v.i;
		break;

	default:
		g_warning ("hscrollbar: Unhandled property '%d'", property);
		return FALSE;
	}

	return TRUE;
}


/**
 * hscrollbar_getarg:
 *   @ec
 *   @object
 *   @property
 **/
static GBValue *
hscrollbar_getarg (GBRunEvalContext *ec,
		  GBRunObject      *object,
		  int               property)
{
	GBRunHScrollbar *hscrollbar = GBRUN_HSCROLLBAR (object);
	GtkAdjustment *adjust = GTK_ADJUSTMENT (hscrollbar->adjustment);

	g_return_val_if_fail (hscrollbar != NULL, NULL);

	switch (property) {

	case LARGECHANGE:
		return gb_value_new_int (adjust->page_increment);

	case SMALLCHANGE:
		return gb_value_new_int (adjust->step_increment);

	case MIN:
		return gb_value_new_int (adjust->lower);

	case MAX:
		return gb_value_new_int (adjust->upper);

	case VALUE:
		return gb_value_new_int (adjust->value);

	default:
		g_warning ("hscrollbar: Unhandled property '%d'", property);
		break;
	}

	return NULL;
}

static void
gbrun_hscrollbar_destroy (GtkObject *object)
{
	GBRunHScrollbar *bar = GBRUN_HSCROLLBAR (object);

	if (bar->adjustment)
		g_free (bar->adjustment);
	g_warning ("Unimplemented %s destruct", ITEM_NAME);
}

static void
gbrun_hscrollbar_construct (GBRunEvalContext *ec, GBRunFormItem *item)
{
	GtkWidget *w;
	GBRunHScrollbar *dest = GBRUN_HSCROLLBAR (item);

	dest->adjustment = gtk_adjustment_new (0.0, 0.0, 32767.0, 1.0, 1.0, 1.0);
	dest->ec = ec;
	w = gtk_hscrollbar_new (GTK_ADJUSTMENT (dest->adjustment));

	gbrun_form_item_set_widget (GBRUN_FORM_ITEM (dest), w);

	gtk_signal_connect (dest->adjustment, "value_changed",
		GTK_SIGNAL_FUNC (hscrollbar_change), dest);
}

static void
gbrun_hscrollbar_class_init (GBRunObjectClass *klass)
{
	GtkObjectClass     *gtk_class    = (GtkObjectClass *) klass;
	GBRunFormItemClass *form_class   = (GBRunFormItemClass *) klass;

	klass->set_arg = hscrollbar_setarg;
	klass->get_arg = hscrollbar_getarg;

	gbrun_object_add_property (
		klass, "largechange", gb_type_int, LARGECHANGE);
	gbrun_object_add_property (
		klass, "smallchange", gb_type_int, SMALLCHANGE);
	gbrun_object_add_property (
		klass, "min", gb_type_int, MIN);
	gbrun_object_add_property (
		klass, "max", gb_type_int, MAX);
	gbrun_object_add_property (
		klass, "value", gb_type_int, VALUE);

	form_class->construct = gbrun_hscrollbar_construct;

	gtk_class->destroy    = gbrun_hscrollbar_destroy;
}

GtkType
gbrun_hscrollbar_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunHScrollbar),
			sizeof (GBRunFormItemClass),
			(GtkClassInitFunc)  gbrun_hscrollbar_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_FORM_ITEM, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}

/**
 * gbrun_hscrollbar_register:
 *
 **/
void
gbrun_hscrollbar_register ()
{
	gbrun_hscrollbar_get_type ();
}


/**
 *  gbrun_hscrollbar_shutdown:
 **/
void
gbrun_hscrollbar_shutdown ()
{
}

