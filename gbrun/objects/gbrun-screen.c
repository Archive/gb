/*
 * gbrun-screen.c: the 'Screen' object.
 *
 * Authors:
 *      Michael Meeks  <michael@helixcode.com>
 */
#include <gb/gb.h>
#include <gb/gb-constants.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-global.h>
#include <gbrun/gbrun-value.h>
#include "gbrun-objects.h"
#include <libgnomeprint/gnome-font.h>

#define ITEM_NAME "gb-screen"

enum {
	ARG_FIRST = 0,
	MOUSE_POINTER,
	FONT_COUNT,
	WIDTH,
	HEIGHT
};

typedef struct {
	GBRunObject item;

	int mousepointer; /* Cursor */
} GBRunScreen;

static GBValue *
screen_func_fonts (GBRunEvalContext *ec,
		   GBRunObject      *object,
		   GBValue          **args)
{
	GList *fonts, *font;
	GBValue *retval;

	GB_IS_VALUE (ec, args [0], GB_VALUE_LONG);

	fonts = gnome_font_list ();
	font  = g_list_nth (fonts, args [0]->v.l);

	retval = gb_value_new_string_chars (font ? font->data : "");

	gnome_font_list_free (fonts);

	return retval;
}


static GdkCursor *
get_cursor (GBRunEvalContext *ec, int type)
{
	switch (type) {

	case GB_C_Default:
	case GB_C_Arrow:
		return gdk_cursor_new (GDK_ARROW);

	case GB_C_Crosshair:
		return gdk_cursor_new (GDK_CROSSHAIR);
	
	case GB_C_IBeam:
		return gdk_cursor_new (GDK_ARROW);
	
	case GB_C_Size:
		return gdk_cursor_new (GDK_ARROW);
	
	case GB_C_SizeNESW:
		return gdk_cursor_new (GDK_CROSS);
	
	case GB_C_SizeNS:
		return gdk_cursor_new (GDK_CROSS);
	
	case GB_C_SizeNWSE:
		return gdk_cursor_new (GDK_CROSS);
	
	case GB_C_SizeWE:
		return gdk_cursor_new (GDK_CROSS);
	
	case GB_C_UpArrow:
		return gdk_cursor_new (GDK_SB_UP_ARROW);
	
	case GB_C_Hourglass:
		return gdk_cursor_new (GDK_WATCH);

	case GB_C_NoDrop:
		return gdk_cursor_new (GDK_CROSS);
	
	case GB_C_ArrowHourglass:
		return gdk_cursor_new (GDK_CROSS);
	
	case GB_C_ArrowQuestion:
		return gdk_cursor_new (GDK_QUESTION_ARROW);
	
	case GB_C_SizeAll:
		return gdk_cursor_new (GDK_CROSS);
	
	default:
		gbrun_exception_firev (
			ec, "Unknown mouse pointer constant %d", type);
		return NULL;
	}
}

static GBValue *
screen_getarg (GBRunEvalContext *ec,
               GBRunObject      *object,
               int               property)
{
	GBRunScreen *screen = (GBRunScreen *) object;

	switch (property) {

	case MOUSE_POINTER:
		return gb_value_new_int (screen->mousepointer);
	
	case FONT_COUNT: {
		GList *f_names = gnome_font_list ();
		guint  num_fonts = g_list_length (f_names);

		gnome_font_list_free (f_names);

		return gb_value_new_long (num_fonts);
	}

	case WIDTH:
		return gb_value_new_long (gdk_screen_width());

        case HEIGHT:
		return gb_value_new_long (gdk_screen_height());

	default:
		g_warning ("screen: Unhandled property '%d'", property);
		return NULL;
	}
}


static gboolean
screen_setarg (GBRunEvalContext *ec,
	       GBRunObject      *object,
	       int               property,
	       GBValue          *val)
{
	GBRunScreen *screen = (GBRunScreen *) object;
	
	switch (property) {

	case MOUSE_POINTER: {
		GdkCursor *cursor;
		GList     *roots;
		
		if (!(cursor = get_cursor (ec, val->v.i)))
			return FALSE;

		roots = gdk_window_get_toplevels ();
		g_list_foreach (roots, (GFunc) gdk_window_set_cursor,
				cursor);
		g_list_free (roots);

		gdk_cursor_destroy (cursor);
		screen->mousepointer = val->v.i;

		return TRUE;
	}

	default:
		g_warning ("Bad property set %d", property);
		return FALSE;
	}
}

void
gbrun_screen_register (void)
{
	GBObject *gba_object;

	gba_object = gtk_type_new (
		gbrun_screen_get_type ());

	gbrun_global_add (GB_OBJECT (gba_object), "screen");
}

void
gbrun_screen_shutdown (void)
{
}

static void
gbrun_screen_class_init (GBRunObjectClass *klass)
{
	GBRunScreen *screen = (GBRunScreen *) klass;
	
	klass->set_arg = screen_setarg;
	klass->get_arg = screen_getarg;

	gbrun_object_add_property (
		klass, "mousepointer", gb_type_int, MOUSE_POINTER);
	gbrun_object_add_property_full (
		klass, "width", gb_type_int, WIDTH, GBRUN_PROPERTY_READABLE);
	gbrun_object_add_property_full (
		klass, "height", gb_type_int, HEIGHT, GBRUN_PROPERTY_READABLE);
	gbrun_object_add_property_full (
		klass, "fontcount", gb_type_long, FONT_COUNT,
		GBRUN_PROPERTY_READABLE);
	gbrun_object_add_method_arg (
		klass, "func;fonts;a,long;string;n",
		screen_func_fonts);

	screen->mousepointer = GB_C_Default;
}

GtkType
gbrun_screen_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunScreen),
			sizeof (GBRunObjectClass),
			(GtkClassInitFunc)  gbrun_screen_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_OBJECT, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}
