/*
 * Gnome Basic FileListBox implementation.
 *
 * Author:
 *      Matthew Mei    (mei@fas.harvard.edu)
 *
 */
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include "gbrun-form-item.h"

#define ITEM_NAME "gb-vb.filelistbox"

#undef DEBUG_LB


#ifdef PATH_MAX
#	define MAXPATHLEN PATH_MAX
#else
#	define MAXPATHLEN 2048
#endif


enum {
	ARG_FIRST = 0,
	FILENAME,
	PATH,
};

static const char *p_name[] = {
	"Arg_First",
	"Filename",
	"Path"
};

#define GBRUN_FILELISTBOX(obj) (GTK_CHECK_CAST ((obj), gbrun_filelistbox_get_type (), GBRunFileListBox))

typedef struct {
	GBRunFormItem		 item;
	GtkCList		*clist;
	GtkWidget		*sw;
	gchar			 path[2 * MAXPATHLEN];
	gint			 current_row;
	GBRunEvalContext	*ec;
} GBRunFileListBox;


static gboolean
filelistbox_refresh (GBRunFileListBox *filelistbox)
{
	gchar	       path_temp[MAXPATHLEN * 2];
	gint	       count, i;
	DIR	      *dir;
	struct dirent *dirstruct;
	struct stat    fileinfo;

	gtk_clist_clear (filelistbox->clist);

	if (strlen (filelistbox->path) == 0) {
		return FALSE;
	}

	if ((dir = opendir (filelistbox->path)) == NULL) {
		gbrun_exception_firev (filelistbox->ec,
			_("FileListBox: Could not read directory"));
		return FALSE;
	}

	count = 0;

	while ((dirstruct = readdir (dir)) != NULL) {
		count ++;
	}

	rewinddir (dir);
	gtk_clist_freeze (filelistbox->clist);

	for (i =0; i < count; i++) {
		gchar *temp[1];

		if ((dirstruct = readdir (dir)) == NULL) {
			gtk_clist_clear (filelistbox->clist);
			gtk_clist_thaw (filelistbox->clist);
			gbrun_exception_firev (filelistbox->ec,
				_("FileListBox: Something happened while "
				"reading directory"));
			return FALSE;
		}

		if (strcmp (dirstruct->d_name, "..") == 0 ||
		    strcmp (dirstruct->d_name, ".") == 0) {
			continue;
		}

		strcpy (path_temp, filelistbox->path);
		strcat (path_temp, "/");
		strcat (path_temp, dirstruct->d_name);

		if (stat (path_temp, &fileinfo) >= 0 && S_ISDIR (fileinfo.st_mode)) {
			continue;
		}
			
		temp[0] = g_strdup (dirstruct->d_name);
		gtk_clist_append (filelistbox->clist, temp);
	}

	closedir (dir);
	gtk_clist_thaw (filelistbox->clist);

	return FALSE;
}


static void
filelistbox_click_signals (GBRunFileListBox *filelistbox,
			   GdkEventButton   *button)
{
	switch (button->type) {

	case GDK_2BUTTON_PRESS:
		gbrun_form_item_invoke (filelistbox->ec,
			filelistbox->item.widget,
			"_DblClick");
		break;

	default:
		gbrun_form_item_invoke (filelistbox->ec,
			filelistbox->item.widget,
			"_Click");
		break;

	}
}


static void
filelistbox_click (GtkWidget        *dummy,
		   gint              row,
		   gint              column,
		   GdkEventButton   *button,
		   GBRunFileListBox *filelistbox)
{
	filelistbox->current_row = row;
	filelistbox_click_signals (filelistbox, button);
}


static void
filelistbox_unclick (GtkWidget        *dummy,
		     gint              row,
		     gint              discard_column,
		     GdkEventButton   *button,
		     GBRunFileListBox *filelistbox)
{
       filelistbox->current_row = -1;
	filelistbox_click_signals (filelistbox, button);
}


static GBValue *
filelistbox_getarg (GBRunEvalContext *ec,
		   GBRunObject      *object,
		   int		     property)
{
	GBRunFileListBox	*dir = GBRUN_FILELISTBOX (object);
	g_return_val_if_fail (dir != NULL, NULL);

	switch (property) {

	case FILENAME:
	{
		GBValue *to_ret;
		gchar **text = malloc (sizeof *text);

               if (dir->current_row == -1)
			return gb_value_new_string_chars ("");
		if (!gtk_clist_get_text (dir->clist, dir->current_row, 0, text)) {
			free (text);
			gbrun_exception_firev (ec, _("FileListBox: Cannot "
				"retrieve filename"));
		}

		to_ret = gb_value_new_string_chars (*text);
		free (text);
		return to_ret;
	}

	case PATH:
		return gb_value_new_string_chars (dir->path);

	default:
		g_warning ("FileListBox: Get of unhandled property '%s'", p_name[property]);
		return NULL;

	};
}


static gboolean
filelistbox_setarg (GBRunEvalContext *ec,
		   GBRunObject	    *object,
		   int		     property,
		   GBValue	    *val)
{
	GBRunFileListBox	*dir = GBRUN_FILELISTBOX (object);
	g_return_val_if_fail (dir != NULL, FALSE);

	switch (property) {

	case FILENAME:
		g_warning ("FileListBox: setting filename not supported yet");

	case PATH:
		strcpy (dir->path, val->v.s->str);
		filelistbox_refresh (dir);
		return TRUE;

	default:
		g_warning ("FileListBox: Set of unhandled property '%s'", p_name[property]);
		return FALSE;

	}
}


static void
gbrun_listbox_destroy (GtkObject *object)
{
	g_warning ("Unimplemented %s destroy", ITEM_NAME);
}


static void
gbrun_filelistbox_construct (GBRunEvalContext *ec,
			     GBRunFormItem    *item)
{
	GBRunFileListBox *dest = GBRUN_FILELISTBOX (item);

	if (!getcwd (dest->path, MAXPATHLEN)) {
		strncpy (dest->path, "", 1);
	}

	dest->sw = gtk_scrolled_window_new (NULL, NULL);
	gbrun_form_item_set_widget (GBRUN_FORM_ITEM (dest), dest->sw);
	dest->clist = GTK_CLIST (gtk_clist_new (1));
	gtk_container_add (GTK_CONTAINER (dest->sw), GTK_WIDGET (dest->clist));
	gtk_widget_show (GTK_WIDGET (dest->clist));
	dest->ec = ec;
       dest->current_row = -1;
	gtk_clist_set_auto_sort (dest->clist, TRUE);
	/* Actually, in VB6.0 there is never a horizontal scrollbar */
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (dest->sw),
		GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	filelistbox_refresh (dest);

	gtk_signal_connect (GTK_OBJECT (dest->clist), "select-row",
		GTK_SIGNAL_FUNC (filelistbox_click), dest);

	gtk_signal_connect (GTK_OBJECT (dest->clist), "unselect-row",
		GTK_SIGNAL_FUNC (filelistbox_unclick), dest);
}


static void
gbrun_filelistbox_class_init (GBRunObjectClass *klass)
{
	GtkObjectClass     *gtk_class	= (GtkObjectClass *) klass;
	GBRunFormItemClass *form_class	= (GBRunFormItemClass *) klass;

	klass->set_arg = filelistbox_setarg;
	klass->get_arg = filelistbox_getarg;

	gbrun_object_add_property (klass, "filename", gb_type_string, FILENAME);

	gbrun_object_add_property (klass, "path", gb_type_string, PATH);

	form_class->construct = gbrun_filelistbox_construct;
	gtk_class->destroy    = gbrun_listbox_destroy;
}


GtkType
gbrun_filelistbox_get_type ()
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunFileListBox),
			sizeof (GBRunFormItemClass),
			(GtkClassInitFunc)  gbrun_filelistbox_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_FORM_ITEM, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;
}


void
gbrun_filelistbox_register ()
{
	gbrun_filelistbox_get_type ();
}

void gbrun_filelistbox_shutdown ()
{
}
