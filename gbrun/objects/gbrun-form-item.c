/*
 * gbrun-form-item.c
 *
 * Gnome Basic Interpreter Form functions.
 *
 * Authors:
 *	Frank Chiulli  <fc-linux@home.com>
 *      Michael Meeks  <michael@helixcode.com>
 */
#include <gbrun/gbrun-eval.h>

#include "gbrun-form-item.h"
#include "gbrun-form.h"

#define ITEM_NAME "--GB.FormItem--"


enum {
	ARG_FIRST = 0,
	WIDTH,
	HEIGHT,
	TOP,
	LEFT,
	SCALE_HEIGHT,
	SCALE_WIDTH,
	SCALE_MODE,

	VISIBLE,

	INDEX,
	COUNT,

	TABINDEX,
	TAG,
	DRAGMODE,

	FORE_COLOR,
	BACK_COLOR,
	BORDER_COLOR,
	FILL_COLOR,
	MASK_COLOR
};

static const char* p_name[] = {
	"Arg_First",
	"Width",
	"Height",
	"Top",
	"Left",
	"Scale_Height",
	"Scale_Width",
	"Scale_Mode",
	"Visible",
	"Index",
	"Count",
	"Tabindex",
	"Tag",
	"Dragmode",
	"Fore_Color",
	"Back_Color",
	"Border_Color",
	"Fill_Color",
	"Mask_Color"
};

static void
item_key_down (GtkWidget        *textbox,
	       GdkEventKey      *event,
	       GBRunEvalContext *ec)
{
	gbrun_form_item_invoke (ec, textbox, "_KeyDown");
}

static void
item_key_up (GtkWidget        *textbox,
	     GdkEventKey      *event,
	     GBRunEvalContext *ec)
{
	gbrun_form_item_invoke (ec, textbox, "_KeyUp");
}

static void
item_mouse_move (GtkWidget        *button,
		 GdkEventMotion   *event,
		 GBRunEvalContext *ec)
{
	gbrun_form_item_invoke (ec, button, "_MouseMove");
}

static void
item_resize (GtkWidget *item, 
		GdkEventMotion *event, 
		GBRunEvalContext *ec)
{
	gbrun_form_item_invoke (ec, item, "_Resize");
}

static void 
item_paint (GtkWidget *item,
		GdkEventMotion *event,
		GBRunEvalContext *ec)
{
	gbrun_form_item_invoke (ec, item, "_Paint");
}

static gboolean
form_item_setarg (GBRunEvalContext *ec,
		  GBRunObject      *object,
		  int               property,
		  GBValue          *val)
{
	GBRunFormItem	*fi = GBRUN_FORM_ITEM (object);
	GtkWidget	*w  = fi->widget;
	GtkFixed	*fixed;

	g_return_val_if_fail (object != NULL, FALSE);
	g_return_val_if_fail (fi     != NULL, FALSE);
	g_return_val_if_fail (fi->parent != NULL, FALSE);

	if (GBRUN_IS_FORM (fi->parent) ) {
		GBRunForm *form = GBRUN_FORM (fi->parent);
		fixed = form->fixed;
	} else {
		GBRunFormItem      *item  = GBRUN_FORM_ITEM (fi->parent);
		GBRunFormItemClass *klass = GBRUN_FORM_ITEM_CLASS (GTK_OBJECT (item)->klass);

		if (klass->get_fixed) {
			fixed = klass->get_fixed (ec, item);
			if (! fixed) {
				g_warning ("     Cannot find fixed in parent form item.\n");
			}
		}
		
	}
	switch (property) {
	case WIDTH:
		w->allocation.width = GBRUN_FORM_TWIPS_TO_X (val->v.i);
		gtk_widget_set_usize (w, w->allocation.width, w->allocation.height);
		return TRUE;

	case HEIGHT:
		w->allocation.height = GBRUN_FORM_TWIPS_TO_Y (val->v.i);
		gtk_widget_set_usize (w, w->allocation.width, w->allocation.height);
		return TRUE;

	case SCALE_HEIGHT:
		fi->scaleheight = val->v.i;
		return TRUE;
	
	case SCALE_WIDTH:
		fi->scalewidth = val->v.i;
		return TRUE;

	case SCALE_MODE:
		fi->scalemode = val->v.i;
		return TRUE;

	case TOP:
		fi->y = GBRUN_FORM_TWIPS_TO_Y (val->v.i);
		gtk_fixed_move (fixed, fi->widget, fi->x, fi->y);
		return TRUE;

	case LEFT:
		fi->x = GBRUN_FORM_TWIPS_TO_X (val->v.i);
		gtk_fixed_move (fixed, fi->widget, fi->x, fi->y);
		return TRUE;

	case TABINDEX:
		fi->tabindex = val->v.i;
		return TRUE;

	case FORE_COLOR:
		gbrun_form_widget_set_color (fi->widget, GBRUN_FORM_COLOR_FORE, val->v.l);
		return TRUE;

	case BACK_COLOR:
		gbrun_form_widget_set_color (fi->widget, GBRUN_FORM_COLOR_BACK, val->v.l);
		return TRUE;

	case FILL_COLOR:
		gbrun_form_widget_set_color (fi->widget, GBRUN_FORM_COLOR_FILL, val->v.l);
		return TRUE;

	case MASK_COLOR:
		gbrun_form_widget_set_color (fi->widget, GBRUN_FORM_COLOR_MASK, val->v.l);
		return TRUE;

	case BORDER_COLOR:
		gbrun_form_widget_set_color (fi->widget, GBRUN_FORM_COLOR_BORDER, val->v.l);
		return TRUE;

	case VISIBLE:
		if (val->v.bool)
			gtk_widget_show (fi->widget);
		else
			gtk_widget_hide (fi->widget);
		return TRUE;

	case INDEX:
		if (val->v.i < 0)
			return FALSE;

		fi->index = val->v.i;
		return TRUE;

	case COUNT:
		g_warning ("form_item: Can not set COUNT.\n");
		return FALSE;
		
	case TAG:
		fi->tag = g_strdup (val->v.s->str);
		return TRUE;

	case DRAGMODE:
		fi->dragmode = val->v.i;
		return TRUE;

	default:
		g_warning ("form_item: Set of unhandled property '%s'", p_name[property]);
		return FALSE;
	}
}

static GBValue *
form_item_getarg (GBRunEvalContext *ec,
		  GBRunObject      *object,
		  int               property)
{
	GBRunFormItem *fi = GBRUN_FORM_ITEM (object);

	switch (property) {
	case WIDTH: {
		guint i = fi->widget->allocation.width;
		return gb_value_new_int (GBRUN_FORM_X_TO_TWIPS (i));
	}

	case HEIGHT: {
		guint i = fi->widget->allocation.height;
		return gb_value_new_int (GBRUN_FORM_Y_TO_TWIPS (i));
	}

	case TOP:
		return gb_value_new_int (GBRUN_FORM_Y_TO_TWIPS (fi->y));

	case SCALE_HEIGHT:
		return gb_value_new_int (fi->scaleheight);
	
	case SCALE_WIDTH:
		return gb_value_new_int (fi->scalewidth);

	case SCALE_MODE:
		return gb_value_new_int (fi->scalemode);

	case LEFT:
		return gb_value_new_int (GBRUN_FORM_X_TO_TWIPS (fi->x));

	case TABINDEX:
		return gb_value_new_int (fi->tabindex);

	case FORE_COLOR:
		return gb_value_new_long (
			gbrun_form_widget_get_color (fi->widget, GBRUN_FORM_COLOR_FORE));

	case BACK_COLOR:
		return gb_value_new_long (
			gbrun_form_widget_get_color (fi->widget, GBRUN_FORM_COLOR_BACK));

	case FILL_COLOR:
		return gb_value_new_long (
			gbrun_form_widget_get_color (fi->widget, GBRUN_FORM_COLOR_FILL));

	case MASK_COLOR: 
		return gb_value_new_long (
			gbrun_form_widget_get_color (fi->widget, GBRUN_FORM_COLOR_MASK));

	case BORDER_COLOR:
		return gb_value_new_long (
			gbrun_form_widget_get_color (fi->widget, GBRUN_FORM_COLOR_BORDER));

	case VISIBLE:
		return gb_value_new_boolean (GTK_WIDGET_VISIBLE (fi->widget));

	case INDEX:
		return gb_value_new_int (fi->index);

	case COUNT: {
		GBRunFormItem	*item = GBRUN_FORM_ITEM (object);
		GSList		*list;
		int		 cnt;

		if (item->carray) {
			cnt = 1;
			for (list = item->carray; list; list = list->next)
				cnt++;
		} else 
			cnt = -1;

		return gb_value_new_int (cnt);
	}

	case TAG:
		return gb_value_new_string_chars (fi->tag);

	case DRAGMODE:
		return gb_value_new_int (fi->dragmode);

	default:
		g_warning ("form_item: Get of unhandled property '%s'", p_name[property]);
		break;
	}

	return NULL;
}

static GBValue *
form_item_getndx (GBRunEvalContext  *ec,
		  GBRunObject       *object,
		  int		     index)
{
	GBRunFormItem		*item = GBRUN_FORM_ITEM (object);
	GBRunFormItem		*sub_item = NULL;
	GBRunObjectClass	*klass;
	GBRunObjectProperty	*prop;
	GBValue			*val;
	GBValue			*rval = NULL;

	GSList			*list;

	g_return_val_if_fail (object != NULL, NULL);

	klass = GBRUN_OBJECT_GET_CLASS (object);

	prop = gbrun_object_get_property (GBRUN_OBJECT_GET_CLASS (object),
		                          "index", &klass,
		                          GBRUN_PROPERTY_READABLE);
	
	if (!prop) {
		g_warning ("form: No index property found.\n");
		return NULL;
	}

	val = klass->get_arg (ec, object, prop->idx);

	if (val) {
		if (val->v.i == index) {
			sub_item = item;
		
		} else {
			for (list = item->carray; list; list = list->next) {
				sub_item = list->data;
				val = klass->get_arg (ec, GBRUN_OBJECT(sub_item), prop->idx);

			        if (val && (val->v.i == index))
					break;
			}
		}
	}
	
	if (sub_item)
		rval = gb_value_new_object (GB_OBJECT (sub_item));

	return rval;
}

static GBValue *
gbrun_form_item_index (GBRunEvalContext  *ec,
		       GBRunObject	  *object,
		       GBValue	         **args)
{
	int               index;
	GBRunObjectClass *klass     = GBRUN_OBJECT_GET_CLASS (object);

	GB_IS_VALUE (ec, args [0], GB_VALUE_INT);

	index = args [0]->v.i;

	return klass->get_ndx (ec, object, index);
}

void
gbrun_form_item_set_widget (GBRunFormItem *fi, GtkWidget *w)
{
	g_return_if_fail (w != NULL);
	g_return_if_fail (GBRUN_IS_FORM_ITEM (fi));

	fi->widget = w;
	if (w) { /* FIXME: set containees data too ? */
		gtk_object_set_data (GTK_OBJECT (w), GBRUN_FORM_ITEM_KEY, fi);
		gtk_widget_show (w);
	}

	fi->x = 0;
	fi->y = 0;
}

GtkWidget *
gbrun_form_item_get_widget (GBRunFormItem *fi)
{
	g_return_val_if_fail (GBRUN_IS_FORM_ITEM (fi), NULL);
       
	return fi->widget;
}

void
gbrun_form_item_invoke (GBRunEvalContext *ec,
			GtkWidget *w, char *suffix)
{
	char          *fn_name;
	GBRunFormItem *fi;

	g_return_if_fail (w != NULL);
	g_return_if_fail (ec != NULL);
	g_return_if_fail (suffix != NULL);

	fi = gtk_object_get_data (GTK_OBJECT (w), GBRUN_FORM_ITEM_KEY);
	g_return_if_fail (GBRUN_IS_FORM_ITEM (fi));

	fn_name = g_strconcat (fi->name, suffix, NULL);

	if (GBRUN_IS_FORM (fi->parent)) {
		gbrun_form_invoke (ec, GBRUN_FORM (fi->parent), fn_name, fi);

	} else if (GBRUN_IS_FORM_ITEM (fi->parent)) {
		GBRunFormItem	*fi2;
		
		fi2 = GBRUN_FORM_ITEM (fi->parent);
		if (GBRUN_IS_FORM (fi2->parent)) {
			gbrun_form_invoke (ec, GBRUN_FORM (fi2->parent), fn_name, fi);
		}
	}
	
	g_free (fn_name);
}

static GBValue *
form_item_show (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	GBRunFormItem *fi = GBRUN_FORM_ITEM (object);

	gtk_widget_show (fi->widget);

	return gb_value_new_empty ();
}

static GBValue *
form_item_hide (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	GBRunFormItem *fi = GBRUN_FORM_ITEM (object);

	gtk_widget_hide (fi->widget);

	return gb_value_new_empty ();
}

void
gbrun_form_item_register (void)
{
	gbrun_form_item_get_type ();
}

void
gbrun_form_item_shutdown (void)
{
}

/**
 * gbrun_form_item_add:
 * @ec: 
 * @item: 
 * @to_form:
 * 
 * Instructs a form item (or derative) to add
 * itself to the form.
 **/
void
gbrun_form_item_add (GBRunEvalContext *ec,
		     GBRunFormItem    *item,
		     GBRunForm        *to_form,
		     const char       *name)
{
	GBRunFormItemClass *klass = GBRUN_FORM_ITEM_CLASS (GTK_OBJECT (item)->klass);

	if (klass->add)
		klass->add (ec, item, to_form, name);
}

/**
 * gbrun_form_subitem_add:
 * @ec: 
 * @subitem:
 * @item: 
 * @to_form:
 * 
 * Instructs a form subitem to add itself to the form item (parent).
 **/
void
gbrun_form_subitem_add (GBRunEvalContext *ec,
		        GBRunFormItem    *subitem,
		        GBRunFormItem    *item,
		        GBRunForm        *to_form,
		        const char       *name)
{
	GBRunFormItemClass *klass = GBRUN_FORM_ITEM_CLASS (GTK_OBJECT (subitem)->klass);

	if (klass->addsub)
		klass->addsub (ec, subitem, item, to_form, name);
}

/**
 * gbrun_form_item_add_virtual:
 * @ec: 
 * @item: 
 * @to_form: 
 * 
 * The default virtual add function
 **/
void
gbrun_form_item_add_virtual (GBRunEvalContext *ec,
			     GBRunFormItem *item,
			     GBRunForm *to_form,
			     const char *name)
{
	gbrun_form_add (to_form, item, name);
}

/**
 * gbrun_form_subitem_add_virtual:
 * @ec: 
 * @item: 
 * @to_form: 
 * 
 * The default virtual add subitem function
 **/
void
gbrun_form_subitem_add_virtual (GBRunEvalContext *ec,
			        GBRunFormItem    *subitem,
			        GBRunFormItem    *item,
			        GBRunForm        *to_form,
			        const char       *name)
{
	g_return_if_fail (subitem != NULL);
	g_return_if_fail (item    != NULL);
	g_return_if_fail (name    != NULL);
	g_return_if_fail (to_form != NULL);

	subitem->parent = GBRUN_OBJECT (item);
	subitem->name   = g_strdup (name);
	
	/* FIXME : Is something supposed to be done with to_form ? */

}

/**
 * gbrun_form_item_get_fixed_virtual:
 * @ec: 
 * @item: 
 * 
 **/
GtkFixed *
gbrun_form_item_get_fixed_virtual (GBRunEvalContext *ec,
			           GBRunFormItem    *item)
{
	return NULL;
}

/**
 * gbrun_form_item_get_radio_btn_virtual:
 * @ec: 
 * @item: 
 * 
 **/
gboolean
gbrun_form_item_get_radio_btn_virtual (GBRunEvalContext *ec,
			               GBRunFormItem    *item,
				       GtkWidget        **radio_btn)
{
	return FALSE;
}

/**
 * gbrun_form_item_set_radio_btn_virtual:
 * @ec: 
 * @item: 
 * 
 **/
void
gbrun_form_item_set_radio_btn_virtual (GBRunEvalContext *ec,
			               GBRunFormItem    *item,
				       GtkWidget        *radio_btn)
{
	g_warning ("gbrun_form_item_set_radio_btn_virtual: should never be called.");
	return;
}

/**
 * gbrun_form_item_new
 *  @ec
 * @type
 **/
GBRunFormItem *
gbrun_form_item_new (GBRunEvalContext *ec,
		     GtkType           type)
{
	GBRunFormItem *item;
	
	item = gtk_type_new (type);
	if (!item)
		return NULL;

	if (GBRUN_IS_FORM_ITEM (item)) {
		GBRunFormItemClass *klass;

		klass = GBRUN_FORM_ITEM_CLASS (GTK_OBJECT (item)->klass);

		if (klass->construct)
			klass->construct (ec, item);

      	  	gtk_signal_connect (GTK_OBJECT (item->widget), "motion-notify-event", 
					(GtkSignalFunc) item_mouse_move, ec);

        	gtk_signal_connect (GTK_OBJECT (item->widget), "key-press-event", 
					(GtkSignalFunc) item_key_down, ec);

        	gtk_signal_connect (GTK_OBJECT (item->widget), "key-release-event", 
					(GtkSignalFunc) item_key_up, ec);
		
			gtk_signal_connect (GTK_OBJECT (item->widget), "size-request",
					(GtkSignalFunc) item_resize, ec);
		
			gtk_signal_connect (GTK_OBJECT (item->widget), "draw",
					(GtkSignalFunc) item_paint, ec);

	}

	return item;
}

static void
gbrun_form_item_destroy (GtkObject *object)
{
	g_warning ("Unimplemented %s destruct", ITEM_NAME);
}

static void
gbrun_form_item_instance_init (GBRunFormItem *fi)
{
	fi->x = fi->y = 0;
	fi->parent = NULL;
	fi->widget = NULL;
	fi->tabindex = -1;
	fi->index = -1;
	fi->carray = NULL;
}

static void
gbrun_form_item_class_init (GBRunObjectClass *klass)
{
	GtkObjectClass     *gtk_class    = (GtkObjectClass *) klass;
	GBRunFormItemClass *form_class   = (GBRunFormItemClass *) klass;

	klass->set_arg = form_item_setarg;
	klass->get_arg = form_item_getarg;
	klass->get_ndx = form_item_getndx;;

	gbrun_object_add_property (klass, "width",       gb_type_int, WIDTH);
	gbrun_object_add_property (klass, "height",      gb_type_int, HEIGHT);
	gbrun_object_add_property (klass, "top",         gb_type_int, TOP);
	gbrun_object_add_property (klass, "left",        gb_type_int, LEFT);
	gbrun_object_add_property (klass, "scaleheight", gb_type_int, SCALE_HEIGHT);
	gbrun_object_add_property (klass, "scalewidth",  gb_type_int, SCALE_WIDTH);
	gbrun_object_add_property (klass, "scalemode",   gb_type_int, SCALE_MODE);

	gbrun_object_add_property (klass, "tabindex", gb_type_int,     TABINDEX);
	gbrun_object_add_property (klass, "index",    gb_type_int,     INDEX);
	gbrun_object_add_property (klass, "count",    gb_type_int,     COUNT);
	gbrun_object_add_property (klass, "visible",  gb_type_boolean, VISIBLE);
	gbrun_object_add_property (klass, "tag",      gb_type_string,  TAG);
	gbrun_object_add_property (klass, "dragmode", gb_type_int,     DRAGMODE);

	gbrun_object_add_property (klass, "ForeColor",   gb_type_long, FORE_COLOR);
	gbrun_object_add_property (klass, "BackColor",   gb_type_long, BACK_COLOR);
	gbrun_object_add_property (klass, "BorderColor", gb_type_long, BORDER_COLOR);
	gbrun_object_add_property (klass, "FillColor",   gb_type_long, FILL_COLOR);
	gbrun_object_add_property (klass, "MaskColor",   gb_type_long, MASK_COLOR);
	
	gbrun_object_add_method_arg (klass, "sub;show;g", form_item_show);
	gbrun_object_add_method_arg (klass, "sub;hide;g", form_item_hide);
	gbrun_object_add_method_arg (klass, "func;item;index,integer;variant;n",
		                            gbrun_form_item_index);


	form_class->add           = gbrun_form_item_add_virtual;
	form_class->addsub        = gbrun_form_subitem_add_virtual;
	form_class->get_fixed     = gbrun_form_item_get_fixed_virtual;
	form_class->get_radio_btn = gbrun_form_item_get_radio_btn_virtual;
	form_class->set_radio_btn = gbrun_form_item_set_radio_btn_virtual;
	
	gtk_class->destroy        = gbrun_form_item_destroy;
}

GtkType
gbrun_form_item_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunFormItem),
			sizeof (GBRunFormItemClass),
			(GtkClassInitFunc)  gbrun_form_item_class_init,
			(GtkObjectInitFunc) gbrun_form_item_instance_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_OBJECT, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}
