/*
 * gbrun-frame.c
 *
 * Gnome Basic Interpreter Frame functions.
 *
 * Author:
 *	Thomas Meeks (thomas@imaginator.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include "gbrun-form-item.h"

#define ITEM_NAME "gb-vb.frame"

#undef DEBUG_FR


/*
 * Frame Properties
 */
enum {
	ARG_FIRST = 0,
	ALIGNMENT,
	CAPTION
};

#define GBRUN_FRAMED(obj) (GTK_CHECK_CAST ((obj), gbrun_frame_get_type (), GBRunFramed))

/*
 *  Function Prototypes.
 */
static GBValue *frame_getarg (GBRunEvalContext *ec,
			      GBRunObject      *object,
			      int		property);

static gboolean frame_setarg (GBRunEvalContext *ec,
			      GBRunObject      *object,
			      int	        property,
			      GBValue	      *val);

static GtkFixed *gbrun_frame_get_fixed (GBRunEvalContext *ec,
				        GBRunFormItem    *item);

static gboolean gbrun_frame_get_radio_btn (GBRunEvalContext *ec,
				           GBRunFormItem    *item,
				           GtkWidget        **radio_btn);

static void gbrun_frame_set_radio_btn (GBRunEvalContext *ec,
				       GBRunFormItem    *item,
				       GtkWidget        *radio_btn);

/*
 * Note: GBRunFrame already exists in ../gbrun.h
 */
typedef struct {
	GBRunFormItem		 item;

	GtkFixed		*fixed;
	GtkJustification	 alignment;
	GtkWidget		*radio_btn;
} GBRunFramed;


/**
 * gbrun_frame_destroy
 *   @object
 **/
static void
gbrun_frame_destroy (GtkObject *object)
{
	g_warning ("gbrun-frame: Unimplemented %s destroy", ITEM_NAME);
}


/**
 * gbrun_frame_construct
 *   @ec
 *   @item
 **/
static void
gbrun_frame_construct (GBRunEvalContext *ec,
		       GBRunFormItem    *item)
{
	GtkWidget   *w;
	GBRunFramed *dest = GBRUN_FRAMED (item);

#ifdef DEBUG_FR
	printf ("Enter gbrun_frame_construct.\n");
#endif
	w = gtk_frame_new ("");
	gbrun_form_item_set_widget (GBRUN_FORM_ITEM (dest), w);
	
	/* Makes it look like a standard windows frame */
	gtk_frame_set_shadow_type (GTK_FRAME (w), GTK_SHADOW_ETCHED_IN);

	dest->alignment = GTK_JUSTIFY_LEFT;
	gtk_frame_set_label_align (GTK_FRAME (w), GTK_JUSTIFY_LEFT, 0.2);

	dest->fixed = GTK_FIXED (gtk_fixed_new ());
	gtk_container_add (GTK_CONTAINER (w), GTK_WIDGET (dest->fixed));
#ifdef DEBUG_FR
	printf ("      fixed is at %#x\n", dest->fixed);
#endif
	
	dest->radio_btn = NULL;
}


/**
 * gbrun_frame_class_init
 *   @klass
 *
 **/
void
gbrun_frame_class_init (GBRunObjectClass *klass)
{
	GtkObjectClass		 *gtk_class = (GtkObjectClass *) klass;
	GBRunFormItemClass	*form_class = (GBRunFormItemClass *) klass;

#ifdef DEBUG_FR
	printf ("Enter gbrun_frame_class_init.\n");	
#endif
	
	klass->set_arg = frame_setarg;
	klass->get_arg = frame_getarg;

	gbrun_object_add_property (klass, "alignment", gb_type_int,    ALIGNMENT);

	gbrun_object_add_property (klass, "caption",   gb_type_string, CAPTION);

	form_class->construct     = gbrun_frame_construct;

	form_class->get_fixed     = gbrun_frame_get_fixed;
	form_class->get_radio_btn = gbrun_frame_get_radio_btn;
	form_class->set_radio_btn = gbrun_frame_set_radio_btn;

	gtk_class->destroy        = gbrun_frame_destroy;

}


/**
 * gbrun_frame_get_type
 *
 **/
GtkType
gbrun_frame_get_type (void)
{
	static GtkType object_type = 0;

#ifdef DEBUG_FR
	printf ("Enter gbrun_frame_get_type.\n");
#endif
	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunFramed),
			sizeof (GBRunFormItemClass),
			(GtkClassInitFunc)  gbrun_frame_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_FORM_ITEM, &object_info);
		gtk_type_class (object_type);
#ifdef DEBUG_FR
		printf ("gbrun_frame_get_type: object_type = %d\n", object_type);
#endif
	}
		
	return object_type;
}


/**
 * frame_setarg:
 *   @ec
 *   @object
 *   @property
 *   @val
 **/
static gboolean
frame_setarg (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      int		property,
	      GBValue          *val)
{
	GBRunFramed *frame = GBRUN_FRAMED (object);
	GtkFrame   *w      = GTK_FRAME (
	                     gbrun_form_item_get_widget (GBRUN_FORM_ITEM  (object)));

#ifdef DEBUG_FR
	printf ("Enter frame_setarg\n");
#endif
	g_return_val_if_fail (frame != NULL, FALSE);

	switch (property) {
	case ALIGNMENT:
	{
#ifdef DEBUG_FR
		printf ("      ALIGNMENT\n");
#endif
		switch (val->v.i) {
			case VB_LEFT_JUSTIFY:
				frame->alignment = GTK_JUSTIFY_LEFT;
				gtk_frame_set_label_align (w, GTK_JUSTIFY_LEFT, 0.2);
				return TRUE;

			case VB_RIGHT_JUSTIFY:
				frame->alignment = GTK_JUSTIFY_RIGHT;
				gtk_frame_set_label_align (w, GTK_JUSTIFY_RIGHT, 0.2);
				return TRUE;

			case VB_CENTER_JUSTIFY:
				frame->alignment = GTK_JUSTIFY_CENTER;
				gtk_frame_set_label_align (w, GTK_JUSTIFY_CENTER, 0.2);
				return TRUE;

			default:
				g_warning ("gbrun-frame: Unhandled alignment: %d",
				           val->v.i);
				return FALSE;
		}
	}

	case CAPTION: {
#ifdef DEBUG_FR
		printf ("      CAPTION\n");
#endif
		gtk_frame_set_label (w, val->v.s->str);
		return TRUE;
	}

	default:
		g_warning ("gbrun-frame: Unhandled property '%d'", property);
		return FALSE;
	}
}


/**
 * frame_getarg:
 *   @ec
 *   @object
 *   @property
 **/
static GBValue *
frame_getarg (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      int		property)
{
	GBRunFramed *frame = GBRUN_FRAMED (object);
	GtkFrame   *w      = GTK_FRAME (
	                     gbrun_form_item_get_widget (GBRUN_FORM_ITEM (object)));

#ifdef DEBUG_FR
	printf ("Enter frame_getarg.\n");
#endif
	g_return_val_if_fail (frame != NULL, NULL);

	switch (property) {
	case ALIGNMENT:
		return gb_value_new_int (w->label_xalign);
		
	case CAPTION:
		return gb_value_new_string_chars (w->label);

	default:
		g_warning ("gbrun-frame: Unhandled property '%d'", property);
	        return NULL;
	}
}

/**
 * gbrun_frame_get_fixed
 *   @ec
 *   @item
 **/
static GtkFixed *gbrun_frame_get_fixed (GBRunEvalContext *ec,
				        GBRunFormItem    *item)
{
	GBRunFramed *frame = GBRUN_FRAMED (item);

#ifdef DEBUG_FR
	printf ("Enter gbrun_frame_get_fixed.\n");
#endif
	return frame->fixed;
}

/**
 * gbrun_frame_get_radio_btn
 *   @ec
 *   @item
 *   @radio_btn
 **/
static gboolean gbrun_frame_get_radio_btn (GBRunEvalContext *ec,
				           GBRunFormItem    *item,
				           GtkWidget        **radio_btn)
{
	GBRunFramed *frame = GBRUN_FRAMED (item);

#ifdef DEBUG_FR
	printf ("Enter gbrun_frame_get_radio_btn.\n");
#endif
	*radio_btn = frame->radio_btn;
	
	return TRUE;
}

/**
 * gbrun_frame_set_radio_btn
 *   @ec
 *   @item
 *   @radio_btn
 **/
static void gbrun_frame_set_radio_btn (GBRunEvalContext *ec,
				       GBRunFormItem    *item,
				       GtkWidget        *radio_btn)
{
	GBRunFramed *frame = GBRUN_FRAMED (item);

#ifdef DEBUG_FR
	printf ("Enter gbrun_frame_set_radio_btn.\n");
#endif
	frame->radio_btn = radio_btn;
	
	return;
}

/**
 * gbrun_frame_register
 *
 **/
void
gbrun_frame_register ()
{
	gbrun_frame_get_type ();
}


/**
 * gbrun_frame_shutdown
 *
 **/
void
gbrun_frame_shutdown ()
{
}
