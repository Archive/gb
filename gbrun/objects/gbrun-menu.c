/*
 * gbrun-menu.c
 *
 * Gnome Basic Interpreter Menu functions.
 *
 * Author(s):
 *   Almer S. Tigelaar
 *
 * Copyright 2000, Almer S. Tigelaar
 */

#include <gdk/gdkkeysyms.h>
#include "gbrun-form-item.h"

#define ITEM_NAME "gb-vb.menu"

enum {
	ARG_FIRST = 0,
	CAPTION,
	TOOLTIP_TEXT,
	ENABLED,
	DEFAULT
};

static const char* p_name[] = {
	"Arg_First",
	"Caption",
	"ToolTip_Text",
	"Enabled",
	"Default"
};

#define GBRUN_MENU(obj) (GTK_CHECK_CAST ((obj), gbrun_menu_get_type (), GBRunMenu))

typedef struct {
	GBRunFormItem	 item;

	GtkWidget	*submenu;
	char		*caption;
} GBRunMenu;

/*************************************************************
 * SIGNAL HANDLERS
 *************************************************************/

static void 
menu_click (GtkWidget *menu, GBRunEvalContext *ec)
{
	gbrun_form_item_invoke (ec, menu, "_Click");
}


/*************************************************************
 * CORE
 *************************************************************/

static gboolean
menu_setarg (GBRunEvalContext *ec,
	     GBRunObject      *object,
	     int               property,
	     GBValue          *val)
{
	GBRunMenu      *menu     = GBRUN_MENU (object);
	GtkMenuItem    *menuitem = GTK_MENU_ITEM (gbrun_form_item_get_widget (GBRUN_FORM_ITEM (object)));
	GtkLabel       *label    = GTK_LABEL (GTK_BIN (menuitem)->child);
	GtkWidget      *parent   = GTK_WIDGET (menuitem)->parent;
	
	g_return_val_if_fail (menu != NULL, FALSE);
	g_return_val_if_fail (menuitem != NULL, FALSE);
	g_return_val_if_fail (label != NULL, FALSE);

	switch (property) {
	case CAPTION: {
		char *txt;
		guint keyval;

		/*
		 * Free the caption if it was set before
		 */
		if (menu->caption)
			g_free (menu->caption);
			
		menu->caption = g_strdup (val->v.s->str);

		txt = gbrun_form_un_shortcutify (menu->caption, NULL);
		gtk_label_set_text (label, txt);

		keyval = gtk_label_parse_uline (GTK_LABEL (label), txt);

		if (keyval != GDK_VoidSymbol) {
			if (GTK_IS_MENU (parent))
				gtk_widget_add_accelerator (
					GTK_WIDGET (menuitem),
					"activate_item",
					gtk_menu_ensure_uline_accel_group (
						GTK_MENU (parent)),
					keyval, 0, 0);

			else if (GTK_IS_MENU_BAR (parent) &&
				 menu->item.parent)
				gtk_widget_add_accelerator (
					GTK_WIDGET (menuitem),
					"activate_item",
					GBRUN_FORM (menu->item.parent)->accel_group,
					keyval, GDK_MOD1_MASK, 0);
			else
				g_warning ("Adding accelerator went bananas");
		}

		g_free (txt);

		return TRUE;
	}

	case ENABLED:
	{
		gtk_widget_set_sensitive (GTK_WIDGET (menuitem),
					  val->v.bool);
		return TRUE;
	}

	default:
		g_warning ("menu: Unhandled property '%s'", p_name[property]);
		return FALSE;
	}
}

static GBValue *
menu_getarg (GBRunEvalContext *ec,
	     GBRunObject      *object,
	     int               property)
{
	GBRunMenu      *menu     = GBRUN_MENU (object);
	GtkMenuItem    *menuitem = GTK_MENU_ITEM (gbrun_form_item_get_widget (GBRUN_FORM_ITEM (object)));
	GtkLabel       *label    = GTK_LABEL (GTK_BIN (menuitem)->child);

	g_return_val_if_fail (menu != NULL, NULL);
	g_return_val_if_fail (menuitem != NULL, NULL);
	g_return_val_if_fail (label != NULL, NULL);
	
	switch (property) {
	case CAPTION:
		return gb_value_new_string_chars (menu->caption);

	case ENABLED:
		return gb_value_new_boolean (GTK_WIDGET_SENSITIVE (menuitem));

	default:
		g_warning ("menu: Unhandled property '%s'", p_name[property]);
		break;
	}

	return NULL;
}

static void
gbrun_menu_destroy (GtkObject *object)
{
	GBRunMenu *menu = GBRUN_MENU (object);

	if (menu->caption)
		g_free (menu->caption);
}

static void
gbrun_menu_add (GBRunEvalContext *ec,
		GBRunFormItem *item,
		GBRunForm *to_form,
		const char *name)
{
	GtkMenuBar *menubar = to_form->menubar;
	GtkMenuItem *menuitem = GTK_MENU_ITEM (item->widget);

	item->parent = GBRUN_OBJECT (to_form);
	item->name   = g_strdup (name);
	
	/* Attach it to the menubar */
	gtk_menu_bar_append (menubar, GTK_WIDGET (menuitem));
	gtk_widget_show_all (GTK_WIDGET (menuitem));
	
	gtk_widget_show (GTK_WIDGET (menubar));

}

static void
gbrun_menu_addsub (GBRunEvalContext *ec,
		   GBRunFormItem    *subitem,
		   GBRunFormItem    *item,
		   GBRunForm        *to_form,
		   const char       *name)
{
	GBRunMenu	*dest = GBRUN_MENU (item);

	subitem->parent = GBRUN_OBJECT (item);
	subitem->name   = g_strdup (name);

	if (! dest->submenu) {
		dest->submenu = gtk_menu_new();
		gtk_menu_item_set_submenu (GTK_MENU_ITEM (item->widget), dest->submenu);
		gtk_widget_show (dest->submenu);
	}

	gtk_menu_append (GTK_MENU (dest->submenu), subitem->widget);
	gtk_widget_show_all (subitem->widget);
}

static void
gbrun_menu_construct (GBRunEvalContext *ec, GBRunFormItem *item)
{
	GBRunMenu	*dest = GBRUN_MENU (item);
	GtkWidget	*menuitem;

	/* We create a new menuitem */
	menuitem = gtk_menu_item_new_with_label ("");
	gbrun_form_item_set_widget (GBRUN_FORM_ITEM (dest), menuitem);

	dest->submenu = NULL;

	/* Connect some signal handlers */
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    (GtkSignalFunc) menu_click, ec);
}

static void
gbrun_menu_class_init (GBRunObjectClass *klass)
{
	GtkObjectClass     *gtk_class    = (GtkObjectClass *) klass;
	GBRunFormItemClass *form_class   = (GBRunFormItemClass *) klass;

	klass->set_arg = menu_setarg;
	klass->get_arg = menu_getarg;

	gbrun_object_add_property (klass, "caption", 
				   gb_type_string, CAPTION);
        gbrun_object_add_property (klass, "enabled",
				   gb_type_boolean, ENABLED);

	form_class->construct = gbrun_menu_construct;
	form_class->add       = gbrun_menu_add;
	form_class->addsub    = gbrun_menu_addsub;

	gtk_class->destroy = gbrun_menu_destroy;
}

GtkType
gbrun_menu_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunMenu),
			sizeof (GBRunFormItemClass),
			(GtkClassInitFunc)  gbrun_menu_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_FORM_ITEM, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}

void
gbrun_menu_register ()
{
	gbrun_menu_get_type ();
}


void
gbrun_menu_shutdown ()
{
}
