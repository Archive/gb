/*
 * GNOME Basic Text Box.
 *
 * Author:
 *	Frank Chiulli (fc-linux@home.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */
#include "gbrun-form-item.h"

#define ITEM_NAME "gb-vb.textbox"


enum {
	ARG_FIRST = 0,
	LOCKED,
	TOOLTIP_TEXT,
	TEXT,
	ALIGNMENT,
	MAX_LENGTH, 
	PASSWORD_CHAR,
	MULTILINE
};


static const char* p_name[] = {
	"Arg_First",
	"Locked",
	"Tooltip_Text",
	"Text",
	"Alignment",
	"MaxLength",
	"PasswordChar",
	"Multiline"
};

#define GBRUN_TEXTBOX(obj) (GTK_CHECK_CAST ((obj), gbrun_textbox_get_type (), GBRunTextBox))

typedef struct {
	GBRunFormItem   item;

	gboolean        locked;
	gboolean        multiline;
	gboolean	password; /* Will change w/ GTK+ 2.0 */
	guint16 	max;

} GBRunTextBox;

/**
 * Event Handlers
 **/

static void
textbox_change (GtkWidget *textbox, GBRunEvalContext *ec)
{
	gbrun_form_item_invoke (ec, textbox, "_Change");
}

/**
 * textbox_setarg
 **/
static gboolean
textbox_setarg (GBRunEvalContext *ec,
		GBRunObject      *object,
		int               property,
		GBValue          *val)
{
	GBRunTextBox *textbox = GBRUN_TEXTBOX (object);
	GtkEntry     *entry = GTK_ENTRY (
		gbrun_form_item_get_widget (GBRUN_FORM_ITEM (object)));

	g_return_val_if_fail (textbox != NULL, FALSE);

	switch (property) {
	case LOCKED:
		textbox->locked = (val->v.i == VB_TRUE);
		gtk_editable_set_editable (GTK_EDITABLE (entry), 
					   textbox->locked);
		return TRUE;

	case TOOLTIP_TEXT: {
		GtkTooltips *t = gtk_tooltips_new();
		gtk_tooltips_set_tip (t, GTK_WIDGET (entry), 
		                      val->v.s->str, NULL);
		return TRUE;
	}

	case TEXT: 
		gtk_editable_set_editable (GTK_EDITABLE (entry), TRUE);
		gtk_entry_set_text (entry, val->v.s->str);
		gtk_editable_set_editable (GTK_EDITABLE (entry), textbox->locked);
		return TRUE;

	case ALIGNMENT: {
		static gboolean warned = FALSE;
		if (!warned) {
			g_warning ("textbox: GtkEntry alignment unsupported");
			warned = TRUE;
		}
		
		return TRUE;
	}

	case MULTILINE:
		textbox->multiline = (val->v.i == VB_TRUE);
		return TRUE;

	case MAX_LENGTH:
		gtk_entry_set_max_length (entry, val->v.i);
		return TRUE;

	case PASSWORD_CHAR: {
		GString *s;
		if (textbox->multiline)
			return TRUE;
		s = val->v.s;
		gtk_entry_set_visibility (entry, s->len ? FALSE : TRUE);
		textbox->password = !!s->len;
		return TRUE;
	}

	default:
		g_warning ("textbox: Set of unhandled property '%s'", p_name[property]);
		return FALSE;
	}
}


/**
 * textbox_getarg:
 *   @ec
 *   @object
 *   @property
 **/
static GBValue *
textbox_getarg (GBRunEvalContext *ec,
		GBRunObject      *object,
		int               property)
{
	GBRunTextBox *textbox = GBRUN_TEXTBOX (object);
	GtkEntry     *entry = GTK_ENTRY (
		gbrun_form_item_get_widget (GBRUN_FORM_ITEM (object)));

	g_return_val_if_fail (textbox != NULL, NULL);

	switch (property) {
	case LOCKED:
		return gb_value_new_boolean (textbox->locked);

	case TEXT:
		return gb_value_new_string_chars (
			gtk_entry_get_text (entry));
		
	case MULTILINE:
		return gb_value_new_boolean (textbox->multiline);

	case MAX_LENGTH:
		return gb_value_new_int (entry->text_max_length);

	case PASSWORD_CHAR:
		if (textbox->password)
			return gb_value_new_string_chars ("*");
		else
			return gb_value_new_string_chars ("");

	default:
		g_warning ("textbox: get of unhandled property '%s'", p_name[property]);
		return NULL;
	}
}

static void
gbrun_textbox_construct (GBRunEvalContext *ec, GBRunFormItem *item)
{

	GtkWidget *w;

	GBRunTextBox *dest = GBRUN_TEXTBOX (item);
	
	w = gtk_entry_new ();
	gbrun_form_item_set_widget (item, w);
	dest->locked = FALSE;
	
	gtk_signal_connect (GTK_OBJECT (w), "changed", 
			    (GtkSignalFunc) textbox_change, ec);

}

static void
gbrun_textbox_class_init (GBRunObjectClass *klass)
{
	GBRunFormItemClass *form_class   = (GBRunFormItemClass *) klass;

	klass->set_arg = textbox_setarg;
	klass->get_arg = textbox_getarg;

	gbrun_object_add_property (klass, "locked", 
				   gb_type_int, LOCKED);

	gbrun_object_add_property (klass, "tooltiptext",
				   gb_type_string, TOOLTIP_TEXT);

        gbrun_object_add_property (klass, "text",
				   gb_type_string, TEXT);

	gbrun_object_add_property (klass, "alignment",
				   gb_type_int, ALIGNMENT);

	gbrun_object_add_property (klass, "maxlength",
				   gb_type_int, MAX_LENGTH);

	gbrun_object_add_property (klass, "passwordchar",
				   gb_type_string, PASSWORD_CHAR);

	gbrun_object_add_property (klass, "multiline",
				   gb_type_int, MULTILINE);

	form_class->construct = gbrun_textbox_construct;
}

GtkType
gbrun_textbox_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunTextBox),
			sizeof (GBRunFormItemClass),
			(GtkClassInitFunc)  gbrun_textbox_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_FORM_ITEM, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}

/**
 * gbrun_textbox_register
 *
 **/
void
gbrun_textbox_register ()
{
	gbrun_textbox_get_type ();	
}


/**
 * gbrun_textbox_shutdown
 *
 **/
void
gbrun_textbox_shutdown ()
{
}
