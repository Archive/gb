/*
 * gbrun-objects.c
 *
 * Gnome Basic Interpreter object registration.
 *
 * Author:
 *	Michael Meeks (mmeeks@gnu.org)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <gbrun/gbrun.h>

#include "gbrun-form.h"
#include "gbrun-form-item.h"
#include "gbrun-objects.h"

void
gbrun_objects_register (GBEvalContext *ec)
{
	gbrun_form_register ();
	gbrun_form_item_register ();
	gbrun_hscrollbar_register ();
	gbrun_cmdbutton_register ();
	gbrun_textbox_register ();
	gbrun_label_register ();
	gbrun_checkbox_register();
	gbrun_menu_register ();
	gbrun_timer_register ();
	gbrun_picturebox_register ();
	gbrun_listbox_register ();
	gbrun_frame_register ();
	gbrun_screen_register ();
	gbrun_combobox_register ();
	gbrun_optbutton_register ();
	gbrun_shape_register ();
	gbrun_vscrollbar_register ();
	gbrun_filelistbox_register ();
}

void
gbrun_objects_shutdown (void)
{
	gbrun_shape_shutdown ();
	gbrun_optbutton_shutdown ();
	gbrun_hscrollbar_shutdown ();
	gbrun_combobox_shutdown ();
	gbrun_screen_shutdown ();
	gbrun_frame_shutdown ();
	gbrun_listbox_shutdown ();
	gbrun_checkbox_shutdown ();
	gbrun_picturebox_shutdown ();
	gbrun_timer_shutdown ();
	gbrun_menu_shutdown ();
	gbrun_textbox_shutdown ();
	gbrun_label_shutdown ();
	gbrun_cmdbutton_shutdown ();
	gbrun_form_item_shutdown ();
	gbrun_form_shutdown ();
	gbrun_vscrollbar_shutdown ();
	gbrun_filelistbox_shutdown ();
}
