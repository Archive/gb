/*
 * gbrun-picturebox.c
 *
 * Gnome Basic Interpreter PictureBox functions.
 *
 * Author:
 *	Thomas Meeks (thomas@imaginator.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */
#include "gbrun-form-item.h"
#include <gnome.h>

#define ITEM_NAME "gb-vb.picturebox"


enum {
	ARG_FIRST = 0,
	PICTURE
};

#define GBRUN_PICTUREBOX(obj) (GTK_CHECK_CAST ((obj), gbrun_picturebox_get_type (), GBRunPictureBox))

typedef struct {
	GBRunFormItem   item;
} GBRunPictureBox;

/**
 * picturebox_setarg
 *
 **/
static gboolean
picturebox_setarg (GBRunEvalContext *ec,
		   GBRunObject      *object,
		   int               property,
		   GBValue          *val)
{
	GBRunPictureBox *picturebox = GBRUN_PICTUREBOX (object);
	GnomePixmap     *w = GNOME_PIXMAP (
		gbrun_form_item_get_widget (GBRUN_FORM_ITEM (object)));

	g_return_val_if_fail (picturebox != NULL, FALSE);

	switch (property) {

	case PICTURE:
		gnome_pixmap_load_file (w, val->v.s->str);
		return TRUE;

	default:
		g_warning ("picturebox: Unhandled property '%d'", property);
		return FALSE;
	}
}


/**
 * picturebox_getarg:
 *   @ec
 *   @object
 *   @property
 **/
static GBValue *
picturebox_getarg (GBRunEvalContext *ec,
		   GBRunObject      *object,
		   int               property)
{
	GBRunPictureBox *picturebox = GBRUN_PICTUREBOX (object);

	g_return_val_if_fail (picturebox != NULL, NULL);

	switch (property) {
	case PICTURE:
		return gb_value_new_string_chars ("It's A Picture!!");

	default:
		g_warning ("picturebox: Unhandled property '%d'", property);
		return NULL;
	}
}

static void
gbrun_picturebox_construct (GBRunEvalContext *ec, GBRunFormItem *item)
{
	GtkWidget  *w;

	w = gnome_pixmap_new_from_file ("tmp.png");
	g_assert (w != NULL);
	gbrun_form_item_set_widget (item, w);
}

static void
gbrun_picturebox_class_init (GBRunObjectClass *klass)
{
	GBRunFormItemClass *form_class   = (GBRunFormItemClass *) klass;

	klass->set_arg = picturebox_setarg;
	klass->get_arg = picturebox_getarg;

	gbrun_object_add_property (klass, "picture", 
				   gb_type_string, PICTURE);

	form_class->construct = gbrun_picturebox_construct;
}

GtkType
gbrun_picturebox_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunPictureBox),
			sizeof (GBRunFormItemClass),
			(GtkClassInitFunc)  gbrun_picturebox_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_FORM_ITEM, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}

/**
 * gbrun_picturebox_register
 *
 **/
void
gbrun_picturebox_register ()
{
	gbrun_picturebox_get_type ();	
}


/**
 * gbrun_picturebox_shutdown
 *
 **/
void
gbrun_picturebox_shutdown ()
{
}
