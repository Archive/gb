
/*
 * gbrun-cmdbutton.c
 *
 * Gnome Basic Interpreter Command Button functions.
 *
 * Authors:
 *	Frank Chiulli (fc-linux@home.com)
 *      Michael Meeks (mmeeks@gnu.org)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <gdk/gdkkeysyms.h>
#include "gbrun-form-item.h"

#define ITEM_NAME "gb-vb.commandbutton"

enum {
	ARG_FIRST = 0,
	CAPTION,
	TOOLTIP_TEXT,
	ENABLED,
	DEFAULT
};

#define GBRUN_CMDBUTTON(obj) (GTK_CHECK_CAST ((obj), gbrun_cmdbutton_get_type (), GBRunCmdButton))

typedef struct {
	GBRunFormItem   item;

	GtkLabel       *label;
	char           *real_caption;
} GBRunCmdButton;

/**
 * cmdbutton_click
 *   @button
 *   @ec
 *
 *   Callback for 'click'.
 *
 **/

static void
cmdbutton_mouse_out (GtkWidget *button, GBRunEvalContext *ec)
{
	gbrun_form_item_invoke (ec, button, "_MouseOut");
}

static void
cmdbutton_mouse_in (GtkWidget *button, GBRunEvalContext *ec)
{
	gbrun_form_item_invoke (ec, button, "_MouseIn");
}

static void
cmdbutton_key_press (GtkWidget *button, GBRunEvalContext *ec)
{
	gbrun_form_item_invoke (ec, button, "_KeyPress");
}

static void
cmdbutton_click (GtkWidget *button, GBRunEvalContext *ec)
{
	gbrun_form_item_invoke (ec, button, "_Click");
}

/**
 * cmdbutton_setarg:
 *   @ec
 *   @object
 *   @property
 *   @val
 **/
static gboolean
cmdbutton_setarg (GBRunEvalContext *ec,
		  GBRunObject      *object,
		  int               property,
		  GBValue          *val)
{
	GBRunCmdButton *button = GBRUN_CMDBUTTON (object);
	GtkWidget      *bwidget = gbrun_form_item_get_widget (GBRUN_FORM_ITEM (object));

	g_return_val_if_fail (button != NULL, FALSE);
	g_return_val_if_fail (button->label != NULL, FALSE);

	switch (property) {
	case CAPTION:
	{
		char *txt;
		guint keyval;

		button->real_caption = g_strdup (val->v.s->str);

		txt = gbrun_form_un_shortcutify (button->real_caption, NULL);
		gtk_label_set_text (button->label, txt);

		gtk_accel_label_set_accel_widget (
			GTK_ACCEL_LABEL (button->label), bwidget);

		keyval = gtk_label_parse_uline (GTK_LABEL (button->label), txt);

		if (keyval != GDK_VoidSymbol &&
		    button->item.parent)
			gtk_widget_add_accelerator (
				bwidget, "clicked",
				GBRUN_FORM (button->item.parent)->accel_group,
				keyval, GDK_MOD1_MASK,
				(GtkAccelFlags) 0);
		
		g_free (txt);

		return TRUE;
	} 
	case TOOLTIP_TEXT:
	{
		GtkTooltips *t = gtk_tooltips_new();
		gtk_tooltips_set_tip (t, GTK_WIDGET (bwidget),
		                      val->v.s->str, NULL);
		return TRUE;
	}
	case ENABLED:
		gtk_widget_set_sensitive (GTK_WIDGET (bwidget),
					  val->v.bool);
		return TRUE;

	case DEFAULT:
		/* FIXME: Is this the right function ? */
		gtk_widget_set (GTK_WIDGET (bwidget), "can_default", val->v.bool, NULL);
/*		if (val->v.bool)
		gtk_window_set_default (button->item.form->window, button->item.widget);*/
		return TRUE;
	default:
		g_warning ("cmdbutton: Unhandled property '%d'", property);
		return FALSE;
	}
}


/**
 * cmdbutton_getarg:
 *   @ec
 *   @object
 *   @property
 **/
static GBValue *
cmdbutton_getarg (GBRunEvalContext *ec,
		  GBRunObject      *object,
		  int               property)
{
	GBRunCmdButton *button  = GBRUN_CMDBUTTON (object);
	GtkWidget      *bwidget = gbrun_form_item_get_widget (
				    GBRUN_FORM_ITEM (object));

	g_return_val_if_fail (button != NULL, NULL);
	g_return_val_if_fail (button->label != NULL, NULL);

	switch (property) {
	case CAPTION:
		return gb_value_new_string_chars (button->real_caption);

	case ENABLED:
		return gb_value_new_boolean (GTK_WIDGET_SENSITIVE (bwidget));

	case DEFAULT:
		/* Is this the right macro to use ? */
		return gb_value_new_boolean (GTK_WIDGET_RECEIVES_DEFAULT (bwidget));

	default:
		g_warning ("cmdbutton: Unhandled property '%d'", property);
		break;
	}

	return NULL;
}

static void
gbrun_cmdbutton_destroy (GtkObject *object)
{
	GBRunCmdButton *cmdbutton = GBRUN_CMDBUTTON (object);

	if (cmdbutton->real_caption)
		g_free (cmdbutton->real_caption);

	g_warning ("Unimplemented %s destruct", ITEM_NAME);
}

static void
gbrun_cmdbutton_construct (GBRunEvalContext *ec, GBRunFormItem *item)
{
	GtkWidget *w;
	GBRunCmdButton *dest = GBRUN_CMDBUTTON (item);

	w = gtk_button_new ();
	gbrun_form_item_set_widget (GBRUN_FORM_ITEM (dest), w);
	dest->label = GTK_LABEL (gtk_accel_label_new ("WIBBLE"));
	gtk_widget_show (GTK_WIDGET (dest->label));

	gtk_container_add (GTK_CONTAINER (w),
			   GTK_WIDGET    (dest->label));

	/* FIXME: more signals etc. & leak fix */

	gtk_signal_connect (GTK_OBJECT (w), "enter", 
				(GtkSignalFunc) cmdbutton_mouse_in, ec);

	gtk_signal_connect (GTK_OBJECT (w), "leave", 
				(GtkSignalFunc) cmdbutton_mouse_out, ec);

	gtk_signal_connect (GTK_OBJECT (w), "pressed", 
				(GtkSignalFunc) cmdbutton_key_press, ec);

	gtk_signal_connect (GTK_OBJECT (item->widget), "clicked", 
			    (GtkSignalFunc) cmdbutton_click, ec);
}

static void
gbrun_cmdbutton_class_init (GBRunObjectClass *klass)
{
	GtkObjectClass     *gtk_class    = (GtkObjectClass *) klass;
	GBRunFormItemClass *form_class   = (GBRunFormItemClass *) klass;

	klass->set_arg = cmdbutton_setarg;
	klass->get_arg = cmdbutton_getarg;

	gbrun_object_add_property (klass, "caption", 
				   gb_type_string, CAPTION);

	gbrun_object_add_property (klass, "tooltiptext",
				   gb_type_string, TOOLTIP_TEXT);

        gbrun_object_add_property (klass, "enabled",
				   gb_type_boolean, ENABLED);

	gbrun_object_add_property (klass, "default",
				   gb_type_boolean, DEFAULT);

	form_class->construct = gbrun_cmdbutton_construct;

	gtk_class->destroy    = gbrun_cmdbutton_destroy;
}

GtkType
gbrun_cmdbutton_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunCmdButton),
			sizeof (GBRunFormItemClass),
			(GtkClassInitFunc)  gbrun_cmdbutton_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_FORM_ITEM, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}

/**
 * gbrun_cmdbutton_register:
 *
 **/
void
gbrun_cmdbutton_register ()
{
	gbrun_cmdbutton_get_type ();
}


/**
 *  gbrun_cmdbutton_shutdown:
 **/
void
gbrun_cmdbutton_shutdown ()
{
}

