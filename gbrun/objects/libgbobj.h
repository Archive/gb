
/*
 * GNOME Basic Interpreter library main include.
 *
 * Author:
 *    Frank Chiulli  <fc-linux@home.com
 */

#ifndef GBE_GBOBJ_H
#define GBE_GBOBJ_H

/* gbrun-form.h */
typedef struct _GBRunForm            GBRunForm;

/* gbrun-form-item.h */
typedef struct _GBRunFormItem        GBRunFormItem;


#include <gbrun/objects/gbrun-form.h>

#endif
