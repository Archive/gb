/*
 * gbrun-timer.c
 *
 * Gnome Basic Interpreter Timer functions.
 *
 * Author:
 *	Michael Meeks	(michael@helixcode.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */
#include <gtk/gtkinvisible.h>
#include "gbrun-form-item.h"

#define ITEM_NAME "gb-vb.timer"


enum {
	ARG_FIRST = 0,
	INTERVAL
};

#define GBRUN_TIMER(obj) (GTK_CHECK_CAST ((obj), gbrun_timer_get_type (), GBRunTimer))

typedef struct {
	GBRunFormItem   item;

	int             interval;
	gboolean        waiting;
} GBRunTimer;

typedef struct {
	GBRunEvalContext *ec;
	GBRunFormItem    *timer;
} timer_closure_t;

void
closure_destroy (gpointer d)
{
	g_free (d);
}

gboolean
timer_func (gpointer data)
{
	timer_closure_t *c = data;

	gbrun_form_item_invoke (
		c->ec, gbrun_form_item_get_widget (c->timer),
		"_Timer");

	return TRUE;
}

static gboolean
timer_setarg (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      int               property,
	      GBValue          *val)
{
	GBRunTimer *timer = GBRUN_TIMER (object);

	g_return_val_if_fail (timer != NULL, FALSE);

	/* This implementation sucks badly */
	switch (property) {
	case INTERVAL: {
		timer_closure_t *c = g_new (timer_closure_t, 1);

		timer->interval = val->v.l;
		c->ec    = ec;
		c->timer = GBRUN_FORM_ITEM (object);

		if (!timer->waiting)
			g_timeout_add_full (
				G_PRIORITY_DEFAULT, timer->interval,
				timer_func, c, closure_destroy);

		timer->waiting = TRUE;
		return TRUE;
	}

	default:
		g_warning ("timer: Unhandled property '%d'", property);
		return FALSE;
	}
}


/**
 * timer_getarg:
 *   @ec
 *   @object
 *   @property
 **/
static GBValue *
timer_getarg (GBRunEvalContext *ec,
	      GBRunObject      *object,
	      int               property)
{
	GBRunTimer *timer = GBRUN_TIMER (object);

	g_return_val_if_fail (timer != NULL, NULL);

	switch (property) {
	case INTERVAL:
		return gb_value_new_long (timer->interval);

	default:
		g_warning ("timer: Unhandled property '%d'", property);
		return NULL;
	}
}

static void
gbrun_timer_init (GBRunFormItem *item)
{
	GBRunTimer *dest = GBRUN_TIMER (item);

	dest->waiting = FALSE;
	dest->interval = 1000;

	gbrun_form_item_set_widget (item, gtk_invisible_new ());
}

static void
gbrun_timer_class_init (GBRunObjectClass *klass)
{
/*	GBRunFormItemClass *form_class = (GBRunFormItemClass *) klass;*/

	klass->set_arg = timer_setarg;
	klass->get_arg = timer_getarg;

	gbrun_object_add_property (klass, "interval", 
				   gb_type_long, INTERVAL);
}

GtkType
gbrun_timer_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			ITEM_NAME,
			sizeof (GBRunTimer),
			sizeof (GBRunFormItemClass),
			(GtkClassInitFunc)  gbrun_timer_class_init,
			(GtkObjectInitFunc) gbrun_timer_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GBRUN_TYPE_FORM_ITEM, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}

/**
 * gbrun_timer_register
 *
 **/
void
gbrun_timer_register ()
{
	gbrun_timer_get_type ();	
}


/**
 * gbrun_timer_shutdown
 *
 **/
void
gbrun_timer_shutdown ()
{
}
