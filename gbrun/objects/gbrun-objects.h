
/*
 * GNOME Basic Object registration header
 *
 * Author:
 *    Michael Meeks <mmeeks@gnu.org>
 *
 * Copyright 2000, Helix Code, Inc
 */

#ifndef GBRUN_OBJECTS_H
#define GBRUN_OBJECTS_H

/* Internal */
GtkType gbrun_checkbox_get_type    (void);
GtkType gbrun_cmdbutton_get_type   (void);
GtkType gbrun_combobox_get_type    (void);
GtkType gbrun_filelistbox_get_type (void);
GtkType gbrun_frame_get_type       (void);
GtkType gbrun_hscrollbar_get_type  (void);
GtkType gbrun_label_get_type       (void);
GtkType gbrun_listbox_get_type     (void);
GtkType gbrun_menu_get_type        (void);
GtkType gbrun_optbutton_get_type   (void);
GtkType gbrun_picturebox_get_type  (void);
GtkType gbrun_screen_get_type      (void);
GtkType gbrun_shape_get_type       (void);
GtkType gbrun_textbox_get_type     (void);
GtkType gbrun_timer_get_type       (void);
GtkType gbrun_vscrollbar_get_type  (void);

void gbrun_checkbox_register  (void);
void gbrun_cmdbutton_register (void);
void gbrun_combobox_register  (void);
void gbrun_filelistbox_register (void);
void gbrun_frame_register     (void);
void gbrun_hscrollbar_register(void);
void gbrun_label_register     (void);
void gbrun_listbox_register   (void);
void gbrun_menu_register      (void);
void gbrun_optbutton_register (void);
void gbrun_picturebox_register(void);
void gbrun_screen_register    (void);
void gbrun_shape_register     (void);
void gbrun_textbox_register   (void);
void gbrun_timer_register     (void);
void gbrun_vscrollbar_register(void);

void gbrun_checkbox_shutdown  (void);
void gbrun_cmdbutton_shutdown (void);
void gbrun_combobox_shutdown  (void);
void gbrun_filelistbox_shutdown (void);
void gbrun_frame_shutdown     (void);
void gbrun_hscrollbar_shutdown(void);
void gbrun_label_shutdown     (void);
void gbrun_listbox_shutdown   (void);
void gbrun_menu_shutdown      (void);
void gbrun_optbutton_shutdown (void);
void gbrun_picturebox_shutdown(void);
void gbrun_screen_shutdown    (void);
void gbrun_shape_shutdown     (void);
void gbrun_textbox_shutdown   (void);
void gbrun_timer_shutdown     (void);
void gbrun_vscrollbar_shutdown(void);

/* External */
void gbrun_objects_register   (GBEvalContext *ec);
void gbrun_objects_shutdown   (void);

#endif
