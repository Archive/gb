/*
 * GNOME Basic Statement evaluator
 *
 * Author:
 *    Michael Meeks <michael@imaginator.com>
 *
 * Copyright 2000, Helix Code, Inc
 */

#include <gbrun/gbrun.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-stack.h>
#include <gbrun/gbrun-array.h>
#include <gbrun/gbrun-collection.h>
#include <gbrun/gbrun-statement.h>
#include <gbrun/gbrun-object.h>
/* for gettimeofday */
#include <sys/time.h> 
#include <unistd.h>
/* for strcmp */
#include <string.h>
/* for file I/O */
#include <gbrun/gbrun-file.h>

#undef STMT_DEBUG

#ifdef STMT_DEBUG
static void
gbrun_frame_dump (GBRunFrame *rf, const char *type)
{
	GBRunSubFrame *sf = rf->cur;

	fprintf (stderr, "%s dump <%p>: | ", type, rf);

	while (sf) {
		GSList *sl = sf->stmts;

		if (sl && sl->data)
			fprintf (stderr, "%s ", gb_stmt_type (sl->data));
		else
			fprintf (stderr, "NullStack ");
		
		sf = sf->parent;
	}
	
	fprintf (stderr, "|\n");
}
#endif

static void
gbrun_frame_stmts_push_full (GBRunEvalContext *ec,
			     GSList           *stmts,
			     gboolean          eval_call)
{
	GBRunFrame    *rf = gbrun_stack_frame (ec->stack);
	GBRunSubFrame *sf = g_new0 (GBRunSubFrame, 1);

	g_return_if_fail (stmts != NULL);
	g_return_if_fail (stmts->data != NULL);

#ifdef STMT_DEBUG
	fprintf (stderr, "Pushing '%s'\n", gb_stmt_type (stmts->data));
#endif

	sf->parent = rf->cur;
	rf->cur    = sf;

	sf->stmts  = stmts;
	sf->init   = TRUE;
	sf->pushed = TRUE;
	sf->eval_call = eval_call;

#ifdef STMT_DEBUG
	gbrun_frame_dump (rf, "after push");
#endif
}

static void
gbrun_frame_stmts_push (GBRunEvalContext *ec, GSList *stmts)
{
	gbrun_frame_stmts_push_full (ec, stmts, FALSE);
}

static void
gbrun_frame_crop_to_depth (GBRunEvalContext *ec, guint depth)
{
	GBRunFrame    *rf = gbrun_stack_frame (ec->stack);
	GBRunSubFrame *sf;
	int            i;

	g_return_if_fail (rf != NULL);

	sf = rf->cur;
	for (i = 0; sf; i++)
		sf = sf->parent;

	while ((sf = rf->cur) && (i >= depth)) {
		rf->cur = sf->parent;
		g_free (sf);
		i--;
	}
}

static void
gbrun_frame_crop_to_ptr (GBRunEvalContext *ec,
			 GBRunSubFrame    *frame)
{
	GBRunFrame    *rf = gbrun_stack_frame (ec->stack);
	GBRunSubFrame *sf;

	g_return_if_fail (rf != NULL);

	for (sf = rf->cur; sf != frame; sf = rf->cur) {
		rf->cur = sf->parent;
		g_free (sf);
	}
}

static GBStatement *
gbrun_frame_stmt_next (GBRunEvalContext *ec,
		       gboolean         *init)
{
	GBRunFrame    *rf = gbrun_stack_frame (ec->stack);
	GBRunSubFrame *sf;

	if (!rf || !rf->cur) {
#ifdef STMT_DEBUG
		fprintf (stderr, "Hit end of stmt list <%p>\n", rf);
#endif
		return NULL;
	}

#ifdef STMT_DEBUG
	gbrun_frame_dump (rf, "before next");
#endif
	sf = rf->cur;

	*init    = sf->init;
	sf->init = TRUE;
	if (sf->pushed) {
		sf->pushed = FALSE;
		g_assert (sf->stmts->data != NULL);
		return sf->stmts->data;
	}

	sf->stmts = g_slist_next (sf->stmts);

	if (!sf->stmts) {
		rf->cur = sf->parent;

		/* gbrun_stmt_evaluate was re-entered */
		if (sf->eval_call) {
			g_free (sf);
			return NULL;
		}
		
		g_free (sf);

		if (rf->cur) {
			rf->cur->init   = FALSE;
			rf->cur->pushed = TRUE;
#ifdef STMT_DEBUG
			fprintf (stderr, "Pop to %s\n", gb_stmt_type (
				rf->cur->stmts->data));
#endif
		}
		
		return gbrun_frame_stmt_next (ec, init);
	}
	g_assert (sf->stmts->data != NULL);

#ifdef STMT_DEBUG
	gbrun_frame_dump (rf, "after next");
#endif
	return sf->stmts->data;
}

static GSList *
seek_label (GSList     *stmts,
	    const char *label,
	    int        *depth)
{
	GSList  *l, *ret = NULL;

	(*depth)++;

	for (l = stmts; l && !ret; l = l->next) {
		const GBStatement *stmt = l->data;

		switch (stmt->type) {
		case GBS_LABEL:
			if (!g_strcasecmp (stmt->parm.label, label))
				ret = l;
			break;

		case GBS_FOR: {
			ret = seek_label (stmt->parm.forloop.body, label, depth);
			break;

		case GBS_DO:
		case GBS_WHILE:
			ret = seek_label (stmt->parm.do_while.body, label, depth);
			break;
			
		case GBS_IF:
			ret = seek_label (stmt->parm.if_stmt.body, label, depth);
			if (ret)
				break;
			ret = seek_label (stmt->parm.if_stmt.else_body, label, depth);
			break;
			
		case GBS_FOREACH:
			ret = seek_label (stmt->parm.foreach.body, label, depth);
			break;

/*			g_warning ("Goto can't cope with flow control stmt '%s'",
			gb_stmt_type (stmt));*/
		default:
			break;
		};
	}
	}

	if (!ret)
		(*depth)--;
	return ret;
}

static gboolean
gbrun_stmt_goto (GBRunEvalContext *ec,
		 const char       *label)
{
	GBRunFrame    *rf = gbrun_stack_frame (ec->stack);
	GBRunSubFrame *sf;
	GSList        *label_stmt;
	int            depth, i;

	if (!rf || !rf->func_root)
		goto stmt_goto_err;

	depth = 0;
	if (!(label_stmt = seek_label (rf->func_root, label, &depth)))
		goto stmt_goto_err;

	sf = rf->cur;
	for (i = 0; sf; i++)
		sf = sf->parent;

	/*
	 * For now we will only roll back the frame, forwards
	 * is more interesting.
	 */
	if (depth > i) {
		gbrun_exception_firev (ec, _("Can't goto label '%s' at depth %d, \
				       when we are at depth %d"), label, depth, i);
		return FALSE;
	}

	gbrun_frame_crop_to_depth (ec, depth - 1);
	gbrun_frame_stmts_push (ec, label_stmt);
	
	return TRUE;
	
 stmt_goto_err:
	gbrun_exception_firev (ec, _("Can't find label '%s'"), label);
	return FALSE;
}

gboolean
gbrun_stmt_assign (GBRunEvalContext *ec,
		   const GBExpr     *lexpr,
		   const GBExpr     *rexpr)
{
	GBValue *rval;
	gboolean result;

/*	printf ("Assign\n");
	gb_expr_print (stdout, lexpr);
	printf (" = ");
	gb_expr_print (stdout, rexpr);*/

	if (lexpr->type != GB_EXPR_OBJREF) {
		gbrun_exception_fire (ec, _("Duff lvalue"));
		return FALSE;
	}

	rval = gb_eval_context_eval (GB_EVAL_CONTEXT (ec), rexpr);
	if (!rval)
		return FALSE;

	result = gbrun_eval_assign (ec, lexpr->parm.objref, rval);
	gb_value_destroy (rval);

	return result;
}

gboolean
gbrun_stmt_set (GBRunEvalContext  *ec,
		const GBStatement *stmt)
{
	GBValue *tmp;
	gboolean result;
		
	g_return_val_if_fail (stmt->parm.set.var != NULL, FALSE);
	g_return_val_if_fail (stmt->parm.set.objref != NULL, FALSE);

	if (stmt->parm.set.objref->type != GB_EXPR_OBJREF) {
		gbrun_exception_fire (ec, _("Reference must be to an object in Set"));
		return FALSE;
	}

	tmp = gbrun_eval_objref (ec, stmt->parm.set.objref);
	if (!tmp)
		return FALSE;
	
	result = gbrun_eval_assign (ec, stmt->parm.set.var->parm.objref, tmp);
	
	return result;
}

gboolean
gbrun_stmt_redim (GBRunEvalContext  *ec,
		  const GBStatement *stmt)
{
	GBValue *tmp;
	GBObjRef r;

	g_return_val_if_fail (stmt->parm.redim.var_name != NULL, FALSE);

	r.method = FALSE;
	r.name   = stmt->parm.redim.var_name;
	r.parms  = NULL;

	tmp = gbrun_objref_deref (ec, NULL, &r, TRUE);
	if (!tmp)
		return FALSE;

	if (!gtk_type_is_a (tmp->gtk_type, GBRUN_TYPE_ARRAY)) {
		gbrun_exception_firev (ec, _("Can only redim arrays"));
		return FALSE;
	}

	gbrun_array_redim (ec, GBRUN_ARRAY (tmp->v.obj),
			   stmt->parm.redim.indices,
			   stmt->parm.redim.preserve);

	gb_value_destroy (tmp);
	
	return TRUE;
}

gboolean
gbrun_stmt_erase (GBRunEvalContext  *ec,
		  const GBStatement *stmt)
{
	GBValue *tmp;
	GBObjRef r;

	g_return_val_if_fail (stmt->parm.erase.var_name != NULL, FALSE);

	r.method = FALSE;
	r.name   = stmt->parm.erase.var_name;
	r.parms  = NULL;

	tmp = gbrun_objref_deref (ec, NULL, &r, TRUE);
	if (!tmp)
		return FALSE;

	if (!gtk_type_is_a (tmp->gtk_type, GBRUN_TYPE_ARRAY)) {
		gbrun_exception_firev (ec, _("Can only erase arrays"));
		return FALSE;
	}

	gbrun_array_erase (ec, GBRUN_ARRAY (tmp->v.obj));

	gb_value_destroy (tmp);
	
	return TRUE;
}

static gboolean
gbrun_stmt_case (GBRunEvalContext *ec,
		 GBValue          *val,
		 GBSelectCase     *sel,
		 gboolean         *err)
{
	GSList      *tmp;
	gboolean     ret = FALSE;

	g_return_val_if_fail (sel != NULL, FALSE);
	g_return_val_if_fail (val != NULL, FALSE);

	for (tmp = sel->case_exprs; tmp; tmp = tmp->next) {
		GBCaseExpr  *c = tmp->data;
		
		switch (c->type) {
		case GB_CASE_ELSE:
			ret = TRUE;
			gbrun_frame_stmts_push (ec, sel->statements);
			*err = FALSE;
			break;

		case GB_CASE_EXPR:
		{
			GBValue *v = gb_eval_context_eval (GB_EVAL_CONTEXT (ec),
							   c->u.expr);
			GBBoolean ok;

			if (!v) {
				ret = *err = TRUE;
				break;
			 }
			
			if (!gb_eval_compare (GB_EVAL_CONTEXT (ec), v,
						      GB_EXPR_EQ, val, &ok)) {
				ret = *err = TRUE;
				gb_value_destroy (v);
				break;
			}	
			
			if (ok) {
				*err = FALSE;
				gbrun_frame_stmts_push (ec, sel->statements);
				ret = TRUE;
				gb_value_destroy (v);
				goto finish;
			}
			break;
		}
		
		case GB_CASE_EXPR_TO_EXPR:
		{
			GBValue   *from, *to;
			GBBoolean  ok;
			
			from = gb_eval_context_eval (GB_EVAL_CONTEXT (ec),
						     c->u.expr_to_expr.from);
			if (!from) {
				*err = ret  = TRUE;
				break;
			}
			
			to   = gb_eval_context_eval (GB_EVAL_CONTEXT (ec),
						     c->u.expr_to_expr.to);
			if (!to) {
				*err = ret  = TRUE;
				gb_value_destroy (from);
				break;
			}
			
			if (!gb_eval_compare (GB_EVAL_CONTEXT (ec), val,
					      GB_EXPR_GE, from, &ok)) {
				ret = *err = TRUE;
				gb_value_destroy (from);
				gb_value_destroy (to);
				break;
			}
			
			if (ok) {
				if (!gb_eval_compare (GB_EVAL_CONTEXT (ec), val,
						      GB_EXPR_LE, to, &ok)) {
					ret = *err = TRUE;
					gb_value_destroy (from);
					gb_value_destroy (to);
					break;
				}
				
				if (ok) {
					*err = FALSE;
					gbrun_frame_stmts_push (ec, sel->statements);
					ret = TRUE;
					gb_value_destroy (from);
					gb_value_destroy (to);
					goto finish;
				}
			}
			
			gb_value_destroy (from);
			gb_value_destroy (to);
			break;
	        }

		case GB_CASE_COMPARISON:
		{
			GBValue  *to = gb_eval_context_eval (GB_EVAL_CONTEXT (ec),
							     c->u.comparison.to);
			GBBoolean ok;
			
			if (!to || !gb_eval_compare (GB_EVAL_CONTEXT (ec), val,
						     c->u.comparison.op, to, &ok)) {
				ret = *err = TRUE;
				break;
			}
			
			if (ok) {
				*err = FALSE;
				gbrun_frame_stmts_push (ec, sel->statements);
				ret = TRUE;
				gb_value_destroy (to);
				goto finish;
			}
			break;
		}
	
		case GB_CASE_CSV:
		{ 
			const GBExprList *tmp;

			g_warning ("Shouldn't be coming here !");

			for (tmp = c->u.exprs; tmp ; tmp = tmp->next) {
				
				GBValue *v = gb_eval_context_eval (GB_EVAL_CONTEXT (ec),
								   tmp->data);
				if (!v)
					ret = *err = TRUE;
				else {
					GBBoolean ok;
					
					if (!gb_eval_compare (GB_EVAL_CONTEXT (ec), v,
							      GB_EXPR_EQ, val, &ok))
						ret = *err = TRUE;
					if (ok) {
						*err = FALSE;
						gbrun_frame_stmts_push (ec, sel->statements);
						ret = TRUE;
					}
					gb_value_destroy (v);
				}
			}
			break;
		}

		default:
			g_warning ("Unimplemented select syntax");
			break;
		}
	}

  finish:	
	return ret;
}

gboolean
gbrun_stmt_for (GBRunEvalContext  *ec,
		const GBStatement *stmt,
		gboolean           init)
{
	GBObjRef i;
        GBValue  *to;
        GBValue  *tmp;
	GBBoolean ok;
	gboolean ret=TRUE;
	
	i.name   = stmt->parm.forloop.var;
	i.method = FALSE;
	i.parms  = NULL;
	
	if (init) {
		GBValue *from;

		from = gb_eval_context_eval (GB_EVAL_CONTEXT (ec), 
					     stmt->parm.forloop.from);
		if (!from)
			return FALSE;
		
		gbrun_stack_set (ec, i.name, from);

		gb_value_destroy (from);
	} else {
		GBValue *step;
                GBValue *tmp2;
                
		if (stmt->parm.forloop.step)
			step = gb_eval_context_eval (GB_EVAL_CONTEXT (ec),
						     stmt->parm.forloop.step);
		else
			step = gb_value_new_int (1);
		
		if (!step)
			return FALSE;

                tmp2 = gbrun_objref_deref (ec, NULL, &i, TRUE);
		tmp = gb_eval_add (tmp2, step);
		gbrun_stack_set (ec, i.name, tmp);

                gb_value_destroy (tmp2);
                gb_value_destroy (tmp);
		gb_value_destroy (step);
	}

	to = gb_eval_context_eval (GB_EVAL_CONTEXT (ec), 
				   stmt->parm.forloop.to);

        tmp = gbrun_objref_deref (ec, NULL, &i, TRUE);
        
        if (!to ||
            !gb_eval_compare (GB_EVAL_CONTEXT (ec),
                              tmp,
                              GB_EXPR_LE, to, &ok)) {
		gb_value_destroy (to);
                gb_value_destroy (tmp);
                return FALSE;
        }
        gb_value_destroy (tmp);
        gb_value_destroy (to);

	if (ok)
		gbrun_frame_stmts_push (ec, stmt->parm.forloop.body);

	return ret;
}


static gboolean
gbrun_stmt_while (GBRunEvalContext  *ec,
		  const GBStatement *stmt,
		  gboolean           init)
{
	GBValue      *val;
	GBBoolean     going = TRUE;

	if (!stmt->parm.do_while.expr)
		going = TRUE;

	else {
		val = gb_eval_context_eval (GB_EVAL_CONTEXT (ec),
					    stmt->parm.do_while.expr);

		if (!gb_eval_compare (GB_EVAL_CONTEXT (ec),
				      val, GB_EXPR_EQ, GBTrue_value, &going))
			return FALSE;
			
		gb_value_destroy (val);
	}
			
	if (going)
		gbrun_frame_stmts_push (ec, stmt->parm.do_while.body);
	       
	return TRUE;
}

gboolean
gbrun_stmt_foreach (GBRunEvalContext  *ec,
		    const GBStatement *stmt,
		    gboolean           init)
{
	GBValue  *collection;
	GBValue  *index;
	GBValue **sval;
	GBValue  *value;
	char     *collection_name;
	char     *index_name;
	gboolean  is_array;
	gboolean  is_collection;
	gboolean  retval = FALSE;
	GBBoolean ok;

	collection_name = g_strconcat (
		stmt->parm.foreach.var, "_collection", NULL);

	index_name = g_strconcat (
		stmt->parm.foreach.var, "_index", NULL);

	if (init) { /* Setup */
		GBValue *col;

		col = gb_eval_context_eval (
			GB_EVAL_CONTEXT (ec), 
			stmt->parm.foreach.collection);

		if (!col)
			goto ret_free_names;

		/* We need to put some stuff on the stack */
		gbrun_stack_set  (ec, collection_name, col);
		gb_value_destroy (col);

		index = gb_value_new_long (0);
	} else {
		sval = gbrun_stack_get (ec, index_name);
		if (!sval || !*sval)
			goto ret_free_names;
		index = gb_value_copy (GB_EVAL_CONTEXT (ec), *sval);
	}

	sval = gbrun_stack_get (ec, collection_name);
	if (!sval || !*sval)
		goto ret_free_names;
	collection = *sval;

	is_collection = gtk_type_is_a (collection->gtk_type, GBRUN_TYPE_COLLECTION);
	is_array      = gtk_type_is_a (collection->gtk_type, GBRUN_TYPE_ARRAY);
	ok            = FALSE;
	value         = NULL;

	if (!(is_collection || is_array)) {
		gbrun_exception_firev (ec, _("Can only use For Each on a collection or array"));
		goto ret_free_names;

	} else if (is_collection) {
		g_warning ("Cannot For Each over a collection yet");
		goto ret_free_names;

	} else if (is_array) {
		GBRunArray *array = GBRUN_ARRAY (collection->v.obj);
		GBValue    *ubound;

		if (!gbrun_array_initialized (array)) {
			gbrun_exception_firev (ec, _("For loop not initialized"));
			goto ret_free_names;
		}

		if (gbrun_array_dimensions (array) > 1) {
			gbrun_exception_firev (ec, _("Too many (%d) dimensions in "
						     "foreach array"),
					       gbrun_array_dimensions (array));
			goto ret_free_names;
		}

		if (init) {
			gb_value_destroy (index);
			if (!(index = gbrun_array_lbound (ec, array, 1)))
				goto ret_free_names;
		}

		if (!(ubound = gbrun_array_ubound (ec, array, 1)))
			goto ret_free_names;

		if (!gb_eval_compare (GB_EVAL_CONTEXT (ec), index,
				      GB_EXPR_LE, ubound, &ok))
			goto ret_free_names;

		if (ok) {
			GBObjRef      r;
			const GBExpr *expr;

			r.method = FALSE;
			r.name   = NULL;
			expr     = gb_expr_new_value (gb_value_copy (
				GB_EVAL_CONTEXT (ec), index));
			r.parms  = g_slist_prepend (NULL, (gpointer) expr);

			value = gb_object_deref (
				GB_EVAL_CONTEXT (ec),
				GB_OBJECT (array),
				&r, FALSE);

			g_slist_free (r.parms);
			gb_expr_destroy (expr);
		}
	}

	if (value) {
		gbrun_stack_set (ec, stmt->parm.foreach.var, value);
		gb_value_destroy (value);
	}

	retval = TRUE;

	if (ok)
		gbrun_frame_stmts_push (ec, stmt->parm.foreach.body);
	
	{ /* Advance index */
		GBValue *step;
		GBValue *tmp;

		step = gb_value_new_int (1);
		tmp  = gb_eval_add (index, step);

		gbrun_stack_set (ec, index_name, tmp);
		
		gb_value_destroy (tmp);
		gb_value_destroy (step);
	}	

 ret_free_names:
	g_free (index_name);
	g_free (collection_name);

	return retval;
}

static gboolean
gbrun_stmt_with (GBRunEvalContext  *ec,
		 const GBStatement *stmt,
		 gboolean init)
{
	GBValue *value;

	value = gb_eval_context_eval (GB_EVAL_CONTEXT (ec),
				      stmt->parm.with.base_obj);

	if (!value)
		return FALSE;

	else if (!gtk_type_is_a (value->gtk_type, GB_TYPE_OBJECT)) {
		gbrun_exception_firev (ec, _("With needs an object"));
		return FALSE;
	}

	if (init) {
		gbrun_frame_stmts_push (ec, stmt->parm.with.body);

		gbrun_eval_context_with_push (ec, value->v.obj);
		gb_value_destroy (value);
	} else {
		GBObject *obj = gbrun_eval_context_with_pop (ec);

		if (obj)
			gb_object_unref (obj);
		else
			g_warning ("Weird - with stack corrupted");
	}

	return TRUE;
}

/*
 * stack_seek_root:
 * 
 *  Finds a stack element that was a function or sub or
 * property call, instead of a construct like 'for'
 **/
static void
stack_seek_root (GBRunEvalContext  *ec,
		 GBRunSubFrame     *sf,
		 GBRunSubFrame    **dest_sf,
		 GSList           **dest_stmt)
{
	if (sf->eval_call) {
		*dest_sf   = sf;   /* this frame */
		*dest_stmt = NULL; /* finish it */
	} else
		stack_seek_root (ec, sf->parent, dest_sf, dest_stmt);
}

static void
stack_find (GBRunEvalContext  *ec,
	    GBRunSubFrame     *sf,
	    GBStatementType    t,
	    GBRunSubFrame    **dest_sf,
	    GSList           **dest_stmt)
{
	if (!sf)
		return;

	if (sf->stmts) {
		GBStatement *stmt = sf->stmts->data;
		if (stmt->type == t) {
			*dest_sf   = sf;
			*dest_stmt = sf->stmts;
			return;
		}
	}

	stack_find (ec, sf->parent, t, dest_sf, dest_stmt);
}

static gboolean
gbrun_stmt_exit (GBRunEvalContext  *ec,
		 const GBStatement *stmt)
{
	GBRunFrame    *rf = gbrun_stack_frame (ec->stack);
	GBRunSubFrame *dest_sf = NULL;
	GSList        *dest_stmt = NULL;

	switch (stmt->parm.exit) {
	case GBS_EXIT_FUNCTION:
	case GBS_EXIT_SUB:
	case GBS_EXIT_PROPERTY:
		stack_seek_root (ec, rf->cur, &dest_sf, &dest_stmt);
		break;
	case GBS_EXIT_DO:
		stack_find (ec, rf->cur, GBS_DO, &dest_sf, &dest_stmt);
		if (!dest_sf)
			stack_find (ec, rf->cur, GBS_WHILE, &dest_sf, &dest_stmt);
		break;
	case GBS_EXIT_FOR:
		stack_find (ec, rf->cur, GBS_FOR, &dest_sf, &dest_stmt);
		if (!dest_sf)
			stack_find (ec, rf->cur, GBS_FOREACH, &dest_sf, &dest_stmt);
		break;
	default:
		g_warning ("Unhandled exit stmt type");
		break;
	}

	if (!dest_sf) {
		gbrun_exception_firev (ec, _("Nothing to Exit"));
		return FALSE;

	} else {
		gbrun_frame_crop_to_ptr (ec, dest_sf);
		dest_sf->stmts = dest_stmt;

		return TRUE;
	}
}

/* 
 * Returns error flag.
 */
static gboolean
gbrun_stmt_evaluate (GBRunEvalContext *ec, const GBStatement *stmt, gboolean init)
{
        g_return_val_if_fail (ec != NULL, FALSE);
	g_return_val_if_fail (stmt != NULL, FALSE);

	gb_eval_context_set_line (GB_EVAL_CONTEXT (ec), stmt->line);

	switch (stmt->type) {

	case GBS_ASSIGN:
		return gbrun_stmt_assign (ec, stmt->parm.assignment.dest,
					  stmt->parm.assignment.val);

	case GBS_FOR:
		return gbrun_stmt_for (ec, stmt, init);

	case GBS_DO:
	case GBS_WHILE:
		return gbrun_stmt_while (ec, stmt, init);

	case GBS_CALL:
		/* FIXME: named parameters are not handled at all seemingly */

		if (stmt->parm.func.call->type != GB_EXPR_OBJREF) {
			g_warning ("Duff function expression");
		} else {
			GBValue *discard;

			discard = gb_eval_context_eval (GB_EVAL_CONTEXT (ec),
							stmt->parm.func.call);

			if (discard)
				gb_value_destroy (discard);
			else
				return FALSE;
		}
		break;

	case GBS_SELECT:
		if (init) {
			GBValue *val;
			GSList  *l;
			gboolean err = FALSE;

			val = gb_eval_context_eval (GB_EVAL_CONTEXT (ec),
						    stmt->parm.select.test_expr);
			if (!val)
				return FALSE;

			for (l = stmt->parm.select.cases; l; l = l->next) {
				if (gbrun_stmt_case (ec, val, l->data, &err))
					break;
			}
		
			gb_value_destroy (val);
			if (err)
				return FALSE;
		}
		break;

	case GBS_IF:
		if (init) {
			GBValue  *cond;
			GBBoolean bool;

			cond = gb_eval_context_eval (GB_EVAL_CONTEXT (ec),
						     stmt->parm.if_stmt.condition);
			if (!cond)
				return FALSE;

			bool = gb_value_get_as_boolean (cond);
			if (bool)
				gbrun_frame_stmts_push (ec, stmt->parm.if_stmt.body);
			else 
				if (stmt->parm.if_stmt.else_body)
					gbrun_frame_stmts_push (ec, stmt->parm.if_stmt.else_body);
			gb_value_destroy (cond);
		}
		break;

	case GBS_RANDOMIZE:
	{
	        GBValue * tmpseed;
		GBInt seed;
		struct timeval tv;

	        if ((stmt->parm.randomize != NULL) &&
		    (ec->random.reseed    == TRUE)) {
		        /* we actually have the seed */
		        ec->random.reseed = FALSE;
			tmpseed = gb_eval_context_eval (GB_EVAL_CONTEXT (ec),
							stmt->parm.randomize);
			/* FIXME: check if value is number and transform to int */
			seed = gb_value_get_as_int (tmpseed);
			gb_value_destroy (tmpseed);

			ec->random.randseed = seed;
		} else {
		        /* we don't have a seed */
		        gettimeofday (&tv,NULL);
			ec->random.randseed = tv.tv_usec;
		}
		break;
	}
	
	case GBS_LOAD:
		gtk_main ();
		g_warning ("Load stubbed %d", gtk_main_level ());
		break;

	case GBS_UNLOAD:
		gtk_main_quit ();
		g_warning ("UnLoad stubbed %d", gtk_main_level ());
		break;

	case GBS_OPEN:
		return gbrun_stmt_open (ec, stmt);

	case GBS_INPUT:
		return gbrun_stmt_input (ec, stmt);

	case GBS_LINE_INPUT:
		return gbrun_stmt_line_input (ec, stmt);
	
	case GBS_CLOSE:
		return gbrun_stmt_close (ec, stmt);

	case GBS_ON_ERROR:
		ec->on_error = stmt->parm.on_error;
		break;
		
	case GBS_GOTO:
		return gbrun_stmt_goto (ec, stmt->parm.label);

	case GBS_LABEL:
		return TRUE;
		
        case GBS_GET:
		return gbrun_stmt_get (ec, stmt);
		
	case GBS_PUT:
		return gbrun_stmt_put (ec, stmt);

	case GBS_SEEK:
		return gbrun_stmt_seek (ec, stmt);

	case GBS_PRINT:
	        return gbrun_stmt_print (ec, stmt);

	case GBS_SET:
		return gbrun_stmt_set (ec, stmt); 

	case GBS_REDIM:
		return gbrun_stmt_redim (ec, stmt);

	case GBS_EXIT:
		return gbrun_stmt_exit (ec, stmt);

	case GBS_FOREACH:
		return gbrun_stmt_foreach (ec, stmt, init);

	case GBS_WITH:
		return gbrun_stmt_with (ec, stmt, init);

	case GBS_ERASE:
		return gbrun_stmt_erase (ec, stmt);

	default:
		gbrun_exception_firev (ec, _("Unhandled statement '%s'"),
				       gb_stmt_type (stmt));
		return FALSE;
	}

	return TRUE;
}

gboolean
gbrun_stmts_evaluate (GBRunEvalContext *ec, GSList *stmts)
{
	GBRunFrame  *rf = gbrun_stack_frame (ec->stack);
	GBStatement *stmt;
	gboolean     init;

	g_return_val_if_fail (rf != NULL, FALSE);

	if (!stmts)
		return TRUE;

	gbrun_frame_stmts_push_full (ec, stmts, TRUE);
	rf->func_root = stmts;

	while ((stmt = gbrun_frame_stmt_next (ec, &init))) {
		gboolean err;
		
		err = !gbrun_stmt_evaluate (ec, stmt, init);

	stmt_eval_recheck:
		
		if (err || gbrun_eval_context_exception (ec)) {
			
			if      (ec->on_error.type == GB_ON_ERROR_FLAG)
				break;

			else if (ec->on_error.type == GB_ON_ERROR_GOTO) {
				/* 0 is a special case apparently */
				if (!strcmp (ec->on_error.label, "0"))
					break;

				if (gbrun_stmt_goto (ec, ec->on_error.label))
					goto stmt_eval_recheck;
				else
					break;
			} else {
				g_assert (ec->on_error.type == GB_ON_ERROR_NEXT);
				gb_eval_context_reset (GB_EVAL_CONTEXT (ec));
			}
		}
	}

	if (stmt)
		gbrun_frame_crop_to_depth (ec, 0);
	
	return stmt == NULL;
}
