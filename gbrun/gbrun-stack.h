
/*
 * GNOME Basic Stack handling
 *
 * Author:
 *    Michael Meeks <mmeeks@gnu.org>
 *
 * Copyright 2000, Helix Code, Inc
 */
#ifndef GBRUN_STACK_H
#define GBRUN_STACK_H

#include <gbrun/gbrun.h>
#include <gb/gb-value.h>
#include <gb/gb-object.h>
#include <gbrun/gbrun-eval.h>

#define GBRUN_TYPE_STACK            (gbrun_stack_get_type ())
#define GBRUN_STACK(obj)            (GTK_CHECK_CAST ((obj), GBRUN_TYPE_STACK, GBRunStack))
#define GBRUN_STACK_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GBRUN_TYPE_STACK, GBRunStackClass))
#define GBRUN_IS_STACK(obj)         (GTK_CHECK_TYPE ((obj), GBRUN_TYPE_STACK))
#define GBRUN_IS_STACK_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GBRUN_TYPE_STACK))

struct _GBRunStack {
	GBObject object;

	GBRunStackLevelList *level;
};

typedef struct {
	GBObjectClass klass;
} GBRunStackClass;

struct _GBRunVar {
	char    *name;
	GBValue *val;
};

GtkType     gbrun_stack_get_type (void);

void        gbrun_stack_call     (GBRunEvalContext *ec,
				  const char       *name);

void        gbrun_stack_return   (GBRunEvalContext *ec);

GBRunFrame *gbrun_stack_frame    (GBRunStack       *stack);

typedef enum {
	GBRUN_STACK_LOCAL = 0,
	GBRUN_STACK_MODULE = 1,
	GBRUN_STACK_GLOBAL = 2
} GBRunStackScope;

void      gbrun_stack_add      (GBRunEvalContext *ec,
				const char       *name,
				const GBValue    *val,
				GBRunStackScope   scope);

void      gbrun_stack_set      (GBRunEvalContext *ec,
				const char       *name,
				GBValue          *value);

GBValue **gbrun_stack_get      (GBRunEvalContext *ec,
				const char       *name);

char    **gbrun_stack_dump     (GBRunStack       *stack);

GBRunStackLevel *gbrun_stack_level_new     (const char       *name);
GBValue        **gbrun_stack_level_lookup  (GBRunStackLevel  *l,
					    const char       *name);
void             gbrun_stack_level_add     (GBRunEvalContext *ec,
					    GBRunStackLevel  *l,
					    const char       *name,
					    const GBValue    *val);
void             gbrun_stack_level_destroy (GBRunStackLevel  *l);

#endif /* GBRUN_STACK_H */
