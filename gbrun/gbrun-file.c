
/*
 * GNOME Basic File I/O statements handler
 *
 * Authors:
 *    Ravi Pratap    (ravi_pratap@email.com)
 *    Michael Meeks  (michael@helixcode.com)
 *    William Miller (William234@aol.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <gbrun/gbrun.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-stack.h>
#include <gbrun/gbrun-statement.h>
#include <gbrun/gbrun-object.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "gbrun-file.h"

#define FILE_STMT_SEC_CHECK(ec,txt) \
	if (ec->flags & GBRUN_SEC_IO) { \
		gbrun_exception_firev (ec, _("Insufficient privilege to %s file"), \
				       txt); \
		return FALSE; \
	}

#define FILE_FUNC_SEC_CHECK(ec,txt) \
	if (ec->flags & GBRUN_SEC_IO) { \
		gbrun_exception_firev (ec, _("Insufficient privilege to %s file"), \
				       txt); \
		return FALSE; \
	}

GBRunFileHandle *
internal_handle_from_gb_no (GBRunEvalContext *ec, int fileno)
{
	GBRunFileHandle *h;
	GBRunFileHandle *handles;

	handles = ec->file_handles;

	if (!(fileno >= 0 && fileno <= 512)) {
		gbrun_exception_fire (ec, _("File number out of range"));
		return NULL;
	}

	if (handles [fileno].used == FALSE) {
		gbrun_exception_firev (ec, _("Invalid filenumber %d"), fileno);
		return NULL;
	} else {
		h = g_new (GBRunFileHandle, 1);
		h->fileno = handles [fileno].fileno;
		h->mode = handles [fileno].mode;
		h->recordlen = handles [fileno].recordlen;
		h->file = handles [fileno].file;
	}

	return h;
}

/********** File I/O functions ************/

GBValue *
gbrun_func_eof (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	GBRunFileHandle *h;

	FILE_FUNC_SEC_CHECK (ec, "eof");

	GB_IS_VALUE (ec, args [0], GB_VALUE_INT);

	h = internal_handle_from_gb_no (ec, args [0]->v.i);
	if (!h) {
		gbrun_exception_fire (ec, _("Bad file handle"));
		return FALSE;
	}
	
	if (feof (h->file)) {
		g_free (h);
		return gb_value_new_boolean (GBTrue);
	} else {
		g_free (h);
		return gb_value_new_boolean (GBFalse);
	}
}

GBValue *
gbrun_func_freefile (GBRunEvalContext *ec,
		     GBRunObject      *object,
		     GBValue         **args)
{
	int              max  = -1;
	int              arg  = 0;
	int              loop = 0;
	GBRunFileHandle *handles = ec->file_handles;

	FILE_FUNC_SEC_CHECK (ec, "freefile");

	if (args [0])
		arg = args[0]->v.i;
	else
		arg = 0;

	switch (arg) {

	/*
	 * 0 - 255
	 * Locked for exclusive use by the application
	 */

	case 0:
	        for (loop = 0; loop < 256; loop++) {
			if (handles [loop].used == FALSE) {
				return gb_value_new_int (loop);
				break;
			}
	  	}
		break;

 	/*
	 * 256 - 511
	 * Not locked for exclusive use.
 	 */
	case 1:
	        for (loop = 256; loop < 512; loop++) {
			if (handles [loop].used == FALSE) {
				return gb_value_new_int (loop);
				break;
			}
	  	}
		break;

	default:
		g_warning("You need to specify either a 0 or a 1");
		break;
	}

	return gb_value_new_int (max);
}

GBValue *
gbrun_func_loc (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	GBRunFileHandle *h;
	GBValue         *res;
	long             pos;

	FILE_FUNC_SEC_CHECK (ec, "loc");

	GB_IS_VALUE (ec, args [0], GB_VALUE_INT);

	h = internal_handle_from_gb_no (ec, args [0]->v.i);
	if (!h) {
		gbrun_exception_fire (ec, _("Bad file handle"));
		return FALSE;
	}

	pos = ftell (h->file) + 1;

	if (h->mode == GB_OPEN_RANDOM)
		res = gb_value_new_long (pos / h->recordlen);
	else if (h->mode == GB_OPEN_INPUT  ||
		 h->mode == GB_OPEN_OUTPUT ||
		 h->mode == GB_OPEN_APPEND)
		res = gb_value_new_long (pos / 128);
	else if (h->mode == GB_OPEN_BINARY)
		res = gb_value_new_long (pos);

	g_free (h);

	return res;
}

long
get_file_len (FILE *fin)
{
	long len, pos;

	pos = ftell (fin);
	if (fseek (fin, 0, SEEK_END))
		return -1;

	len = ftell (fin);
	if (fseek (fin, pos, SEEK_SET))
		return -1;

	return len;
}


GBValue *
gbrun_func_lof (GBRunEvalContext *ec,
		GBRunObject      *object,
		GBValue         **args)
{
	GBRunFileHandle *h;
	GBValue         *res;
	long             len;

	FILE_FUNC_SEC_CHECK (ec, "lof");

	GB_IS_VALUE (ec, args[0], GB_VALUE_INT);
	h = internal_handle_from_gb_no (ec, args [0]->v.i);
	if (!h) {
		gbrun_exception_fire (ec, _("Bad file handle"));
		return FALSE;
	}

	len = get_file_len (h->file);

	if (len != -1) {
		res = gb_value_new_long (len);
	} else {
		gbrun_exception_fire (ec, _("Unable to get the LOF."));
		return NULL;
	}

	g_free (h);
	return res;
}

GBValue *
gbrun_func_seek (GBRunEvalContext *ec,
		 GBRunObject      *object,
		 GBValue         **args)
{
	GBRunFileHandle *h;
	GBValue         *res;
	long             pos;

	FILE_FUNC_SEC_CHECK (ec, "seek");

	GB_IS_VALUE (ec, args [0], GB_VALUE_INT);

	h = internal_handle_from_gb_no (ec, args [0]->v.i);
	if (!h) {
		gbrun_exception_fire (ec, _("Bad file handle"));
		return FALSE;
	}

	pos = ftell (h->file) + 1;

	if (h->mode == GB_OPEN_RANDOM)
		res = gb_value_new_long (pos / h->recordlen);
	else
		res = gb_value_new_long (pos);

	g_free (h);
	return res;
}

gboolean
gbrun_stmt_open (GBRunEvalContext *ec, const GBStatement *stmt)
{
	GBRunFileHandle  *h;
	GBRunFileHandle  *handles = ec->file_handles;

	GBValue  *filename  = gb_eval_context_eval (GB_EVAL_CONTEXT (ec), stmt->parm.open.filename);
	GBValue  *handle    = gb_eval_context_eval (GB_EVAL_CONTEXT (ec), stmt->parm.open.handle);
	GBValue  *reclen;
	FILE     *fd;
	int		 filedes;
	int		 lock = 0;

	FILE_STMT_SEC_CHECK (ec, "open");

	if (filename->gtk_type != gb_gtk_type_from_value (GB_VALUE_STRING) &&
	    handle->gtk_type   != gb_gtk_type_from_value (GB_VALUE_INT)) {
		gbrun_exception_fire (ec, _("Filename and filenumber are of incorrect types"));
		return FALSE;
	}

	if (handles [handle->v.i].used) {
		gbrun_exception_fire (ec, _("File number already in use !"));
		return FALSE;
	}

	/*
	 * If the file is opened with a filenumber between
	 * 256 and 512, open it locked.
	 */
	if (handle->v.i >= 256 && handle->v.i <= 512) {
		filedes = open (filename->v.s->str, O_CREAT);
		lock = 1;
	}

	switch (stmt->parm.open.mode) {

	case GB_OPEN_OUTPUT:
		if (lock != 1) {
			fd = fopen (filename->v.s->str, "w");
		} else {
			fd = fdopen (filedes, "w");
		}
		break;

	case GB_OPEN_INPUT:
		if (lock != 1) {
			fd = fopen (filename->v.s->str, "r");
		} else {
			fd = fdopen (filedes, "r");
		}
		break;

	case GB_OPEN_APPEND:
		if (lock != 1) {
			fd = fopen (filename->v.s->str, "a");
		} else {
			fd = fdopen (filedes, "a");
		}
		break;

	case GB_OPEN_BINARY: case GB_OPEN_RANDOM:
		/*
		 * Small workaround to create file if it doesn't
		 * exist. Apparently, directly using ab+ doesn't allow
		 * repositioning of stream : behaves funnily
		 */
		if (lock != 1 ) {
			fd = fopen (filename->v.s->str, "ab+");
			fclose (fd);
			fd = fopen (filename->v.s->str, "rb+");
		} else {
			fd = fdopen (filedes, "rb+");
		}
		break;

	default:
		g_warning ("Unhandled OPEN mode");
		break;
	}

	if (!fd) {
		gbrun_exception_fire (ec, _("Open failed"));
		return FALSE;
	}

	h = &handles [handle->v.i];

	h->fileno = handle->v.i;
	h->mode   = stmt->parm.open.mode;
	h->file   = fd;
	h->used   = TRUE;

	if (stmt->parm.open.recordlen) {
		reclen = gb_eval_context_eval (GB_EVAL_CONTEXT (ec), stmt->parm.open.recordlen);
		h->recordlen = reclen->v.i;
	} else
		h->recordlen = 1;   /* Nothing specified for record length */

	if (h->mode == GB_OPEN_BINARY)
		h->recordlen = 1;  /* Ignore recordlen for Binary mode */

	/* Rewind file pointer to beginning always */
	fseek (h->file, 0, SEEK_SET);

	return TRUE;
}

gboolean
gbrun_stmt_close (GBRunEvalContext *ec, const GBStatement *stmt)
{
	GSList *l;
	GBRunFileHandle *handles = ec->file_handles;

	FILE_STMT_SEC_CHECK (ec, "close");

	/* If no arguments are passed, close all open files */
	if (!stmt->parm.close.handles)
		return gbrun_files_clean (ec);

	for (l = stmt->parm.close.handles; l != NULL; l = l->next) {
		GBRunFileHandle *h;
		GBExpr          *handle = l->data;
		GBValue         *fileno;

		fileno = gbrun_eval_as (ec, handle, GB_VALUE_INT);

		h = internal_handle_from_gb_no (ec, fileno->v.i);

		if (h) {
			fclose (h->file);
			g_free (h);
			handles [fileno->v.i].used = FALSE;
			handles [fileno->v.i].file = NULL;
		} else {
			gb_value_destroy (fileno);
			gbrun_exception_fire (ec, _("Bad file handle"));
			return FALSE;
		}

		gb_value_destroy (fileno);
	}

	return TRUE;
}

static GBValue *
read_string (GBRunEvalContext *ec, GBRunFileHandle *h, gboolean allow_space)
{
	GArray  *chars;
	GBValue *val;
	char     chr;

	chars = g_array_new (FALSE, FALSE, sizeof (char));

	while (!feof (h->file)) {
		int  c;

		c = fgetc (h->file);
		if (c < 0)
			break;

		if (c == '\n')
			break;

		if (!allow_space && c == ' ')
			break;

		chr = (char)c;
		g_array_append_val (chars, chr);
	}

	chr = '\0';
	g_array_append_val (chars, chr);

	val = gb_value_new_string_chars (chars->data);

	g_array_free (chars, TRUE);

	return val;
}

gboolean
gbrun_stmt_line_input (GBRunEvalContext *ec, const GBStatement *stmt)
{
	GBValue         *handle;
	GBValue         *val;
	const GBExpr    *objref = stmt->parm.line_input.objref;
	GBRunFileHandle *h;

	FILE_STMT_SEC_CHECK (ec, "line input from");

	handle = gbrun_eval_as (ec, stmt->parm.line_input.handle, GB_VALUE_INT);

	h = internal_handle_from_gb_no (ec, handle->v.i);
	gb_value_destroy (handle);

	if (!h) {
		gbrun_exception_fire (ec, _("Bad file handle"));
		return FALSE;
	}



	if (objref->type != GB_EXPR_OBJREF) {
		gbrun_exception_fire (ec, _("Need an object ref"));
		return FALSE;

	} else if (h->mode != GB_OPEN_INPUT  &&
		   h->mode != GB_OPEN_RANDOM &&
		   h->mode != GB_OPEN_BINARY) {
		gbrun_exception_fire (ec, _("Can't read from a file not opened for input/random/binary"));
		return FALSE;
	}

	val = read_string (ec, h, FALSE);
	if (!val)
		return FALSE;

	if (!gbrun_eval_assign (ec, objref->parm.objref, val)) {
		gbrun_exception_fire (ec, _("Assignment of value to objectref failed"));
		return FALSE;
	}

	g_free (val);
	g_free (h);

	return TRUE;
}

gboolean
gbrun_stmt_input (GBRunEvalContext *ec, const GBStatement *stmt)
{
	GBValue         *handle;
	GBExprList      *objrefs = stmt->parm.input.objrefs;
	GBExprList      *list;
	GBRunFileHandle *h;

	FILE_STMT_SEC_CHECK (ec, "input from");

	handle = gbrun_eval_as (ec, stmt->parm.input.handle,
				GB_VALUE_INT);

	h = internal_handle_from_gb_no (ec, handle->v.i);
	gb_value_destroy (handle);

	if (!h) {
		gbrun_exception_fire (ec, _("Bad file handle"));
		return FALSE;
	}



	if (h->mode != GB_OPEN_INPUT  &&
	    h->mode != GB_OPEN_RANDOM &&
	    h->mode != GB_OPEN_BINARY) {
		gbrun_exception_fire (ec, _("Can't read from a file not opened for input/random/binary"));
		return FALSE;
	}

	for (list = objrefs; list != NULL; list = list->next) {
		GBValue *val;
		GBExpr  *objref = (GBExpr *)list->data;

		if (objref->type != GB_EXPR_OBJREF) {
			gbrun_exception_fire (ec, _("Need an object ref"));
			return FALSE;
		}

		val = read_string (ec, h, TRUE);
		if (!val)
			return FALSE;

		if (!gbrun_eval_assign (ec, objref->parm.objref, val)) {
			gbrun_exception_fire (ec, _("Assignment of value to objectref failed"));
			return FALSE;
		}
	}

	g_free (h);
	return TRUE;
}

gboolean
gbrun_stmt_get (GBRunEvalContext *ec, const GBStatement *stmt)
{
	GBValue         *handle;
	GBRunFileHandle *h;
	GBValue         *recordnum = NULL;
	long  recl;
	long  len;

	FILE_STMT_SEC_CHECK (ec, "get from");

	if (stmt->parm.get.recordnum)
		recordnum = gbrun_eval_as (ec, stmt->parm.get.recordnum,
					   GB_VALUE_LONG);

	handle = gbrun_eval_as (ec, stmt->parm.get.handle,
				GB_VALUE_INT);

	h = internal_handle_from_gb_no (ec, handle->v.i);
	gb_value_destroy (handle);

	if (!h) {
		gbrun_exception_fire (ec, _("Bad file handle"));
		return FALSE;
	}

	if (h->mode != GB_OPEN_RANDOM &&
	    h->mode != GB_OPEN_BINARY) {
		gbrun_exception_fire (ec, _("Can't Get from a file not opened in random/binary"));
		return FALSE;
	}

	if (h->mode == GB_OPEN_RANDOM)
		g_warning ("Get: Can't handle Random mode correctly yet.");

	/*
	 * FIXME: need to check bounds on recordnum ideally.
	 * The second IF should prevent trying to read past the
	 * end of the file.
	 */

	if (h->recordlen && recordnum) {
		recl = h->recordlen * recordnum->v.l - 1;
		len = get_file_len (h->file);

		if (recl < len) {
			fseek (h->file, recl, SEEK_SET);
		} else {
			gbrun_exception_fire (ec, _("Attempted to read past end of file"));
			return FALSE;
		}
	}

	if (!feof (h->file)) {
		GBValue      *val;
		GString      *str = g_string_new ("");
		const GBExpr *objref = stmt->parm.get.objref;
		int          i;

		for (i = 0; i < h->recordlen; ++i) {
			char c;
			c = (char) fgetc (h->file);
			g_string_append_c (str, c);
		}

		val = gb_value_new_string (str);

		if (!gbrun_eval_assign (ec, objref->parm.objref, val)) {
			gbrun_exception_fire (ec, _("Assignment of value to objref failed"));
			return FALSE;
		}

		gb_value_destroy (val);
		g_string_free (str, TRUE);
	}

	g_free (h);
	return TRUE;
}

gboolean
gbrun_stmt_put (GBRunEvalContext *ec, const GBStatement *stmt)
{
	GBValue         *handle;
	GBRunFileHandle *h;
	GBValue         *recordnum = NULL;
	GBValue         *tmp, *str;

	FILE_STMT_SEC_CHECK (ec, "put to");

	if (stmt->parm.put.recordnum)
		recordnum = gbrun_eval_as (ec, stmt->parm.put.recordnum,
					   GB_VALUE_LONG);

	handle = gbrun_eval_as (ec, stmt->parm.put.handle,
				GB_VALUE_INT);

	h = internal_handle_from_gb_no (ec, handle->v.i);
	gb_value_destroy (handle);

	if (!h) {
		gbrun_exception_fire (ec, _("Bad file handle"));
		return FALSE;
	}

	if (h->mode != GB_OPEN_RANDOM &&
	    h->mode != GB_OPEN_BINARY) {
		gbrun_exception_fire (ec, _("Can't Put to a file not opened in random/binary"));
		return FALSE;
	}

	if (h->mode == GB_OPEN_RANDOM)
		g_warning ("Put: Can't handle Random mode correctly yet.");

	/* FIXME: do we need to check ranges here ? */
	if (h->recordlen && recordnum)
		fseek (h->file, (h->recordlen) * (recordnum->v.l - 1), SEEK_SET);

        tmp = gbrun_eval_objref (ec, stmt->parm.put.objref);
	str = gb_value_promote (GB_EVAL_CONTEXT (ec),
				gb_gtk_type_from_value (GB_VALUE_STRING), tmp);
	gb_value_destroy (tmp);

	if ((fputs (str->v.s->str, h->file)) == EOF) {
		gbrun_exception_fire (ec, _("Error while putting to file"));
		return FALSE;
	}

	gb_value_destroy (str);
	g_free (h);

	return TRUE;
}

gboolean
gbrun_stmt_seek (GBRunEvalContext *ec, const GBStatement *stmt)
{
	GBValue          *handle;
	GBRunFileHandle  *h;
	GBValue          *pos;

	FILE_STMT_SEC_CHECK (ec, "seek within");

	handle = gbrun_eval_as (ec, stmt->parm.seek.handle,
				GB_VALUE_INT);

	h = internal_handle_from_gb_no (ec, handle->v.i);
	gb_value_destroy (handle);

	if (!h) {
		gbrun_exception_fire (ec, _("Bad file handle"));
		return FALSE;
	}

	pos = gbrun_eval_as (ec, stmt->parm.seek.pos,
			     GB_VALUE_LONG);
	if (pos->v.l <= 0) {
		gbrun_exception_fire (ec, _("Can't seek to a non-positive position"));
		return FALSE;
	}

	if (h->mode != GB_OPEN_RANDOM) {
		if (fseek (h->file, pos->v.l - 1, SEEK_SET)) {
			gbrun_exception_fire (ec, _("Unable to seek"));
			return FALSE;
		}
	} else {
		if (fseek (h->file, (pos->v.l - 1) * h->recordlen, SEEK_SET)) {
			gbrun_exception_fire (ec, _("Unable to seek"));
			return FALSE;
		}
	}

	g_free (h);
	return TRUE;

}

gboolean
gbrun_stmt_print (GBRunEvalContext *ec, const GBStatement *stmt)
{
	GBValue           *handle;
	GBRunFileHandle   *h;
	GBExprList        *l;

	FILE_STMT_SEC_CHECK (ec, "print to");

	handle = gbrun_eval_as (ec, stmt->parm.print.handle,
				GB_VALUE_INT);
	
	h = internal_handle_from_gb_no (ec, handle->v.i);
	gb_value_destroy (handle);

	if (!h) {
		gbrun_exception_fire (ec, _("Bad file handle"));
		return FALSE;
	}

	if (h->mode != GB_OPEN_OUTPUT &&
	    h->mode != GB_OPEN_APPEND) {
		gbrun_exception_fire (ec, _("Print valid only for Output/Append modes"));
		return FALSE;
	}

	for (l = stmt->parm.print.items; l ; l = l->next) {
		GBValue           *tmp, *str;	
		GBExpr            *expr = (GBExpr *) l->data;
		
		if (expr->type == GB_EXPR_OBJREF)
			tmp = gbrun_eval_objref (ec, expr);
		else 
			tmp = gbrun_eval_as (ec, expr, GB_VALUE_STRING);

		str = gb_value_promote (GB_EVAL_CONTEXT (ec), 
					gb_gtk_type_from_value (GB_VALUE_STRING),
						tmp);

		gb_value_destroy (tmp);
		
		if (!str) {
			gbrun_exception_fire (ec, _("Couldn't promote to string before printing"));
			return FALSE;
		}

		if ((fputs (str->v.s->str, h->file)) == EOF) {
			gbrun_exception_fire (ec, _("Error while printing to file"));
			return FALSE;
		}		     
		
		gb_value_destroy (str);
		
	}
	fputs ("\n", h->file);
	g_free (h);
	return TRUE;
	
}

void
gbrun_files_init (GBRunEvalContext *ec)
{
	ec->file_handles = g_new0 (GBRunFileHandle, 512);
}

gboolean
gbrun_files_clean (GBRunEvalContext *ec)
{
	int              loop;
	GBRunFileHandle *handles = ec->file_handles;

	for (loop = 0; loop < 512; loop++) {
		if (handles [loop].used == TRUE)
			fclose (handles [loop].file);
 	}

	g_free (ec->file_handles);

	return TRUE;
}
