/**
 * GNOME Basic User defined type bits.
 *
 *   A 'Type' is ( confusingly ) the
 * analogue of a C structure.
 *
 * Author:
 *    Michael Meeks <mmeeks@gnu.org>
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <gbrun/gbrun-type.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-object.h>

static void
gbrun_type_destroy (GtkObject *object)
{
	GBRunType *type = (GBRunType *) object;

	if (type->values)
		gbrun_stack_level_destroy (type->values);
	type->values = NULL;
}

static void
gbrun_type_copy (GBEvalContext  *ec,
		  GBObject       *src,
		  GBObject       *dest)
{
	g_warning ("FIXME: copying a type, unimplemented");
}

static gboolean
gbrun_type_assign (GBEvalContext  *ec,
		   GBObject       *object,
		   const GBObjRef *ref,
		   GBValue        *value,
		   gboolean        try_assign)
{
	GBValue **type_val;
	GBRunType *type = GBRUN_TYPE (object);

	type_val = gbrun_stack_level_lookup (type->values, ref->name);

	if (!type_val && try_assign)
		return FALSE;

	if (ref->parms) {
		if (!type_val) {
			if (!try_assign)
				gbrun_exception_firev (
					GBRUN_EVAL_CONTEXT (ec),
					_("No array or collection in type %s"), ref->name);

			return FALSE;

		} else if (*type_val &&
			   GB_IS_AN_OBJECT ((*type_val)->gtk_type)) {
			GBObjRef newref = *ref;

			newref.name = NULL;

			return gb_object_assign (ec, (*type_val)->v.obj, &newref, value, try_assign);

		} else {
			if (!try_assign)
				gbrun_exception_firev (
					GBRUN_EVAL_CONTEXT (ec),
					_("Type element %s is not a method"), ref->name);

			return FALSE;
		}
	} else if (type_val) {
		if (*type_val)
			gb_value_destroy (*type_val);
		*type_val = gbrun_value_copy (ec, value);
		return TRUE;

	} else {
		if (!try_assign)
			gbrun_exception_firev (
				GBRUN_EVAL_CONTEXT (ec),
				_("Type has no element %s"), ref->name);

		return FALSE;
	}
}

static GBValue *
gbrun_type_deref (GBEvalContext  *gb_ec,
		  GBObject       *object,
		  const GBObjRef *ref,
		  gboolean        try_deref)
{
	GBRunEvalContext *ec = GBRUN_EVAL_CONTEXT (gb_ec);
	GBValue  **val, *ret;
	GBRunType *type = GBRUN_TYPE (object);

	val = gbrun_stack_level_lookup (type->values, ref->name);

	if (!val || !*val) {
		if (try_deref)
			return NULL;
		else
			return gbrun_exception_firev (
				ec, _("No such element %s"), ref->name);
	}

	if (ref->method || ref->parms) {

		if (GB_IS_AN_OBJECT ((*val)->gtk_type)) {
			GBObjRef newref = *ref;

			newref.name = NULL;

			ret = gb_object_deref (gb_ec, (*val)->v.obj, &newref, try_deref);

		} else {
			if (try_deref)
				return NULL;
			else
				return gbrun_exception_firev (
					ec, _("element %s is not a method"), ref->name);
		}
	} else
		ret = gbrun_value_copy (ec, *val);

	return ret;
}

static void
gbrun_type_class_init (GBObjectClass *klass)
{
	GtkObjectClass *gtk_class = (GtkObjectClass *) klass;

	klass->copy   = gbrun_type_copy;
	klass->assign = gbrun_type_assign;
	klass->deref  = gbrun_type_deref;

	gtk_class->destroy = gbrun_type_destroy;
}

static void
gbrun_type_init (GBRunType *type, GBRunTypeClass *klass)
{
	GSList *l;

	type->values = gbrun_stack_level_new ("a gb structure");
	
	g_return_if_fail (klass->type != NULL);
	
	for (l = klass->type->vars; l; l = l->next) {
		/* FIXME: dodgy, these context issues need nailing */
		GBVar *var = l->data;
		GBValue *val = gbrun_value_default_from_var (NULL, var);

		gbrun_stack_level_add (NULL, type->values, var->name, val);
	}
}

GtkType
gbrun_type_get_type (void)
{
	static GtkType type_type = 0;

	if (!type_type) {
		static const GtkTypeInfo type_info = {
			"GBRunType",
			sizeof (GBRunType),
			sizeof (GBRunTypeClass),
			(GtkClassInitFunc)  gbrun_type_class_init,
			(GtkObjectInitFunc) gbrun_type_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type_type = gtk_type_unique (GB_TYPE_OBJECT, &type_info);
	}

	return type_type;	
}


void
gbrun_register_types (GBRunEvalContext *ec,
		      GBRunObjectClass *klass,
		      GSList           *types)
{
	GSList *l;

	/*
	 * FIXME: this is a hack; we need to be able to scope
	 * types, which could be really, really fun. This would
	 * seem to involve re-writing the gtk type system, or
	 * at least, hacking it in some clever ( ugly ) way.
	 * for now, a Private type is a pipe-dream, all are public.
	 */

	for (l = types; l; l = l->next) {
		GBType *type = l->data;
		GBRunTypeClass *klass;
		GtkType t = gbrun_object_subclass_simple (
			gbrun_type_get_type (), type->name);

		klass = gtk_type_class (t);

		klass->type = type;
	}
}

