/*
 * GNOME Basic Enterpreter library main.
 *
 * Author:
 *    Michael Meeks <mmeeks@gnu.org>
 *
 * Copyright 2000, Helix Code, Inc
 */

#include <string.h>
#include <gbrun/gbrun.h>
#include <gb/libgb.h>
#include <gb/gb-main.h>
#include <gb/gb-form.h>
#include <gb/gb-mmap-lex.h>
#include <gbrun/libgbrun.h>
#include <gbrun/gbrun-type.h>
#include <gbrun/gbrun-stack.h>
#include <gbrun/gbrun-statement.h>
#include <gbrun/objects/libgbobj.h>
#include <gbrun/objects/gba/libgba.h>
#include <gbrun/objects/gbrun-objects.h>

#undef PROJECT_DEBUG

#define HAVE_FORM_CODE

struct _GBRunProjectPriv {
	GBRunStreamProvider *provider;
	gpointer             user_data;
	GBProject           *gb_proj;
	GHashTable          *objects;
	GSList              *modules;
};

static gboolean
obj_release (gpointer key, gpointer value, gpointer user_data)
{
	g_free (key);
	gb_object_unref (value);

	return TRUE;
}

static void
project_destroy (GtkObject *obj)
{
	GBRunProject     *proj = GBRUN_PROJECT (obj);
	GBRunProjectPriv *priv = proj->priv;
	GSList           *l;

	gb_project_destroy (priv->gb_proj);
	priv->gb_proj = NULL;

	g_hash_table_foreach_remove (priv->objects, obj_release, NULL);
	g_hash_table_destroy (priv->objects);
	priv->objects = NULL;

	for (l = priv->modules; l; l = l->next)
		gb_object_unref (l->data);
	g_slist_free (priv->modules);
	priv->modules = NULL;

	g_free (priv);
}

static void
gbrun_project_copy (GBEvalContext  *ec,
		    GBObject       *src,
		    GBObject       *dest)
{
	g_warning ("FIXME: copying a project, wierd");
}

static gboolean
gbrun_project_assign (GBEvalContext  *ec,
		      GBObject       *obj,
		      const GBObjRef *ref,
		      GBValue        *value,
		      gboolean        try_assign)
{
	GSList *l;
	gboolean ret;
	GBRunProject *proj = GBRUN_PROJECT (obj);

	if (!try_assign)
		g_error ("Can't assign to project");

	for (l = proj->priv->modules; l; l = l->next) {
		if ((ret = gb_object_assign (ec, GB_OBJECT (l->data),
					     ref, value, TRUE))
		    || gb_eval_exception (ec)) {
#ifdef PROJECT_DEBUG
			g_warning ("Found method '%s' on a module", ref->name);
#endif
			return ret;
		}
	}

	return FALSE;
}

static GBValue *
gbrun_project_deref (GBEvalContext  *ec,
		     GBObject       *obj,
		     const GBObjRef *ref,
		     gboolean        try_deref)
{
	GSList *l;
	GBObject *o;
	GBValue *ret = NULL;
	GBRunProject *proj = GBRUN_PROJECT (obj);

#ifdef PROJECT_DEBUG
	g_warning ("In proj deref '%d'",
		   g_hash_table_size (proj->priv->objects));
#endif

	/* FIXME: consider public / private here */
	if ((o = g_hash_table_lookup (proj->priv->objects, ref->name))) {
#ifdef PROJECT_DEBUG
		g_warning ("Found object '%s' on project ", ref->name);
#endif
		return gb_value_new_object (o);
	}

	for (l = proj->priv->modules; l; l = l->next) {
		if ((ret = gb_object_deref (ec, GB_OBJECT (l->data),
					    ref, TRUE))
		    || gb_eval_exception (ec)) {
#ifdef PROJECT_DEBUG
			g_warning ("Found method '%s' on a module", ref->name);
#endif
			return ret;
		}
	}

#ifdef PROJECT_DEBUG
	g_warning ("Found no method '%s' on module", ref->name);
#endif
	return NULL;
}

static void
gbrun_project_class_init (GBObjectClass *klass)
{
	GtkObjectClass    *gtk_class  = (GtkObjectClass *) klass;
/*	GBRunProjectClass *proj_class = (GBRunProjectClass *) klass;*/
	
	klass->copy   = gbrun_project_copy;
	klass->assign = gbrun_project_assign;
	klass->deref  = gbrun_project_deref;

	gtk_class->destroy = project_destroy;
}

static void
gbrun_project_init (GBRunProject *proj)
{
	proj->priv = g_new0 (GBRunProjectPriv, 1);
	proj->priv->objects = g_hash_table_new (
		gb_strcase_hash, gb_strcase_equal);
}

GtkType
gbrun_project_get_type (void)
{
	static GtkType proj_type = 0;

	if (!proj_type) {
		static const GtkTypeInfo proj_info = {
			"GBRunProject",
			sizeof (GBRunProject),
			sizeof (GBRunProjectClass),
			(GtkClassInitFunc) gbrun_project_class_init,
			(GtkObjectInitFunc) gbrun_project_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		proj_type = gtk_type_unique (GB_TYPE_OBJECT, &proj_info);
	}

	return proj_type;
}

static GBRunObjectClass *
get_class (const char *opt_name, const GBParseData *pd)
{
	GBRunObjectClass *klass;
	GtkType           type;

	g_return_val_if_fail (pd != NULL, NULL);

	if (pd->form) {
#ifdef HAVE_FORM_CODE
		type = gbrun_object_subclass_simple (
			gbrun_form_get_type (), pd->form->name);
#else
		g_warning ("No form code");
		return NULL;
#endif
	} else
		type = gbrun_object_subclass_simple (
			gbrun_object_get_type (), opt_name);
	
	klass = gtk_type_class (type);

	return klass;
}

static GBRunObjectClass *
add_data_to_class (GBRunEvalContext  *ec,
		   GBRunObjectClass  *klass,
		   const GBParseData *pd)
{
	g_return_val_if_fail (ec != NULL, NULL);
	g_return_val_if_fail (pd != NULL, NULL);
	g_return_val_if_fail (klass != NULL, NULL);

	/*
	 * FIXME: this is a hack; we need to be able to scope
	 * types, which could be really, really fun. This would
	 * seem to involve re-writing the gtk type system, or
	 * at least, hacking it in some clever ( ugly ) way.
	 * for now, a Private type is a pipe-dream, all are public.
	 */
	gbrun_register_types (ec, klass, pd->types);
	gbrun_object_add_routines  (ec, klass, pd->routines);
	gbrun_object_add_variables (ec, klass, pd->variables);
	gbrun_object_add_constants (ec, klass, pd->constants);

	return klass;
}

const GBParseData *
parsed_load (GBRunEvalContext    *ec,
	     const char          *filename,
	     GBRunStreamProvider *provider,
	     gpointer             user_data,
	     GBParsingState       state)
{
	GBLexerStream     *ls;
	const GBParseData *pd;

	if (!(ls = provider (ec, filename, user_data)))
		return NULL;

	/* Set parser state before and then turn it off later in grammar */
	gb_lexer_stream_state_set (ls, state);

	pd = gb_parse_stream (GB_EVAL_CONTEXT (ec), ls);
	gtk_object_destroy (GTK_OBJECT (ls));

	return pd;
}

void
gbrun_project_register_module (GBRunProject *project,
			       GBObject     *object)
{
	project->priv->modules = g_slist_prepend (
		project->priv->modules, gb_object_ref (object));
}

void
gbrun_project_deregister_module (GBRunProject *project,
				 GBObject     *object)
{
	if (g_slist_find (project->priv->modules, object)) {
		project->priv->modules = g_slist_remove (
			project->priv->modules, object);
		gb_object_unref (object);
	} else
		g_warning ("Can't find module object to remove");
}

GBRunProject *
gbrun_project_new (GBRunEvalContext *ec, GBProject *p,
		   GBRunStreamProvider *provider,
		   gpointer user_data)
{
	GSList           *l;
	GBRunProject     *proj;
	GBRunProjectPriv *priv;
	char             *proj_name;

	g_return_val_if_fail (p != NULL, NULL);
	g_return_val_if_fail (provider != NULL, NULL);
	proj = GBRUN_PROJECT (gtk_type_new (GBRUN_TYPE_PROJECT));

	priv = proj->priv;

	proj_name = gbrun_eval_context_get_module (ec);

	priv->provider  = provider;
	priv->user_data = user_data;
	priv->gb_proj   = p;

	gbrun_eval_context_proj_push (ec, proj);

	for (l = p->modules; l; l = l->next) {
		GBProjectPair     *pp = l->data;
		const GBParseData *pd;
		GBRunObject       *obj;

#ifdef PROJECT_DEBUG
		fprintf (stderr, "Loading module '%s'\n", pp->filename);
#endif

		gbrun_eval_context_set_module (ec, pp->filename);

		if (!(pd = parsed_load (ec, pp->filename, provider, user_data, GB_PARSING_BASIC)))
			return NULL;

		
		obj = gbrun_object_new (
			add_data_to_class (
				ec, get_class (pp->name, pd), pd));

		/* FIXME: this should be attached to the object somehow */
		gb_options_copy (&ec->options, &pd->options);

/*		gb_parse_data_destroy (pd);*/

		gbrun_project_register_module (proj, GB_OBJECT (obj));
		gb_object_unref (GB_OBJECT (obj));
	}

	for (l = p->classes; l; l = l->next) {
		GBProjectPair     *pp = l->data;
		const GBParseData *pd;
		GBRunObject       *obj;

#ifdef PROJECT_DEBUG
		fprintf (stderr, "Loading class '%s'\n", pp->filename);
#endif

		gbrun_eval_context_set_module (ec, pp->filename);

		if (!(pd = parsed_load (ec, pp->filename, provider, user_data, GB_PARSING_CLASS)))
			return NULL;

		obj = gbrun_object_new (
			add_data_to_class (
				ec, get_class (pp->name, pd), pd));
/*		gb_parse_data_destroy (pd);*/
		
		/* FIXME: this should be attached to the object somehow */
		gb_options_copy (&ec->options, &pd->options);

		gbrun_project_register_object (proj, pp->name, obj);
		gb_object_unref (GB_OBJECT (obj));
	}
		
	for (l = p->forms; l; l = l->next) {
		char              *filename = l->data;
		const GBParseData *pd;
		GBRunObject       *obj;

#ifdef PROJECT_DEBUG
		fprintf (stderr, "Loading form '%s'\n", (char *)l->data);
#endif

		gbrun_eval_context_set_module (ec, filename);

		if (!(pd = parsed_load (ec, filename, provider, user_data, GB_PARSING_FORM)))
			return NULL;

		obj = gbrun_object_new (
			add_data_to_class (
				ec, get_class ("Unused", pd), pd));

#ifdef HAVE_FORM_CODE	
		gbrun_project_register_object (proj, pd->form->name, obj);
		gb_object_unref (GB_OBJECT (obj));

		gbrun_form_init (ec, GBRUN_FORM (obj), pd);
		gbrun_form_invoke (ec, GBRUN_FORM (obj), "Form_Load", NULL);
#endif
		/* FIXME: this should be attached to the object somehow */
		gb_options_copy (&ec->options, &pd->options);

/*		gb_parse_data_destroy (pd);*/
	}

	gbrun_eval_context_set_module (ec, proj_name);
	g_free (proj_name);

	gbrun_eval_context_proj_pop (ec);

	return proj;
}

gboolean
gbrun_project_execute (GBRunEvalContext *ec, GBRunProject *proj)
{
	const char *startup;
	gboolean    success;

	g_return_val_if_fail (GBRUN_IS_EVAL_CONTEXT (ec), FALSE);
	g_return_val_if_fail (GBRUN_IS_PROJECT (proj), FALSE);
	g_return_val_if_fail (proj->priv != NULL, FALSE);
	g_return_val_if_fail (proj->priv->gb_proj != NULL, FALSE);

	startup = gb_project_startup_get (proj->priv->gb_proj);

	if (!g_strncasecmp (startup, "Sub ", 4)) {
		char    *subname = g_strchug (g_strchomp (g_strdup (startup + 4)));
		GBValue *val;

/*		g_warning ("Project sub : '%s'", subname);*/
		
		val = gbrun_project_invoke (ec, proj, subname, NULL);
		gb_value_destroy (val);

		g_free (subname);

		success = !gbrun_eval_context_exception (ec);
	} else { /* A form */
/*		g_warning ("Project form : '%s'", startup);*/
		/* FIXME: think about all this ... */
		gbrun_eval_context_proj_push (ec, proj);
		gtk_main ();
		gbrun_eval_context_proj_pop (ec);
		success = TRUE;
	}

	return success;
}

void
gbrun_init (GBEvalContext *ec)
{
	gbrun_object_init (ec);
	if (gb_eval_exception (ec))
		return;

	libgba_register (ec);
	if (gb_eval_exception (ec))
		return;

	gbrun_objects_register (ec);
}


void
gbrun_shutdown (void)
{
	gbrun_objects_shutdown ();
	libgba_shutdown        ();
	gbrun_object_shutdown  ();
}

void
gbrun_project_register_object (GBRunProject *proj,
			       const char   *name,
			       GBRunObject  *object)
{
	g_return_if_fail (name != NULL);
	g_return_if_fail (GBRUN_IS_PROJECT (proj));

	if (!g_hash_table_lookup (proj->priv->objects, name)) {
		g_hash_table_insert (proj->priv->objects, g_strdup (name),
				     gb_object_ref (GB_OBJECT (object)));
	} else
		g_warning ("Registered project object '%s' twice", name);
}

void
gbrun_project_deregister_object (GBRunProject *proj,
				 const char   *name)
{
	gpointer orig_key;
	gpointer orig_value;

	g_return_if_fail (GBRUN_IS_PROJECT (proj));

	if (g_hash_table_lookup_extended (proj->priv->objects, name,
					  &orig_key, &orig_value)) {
		g_hash_table_remove (proj->priv->objects, name);
		g_free (orig_key);
		gb_object_unref (GB_OBJECT (orig_value));
	} else
		g_warning ("Trying to deregister '%s' which is not registered", name);
}

GBValue *
gbrun_project_invoke (GBRunEvalContext *ec, GBRunProject *proj,
		      const char *name, GSList *args)
{
	GBObjRef ref;
	GSList  *l, *exprs = NULL;
	GBValue *val;

	g_return_val_if_fail (ec != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);

	gbrun_eval_context_proj_push (ec, proj);

	/* FIXME */
	ref.method = FALSE;
	ref.name   = name;

	/* FIXME: Amusingly inefficient */
	for (l = args; l; l = l->next) {
		gpointer expr = (gpointer) gb_expr_new_value (gbrun_value_copy (ec, l->data));
		exprs = g_slist_prepend (exprs, expr);
	}

	exprs = g_slist_reverse (exprs);
	ref.parms = exprs;
	ref.method = TRUE;

	val = gbrun_objref_deref (ec, NULL, &ref, TRUE);

	while (exprs) {
		gb_expr_destroy (exprs->data);
		exprs = g_slist_remove (exprs, exprs->data);
	}

	gbrun_eval_context_proj_pop (ec);

	return val;
}

/*
 * An ugly hack of a function: do not use.
 */
GSList *
gbrun_project_fn_names (GBRunProject *proj)
{
	GSList *ans = NULL;
	GSList *l;
	GSList *m, *i;

	g_return_val_if_fail (proj != NULL, NULL);
	g_return_val_if_fail (proj->priv != NULL, NULL);

	for (l = proj->priv->modules; l; l = l->next) {
		m = gbrun_object_get_methods (GBRUN_OBJECT_GET_CLASS (l->data));

		for (i = m; i; i = i->next) {
			GBRunObjMethod *method = i->data;
			
			ans = g_slist_prepend (ans, method->name);
		}
		g_slist_free (m);
	}

	return ans;
}

static const GBParseData *
parse_str (GBRunEvalContext *ec,
	   const char       *str,
	   gboolean          needs_eol,
	   GBParsingState    state)
{
	GBLexerStream *ls;
	char          *terminated;
	int            len;
	const GBParseData *pd;

	g_return_val_if_fail (str != NULL, NULL);

	len = strlen (str);
	if (needs_eol) {
		if (str [len] != '\n') {
			terminated = g_strconcat (str, "\n", NULL);
			len++;
		} else
			terminated = g_strdup (str);
	} else {
		if (str [len] == '\n')
			len--;

		terminated = g_strdup (str);
	}

/*	g_warning ("Trying to parse '%s'", terminated); */

	ls = gb_mmap_stream_new (terminated, len);

	gb_lexer_stream_state_set (ls, state);

	pd = gb_parse_stream (GB_EVAL_CONTEXT (ec), ls);
	gtk_object_destroy (GTK_OBJECT (ls));

	if (!pd)
		return NULL;

/*	g_warning ("Parsed ok"); */

	return pd;
}

void
gbrun_exec_str (GBRunEvalContext *ec,
		GBRunObject      *opt_object,
		const char       *basic_string)
{
	const GBParseData *pd;

	g_return_if_fail (GBRUN_IS_EVAL_CONTEXT (ec));
	g_return_if_fail (!opt_object || GBRUN_IS_OBJECT (opt_object));

	pd = parse_str (ec, basic_string, TRUE, GB_PARSING_STATEMENTS);
	if (!pd)
		return;

	if (pd->stmts) {
		if (opt_object)
			gbrun_eval_context_me_set (
				ec, GB_OBJECT (opt_object));

		gbrun_stmts_evaluate (ec, pd->stmts);
	}

	gb_parse_data_destroy (pd);
}

GBValue *
gbrun_eval_str (GBRunEvalContext *ec,
		GBRunObject      *opt_object,
		const char       *basic_string)
{
	const GBParseData *pd;
	GBValue           *val = NULL;

	g_return_val_if_fail (GBRUN_IS_EVAL_CONTEXT (ec), NULL);
	g_return_val_if_fail (!opt_object || GBRUN_IS_OBJECT (opt_object), NULL);

	pd = parse_str (ec, basic_string, FALSE, GB_PARSING_EXPR);
	if (!pd)
		return NULL;

	if (pd->expr) {
		if (opt_object)
			gbrun_eval_context_me_set (
				ec, GB_OBJECT (opt_object));

		val = gb_eval_context_eval (
			GB_EVAL_CONTEXT (ec), pd->expr);
	}

	gb_parse_data_destroy (pd);

	return val;
}
