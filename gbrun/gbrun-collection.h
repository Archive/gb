/*
 * GNOME Basic Collection handling,
 * a fairly abstract base class
 *
 * Author:
 *    Michael Meeks <mmeeks@gnu.org>
 *
 * Copyright 2000, Helix Code, Inc
 */
#ifndef GBRUN_COLLECTION_H
#define GBRUN_COLLECTION_H

#include <gbrun/gbrun-object.h>

#define GBRUN_TYPE_COLLECTION            (gbrun_collection_get_type ())
#define GBRUN_COLLECTION(obj)            (GTK_CHECK_CAST ((obj), GBRUN_TYPE_COLLECTION, GBRunCollection))
#define GBRUN_COLLECTION_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GBRUN_TYPE_COLLECTION, GBRunCollectionClass))
#define GBRUN_IS_COLLECTION(obj)         (GTK_CHECK_TYPE ((obj), GBRUN_TYPE_COLLECTION))
#define GBRUN_IS_COLLECTION_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GBRUN_TYPE_COLLECTION))
#define GBRUN_COLLECTION_GET_CLASS(obj)  (GBRUN_COLLECTION_CLASS (GTK_OBJECT (obj)->klass))

struct _GBRunCollection {
	GBRunObject object;

	gpointer    private; /* A GSList for now */
};

/*
 * NB. elements can have NULL names if they
 * are added at a numeric index.
 */
typedef struct {
	char    *name;
	GBValue *value;
} GBRunCollectionElement;

typedef struct {
	GBRunObjectClass klass;

	/*
	 * Sub-classers can choose 4 use cases.
	 *   a) No overriding, uses an internal list
	 *   b) override add, remove, enumerate
	 *   c) as for b) + override count & lookup
	 *   d) Override using VB methods of the same name.
	 */
	GBValue *(*add)       (GBRunEvalContext *ec,
			       GBRunCollection  *collection,
			       const char       *name,
			       GBValue          *value);

	void     (*remove)    (GBRunEvalContext *ec,
			       GBRunCollection  *collection,
			       const char       *name);

	/* Returns a list of allocated GBRunCollectionElements */
	GSList  *(*enumerate) (GBRunEvalContext *ec,
			       GBRunCollection  *collection);

	guint    (*count)     (GBRunEvalContext *ec,
			       GBRunCollection  *collection);

	GBValue *(*lookup)    (GBRunEvalContext *ec,
			       GBRunCollection  *collection,
			       const char       *name);
} GBRunCollectionClass;

GtkType          gbrun_collection_get_type    (void);

GBRunCollection *gbrun_collection_construct   (GBRunCollection *collection);

GBRunCollectionElement *
                 gbrun_collection_element_new     (GBEvalContext *ec,
						   const char    *name,
						   GBObject      *object);

GBRunCollectionElement *
                 gbrun_collection_element_new_val (GBEvalContext *ec,
						   const char    *name,
						   const GBValue *value);

void             gbrun_collection_element_destroy (GBRunCollectionElement *elem);


#endif /* GBRUN_COLLECTION_H */
