/*
 * GNOME Basic Object runtime ...
 *
 * Authors:
 *    Michael Meeks (mmeeks@gnu.org)
 *
 * Copyright 2000, Helix Code Inc.
 */

#include <math.h>
#include <setjmp.h>
#include <ctype.h>

#include <gbrun/gbrun.h>
#include <gbrun/gbrun-object.h>
#include <gbrun/gbrun-array.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-stack.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-statement.h>
#include <gbrun/gbrun-project.h>
#include <gbrun/gbrun-global.h>
#include <gbrun/gbrun-collection.h>

#undef VB_METHOD_DEBUG
#undef VB_INIT_DEBUG

#undef TRACE


struct _GBRunObjectPrivate {
	GBRunStackLevel    *vars;
};

struct _GBRunObjectPrivateClass {
	GSList *variables;

	GSList *constants;

	GSList *classes;

	GSList *properties;

	GBRunObjMethod *default_method;
	GSList         *methods;
};

void
gbrun_object_var_add (GBRunEvalContext *ec,
		      GBRunObject      *obj,
		      const char       *name,
		      GBValue          *value)
{
	g_return_if_fail (name != NULL);
	g_return_if_fail (value != NULL);
	g_return_if_fail (GBRUN_IS_OBJECT (obj));

	gbrun_stack_level_add (ec, obj->priv->vars, name, value);
}

void
gbrun_object_var_set (GBRunEvalContext *ec,
		      GBRunObject      *obj,
		      const char       *name,
		      GBValue          *value)
{
	GBValue **val;

	g_return_if_fail (GBRUN_IS_OBJECT (obj));

	val = gbrun_stack_level_lookup (obj->priv->vars, name);

	if (!val)
		gbrun_object_var_add (ec, obj, name, value);

	else {
		if (*val)
			gb_value_destroy (*val);

		*val = gbrun_value_copy (ec, value);
	}
}

GBValue *
gbrun_object_var_get (GBRunEvalContext *ec,
		      GBRunObject      *obj,
		      const char       *name)
{
	GBValue  *ret;
	GBValue **val;

	g_return_val_if_fail (GBRUN_IS_OBJECT (obj), NULL);

	val = gbrun_stack_level_lookup (obj->priv->vars, name);

	ret = (val && *val) ?
		gb_value_copy (GB_EVAL_CONTEXT (ec), *val) : NULL;

/*	g_warning ("Var get of '%s' on '%s' '%p'",
	name, gtk_type_name (GTK_OBJECT (obj)->klass->type), ret);*/
	
	return ret;
}

static GBValue *
gbrun_object_const_get (GBRunEvalContext *ec,
			GBRunObjectClass *klass,
			const char       *name)
{
	GSList *l;

	for (l = klass->priv->constants; l; l = l->next) {
		GBConst *cons = l->data;

		if (!g_strcasecmp (cons->name, name))
			return gb_value_copy (GB_EVAL_CONTEXT (ec),
					      cons->value);
	}
	return NULL;
}

static gboolean
gbrun_object_set_arg_unimplemented (GBRunEvalContext  *ec,
				    GBRunObject       *object,
				    int                property,
				    GBValue           *val)
{
	g_warning ("Set arg unimplemented");
	return FALSE;
}
	
static GBValue *
gbrun_object_get_arg_unimplemented (GBRunEvalContext  *ec,
				    GBRunObject       *object,
				    int                property)
{
	g_warning ("Get arg unimplemented");

	return NULL;
}

typedef struct {
	GBRunEvalContext *ec;
	GBRunObject      *obj;
} setup_vars_closure_t;

void
setup_vars (gpointer key, gpointer value, gpointer user_data)
{
	setup_vars_closure_t *c = user_data;
	GBVar                *var = value;
	GBValue              *val;

	if (var->is_array)
		val = gb_value_new_object (GB_OBJECT (gbrun_array_new (c->ec, var)));

	else {
		GtkType t;
		t = gb_gtk_type_from_name (var->type);
		if (!t)
			gb_eval_exception_firev (GB_EVAL_CONTEXT (c->ec),
						 _("Unknown type '%s'"), var->type);
		else
			val = gb_value_new_default (GB_EVAL_CONTEXT (c->ec), t);
	}

/*	g_warning ("Adding variable '%s' to class %s", var->name,
	gbrun_object_name (c->obj));*/

	gbrun_object_var_add (c->ec, c->obj, var->name, val);
}

/**
 * gbrun_object_add_routine:
 * @klass:   the class to add it to.
 * @routine: the routine structure
 * 
 * Add a VB routine to an existing class.
 **/
void
gbrun_object_add_routine (GBRunObjectClass *klass,
			  GBRoutine        *routine)
{
	GBRunObjMethod	*m;

/*	g_warning ("Adding '%s' to object '%s'\n",
		   routine->name,
		   gtk_type_name (GTK_OBJECT_CLASS (klass)->type));*/

	g_return_if_fail (klass   != NULL);
	g_return_if_fail (routine != NULL);

	m                       = g_new0   (GBRunObjMethod, 1);

	m->type                 = GBRUN_METHOD_VB;
	m->name                 = g_strdup (routine->name);
	m->args_parsed          = TRUE;
	m->args.parsed.args     = g_slist_copy (routine->args);
	m->args.parsed.as_type  = routine->as_type ?
		gb_gtk_type_from_name (routine->as_type) :
		0;
	m->handler.vb           = routine;
	/* FIXME: need to split parse_args code */
	m->args.parsed.max_args = g_slist_length (routine->args);
	m->args.parsed.min_args = m->args.parsed.max_args;
	/* No pure VB can have security implications */
	m->args.parsed.mask     = 0;
	m->is_sub               = !routine->is_function;
	
	klass->priv->methods    = g_slist_prepend (klass->priv->methods, m);
}

static void
add_routine (gpointer key, gpointer value, gpointer user_data)
{
	gbrun_object_add_routine (user_data, value);
}


/**
 * gbrun_object_add_routines
 * @ec:
 * @klass:   the class to add it to.
 * @routine: the routine structures
 * 
 * Add VB routines to an existing class.
 **/
void
gbrun_object_add_routines (GBRunEvalContext *ec,
			   GBRunObjectClass *klass,
			   GHashTable       *routines)
{
	g_return_if_fail (ec != NULL);
	g_return_if_fail (klass != NULL);

	if (!routines)
		return;

	g_hash_table_foreach (routines, add_routine, klass);
}

static void
add_variables (gpointer key, gpointer value, gpointer user_data)
{
	GBRunObjectClass *klass = user_data;
	GBVar            *var   = value;

	/* FIXME: we should dup var really */
	klass->priv->variables = g_slist_prepend (
		klass->priv->variables, var);
}

void
gbrun_object_add_variables (GBRunEvalContext *ec,
			    GBRunObjectClass *klass,
			    GHashTable       *variables)
{
	g_return_if_fail (ec != NULL);
	g_return_if_fail (klass != NULL);

	if (!variables) {
/*		g_warning ("No variables on object '%s'",
		gtk_type_name (GTK_OBJECT_CLASS (klass)->type));*/
		return;
	}

/*	g_warning ("Adding %d variables", g_hash_table_size (variables));*/

	g_hash_table_foreach (variables, 
			      add_variables, klass);
}

static void
add_constant (gpointer key, gpointer value, gpointer user_data)
{
	GBRunObjectClass *klass = user_data;
	GBConst          *cons  = value;

	/* FIXME: we should dup cons really */
	klass->priv->constants = g_slist_prepend (
		klass->priv->constants, cons);
}

void
gbrun_object_add_constants (GBRunEvalContext *ec,
			    GBRunObjectClass *klass,
			    GHashTable       *constants)
{
	g_return_if_fail (ec != NULL);
	g_return_if_fail (klass != NULL);

	if (!constants)
		return;

/*	g_warning ("Adding %d constants",
	g_hash_table_size (constants));*/

	g_hash_table_foreach (constants, add_constant, klass);
}

/*
 * FIXME: ugly, sluggish, tedious.
 */
GBValue *
parse_def (const char       *str,
	   GtkType           to)
{
	GBValue          *def_val, *tmp;
	GBRunEvalContext *ec;
	
	tmp = gb_value_new_string_chars (str);
	ec  = gbrun_eval_context_new ("Default arg promotion", GBRUN_SEC_HARD);

	def_val = gb_value_promote (GB_EVAL_CONTEXT (ec), to, tmp);
	gb_value_destroy (tmp);

	gtk_object_unref (GTK_OBJECT (ec));

	return def_val;
}

inline static GBArgDesc *
parse_arg (const char *arg_txt)
{
	GBArgDesc    *arg;
	gchar       **txt;
	int           i, max;
	const GBExpr *def_expr = NULL;
	gboolean      by_val   = FALSE;
	gboolean      optional = FALSE;

	txt = g_strsplit (arg_txt, ",", -1);

/*
 * txt [0]: Name
 * txt [1]: Type
 * txt [2]: [byref]
 * txt [3]: [default value] ( implies optional )
 */

	for (i = 0; txt [i]; i++);
	max = i;
	
	if (max < 2)
		g_error ("We must have 'name,type' at minimum in '%s'", arg_txt);

	if (txt [2]) {
		if (!g_strcasecmp (txt [2], "byref"))
			by_val = FALSE;
		
		if (txt [3]) { /* Extract default */
			GtkType t = gb_gtk_type_from_name (txt [1]);
			GBValue *def_value;
			
			if (g_strcasecmp (txt [3], "NULL")) {
				def_value = parse_def (txt [3], t);
				if (!def_value)
					g_error ("Syntax error in '%s', trying to promote "
						 "'%s' to type '%s'", arg_txt, txt [3], txt [1]);
				def_expr = gb_expr_new_value (def_value);
			}
			
			optional = TRUE;
		}
	}

	arg = gb_arg_desc_new (txt [0], txt [1], def_expr,
			       by_val, optional);

	g_strfreev (txt);

	return arg;
}

static GBRunSecurityFlag
parse_security (const char *txt)
{
	GBRunSecurityFlag f;

	while (*txt) {
		switch (*txt) {
		case 'n':
		case 'N':
			f = GBRUN_SEC_NONE;
			break;

		case 'x':
		case 'X':
			f |= GBRUN_SEC_EXEC;
			break;

		case 'g':
		case 'G':
			f |= GBRUN_SEC_GUI;
			break;

		case 'w':
		case 'W':
			f |= GBRUN_SEC_WINE;
			break;

		case 'i':
		case 'I':
			f |= GBRUN_SEC_IO;
			break;

		default:
			g_warning ("Unknown security type '%c'\n", *txt);
			break;
		}
		txt++;
	}
	
	return 0;
}

/**
 * parse_args
 *   @m: GBRunObjMethod containing unparsed arguments.
 *
 * Parses the previously saved unparsed arguments.
 **/
static void
parse_args (GBRunObjMethod *m)
{
	char	 *unparsed_args;

	gchar	**txt;

	int	  i;
	int	  max;

	gboolean  vararg;

#ifdef TRACE
	printf ("parse_args entered.\n");
#endif
	if (m->args_parsed)
		return;

	unparsed_args = g_strdup (m->args.unparsed.args);
	g_free (m->args.unparsed.args);
	vararg        = m->args.unparsed.vararg;
	
	m->args.parsed.args     = NULL;
	m->args.parsed.min_args = 0;
	m->args.parsed.max_args = 0;

	txt = g_strsplit (unparsed_args, ";", -1);

	for (i = 0; txt [i]; i++);
	max = i;

	if (max < 1)
		g_error ("parse_args: Must have at least 1 sections in '%s'", unparsed_args);

	m->args.parsed.mask = parse_security (txt [max - 1]);

	for (i = max - (m->is_sub?1:2) - 1; i >= 0; i--) {
		GBArgDesc *arg;

		if (txt [i] [0] == '.')
			break;

		arg = parse_arg (txt [i]);
		
		m->args.parsed.args = g_slist_prepend (m->args.parsed.args, arg);
		if (!arg->optional)
			m->args.parsed.min_args++;
		m->args.parsed.max_args++;
	}

	if (m->is_sub)
		m->args.parsed.as_type = 0;
	else
		m->args.parsed.as_type = gb_gtk_type_from_name (txt [max - 2]);

	if (vararg)
		m->args.parsed.max_args = G_MAXINT;	

	m->args_parsed = TRUE;

	g_strfreev (txt);
	g_free (unparsed_args);
}


/**
 * save_arg_desc
 *   @a
 *   @arg_desc
 *   vararg
 *
 * Internal function to save arguments
 **/
static void
save_arg_desc (GBRunObjMethod *m, const char *arg_desc, gboolean vararg)
{
	gchar  **txt;

	int	 i;
	int	 max;

#ifdef TRACE
	printf ("save_arg_desc entered.\n");
#endif
	g_return_if_fail (arg_desc != NULL);

	m->args_parsed = FALSE;

	txt = g_strsplit (arg_desc, ";", 2);

	for (i = 0; txt [i]; i++);
	max = i;

	if (max < 3)
		g_error ("save_arg_desc: Must have at least 3 sections in '%s'", arg_desc);


	g_strchomp (txt [0]);
	if (!g_strcasecmp (txt [0], "sub")) {
		m->is_sub = TRUE;
	} else if (!g_strcasecmp (txt [0], "func")) {
		m->is_sub = FALSE;
	} else
		g_error ("Incorrect sub/func specifier '%s'", txt [0]);

	m->name                 = g_strdup (txt [1]);
	m->args.unparsed.args   = g_strdup (txt [2]);
	m->args.unparsed.vararg = vararg;
	
	g_strfreev (txt);
}


/**
 * gbrun_object_add_method_var:
 * @klass:  the class to add it to.
 * @desc:   the method's description.
 * @method: Method pointer.
 * 
 *  Add a variable no. of arguments method to
 * an existing class.
 **/
void
gbrun_object_add_method_var (GBRunObjectClass *klass,
			     const char       *desc,
			     GBRunMethodVar   *method)
{
	GBRunObjMethod *m;

	g_return_if_fail (desc  != NULL);
	g_return_if_fail (klass != NULL);

	m              = g_new0   (GBRunObjMethod, 1);
	m->type        = GBRUN_METHOD_VAR;
	m->handler.var = method;
	save_arg_desc (m, desc, TRUE);
	
	klass->priv->methods = g_slist_prepend (klass->priv->methods, m);
}

/**
 * gbrun_object_add_method_arg:
 * @klass:  the class to add it to.
 * @desc:   the method's description.
 * @method: Method pointer.
 * 
 * Add a method to an existing class.
 **/
void
gbrun_object_add_method_arg (GBRunObjectClass *klass,
			     const char       *desc,
			     GBRunMethodArg   *method)
{
	GBRunObjMethod *m;

	g_return_if_fail (desc != NULL);
	g_return_if_fail (klass != NULL);

	m              = g_new0   (GBRunObjMethod, 1);
	m->type        = GBRUN_METHOD_ARG;
	m->handler.arg = method;
	save_arg_desc (m, desc, FALSE);
	
	klass->priv->methods = g_slist_prepend (klass->priv->methods, m);
}

static GBRunObjMethod *
gbrun_object_get_method (GBRunObjectClass *klass,
			 const char       *name)
{
	GSList *l, *i;

	g_return_val_if_fail (klass != NULL, NULL);

	for (i = klass->priv->classes; i; i = i->next) {
		GBRunObjectClass *cur_class = i->data;

		if (!name) {
			if (cur_class->priv->default_method)
				return cur_class->priv->default_method;
			else
				continue;
		}

/*		g_warning ("Searching class of type '%s' for '%s'",
			   gtk_type_name (GTK_OBJECT_CLASS (cur_class)->type),
			   name);*/

		for (l = cur_class->priv->methods; l; l = l->next) {
			GBRunObjMethod *m = l->data;

			if (!g_strcasecmp (m->name, name))
				return m;
		}
	}

	return NULL;
}


void
gbrun_object_set_default_method (GBRunObjectClass *klass,
				 const char       *desc)
{
	g_return_if_fail (klass != NULL);

	klass->priv->default_method = 
		gbrun_object_get_method (klass, desc);
}

GSList *
gbrun_object_get_methods (GBRunObjectClass *klass)
{
	g_return_val_if_fail (klass != NULL, NULL);
	
	return g_slist_copy (klass->priv->methods);
}

gboolean
gbrun_object_has_method (GBRunObjectClass *klass,
			 const char       *name)
{
	if (gbrun_object_get_method (klass, name))
		return TRUE;
	else
		return FALSE;
}

GBRunObjectProperty *
gbrun_object_get_property (GBRunObjectClass  *klass,
			   const char        *name,
			   GBRunObjectClass **on_class,
			   GBRunPropertyType  flags)
{
	GSList *l, *i;

	g_return_val_if_fail (name != NULL, NULL);
	g_return_val_if_fail (klass != NULL, NULL);

	for (i = klass->priv->classes; i; i = i->next) {
		GBRunObjectClass *cur_class = i->data;
	
		*on_class = cur_class;

		for (l = cur_class->priv->properties; l; l = l->next) {
			GBRunObjectProperty *prop = l->data;

/*			g_warning ("is '%s' == '%s' && %d:%d (%d)",
				   prop->name, name, prop->flags, flags,
				   prop->flags & flags);*/

			if ((prop->flags & flags) &&
			    !g_strcasecmp (prop->name, name))
				return prop;
		}
	}
	*on_class = NULL;
		
	return NULL;
}

void
gbrun_object_add_property_full (GBRunObjectClass *klass,
				const char       *name,
				GtkType           type,
				int               idx,
				GBRunPropertyType flags)
{
	GBRunObjectProperty *prop;

	g_return_if_fail (name != NULL);
	g_return_if_fail (klass != NULL);

	prop = g_new0 (GBRunObjectProperty, 1);

	prop->type  = type;
	prop->name  = g_strdup (name);
	prop->idx   = idx;
	prop->flags = flags;

	klass->priv->properties =
		g_slist_prepend (klass->priv->properties, prop);
}

void
gbrun_object_add_property (GBRunObjectClass *klass,
			   const char       *name,
			   GtkType           type,
			   int               idx)
{
	gbrun_object_add_property_full (
		klass, name, type, idx,
		GBRUN_PROPERTY_READABLE | 
		GBRUN_PROPERTY_WRITEABLE);
}

gboolean
gbrun_object_has_property (GBRunObjectClass *klass,
			   const char       *name,
			   GBRunPropertyType type)
{
	GBRunObjectClass *dummy;

	return gbrun_object_get_property (
		klass, name, &dummy, type) != NULL;
}

static GBValue *
gbrun_method_invoke_arg (GBRunEvalContext *ec, GBRunObject    *obj,
			 GBRunObjMethod   *m,  const GBObjRef *func)
{
	GBValue         *val;
	GBValue        **vals;
	GSList          *lp, *lm;
	int              i = 0;
	int              num_opt;
	int              len;
	gboolean         exception;

	g_return_val_if_fail (m != NULL, NULL);
	g_return_val_if_fail (func != NULL, NULL);

	len = g_slist_length (func->parms);

	if (! m->args_parsed)
		parse_args (m);

	if (len < m->args.parsed.min_args)
		return gbrun_exception_firev (
			ec, _("Too few args to %s %d expected %d given"),
			func->name, m->args.parsed.min_args, len);

	if (len > m->args.parsed.max_args)
		return gbrun_exception_firev (
			ec, _("Too many args to %s %d expected %d given"),
			func->name, m->args.parsed.max_args, len);
	
	vals = g_new (GBValue *, m->args.parsed.max_args + 1);

	/*
	 * FIXME: see instr for something that will cock this up; we need to work
	 * out how many optional args to use, and use them as we go along.
	 */
	num_opt = len - m->args.parsed.min_args;
	exception = FALSE;
	lp = func->parms;
	for (lm = m->args.parsed.args; lm; i++, lm = lm->next) {
		GBArgDesc *arg = lm->data;
		GBExpr    *expr;

		if (lp) {
			expr = lp->data;
			lp   = lp->next;
		} else if (arg->optional)
			expr = arg->def_value;

		if (expr) {
			GBValue *v = gb_eval_context_eval (GB_EVAL_CONTEXT (ec), expr);
			
			if (!v) {
				exception = TRUE;
				break;
			} else {
				vals [i] = gbrun_value_promote_name (ec, v, arg->type_name);
				gb_value_destroy (v);
			}
		} else
			vals [i] = NULL;
	}
	vals [i] = NULL;
	
	if (!exception)
		val = m->handler.arg (ec, obj, vals);
	else
		val = NULL;
	
	i = 0;
	while (vals [i])
		gb_value_destroy (vals [i++]);
	
	g_free (vals);

	return val;
}

static GBValue *
gbrun_method_invoke_var (GBRunEvalContext *ec, GBRunObject    *obj,
			 GBRunObjMethod   *m,  const GBObjRef *func)
{
	int              len;

	g_return_val_if_fail (m != NULL, NULL);
	g_return_val_if_fail (func != NULL, NULL);

	len = g_slist_length (func->parms);

	if (! m->args_parsed)
		parse_args (m);

	if (len < m->args.parsed.min_args)
		return gbrun_exception_firev (
			ec, _("Too few args to %s %d expected %d given"),
			func->name, m->args.parsed.min_args, len);

	return m->handler.var (ec, obj, func->parms); 
}

static void
stack_setup_vars (gpointer key, gpointer value, gpointer user_data)
{
	GBVar   *var = value;
	GBValue *val = gbrun_value_default_from_var (user_data, var);

/*	g_warning ("Adding variable '%s' at stack level %d", var->name,
	g_list_length (ec->stack->level));*/
	gbrun_stack_set (user_data, var->name, val);
}

static GBValue *
gbrun_method_invoke_vb (GBRunEvalContext *ec, GBRunObject      *obj,
			GBRunObjMethod   *m,  const GBObjRef *func)
{
	GBValue     *ans = NULL, **ans_var;
	GSList      *l;
	GSList      *def;
	GSList      *parms;
	GBObject    *old_me;
	int          len;

	g_return_val_if_fail (m != NULL, NULL);
	g_return_val_if_fail (func != NULL, NULL);
	g_return_val_if_fail (func->name != NULL, NULL);

	len = g_slist_length (func->parms);

	if (! m->args_parsed)
		parse_args (m);

	if (len < m->args.parsed.min_args)
		return gbrun_exception_firev (ec, _("Too few args to %s %d expected %d given"),
					    func->name, m->args.parsed.min_args, len);
	if (len > m->args.parsed.max_args)
		return gbrun_exception_firev (ec, _("Too many args to %s %d expected %d given"),
					    func->name, m->args.parsed.max_args, len);

	/* Set 'Me' up */
	old_me = gbrun_eval_context_me_get (ec);
	gbrun_eval_context_me_set (ec, GB_OBJECT (obj));

#ifdef VB_METHOD_DEBUG
	fprintf (stderr, "Calling %s (", func->name?func->name:"[Unknown]");
#endif
	/*
	 * Evaluate the arguments in the current stack frame.
	 */
	parms = NULL;
	for (l = func->parms; l; l = l->next) {
		GBValue   *val;

		val = gb_eval_context_eval (GB_EVAL_CONTEXT (ec), l->data);
		if (!val)
			goto gb_cleanup;

		parms = g_slist_append (parms, val);
	}
	
	/*
	 * Create the callee's frame.
	 */
	gbrun_stack_call (ec, func->name);

	def = m->args.parsed.args;
	l   = parms;
	while (l && def) {
		GBArgDesc *arg = def->data;
		GBValue   *v;

		v = gbrun_value_promote_name (ec, l->data, arg->type_name);
		if (v) {
#ifdef VB_METHOD_DEBUG
			fprintf (stderr, "%s%s'%s'%s",arg->name?arg->name:"",
				 arg->name?"=":"", gb_value_get_as_string (v)->str,
				 l->next?", ":"");
#endif
			gbrun_stack_add (ec, arg->name, v, GBRUN_STACK_LOCAL);
			gb_value_destroy (v);
		} else
			goto gb_cleanup;

		l = l->next;
		def = def->next;
	}
	
#ifdef VB_METHOD_DEBUG
	fprintf (stderr, ")\n");
#endif

	if (!m->is_sub) {
		/* Add a blank return var. to the stack */
		GBValue *v;

		v = gb_value_new_default (GB_EVAL_CONTEXT (ec),
					  m->args.parsed.as_type);
		gbrun_stack_add (ec, m->name, v, GBRUN_STACK_LOCAL);
	}

	/* Setup the functions variables */
	if (m->handler.vb->variables)
		g_hash_table_foreach (m->handler.vb->variables,
				      stack_setup_vars, ec);
	if (gbrun_eval_context_exception (ec))
		goto gb_cleanup;

	if (!gbrun_stmts_evaluate (ec, m->handler.vb->body))
		goto gb_cleanup;

	/* Get the return value */
	ans_var = gbrun_stack_get (ec, func->name);
	if (!ans_var || !*ans_var) {
		if (m->is_sub)
			ans = gb_value_new_empty ();
		else
			gbrun_exception_firev (ec, _("No return value for function '%s'"),
					       func->name);
	} else
		ans = gbrun_value_copy (ec, *ans_var);

#ifdef VB_METHOD_DEBUG
	if (ans && ans->type != GB_VALUE_EMPTY)
		fprintf (stderr, "%s returns '%s'\n", func->name,
			 gb_value_get_as_string (ans)->str);
#endif

gb_cleanup:	
	for (l = parms; l; l = g_slist_remove (l, l->data))
		gb_value_destroy (l->data);

	gbrun_stack_return (ec);
	gbrun_eval_context_me_set (ec, old_me);

	return ans;
}

static void
gbrun_object_copy (GBEvalContext  *ec,
		   GBObject       *src,
		   GBObject       *dest)
{
	g_warning ("We badly need to copy all the object variables here");
}

static gboolean
chain_assign_to_value (GBEvalContext  *ec,
		       GBValue        *object,
		       const GBObjRef *ref,
		       GBValue        *value,
		       gboolean        try_assign)
{
	g_return_val_if_fail (object != NULL, FALSE);

	if (GB_IS_AN_OBJECT (object->gtk_type)) {
		GBObjRef newref;
		gboolean ret;
		
		newref = *ref;
		newref.name = NULL;

		ret = gb_object_assign (
			ec, object->v.obj, &newref, value, try_assign);
		gb_value_destroy (object);
		
		return ret;
	} else {
		gb_value_destroy (object);
		
		if (!try_assign)
			gbrun_exception_firev (
				GBRUN_EVAL_CONTEXT (ec),
				_("Cannot use parameters on '%s'"), ref->name);
	}
	return FALSE;
}


static gboolean
gbrun_object_assign (GBEvalContext  *ec,
		     GBObject       *object,
		     const GBObjRef *ref,
		     GBValue        *value,
		     gboolean        try_assign)
{
	GBRunObjectProperty *prop;
	GBRunObjectClass *klass;
	gboolean ret;
	GBValue *v, *sval;

	if (!object)
		return FALSE;

	if ((sval = gbrun_object_var_get (GBRUN_EVAL_CONTEXT (ec),
					  GBRUN_OBJECT (object),
					  ref->name))) {

		if (ref->parms)
			return chain_assign_to_value (
				ec, sval, ref, value, try_assign);

		gbrun_object_var_set (GBRUN_EVAL_CONTEXT (ec),
				      GBRUN_OBJECT (object),
				      ref->name, value);
		gb_value_destroy (sval);

		return TRUE;
	}

	prop = gbrun_object_get_property (
		GBRUN_OBJECT_GET_CLASS (object), ref->name,
		&klass, GBRUN_PROPERTY_WRITEABLE);

	if (!prop) {
		if (!try_assign)
			gbrun_exception_firev (
				GBRUN_EVAL_CONTEXT (ec),
				_("No writeable property '%s' on object '%s'"),
				ref->name, gbrun_object_name (GBRUN_OBJECT (object)));
		return FALSE;
	}

	v = gb_value_promote (GB_EVAL_CONTEXT (ec), prop->type, value);

	if (!v)
		return FALSE;
	
	ret = klass->set_arg (GBRUN_EVAL_CONTEXT (ec),
			      GBRUN_OBJECT (object), prop->idx, v);
	
	gb_value_destroy (v);

	return ret;
}

static GBValue *
chain_deref_to_value (GBEvalContext  *gb_ec,
		      GBValue        *val,
		      const GBObjRef *ref,
		      gboolean        try_deref)
{
	GBValue *ret = NULL;

	if (GB_IS_AN_OBJECT (val->gtk_type)) {
		GBObjRef newref;
		
		newref = *ref;
		newref.name = NULL;
		ret = gb_object_deref (
			gb_ec, val->v.obj, &newref, try_deref);
	} else
		gb_eval_exception_firev (
			gb_ec, _("Cannot use parameters on '%s'"),
			ref->name);

	gb_value_destroy (val);

	return ret;
}

static GBValue *
gbrun_object_deref (GBEvalContext  *gb_ec,
		    GBObject       *obj,
		    const GBObjRef *ref,
		    gboolean        try_deref)
{
	GBRunEvalContext *ec;
	GBValue          *val = NULL;
	GBRunObjMethod   *m;
	GBRunObjectClass *klass;

	g_return_val_if_fail (obj != NULL, NULL);

	ec = GBRUN_EVAL_CONTEXT (gb_ec);

	/*g_warning ("Object '%s' deref '%s' method '%d' params: %d",
	  gtk_type_name (GTK_OBJECT (obj)->klass->type),
	  ref->name, ref->method, g_slist_length (ref->parms));*/
	
	/* 1. See if we have an object variable */
	if ((val = gbrun_object_var_get (GBRUN_EVAL_CONTEXT (ec),
					 GBRUN_OBJECT (obj),
					 ref->name))) {
		if (ref->parms)
			return chain_deref_to_value (
				gb_ec, val, ref, try_deref);
		else
			return val;
	}

	klass = GBRUN_OBJECT_GET_CLASS (obj);

	/* 2. See if we have a constant */
	if (!ref->parms &&
	    (val = gbrun_object_const_get (ec, klass, ref->name)))
		return val;

	/* 3. See if we have a method */
	if ((m = gbrun_object_get_method (klass, ref->name))) {
		GBObjRef parmref;
		GSList  *parms;

		if (!m->args_parsed)
			parse_args (m);
			
		if (ec->flags & m->args.parsed.mask) {
			if (!try_deref)
				gbrun_exception_firev (
					ec, _("Security block on function '%s'"),
					ref->name);
			return NULL;
		}

		parmref = *ref;
		parmref.name = m->name; /* Might be a default method */

		parms = ref->parms;

		/* This copes with no-arg functions that return Arrays etc. */
		if (m->args.parsed.max_args == 0)
			parmref.parms = NULL;
		else
			parms = NULL;

		switch (m->type) {
			
		case GBRUN_METHOD_ARG:
			val = gbrun_method_invoke_arg (
				ec, GBRUN_OBJECT (obj), m, &parmref);
			break;
			
		case GBRUN_METHOD_VAR:
			val = gbrun_method_invoke_var (
				ec, GBRUN_OBJECT (obj), m, &parmref);
			break;
			
		case GBRUN_METHOD_VB:
			val = gbrun_method_invoke_vb  (
				ec, GBRUN_OBJECT (obj), m, &parmref);
			break;
		}

		if (parms) {
			parmref.parms = parms;

			if (val && GB_IS_AN_OBJECT (val->gtk_type))
				val = chain_deref_to_value (
					gb_ec, val, &parmref, try_deref);
			else {
				if (!try_deref)
					gbrun_exception_firev (
						ec, _("Too many arguments to '%s'"),
						ref->name);
				if (val)
					gb_value_destroy (val);

				val = NULL;
			}
		}

 	} else { /* 4. Fall back to an argument */
		GBRunObjectProperty *prop;

		if (!obj)
			g_assert_not_reached ();

		prop = gbrun_object_get_property (
			GBRUN_OBJECT_GET_CLASS (obj),
			ref->name, &klass,
			GBRUN_PROPERTY_READABLE);
		
		if (!prop) {
			if (!try_deref)
				gbrun_exception_firev (
					ec, _("No readable property '%s' on object '%s'"),
					ref->name, gbrun_object_name (GBRUN_OBJECT (obj)));
			return NULL;
		}
		
		val = klass->get_arg (ec, GBRUN_OBJECT (obj), prop->idx);

		if (val && ref->parms)
			val = chain_deref_to_value (
				gb_ec, val, ref, try_deref);
	}

	return val;
}

static void
gbrun_object_destroy (GtkObject *object)
{
	GBRunObject *obj = (GBRunObject *) object;

	if (obj->priv) {
		if (obj->priv->vars) {
			gbrun_stack_level_destroy (obj->priv->vars);
			obj->priv->vars = NULL;
		}
		g_free (obj->priv);
	}
	obj->priv = NULL;
}

static void
gbrun_object_instance_init (GBRunObject      *object,
			    GBRunObjectClass *klass)
{
	GSList *l;

	/* FIXME: we badly need our own init fn ! */
	GBRunEvalContext *ec = gbrun_eval_context_new (
		"brutal-instance-init-hack", GBRUN_SEC_HARD);

	object->priv = g_new0 (GBRunObjectPrivate, 1);

	object->priv->vars = gbrun_stack_level_new ("Object vars");

/*	g_warning ("Instance init '%s' ( '%d' vars )",
	gtk_type_name (GTK_OBJECT_CLASS (klass)->type),
	g_slist_length (klass->priv->variables));*/

	/* Setup the object's data */
	for (l = klass->priv->variables; l; l = l->next) {

		GBVar   *var = l->data;
		GBValue *val = gbrun_value_default_from_var (ec, var);

		if (!val) {
			g_warning ("Can't create default for '%s'", var->name);
		} else {
/*			g_warning ("Adding instance object variable '%s'", var->name);*/

			gbrun_stack_level_add (ec, object->priv->vars,
					       var->name, val);
		}
	}

	gtk_object_unref (GTK_OBJECT (ec));
}

/*
 * FIXME:
 *   Ok; so when we sub-class we need to _copy_ the
 * data on the ( same ) class of the parent that we
 * are 'sub-classing'; should be fun / impossible :-)
 */

static GSList *
build_classes (GBRunObjectClass *klass)
{
	GSList *ret;
	GtkType type = GTK_OBJECT_CLASS (klass)->type;

	ret = g_slist_append (NULL, klass);

	if (GBRUN_IS_AN_OBJECT (type) &&
	    type != GBRUN_TYPE_OBJECT) /* we are abstract */
		ret = g_slist_concat (ret, build_classes (
			gtk_type_parent_class (type)));

	return ret;
}

static void
gbrun_object_class_init (GBRunObjectClass *klass)
{
	GBObjectClass  *object_class = (GBObjectClass *) klass;
	GtkObjectClass *gtk_class    = (GtkObjectClass *) klass;

	klass->priv = g_new0 (GBRunObjectPrivateClass, 1);

	klass->priv->classes = build_classes (klass);

	klass->set_arg = gbrun_object_set_arg_unimplemented;
	klass->get_arg = gbrun_object_get_arg_unimplemented;

	object_class->copy   = gbrun_object_copy;
	object_class->assign = gbrun_object_assign;
	object_class->deref  = gbrun_object_deref;

	gtk_class->destroy   = gbrun_object_destroy;
}

GtkType
gbrun_object_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			"GBRunObject",
			sizeof (GBRunObject),
			sizeof (GBRunObjectClass),
			(GtkClassInitFunc)  NULL,
			(GtkObjectInitFunc) gbrun_object_instance_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) gbrun_object_class_init
		};

		object_type = gtk_type_unique (GB_TYPE_OBJECT, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}

GBRunObject *
gbrun_object_new (GBRunObjectClass *klass)
{
	g_return_val_if_fail (GBRUN_IS_OBJECT_CLASS (klass), NULL);

	return gtk_type_new (GTK_OBJECT_CLASS (klass)->type);
}

GtkType
gbrun_object_subclass (GtkType           parent,
		       const char       *vb_name,
		       GtkClassInitFunc  class_init,
		       GtkObjectInitFunc object_init)
{
	char   *gtk_name;
	GtkType type;

	gtk_name = gb_gtk_type_name (vb_name);

	if (!(type = gtk_type_from_name (gtk_name))) {
		GtkTypeInfo   object_info;
		GtkTypeQuery *query;

		query = gtk_type_query (parent);
		g_return_val_if_fail (query != NULL, 0);

		object_info.type_name   = (char *) gtk_name;
		object_info.object_size = query->object_size;
		object_info.class_size  = query->class_size;
		object_info.class_init_func  = NULL;
		object_info.object_init_func = object_init;
		object_info.reserved_1 = NULL;
		object_info.reserved_2 = NULL;
		object_info.base_class_init_func = class_init;

		type = gtk_type_unique (parent, &object_info);
		gtk_type_class (type);

		g_free (query);
	}

	g_free (gtk_name);

	return type;
}

GtkType
gbrun_object_subclass_simple (GtkType     parent,
			      const char *vb_name)
{
	return gbrun_object_subclass (parent, vb_name, NULL, NULL);
}

char *
gbrun_object_name (GBRunObject *object)
{
	g_return_val_if_fail (GBRUN_IS_OBJECT (object),
			      g_strdup ("Null object"));

	return gb_type_name_from_gtk (
		gtk_type_name (GTK_OBJECT (object)->klass->type));
}

void
gbrun_object_init (GBEvalContext *ec)
{
	gbrun_global_get ();
	gbrun_collection_get_type ();
}

void 
gbrun_object_shutdown ()
{
}
