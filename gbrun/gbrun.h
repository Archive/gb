
/*
 * GNOME Basic Internal header file.
 *
 * Author:
 *    Michael Meeks <michael@imaginator.com>
 *
 * Copyright 2000, Helix Code, Inc
 */
#ifndef GBE_GBE_H
#define GBE_GBE_H

#include <gb/gb.h>

/* gbrun-stack.h */
typedef struct _GBRunStack      GBRunStack;
typedef struct _GBRunStackLevel GBRunStackLevel;
typedef GList                   GBRunStackLevelList;
typedef struct _GBRunVar        GBRunVar;
typedef GList                   GBRunVarList;

/* gbrun-statement.h */
typedef struct _GBRunSubFrame  GBRunSubFrame;
typedef struct _GBRunFrame     GBRunFrame;

/* gbrun-value.h */
typedef struct _GBEArray       GBEArray;

/* gbrun-eval.h */
/* NB. A set SEC flag means this operation family is blocked.*/
typedef enum {
	GBRUN_SEC_NONE = 0,	/* 'n' */
	GBRUN_SEC_EXEC = 1,	/* 'x' */
	GBRUN_SEC_GUI  = 2,	/* 'g' */
	GBRUN_SEC_WINE = 4,	/* 'w' */
	GBRUN_SEC_IO   = 8,	/* 'i' */
	GBRUN_SEC_HARD = 0xffff
} GBRunSecurityFlag;
typedef struct _GBRunEvalContext      GBRunEvalContext;
typedef struct _GBRunEvalContextClass GBRunEvalContextClass;

/* gbrun-object.h */
typedef struct _GBRunDerefClass  GBRunDerefClass;
typedef struct _GBRunObjectRef   GBRunObjectRef;
typedef const GBValue * const    GBRunArg;
typedef GSList                   GBRunArgList;
typedef struct _GBRunArgStruct   GBRunArgStruct;              

typedef struct _GBRunObject          GBRunObject;
typedef struct _GBRunObjectClass     GBRunObjectClass;
typedef struct _GBRunObjMethod       GBRunObjMethod;
typedef struct _GBRunObjProperty     GBRunObjProperty;
typedef struct _GBRunObjValue        GBRunObjValue;

typedef GBValue *(GBRunMethodVar) (GBRunEvalContext  *context,
				   GBRunObject       *object,
				   GSList            *exprs);
typedef GBValue *(GBRunMethodArg) (GBRunEvalContext  *context,
				   GBRunObject       *object,
				   GBValue          **args);
/* gbrun-array.h */
typedef struct _GBRunArray      GBRunArray;
typedef struct _GBRunArrayClass GBRunArrayClass;

/* gbrun-collection.h */
typedef struct _GBRunCollection GBRunCollection;

/* gbrun-global.h */
typedef struct _GBRunGlobal      GBRunGlobal;
typedef struct _GBRunGlobalClass GBRunGlobalClass;

/* gbrun-project.h */
typedef GBLexerStream *(GBRunStreamProvider) (GBRunEvalContext *ec,
					      const char       *name,
					      gpointer          user_data);

typedef struct _GBRunProject      GBRunProject;
typedef struct _GBRunProjectClass GBRunProjectClass;

/* gbrun-type.h */
typedef struct _GBRunType         GBRunType;
typedef struct _GBRunTypeClass    GBRunTypeClass;

#endif
