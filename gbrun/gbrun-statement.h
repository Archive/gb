
/*
 * GNOME Basic Statement enterpreter
 *
 * Author:
 *    Michael Meeks <mmeeks@gnu.org>
 *
 * Copyright 2000, Helix Code, Inc
 */
#ifndef GBRUN_STMT_H
#define GBRUN_STMT_H

#include <gbrun/gbrun.h>
#include <gb/gb-statement.h>

struct _GBRunSubFrame {
	GBRunSubFrame *parent;

	/* Flags that having finished executing
	   this we need to return and not chew up the
	   statement stack, to make gbrun_stmts_evaluate
	   re-enterant */
	gboolean       eval_call;
	
	/* Emulates top / bottom recursion marker */
	gboolean       init;

	/*
	 * Allows efficient single linked lists, and
	 * marks where we need a bottom half
	 */
	gboolean       pushed;
	GSList        *stmts;
};

struct _GBRunFrame {
	GBRunSubFrame *cur;

	GSList        *func_root;
};

gboolean gbrun_stmt_assign    (GBRunEvalContext *ec,
			       const GBExpr *lexpr, const GBExpr *rexpr);

gboolean gbrun_stmts_evaluate (GBRunEvalContext *ec,
			       GSList           *stmts);

#endif /* GRUN_STMT_H */
