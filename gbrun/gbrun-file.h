
/*
 * GNOME Basic File I/O statements handler
 *
 * Author:
 *    Ravi Pratap <ravi_pratap@email.com>
 *
 * Copyright 2000, Helix Code, Inc.
 */

#ifndef GBRUN_FILE_H
#define GBRUN_FILE_H

#include <gbrun/gbrun.h>
#include <gb/gb-statement.h>

struct _GBRunFileHandle {
        /* VB related */
        int         used;
	int         fileno;
	GBSOpenMode mode;
	guint       recordlen;

	/* Internal */
	FILE       *file;
};

typedef struct _GBRunFileHandle GBRunFileHandle;


GBValue *gbrun_func_eof         (GBRunEvalContext *ec,
				 GBRunObject      *object,
				 GBValue         **args);
GBValue *gbrun_func_freefile    (GBRunEvalContext *ec,
				 GBRunObject      *object,
				 GBValue         **args);
GBValue *gbrun_func_loc         (GBRunEvalContext *ec,
				 GBRunObject      *object,
				 GBValue         **args);
GBValue *gbrun_func_lof         (GBRunEvalContext *ec,
				 GBRunObject      *object,
				 GBValue         **args);
GBValue *gbrun_func_seek        (GBRunEvalContext *ec,
				 GBRunObject      *object,
				 GBValue         **args);
GBValue *gbrun_func_loc         (GBRunEvalContext *ec,
				 GBRunObject      *object,
				 GBValue         **args);
GBValue *gbrun_func_lof         (GBRunEvalContext *ec,
				 GBRunObject      *object,
				 GBValue         **args);

GBRunFileHandle *internal_handle_from_gb_no (GBRunEvalContext *ec, int fileno);


gboolean gbrun_stmt_open        (GBRunEvalContext *ec, const GBStatement *stmt);
gboolean gbrun_stmt_close       (GBRunEvalContext *ec, const GBStatement *stmt);
gboolean gbrun_stmt_line_input  (GBRunEvalContext *ec, const GBStatement *stmt);
gboolean gbrun_stmt_input       (GBRunEvalContext *ec, const GBStatement *stmt);
gboolean gbrun_stmt_get         (GBRunEvalContext *ec, const GBStatement *stmt);
gboolean gbrun_stmt_put         (GBRunEvalContext *ec, const GBStatement *stmt);
gboolean gbrun_stmt_seek        (GBRunEvalContext *ec, const GBStatement *stmt);
gboolean gbrun_stmt_print       (GBRunEvalContext *ec, const GBStatement *stmt);

gboolean gbrun_files_clean      (GBRunEvalContext *ec);
void     gbrun_files_init       (GBRunEvalContext *ec);


#endif /* GBRUN_FILE_H */
