/**
 * GNOME Basic Stack
 *
 * Author:
 *    Michael Meeks <michael@imaginator.com>
 *
 * Copyright 2000, Helix Code, Inc
 */

#include <gbrun/gbrun.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-stack.h>
#include <gbrun/gbrun-object.h>
#include <gbrun/gbrun-statement.h>

#undef STACK_DEBUG

/*
 *  Keeps track of all the variables on the
 * stack at a certain level.
 */
struct _GBRunStackLevel {
	char         *name;
	GBRunVarList *vars;
	GBRunFrame    frame;
};

static GBRunVar *
gbrun_var_new (GBRunEvalContext *ec,
	       const char       *name,
	       const GBValue    *val)
{
	GBRunVar *var = g_new (GBRunVar, 1);

	var->name = g_strdup (name);
	var->val  = gbrun_value_copy (ec, val);

	return var;
}

static void
gbrun_var_destroy (GBRunVar *v)
{
	if (v) {
		if (v->name)
			g_free (v->name);
		v->name = NULL;
		if (v->val)
			gb_value_destroy (v->val);
		v->val = NULL;

		g_free (v);
	}
}

GBRunStackLevel *
gbrun_stack_level_new (const char *name)
{
	GBRunStackLevel *l = g_new0 (GBRunStackLevel, 1);

	l->name = g_strdup (name);
	l->frame.cur = NULL;

#ifdef STACK_DEBUG
	fprintf (stderr, " New stack level %p\n", l);
#endif

	return l;
}

static char *
stack_level_dump (GBRunStackLevel *l)
{
	return g_strdup (l->name);
}

void
gbrun_stack_level_destroy (GBRunStackLevel *l)
{
	if (l) {
		if (l->name)
			g_free (l->name);
		l->name = NULL;

		while (l->vars) {
			GBRunVar *v = l->vars->data;

			l->vars = g_list_remove (l->vars, v);

			gbrun_var_destroy (v);
		}

		if (l->frame.cur)
			g_warning ("Leaking frame stack");

#ifdef STACK_DEBUG
		fprintf (stderr, " Destroy stack level %p\n", l);
#endif
		g_free (l);
	}
}

void
gbrun_stack_call (GBRunEvalContext *ec,
		  const char       *name)
{
	GBRunStackLevel *l;
	GBRunStack      *stack = ec->stack;

	g_return_if_fail (stack != NULL);

	l = gbrun_stack_level_new (name);
	stack->level = g_list_prepend (stack->level, l);
}

inline static GBRunStackLevel *
get_stack_level (GBRunStack *s)
{
	if (!s || !s->level || !s->level->data)
		return NULL;

	return s->level->data;
}

GBRunFrame *
gbrun_stack_frame (GBRunStack *stack)
{
	GBRunStackLevel *l = get_stack_level (stack);

	if (!l)
		return NULL;

	return &l->frame;
}

void
gbrun_stack_return (GBRunEvalContext *ec)
{
	GBRunStack      *stack = ec->stack;
	GBRunStackLevel *l = get_stack_level (stack);

	g_return_if_fail (l != NULL);

	l = stack->level->data;
	stack->level = g_list_remove (stack->level, l);

	gbrun_stack_level_destroy (l);
}

void
gbrun_stack_level_add (GBRunEvalContext *ec,
		       GBRunStackLevel  *l,
		       const char       *name,
		       const GBValue    *val)
{
	GBRunVar *v;

	v = gbrun_var_new (ec, name, val);

	g_return_if_fail (v != NULL);

	l->vars = g_list_prepend (l->vars, v);
}

void
gbrun_stack_add (GBRunEvalContext *ec,
		 const char       *name,
		 const GBValue    *val,
		 GBRunStackScope   scope)
{
	GBRunStackLevel *l;

	if (!val) {
		/* FIXME: we need to propagate and check types ! */
		return;
	}

	g_return_if_fail (GB_IS_AN_OBJECT (val->gtk_type) ||
			  GB_IS_A_FUNDAMENTAL (val->gtk_type));

	switch (scope) {
	case GBRUN_STACK_LOCAL:
		l = get_stack_level (ec->stack);
		break;
	case GBRUN_STACK_MODULE:
		l = g_list_last (ec->stack->level)->data;
		break;
	default:
		g_warning ("Unimplemented");
		break;
	}

	g_return_if_fail (l != NULL);
	gbrun_stack_level_add (ec, l, name, val);
}

GBValue **
gbrun_stack_level_lookup (GBRunStackLevel *l, const char *name)
{
	GBRunVarList    *vl;

	g_return_val_if_fail (l != NULL, NULL);

	/* FIXME: need a hash */
	for (vl = l->vars; vl; vl = vl->next) {
		GBRunVar *v = vl->data;
		
		if (!g_strcasecmp (v->name, name))
			return &v->val;
	}
	return NULL;
}

GBValue **
gbrun_stack_get (GBRunEvalContext *ec,
		 const char       *name)
{
	GBValue **val;

	g_return_val_if_fail (name != NULL, NULL);

	if (!g_strcasecmp (name, "Me"))
		return &ec->me;

	val = gbrun_stack_level_lookup (get_stack_level (ec->stack),
					name);

	if (!val) { /* FIXME: looks like a hack to me */
		GList *l = g_list_last (ec->stack->level);
		if (l)
			val = gbrun_stack_level_lookup (
				l->data, name);
	}

#ifdef STACK_DEBUG
	fprintf (stderr, "Getting %s == '%s'\n", name,
		 val?gb_value_get_as_string (*val)->str:"[not on stack]");
#endif

	return val;
}

void
gbrun_stack_set (GBRunEvalContext *ec,
		 const char       *name,
		 GBValue          *value)
{
	GBValue **val;

	if (!g_strcasecmp (name, "Me")) {
		gbrun_exception_fire (ec, _("Serious error setting 'Me'"));
		return;
	}

	val = gbrun_stack_get (ec, name);
	if (!val) {
		gbrun_stack_add (ec, name, value, GBRUN_STACK_LOCAL);
#ifdef STACK_DEBUG
		fprintf (stderr, "Adding %s to stack as '%s'\n", name,
			 gb_value_get_as_string (value)->str);
#endif
	} else {
		if (*val)
			gb_value_destroy (*val);

		*val = gbrun_value_copy (ec, value);
#ifdef STACK_DEBUG
		fprintf (stderr, "Setting %s to '%s'\n", name,
			 gb_value_get_as_string (value)->str);
#endif
	}
}

char **
gbrun_stack_dump (GBRunStack *stack)
{
	char **ans;
	int    i;
	GList *l;

	g_return_val_if_fail (stack != NULL, NULL);

	ans = g_new (char *, g_list_length (stack->level) + 1);
	i   = 0;

	for (l = stack->level; l; l = l->next)
		ans [i++] = stack_level_dump (l->data);
	ans [i] = NULL;

	return ans;
}

static void
gbrun_stack_destroy (GtkObject *object)
{
	GBRunStack *stack = (GBRunStack *) object;

	if (stack) {
		while (stack->level) {
			GBRunStackLevel *l = stack->level->data;

			stack->level = g_list_remove (stack->level, l);

			gbrun_stack_level_destroy (l);
		}
	}
}

static void
gbrun_stack_copy (GBEvalContext  *ec,
		  GBObject       *src,
		  GBObject       *dest)
{
	g_warning ("FIXME: copying the stack, wierd");
}

static gboolean
gbrun_stack_assign (GBEvalContext  *ec,
		    GBObject       *object,
		    const GBObjRef *ref,
		    GBValue        *value,
		    gboolean        try_assign)
{
	GBValue **stack_val;

	stack_val = gbrun_stack_get (GBRUN_EVAL_CONTEXT (ec), ref->name);

	if (!stack_val && try_assign)
		return FALSE;

	if (ref->parms) {
		if (!stack_val) {
			if (!try_assign)
				gbrun_exception_firev (
					GBRUN_EVAL_CONTEXT (ec),
					_("No array or collection %s"), ref->name);

			return FALSE;

		} else if (*stack_val &&
		    GB_IS_AN_OBJECT ((*stack_val)->gtk_type)) {
			GBObjRef newref = *ref;

			newref.name = NULL;

			return gb_object_assign (ec, (*stack_val)->v.obj, &newref, value, try_assign);

		} else {
			if (!try_assign)
				gbrun_exception_firev (
					GBRUN_EVAL_CONTEXT (ec),
					_("Variable %s is not a method"), ref->name);

			return FALSE;
		}
	} else
		gbrun_stack_set (GBRUN_EVAL_CONTEXT (ec), ref->name, value);

	return TRUE;
}

static GBValue *
gbrun_stack_deref (GBEvalContext  *gb_ec,
		   GBObject       *obj,
		   const GBObjRef *ref,
		   gboolean        try_deref)
{
	GBRunEvalContext *ec = GBRUN_EVAL_CONTEXT (gb_ec);
	GBValue **val, *ret;

	val = gbrun_stack_get (ec, ref->name);

	if (!val || !*val) {
		if (try_deref)
			return NULL;
		else
			return gbrun_exception_firev (
				ec, _("No such variable %s"), ref->name);
	}

	if (ref->method || ref->parms) {

		if (GB_IS_AN_OBJECT ((*val)->gtk_type)) {
			GBObjRef newref = *ref;

			newref.name = NULL;

			ret = gb_object_deref (gb_ec, (*val)->v.obj, &newref, try_deref);

		} else {
			if (try_deref)
				return NULL;
			else
				return gbrun_exception_firev (
					ec, _("Variable %s is not a method"), ref->name);
		}
	} else
		ret = gbrun_value_copy (ec, *val);

	return ret;
}

static void
gbrun_stack_class_init (GBObjectClass *klass)
{
	GtkObjectClass *gtk_class = (GtkObjectClass *) klass;

	klass->copy   = gbrun_stack_copy;
	klass->assign = gbrun_stack_assign;
	klass->deref  = gbrun_stack_deref;

	gtk_class->destroy = gbrun_stack_destroy;
}

GtkType
gbrun_stack_get_type (void)
{
	static GtkType stack_type = 0;

	if (!stack_type) {
		static const GtkTypeInfo stack_info = {
			"GBRunStack",
			sizeof (GBRunStack),
			sizeof (GBRunStackClass),
			(GtkClassInitFunc)  gbrun_stack_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		stack_type = gtk_type_unique (GB_TYPE_OBJECT, &stack_info);
		gtk_type_class (stack_type);
	}

	return stack_type;	
}
