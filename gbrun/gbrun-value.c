/*
 * GNOME Basic Values Manuipulation
 *
 * Author:
 *    Michael Meeks <mmeeks@gnu.org>
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include <gbrun/gbrun.h>
#include <gbrun/gbrun-value.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-object.h>
#include <gbrun/gbrun-array.h>
#include <gbrun/gbrun-stack.h>
#include <gbrun/gbrun-global.h>
#include <gbrun/gbrun-project.h>

GBValue *
gbrun_value_promote_name (GBRunEvalContext *ec,
			  GBValue          *v,
			  const char       *type_name)
{
	GtkType t;

	g_return_val_if_fail (v != NULL, NULL);
	g_return_val_if_fail (ec != NULL, NULL);
	g_return_val_if_fail (type_name != NULL, NULL);

	t = gb_gtk_type_from_name (type_name);

	return gb_value_promote (GB_EVAL_CONTEXT (ec), t, v);
}

static GSList *
get_try_list (GBRunEvalContext *ec,
	      const GBObjRef   *ref)
{
	GSList  *to_try;
	gpointer test;
	GBRunProject *proj;

	/* 1. Stack */
	to_try = g_slist_prepend (NULL, ec->stack);

	/* 2. This */
	test = gbrun_eval_context_me_get (ec);
	if (test) {
		to_try = g_slist_prepend (to_try, test);
/*		g_warning ("Me is '%s'",
		gtk_type_name (GTK_OBJECT (test)->klass->type));*/
	}

	/* 3. Global functions */
	test = GB_OBJECT (gbrun_global_get ());
	if (test)
		to_try = g_slist_prepend (to_try, test);

	/* 4. Project modules */
	if ((proj = gbrun_eval_context_proj_get (ec)))
		to_try = g_slist_prepend (to_try, proj);

 	to_try = g_slist_reverse (to_try);
	
	/* 5. 'With' list */
	to_try = g_slist_concat (to_try, g_slist_copy (ec->with));

	return to_try;
}

GBValue *
gbrun_objref_deref (GBRunEvalContext *ec,
		    GBObject         *object,
		    const GBObjRef   *ref,
		    gboolean          first_deref)
{
	g_return_val_if_fail (ec != NULL, NULL);
	g_return_val_if_fail (ref != NULL, NULL);

	if (first_deref || object == NULL) {
		GSList *l, *try;

		for (try = l = get_try_list (ec, ref); l; l = l->next) {
			GBValue *ret;

			if ((ret = gb_object_deref (GB_EVAL_CONTEXT (ec),
						    GB_OBJECT (l->data),
						    ref, TRUE)) ||
			    gbrun_eval_context_exception (ec)) {

				g_slist_free (try);
				return ret;
			}
		}
		g_slist_free (try);
	}

	if (!object)
		return gbrun_exception_firev (
			ec, _("No such method / variable '%s'"), ref->name);

	return gb_object_deref (GB_EVAL_CONTEXT (ec),
				GB_OBJECT (object),
				ref, FALSE);

/*	if (!object &&
	    !ref->method &&
	    !ref->parms)
		return objref_stack_deref (ec, ref);
	else {
		if (ref->method || ref->parms) {
			GBValue **t = gbrun_stack_get (ec, ref->name);
			if (t && GB_IS_OBJECT ((*t)->gtk_type) &&
			    (*t)->v.obj && GBRUN_IS_ARRAY ((*t)->v.obj))
				return gbrun_array_deref (ec, (*t)->v.obj, ref);
			else
				return gbrun_method_invoke (ec, object, ref);
		} else
			return gbrun_object_get_arg (ec, object,
						     ref->name);
						     }*/
}

static GBObject *
eval_to_penultimate (GBRunEvalContext *ec,
		     const GSList     *objref)
{
	int i = 0;
	const GSList *l;
	GBObject *object = NULL;

	for (l = objref; l && l->next; l = l->next) {
		GBValue *nxt = gbrun_objref_deref (ec, object, l->data, (i++) == 0);

		if (!nxt)
			return FALSE;

		else if (!GB_IS_AN_OBJECT (nxt->gtk_type)) {
			gbrun_exception_firev (ec, _("Duff object dereference %s"),
					       ((GBObjRef *)l->data)->name);
			return FALSE;
		}

		object = nxt->v.obj;
		gb_object_ref (object);
		gb_value_destroy (nxt);
	}

	return object;
}

gboolean
gbrun_eval_assign (GBRunEvalContext *ec,
		   const GSList     *objref,
		   GBValue          *value)
{
	GBObject *object = NULL;
	const GBObjRef *ref;
	GSList *l, *try;

	g_return_val_if_fail (ec != NULL, FALSE);
	g_return_val_if_fail (objref != NULL, FALSE);

	if (objref->next)
		object = eval_to_penultimate (ec, objref);

	try = get_try_list (ec, ref);
	if (object)
		try = g_slist_prepend (try, object);

	ref = g_slist_last ((GSList *) objref)->data;

	for (l = try; l; l = l->next) {

		if (gb_object_assign (GB_EVAL_CONTEXT (ec),
				      GB_OBJECT (l->data),
				      ref, value, TRUE)) {
			g_slist_free (try);
			return TRUE;
		}

		if (gbrun_eval_context_exception (ec)) {
			g_slist_free (try);
			return FALSE;
		}
		
	}
	g_slist_free (try);
	if (ec->options.explicit == TRUE) {
		gbrun_exception_firev (
			ec, _("variable '%s' not defined."), ref->name);
		return FALSE;
	} else {
		object = GB_OBJECT (ec->stack);

		return gb_object_assign (GB_EVAL_CONTEXT (ec),
					 GB_OBJECT (object),
					 ref, value, FALSE);
	} 
}

GBValue *
gbrun_eval_objref  (GBRunEvalContext *ec,
		    const GBExpr     *expr)
{
	GSList   *objref;
	GBObject *object;

	g_return_val_if_fail (ec != NULL, NULL);
	g_return_val_if_fail (expr != NULL, NULL);
	g_return_val_if_fail (expr->type == GB_EXPR_OBJREF, NULL);

	objref = expr->parm.objref;
	g_return_val_if_fail (objref != NULL, NULL);

	object = eval_to_penultimate (ec, objref);

	return gbrun_objref_deref (
		ec, object, g_slist_last (objref)->data, FALSE);
}

GBValue *
gbrun_value_default_from_var (GBRunEvalContext *ec,
			      GBVar            *var)
{
	GBValue *val;

	if (var->is_array)
		val = gb_value_new_object (GB_OBJECT (gbrun_array_new (ec, var)));

	else {
		GtkType t;

		if (!g_strcasecmp (var->type, "Object")) {
			val = gb_value_new_object (gtk_type_new (GB_TYPE_OBJECT));
			
		} else {
			t = gb_gtk_type_from_name (var->type);
			
			if (!t)
				gb_eval_exception_firev (GB_EVAL_CONTEXT (ec),
							 _("Unknown type '%s'"), var->type);
			else
				val = gb_value_new_default ((GBEvalContext *) ec, t);
		}
	}
	
	return val;
}
