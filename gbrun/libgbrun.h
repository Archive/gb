
/*
 * GNOME Basic Interpreter library main include.
 *
 * Author:
 *    Michael Meeks <mmeeks@gnu.org>
 *
 * Copyright 2000, Helix Code, Inc
 */

#ifndef GBE_GBRUN_H
#define GBE_GBRUN_H

#include <gbrun/gbrun.h>
#include <gbrun/gbrun-eval.h>
#include <gbrun/gbrun-object.h>
#include <gbrun/gbrun-project.h>

void          gbrun_init             (GBEvalContext *ec);
void          gbrun_shutdown         (void);

GBRunProject *gbrun_project_new      (GBRunEvalContext *ec, GBProject *p,
				      GBRunStreamProvider *provider,
				      gpointer user_data);

gboolean      gbrun_project_execute  (GBRunEvalContext *ec,
				      GBRunProject *p);

GBValue      *gbrun_project_invoke   (GBRunEvalContext *ec,
				      GBRunProject     *proj,
				      const char       *name,
				      GSList           *args);

void          gbrun_exec_str         (GBRunEvalContext *ec,
				      GBRunObject      *opt_object,
				      const char       *basic_string);

GBValue      *gbrun_eval_str         (GBRunEvalContext *ec,
				      GBRunObject      *opt_object,
				      const char       *basic_string);

/* This is an ugly hack to work around gnumeric */
GSList       *gbrun_project_fn_names (GBRunProject     *proj);

#endif
