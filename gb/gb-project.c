/*
 * GNOME Basic Project support
 *
 * Author:
 *    Michael Meeks (michael@helixcode.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <string.h>

#include <gb-lex.h>
#include <gb-eval.h>
#include <gb-value.h>
#include <grammar.h>
#include <gb-project.h>

extern int gb_lex (YYSTYPE *res, void *closure);

static GBProjectPair *
get_project_pair (GBEvalContext *ec, GBValue *v, GBLexerStream *ls)
{
	GBProjectPair *pp;
	char          *s;

	if (v->gtk_type != gb_gtk_type_from_value (GB_VALUE_STRING)) {
		gb_eval_exception_fire (ec, _("Expecting a string';'"));
		return NULL;
	}

	pp = g_new (GBProjectPair, 1);

	if (!(s = strchr (v->v.s->str, ';'))) {
		pp->name     = g_strdup (v->v.s->str);
		pp->filename = g_strdup (v->v.s->str);
	} else {
		pp->name     = g_strndup (v->v.s->str, s - v->v.s->str);
		pp->filename = g_strdup (g_strchug (s + 1));
	}

	gb_value_destroy (v);

	return pp;
}

static gboolean
split_known (GBEvalContext *ec, char *key, GBValue *value,
	     GBLexerStream *ls, GBProject *proj)
{
	GBProjectPair *pp;

	if (!g_strcasecmp (key, "Module")) {
		pp = get_project_pair (ec, value, ls);
		if (!pp)
			return FALSE;

		proj->modules = g_slist_prepend (proj->modules, pp);
		g_free (key);
		return TRUE;

	} else if (!g_strcasecmp (key, "Startup")) {
		if (!value->gtk_type == gb_gtk_type_from_value (GB_VALUE_STRING)) {
			gb_eval_exception_fire (ec, _("Expecting a textual startup"));
			return FALSE;
		}

		if (proj->startup)
			g_warning ("Dual startups, superceeding '%s'", proj->startup);
		g_free (proj->startup);
	       
		proj->startup = g_strdup (value->v.s->str);
		return TRUE;

	} else if (!g_strcasecmp (key, "Class")) {
		pp = get_project_pair (ec, value, ls);
		if (!pp)
			return FALSE;

		proj->classes = g_slist_prepend (proj->classes, pp);
		g_free (key);
		return TRUE;

	} else if (!g_strcasecmp (key, "Form")) {

		if (!value->gtk_type == gb_gtk_type_from_value (GB_VALUE_STRING)) {
			gb_eval_exception_fire (ec, _("Expecting a value after ';'"));
			return FALSE;
		}
		proj->forms = g_slist_prepend (proj->forms,
					       g_strdup (value->v.s->str));
		gb_value_destroy (value);
		g_free (key);
		return TRUE;
	}

	return FALSE;
}

static gboolean
parse_project (GBEvalContext *ec, GBLexerStream *ls, GBProject *proj)
{
	YYSTYPE res;
	int     token;

	do {
		char    *key;
		GBValue *value;

		token = gb_lex_real (&res, ls);
		if (token == 0)
			return TRUE;

		if (token == '\n')
			continue;

		if (token == '[') { /* Discard a section heading */
			token = gb_lex_real (&res, ls);
			if (token != NAME) {
				gb_eval_exception_fire (ec, _("Malformed section heading"));
				return FALSE;
			}

			g_warning ("Unhandled section heading '%s'", res.v_str);

			token = gb_lex_real (&res, ls);
			if (token != ']') {
				gb_eval_exception_fire (ec, _("Missing section delimiter"));
				return FALSE;
			}
			continue;
		}

		if (token != NAME) {
			gb_eval_exception_fire (ec, _("Expecting name"));
			return FALSE;
		}
		key = (char *)res.v_str;

		token = gb_lex_real (&res, ls);
		if (token != '=') {
			gb_eval_exception_fire (ec, _("Expecting '='"));
			return FALSE;
		}

		token = gb_lex_real (&res, ls);
		switch (token) {
		case NAME:
		case STRING:
			value = gb_value_new_string_chars (res.v_str);
			break;

		case INTEGER:
			value = gb_value_new_int (res.v_int);
			break;

		default:
			gb_eval_exception_fire (ec, _("Expecting a value"));
			g_free (key);
			return FALSE;
		}

		if (!split_known (ec, key, value, ls, proj)) {
			if (gb_eval_exception (ec)) {
				g_free (key);
				gb_value_destroy (value);
				return FALSE;
			} else
				g_hash_table_insert (proj->values, key, value);
		}

	} while (TRUE);
}

GBProject *
gb_project_new (GBEvalContext *ec, GBLexerStream *ls)
{
	GBProject *proj;

	g_return_val_if_fail (GB_IS_EVAL_CONTEXT (ec), NULL);
	g_return_val_if_fail (GB_IS_LEXER_STREAM (ls), NULL);

	proj = g_new0 (GBProject, 1);

	proj->values = g_hash_table_new (g_str_hash, g_str_equal);

	gb_lexer_stream_state_set (ls, GB_PARSING_PROJECT);

	if (!parse_project (ec, ls, proj)) {
		gb_project_destroy (proj);
		return NULL;
	}

	return proj;
}

static void
gb_project_pair_destroy (GBProjectPair *pp)
{
	if (pp) {
		g_free (pp->name);
		g_free (pp->filename);
	}
}

static void
pplist_destroy (GSList **list)
{
	while (*list) {
		gb_project_pair_destroy ((*list)->data);
		*list = g_slist_remove (*list, (*list)->data);
	}
}

static void
strlist_destroy (GSList **list)
{
	while (*list) {
		g_free ((*list)->data);
		*list = g_slist_remove (*list, (*list)->data);
	}
}

static gboolean
free_value (gpointer key, gpointer value, gpointer user_data)
{
	g_free (key);
	g_free (value);
	return TRUE;
}

void
gb_project_destroy (GBProject *proj)
{
	if (proj) {
		pplist_destroy (&proj->modules);
		pplist_destroy (&proj->classes);

		strlist_destroy (&proj->forms);

		if (proj->startup)
			g_free (proj->startup);
		proj->startup = NULL;

		g_hash_table_foreach_remove (proj->values, free_value, NULL);
		g_hash_table_destroy (proj->values);
	}
}

const char *
gb_project_startup_get (GBProject *proj)
{
	g_return_val_if_fail (proj != NULL, NULL);
	
	if (!proj->startup)
		proj->startup = g_strdup ("Sub Main");

	return proj->startup;
}
