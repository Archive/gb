/**
 * gb-frx-get
 *   Get/load data from the binary frx file.
 *
 * Author:
 *	Frank Chiulli	(fc-linux@home.com)
 *
 * Copyright 2000, Helix Code, Inc.
 **/

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <glib.h>

#include "gb.h"
#include "gb-frx.h"
#include "gb-frx-get.h"
#include "gb-mmap-frx.h"
#include "gb-value.h"


/**
 * frx_from_file
 *   @filename	name of frx file	
 *
 **/
GBFrx *
frx_from_file (const char *filename)
{
	int		 fd;
	int		 len;

	guint8  	*data;
	guint8  	*datacpy;

	struct stat	 sbuf;

	GBFrx		*ans;

	
	g_return_val_if_fail (filename != NULL, NULL);

	fd = open (filename, O_RDONLY);
	if (fd < 0 || fstat (fd, &sbuf) < 0) {
		fprintf (stderr, "gb: %s : %s\n", filename, strerror (errno));

		if (fd >= 0)
			close (fd);
		return NULL;
	}

	if ((len = sbuf.st_size) <= 0) {
		fprintf (stderr, "%s : empty file\n", filename);
		close (fd);
		return NULL;
	}

	data = mmap (0, len, PROT_READ, MAP_PRIVATE, fd, 0);
	if (data == MAP_FAILED) {
		fprintf (stderr, "%s : unable to mmap\n", filename);
		close (fd);
		return NULL;
	}

	datacpy = g_new (guint8, len);
	memcpy (datacpy, data, len);

	ans = gb_mmap_frx_new (datacpy, len);

	munmap ((char *)data, len);
	close (fd);

	return ans;
}


/**
 * gb_get_frx_text
 *   @v		GBValue structure containing information about frx file.
 *
 **/
GPtrArray *
gb_get_frx_text (const GBValue *v)
{
	gchar		*item;

	guint		 cur_offset;
	guint		 item_len;

	GBFrx		*frx;

	GPtrArray	*ptr_array;

	g_return_val_if_fail (v, NULL);
	g_return_val_if_fail (v->v.frx, NULL);

	frx         = frx_from_file (v->v.frx->filename);
	g_return_val_if_fail (frx, NULL);
	
	ptr_array   = g_ptr_array_new ();
	cur_offset  = v->v.frx->offset;
	item_len    = gb_frx_getu8 (frx, cur_offset);
	if (item_len > 0) {
		cur_offset++;
		item = g_new (gchar, item_len + 1);

		item = gb_frx_getcn (frx, cur_offset, item_len);
		if (item != NULL)
			g_ptr_array_add (ptr_array, item);
	}
	
	return ptr_array;
}


/**
 * gb_get_frx_list
 *   @v		GBValue structure containing information about frx file.
 *
 **/
GPtrArray *
gb_get_frx_list (const GBValue *v)
{
	int		 cur_item;

	gchar		*item; 

	guint		 cur_offset;
	guint		 no_items;
	guint		 item_len;

	GBFrx		*frx;

	GPtrArray	*ptr_array;
	
		
	g_return_val_if_fail (v, NULL);	
	g_return_val_if_fail (v->v.frx, NULL);

#ifdef DEBUG
	printf ("Entered gb-frx-get:gb_get_frx_list\n")	;
#endif
	frx         = frx_from_file (v->v.frx->filename);
	g_return_val_if_fail (frx, NULL);
	
	cur_offset  = v->v.frx->offset;
	no_items    = gb_frx_getu16 (frx, cur_offset);
	
#ifdef DEBUG
	printf ("        Number of items = %d\n", no_items);
#endif
	if (no_items == 0) {
		g_warning ("gb_get_frx_list: no items");
		return NULL;
	}
	
	cur_offset += 4;
	ptr_array  = g_ptr_array_new ();

	for (cur_item = 0; cur_item < no_items; cur_item++) {
		item_len    = gb_frx_getu16 (frx, cur_offset);
		cur_offset += 2;

		if (item_len > 0) {
			item = g_new (gchar, item_len + 1);
			item = gb_frx_getcn (frx, cur_offset, item_len);

			if (item != NULL) {
				g_ptr_array_add (ptr_array, item);
#ifdef DEBUG
				printf ("        item = %s\n", item);
#endif
			}


			cur_offset += item_len;
		} else
			g_warning ("gb_get_frx_list: item_len is zero.");
	}

	return ptr_array;
}
