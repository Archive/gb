/*
 * GNOME Basic Frx interface
 *
 * Author:
 *    Frank Chiulli  <fchiulli@home.com>
 *
 * Based on gb-lex.h written by Michael Meeks <mmeeks@gnu.org>
 *
 * Copyright 2000, Helix Code, Inc.
 */
#ifndef GB_FRX_H
#define GB_FRX_H

#include <gb/gb.h>

#define GB_TYPE_FRX		(gb_frx_get_type ())
#define GB_FRX(obj)		(GTK_CHECK_CAST ((obj), GB_TYPE_FRX, GBFrx))
#define GB_FRX_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GB_TYPE_FRX, GBFrxClass))
#define GB_IS_FRX(obj)		(GTK_CHECK_TYPE ((obj), GB_TYPE_FRX))
#define GB_IS_FRX_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GB_TYPE_FRX))

struct _GBFrx {
	GtkObject	 object;

	gchar		*filename;
	guint32		 offset;
	
};


struct _GBFrxClass {
	GtkObjectClass	 klass;

	guint8   (*s_getu8)	(GBFrx *frx, const guint offset);
	guint16  (*s_getu16)	(GBFrx *frx, const guint offset);
	
	char    *(*s_getcn)	(GBFrx *frx, const guint offset, const guint n);
};


GtkType  gb_frx_get_type (void);
GBFrx   *gb_frx_new      (void);

/* Stream bits - abstract */
guint8	 gb_frx_getu8  (GBFrx *frx, const guint cur_offset);
guint16	 gb_frx_getu16 (GBFrx *frx, const guint cur_offset);
char    *gb_frx_getcn  (GBFrx *frx, const guint cur_offset, const guint32 item_len);

#endif
