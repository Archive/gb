/*
 * The Gnome Basic scanner.
 *
 * Authors:
 *     Michael Meeks (michael@helixcode.com)
 *     Jody Goldberg (jgoldberg@home.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>

#include "gb-statement.h"
#include "gb-expr.h"
#include "grammar.h"
#include "gb-lex.h"
#include "gb-mmap-lex.h"

#undef GB_LEX_DEBUG

extern int gb_lex (YYSTYPE *res, void *closure);

static int
read_number (GBLexerStream *ls, char c, YYSTYPE *res)
{
	GString *str = g_string_new ("");
	gboolean exp = FALSE;
	gboolean is_float = FALSE;
	int val_int = 0;
	double val_float = 0;
	int tok = FLOAT;

	g_string_append_c (str, c);
		
	is_float = (c == '.');
	while ((!gb_lexer_stream_eof (ls))) {
		c = gb_lexer_stream_peek (ls);

		if ((c == 'E' || c == 'e' || c == 'D' || c == 'd') && !exp) {
			exp = TRUE;
			is_float = TRUE;
			c = gb_lexer_stream_getc (ls);
			if (c == 'D' || c == 'd')
				c = 'e';
			g_string_append_c (str, c);

			c = gb_lexer_stream_peek (ls);
			if (c == '+' || c == '-')
				g_string_append_c (str, gb_lexer_stream_getc (ls));

		} else if ((c == '.' && !is_float) || isdigit ((unsigned int)c)) {
			if (c == '.')
				is_float = TRUE;

			g_string_append_c (str, gb_lexer_stream_getc (ls));

		} else
			break;
	}

	val_float = strtod (str->str, NULL);
	if (!is_float) {
		errno = 0;
		val_int = strtol (str->str, NULL, 10);
		if (errno) /* Overflow? */
			is_float = TRUE;
	}
	if (is_float)
		tok = FLOAT;
	else if (val_int < GB_VALUE_INT_MIN || val_int > GB_VALUE_INT_MAX)
		tok = LONG;
	else
		tok = INTEGER;

	if ((!gb_lexer_stream_eof (ls))) {
		int new_tok = 0;
		switch (gb_lexer_stream_peek (ls)) {
			case '%':
				if (tok == INTEGER)
					new_tok = INTEGER;
				break;
			case '&':
				if (!is_float)
					new_tok = LONG;
				break;
			case '!':
				new_tok = SINGLE;
				break;
			case '#':
				new_tok = FLOAT;
				break;
			/*
			FIXME: no currency support yet
			case '@':
				new_tok = CURRENCY;
				break;
			*/
		}
		if (new_tok) {
			gb_lexer_stream_getc (ls);
			tok = new_tok;
		}
	}

	switch (tok) {
		case INTEGER:
		case LONG:
			res->v_int = val_int;
			break;
		case SINGLE:
		case FLOAT:
			res->v_float = val_float;
			break;
	}

	g_string_free (str, TRUE);

	return tok;
}

static int
read_hex_number (GBLexerStream *ls, char c, YYSTYPE *res)
{
	GString *str;
	int n;
	int tok = INTEGER;

	if (gb_lexer_stream_eof (ls) ||
	    gb_lexer_stream_peek (ls) != 'H')
		return c;

	gb_lexer_stream_getc (ls);

	/* Hex. number */
	str = g_string_new ("");

	while (!gb_lexer_stream_eof (ls) &&
	       isxdigit ((unsigned int)(c = gb_lexer_stream_peek (ls))))
		g_string_append_c (str, gb_lexer_stream_getc (ls));

	n = strtol (str->str, NULL, 16);

	if (!gb_lexer_stream_eof (ls)) { 
		switch (gb_lexer_stream_peek (ls)) { /* Trailing type characters? */
			case '&':
				tok = LONG;
				/* Fall through */
			case '%':
				gb_lexer_stream_getc (ls);
		}
	}

	if (tok == INTEGER && n > GB_VALUE_INT_MAX && n < 65536)
		n -= 65536;

	res->v_int = n;
#ifdef GB_LEX_DEBUG
	printf ("Hex : 0x%x\n", res->v_int);
#endif
	g_string_free (str, TRUE);

	return tok;
}

static int
read_dot (GBLexerStream *ls, char c, YYSTYPE *res)
{
#ifdef GB_LEX_DEBUG
	fprintf (stderr, "Dot : '%c', '%c'\n",
		 gb_lexer_stream_peek (ls), ls->lastc);
#endif
	if (!gb_lexer_stream_eof (ls)) {
		if (isdigit (gb_lexer_stream_peek (ls)))
			/* Drop through to a float. */
			return -1;
		else if (!(isalnum ((guchar) ls->lastc) || ls->lastc == '$') &&
			 ls->lastc != ')')
			return GB_SPACE_DOT;
		else 
			return '.';
	} else
		return '.';
}

static int
parse_basic (YYSTYPE *res, GBLexerStream *ls, char c)
{
#ifdef GB_LEX_DEBUG
	g_warning ("Parse basic (0x%x) '%c'", c, c);
#endif

	switch (c) {

	case '#': case ',':
	case '(': case ')': case '=':
	case '*': case '/': case '\\':
	case '+': case '-': case '^':
		return c;
		
	case '<':
		if (!gb_lexer_stream_eof (ls)) {
			char nxt = gb_lexer_stream_peek (ls);
		
			if (nxt == '>') {
				c = gb_lexer_stream_getc (ls);
				return GB_NE;
			}
		} else
			return 0;

	case '>':
	case ':':
		if (!gb_lexer_stream_eof (ls)) {
			char nxt = gb_lexer_stream_peek (ls);
			
			if (nxt == '=') {
				gb_lexer_stream_getc (ls);
				return (c == '<' ) ? GB_LE : ((c == '>') ? GB_GE : GB_ARG_ASSIGN);
			}
		} else
			return 0;

		return c;
	
	case '&':
		return read_hex_number (ls, c, res);

	case '!':
		return c;

	case '.':
	{
		int tok;
		tok = read_dot (ls, c, res);
		
		if (tok > 0)
			return tok;
	}    /* else drop through */

	case '0': case '1': case '2': case '3': case '4': case '5':
	case '6': case '7': case '8': case '9':

		return read_number (ls, c, res);

	default:
	{
		int   tmp;
		char *str;

		g_assert (!gb_lexer_stream_eof (ls));
		
		str = gb_lexer_stream_gets (ls, c);

		/* Look for Remarks */
		if (!g_strcasecmp (str, "Rem")) {
			while (!gb_lexer_stream_eof (ls)) {
				c = gb_lexer_stream_getc (ls);

				if (c != '\n' && c != '\r')
					continue;
				else {
					g_free (str);
					return GB_KEEP_LOOKING;
				}
			}
			g_free (str);
			return GB_KEEP_LOOKING;
		}

	
		res->v_str = str;


		/* See if we have a GB_OBJ_NAME_LPAR */
		if (gb_lexer_stream_state (ls) == GB_PARSING_INSIDE_SUB &&
		    gb_lexer_stream_state (ls) != GB_PARSING_INSIDE_DIM) {
			GBLexerStream *tmp = gb_mmap_stream_copy (ls);
			int            i;

			while (!gb_lexer_stream_eof (tmp)) {
				c = gb_lexer_stream_getc (tmp);

				if (c == ' ' || c == '\t') {
					gb_lexer_stream_getc (ls);
					continue;
				} else if (c == '(') {
			
					g_free (tmp);
					
					if ((i = gb_is_keyword (str, strlen (str))) > 0) {
						
						/* 
						 * Since there are both functions and statements by these names,
						 * we have to be careful to see if we return a keyword
						 * or GB_OBJ_NAME_LPAR
						 */
						if (!gb_is_function (str, strlen (str))) {
							g_free (str);
							return i;
						}
					}

					ls->c     = gb_lexer_stream_getc (ls); /* Eat off the '(' */
					ls->lastc = ' ';

					return GB_OBJ_NAME_LPAR;
				} else 
					break;
			}

		}

		if ((tmp = gb_is_keyword (str, strlen (str))) > 0) {
#ifdef GB_LEX_DEBUG
			fprintf (stderr, "Keyword '%s'\n", str);
#endif
			g_free (str);
			
			return tmp;
		} 
				

		/* Check if we have a Goto label */
		if (gb_lexer_stream_peek (ls) == ':') {
			GBLexerStream *tmp = gb_mmap_stream_copy (ls);
			
			gb_lexer_stream_getc (tmp);
			
			while (!gb_lexer_stream_eof (tmp)) {
				c = gb_lexer_stream_getc (tmp);

				if (c == ' ' || c == '\t') {
					continue;
				} else if (c == '\n' || c == '\r') {
					g_free (tmp);
					return GB_GOTO_LABEL;
				} else 
					return NAME;
			}					
			
		}


#ifdef GB_LEX_DEBUG
		fprintf (stderr, "Name : %s\n", str);
#endif
		return NAME;
	}
	}
	return 0;
}

static int
parse_form (YYSTYPE *res, GBLexerStream *ls, char c)
{
#ifdef GB_LEX_DEBUG
	g_warning ("Parse form %c", c);
#endif

	switch (c) {

	case '+': case '-':
		return c;

	case ';':
		return c;

	case '=':
		return c;

	case '^':
		res->v_str = gb_lexer_stream_gets (ls, c);
		return STRING;
		
	case '&':
		return read_hex_number (ls, c, res);

	case '.':
	{
		int tok;
		tok = read_dot (ls, c, res);
		
		if (tok > 0)
			return tok;
	}    /* else drop through */

	case '0': case '1': case '2': case '3': case '4': case '5':
	case '6': case '7': case '8': case '9':

		return read_number (ls, c, res);

	default:
	{
		char *str = gb_lexer_stream_gets (ls, c);
		int   state = -1;
		
		if (!g_strcasecmp (str, "End"))
			state = GB_END;
		else if (!g_strcasecmp (str, "Begin"))
			state = GB_BEGIN;
		else if (!g_strcasecmp (str, "BeginProperty"))
			state = GB_BEGIN_PROPERTY;
		else if (!g_strcasecmp (str, "EndProperty"))
			state = GB_END_PROPERTY;
		else if (!g_strcasecmp (str, "Object"))
			state = GB_OBJECT;
		else if (!g_strcasecmp (str, "VERSION"))
			state = GB_VERSION;
		else if (!g_strcasecmp (str, "Class"))
			state = GB_CLASS;

#ifdef GB_LEX_DEBUG
		g_warning ("form name '%s': %d", str, state);
#endif

		if (state > 0) {
			g_free (str);
			return state;
		}

		res->v_str = str;

		return NAME;
	}
	}
	return 0;
}

static int
parse_project (YYSTYPE *res, GBLexerStream *ls, char c)
{
	char *str;


#ifdef GB_LEX_DEBUG
	g_warning ("Parse project %c", c);
#endif
	if (c == '=' ||
	    c == ';' ||
	    c == '[' ||
	    c == ']')
		return c;

	str = gb_lexer_stream_gets (ls, c);

	res->v_str = str;

#ifdef GB_LEX_DEBUG
	g_warning ("Returning name: '%s'", str);
#endif
	return NAME;
}

int
gb_lex (YYSTYPE *res, void *closure)
{
	GBParseData *buf = closure;

	return gb_lex_real (res, buf->ls);
}

int
gb_lex_real (gpointer r, GBLexerStream *ls)
{
	YYSTYPE *res = r;
	gboolean line_continue;
	char     c;

 keep_looking:
	if (gb_lexer_stream_eof (ls))
		return 0;
	
	if (ls->incr_line) {
		gb_lexer_stream_line_inc (ls);
		ls->incr_line = FALSE;
	}

	if (ls->first_symbol) {
		int r = ls->first_symbol;

		ls->first_symbol = 0;

		return r;
	}

	ls->lastc = ls->c;

	ls->c = c = gb_lexer_stream_getc (ls);

	switch (c)
	{
	case '\0':
	case ' ':
	case '\t':
		goto keep_looking;

	case '\'':
		while (!gb_lexer_stream_eof (ls)) {
			c = gb_lexer_stream_getc (ls);

			if (c != '\n' && c != '\r')
				continue;
			else
				break;
		}

	case '_':
	case '\n': case '\r':
		ls->incr_line = TRUE;
			
		if ((line_continue = (c == '_'))) {
			if (gb_lexer_stream_eof (ls))
				return '_';
			c = gb_lexer_stream_getc (ls);
		}
		if (!gb_lexer_stream_eof (ls)) {
			/* Quick attempt to handle, (CR,LF) (LF,CR) (LF) (CR) */
			char nxt = gb_lexer_stream_peek (ls);

			if (nxt != c && (nxt == '\r' || nxt == '\n'))
				c = gb_lexer_stream_getc (ls);
		}

		if (line_continue)
			goto keep_looking;

		return '\n';
		
	case '"': 
	{
		GString *str = g_string_new ("");
		
		while (!gb_lexer_stream_eof (ls)) {
			c = gb_lexer_stream_getc (ls);

			if (c == '"' && gb_lexer_stream_peek (ls) == '"') {
				c = gb_lexer_stream_getc (ls);
				g_string_append_c (str, c);
			} else if (c == '"' && gb_lexer_stream_peek (ls) != '"')
				break;
			else 
				g_string_append_c (str, c);
		}

		res->v_str = str->str;

		if (gb_lexer_stream_state (ls) == GB_PARSING_FORM &&
		    gb_lexer_stream_peek  (ls) == ':') {
			/*
			 *   If this gets implemented we need to look into security &
			 * sandboxes.
			 */

			g_string_prepend_c (str, '"');
			g_string_append_c (str, '"');
			g_string_append_c  (str, gb_lexer_stream_getc(ls));

			while (!gb_lexer_stream_eof (ls) &&
			       isxdigit ((unsigned int)(c = gb_lexer_stream_peek (ls))))
				g_string_append_c (str, gb_lexer_stream_getc (ls));

 			res->v_str = str->str;
			
			g_string_free (str, FALSE);
			
			return FRX;

		}

		g_string_free (str, FALSE);

#ifdef GB_LEX_DEBUG
		fprintf (stderr,"String: \"%s\"\n",res->v_str);
#endif
		return STRING;
	}

        default:
	{
		GBParsingState s = gb_lexer_stream_state (ls);
		int            r;
		
		switch (s) {
		case GB_PARSING_BASIC: case GB_PARSING_INSIDE_SUB: 
		case GB_PARSING_INSIDE_DIM:
			r = parse_basic (res, ls, c);
			break;

		case GB_PARSING_FORM: case GB_PARSING_CLASS:
			r = parse_form (res, ls, c);
			break;

		case GB_PARSING_PROJECT:
			r = parse_project (res, ls, c);
			break;
		default:
			g_error ("Serious lexing error; unknown state %d", s);
			break;
		};

		if (r == GB_KEEP_LOOKING)
			goto keep_looking;
		else
			return r;
	};
	}
	
	/* NOTREACHED */
	return 0;
}

#define GB_LEXER_STREAM_ERR_CHAR '\0'

char
gb_lexer_stream_getc (GBLexerStream *ls)
{
	GBLexerStreamClass *klass;

	g_return_val_if_fail (ls != NULL, GB_LEXER_STREAM_ERR_CHAR);

	klass = GB_LEXER_STREAM_CLASS (GTK_OBJECT (ls)->klass);

	g_return_val_if_fail (klass != NULL, GB_LEXER_STREAM_ERR_CHAR);
	g_return_val_if_fail (klass->s_getc != NULL, GB_LEXER_STREAM_ERR_CHAR);

	return klass->s_getc (ls);
}

gboolean
gb_lexer_stream_eof (GBLexerStream *ls)
{
	GBLexerStreamClass *klass;

	g_return_val_if_fail (ls != NULL, GB_LEXER_STREAM_ERR_CHAR);

	klass = GB_LEXER_STREAM_CLASS (GTK_OBJECT (ls)->klass);

	g_return_val_if_fail (klass != NULL, GB_LEXER_STREAM_ERR_CHAR);
	g_return_val_if_fail (klass->s_eof != NULL, GB_LEXER_STREAM_ERR_CHAR);

	return klass->s_eof (ls);
}

char
gb_lexer_stream_peek (GBLexerStream *ls)
{
	GBLexerStreamClass *klass;

	g_return_val_if_fail (ls != NULL, GB_LEXER_STREAM_ERR_CHAR);

	klass = GB_LEXER_STREAM_CLASS (GTK_OBJECT (ls)->klass);

	g_return_val_if_fail (klass != NULL, GB_LEXER_STREAM_ERR_CHAR);
	g_return_val_if_fail (klass->s_peek != NULL, GB_LEXER_STREAM_ERR_CHAR);

	return klass->s_peek (ls);
}

void
gb_lexer_stream_state_set (GBLexerStream *ls, GBParsingState state)
{
	GBLexerStreamClass *klass;

	g_return_if_fail (ls != NULL);

	klass = GB_LEXER_STREAM_CLASS (GTK_OBJECT (ls)->klass);

	g_return_if_fail (klass != NULL);
	g_return_if_fail (klass->state_set != NULL);

	klass->state_set (ls, state);
}

static void
state_set (GBLexerStream *ls, GBParsingState state)
{
	switch (state) {
/*	case GB_LEX_FILE:
		ls->first_symbol = GB_LEXTYPE_FILE;
		break;*/

	case GB_PARSING_EXPR:
		ls->first_symbol = GB_LEXTYPE_EXPR;
		ls->state = GB_PARSING_BASIC;
		break;

	case GB_PARSING_STATEMENTS:
		ls->first_symbol = GB_LEXTYPE_STATEMENTS;
		ls->state = GB_PARSING_BASIC;
		break;

	case GB_PARSING_ASP:
		ls->first_symbol = GB_LEXTYPE_ASP;
		/* drop through */

	default:
		ls->state = state;
		break;
	}
}

gboolean
gb_lexer_is_string_char (GBLexerStream *ls, char c)
{
	switch (ls->state) {

	case GB_PARSING_ASP:
	case GB_PARSING_BASIC: case GB_PARSING_INSIDE_SUB:
	case GB_PARSING_INSIDE_DIM:
		return isalnum ((guchar) c) || c == '_' || c == '$';

	case GB_PARSING_FORM: case GB_PARSING_CLASS:
		return isalnum ((guchar) c) || c == '_' || c == '$' || c == '^' ||
			c == '(' || c == ')' || c == '.' ;

	case GB_PARSING_PROJECT:
		return (c != '=' && c != ']' && c != '\n' && c != '\r');
	default:
		g_error ("Serious lexer string error state %d", ls->state);
		break;
	}
	return FALSE;
}

char *
gb_lexer_stream_gets (GBLexerStream *ls, char c)
{
	GBLexerStreamClass *klass;

	g_return_val_if_fail (ls != NULL, NULL);

	klass = GB_LEXER_STREAM_CLASS (GTK_OBJECT (ls)->klass);

	g_return_val_if_fail (klass != NULL, NULL);

	if (!klass->s_gets) { /* Emulate it then */
		GString *str = g_string_new ("");
		char    *ans;

		g_return_val_if_fail (klass->s_eof != NULL, NULL);
		g_return_val_if_fail (klass->s_getc != NULL, NULL);
		g_return_val_if_fail (klass->s_peek != NULL, NULL);

		g_string_append_c (str, c);
			
		while (!klass->s_eof (ls)) {
			char nxt = klass->s_peek (ls);

			if (!gb_lexer_is_string_char (ls, nxt))
				break;
			g_string_append_c (str, klass->s_getc (ls));
		}

		ans = str->str;
		g_string_free (str, FALSE);

		return ans;
	} else
		return klass->s_gets (ls, c);
}

static void
gb_lexer_stream_class_init (GBLexerStreamClass *klass)
{
	klass->state_set = state_set;

	klass->s_getc    = NULL;
	klass->s_peek    = NULL;
	klass->s_gets    = NULL;
	klass->s_eof     = NULL;
}

static void
gb_lexer_stream_init (GBLexerStream *ls)
{
	ls->state = GB_PARSING_BASIC;
	ls->line  = 1;
	ls->c     = '\0';
	ls->lastc = '\0';
}

GtkType
gb_lexer_stream_get_type (void)
{
	static GtkType lex_type = 0;

	if (!lex_type) {
		static const GtkTypeInfo lex_info = {
			"GBLexerStream",
			sizeof (GBLexerStream),
			sizeof (GBLexerStreamClass),
			(GtkClassInitFunc) gb_lexer_stream_class_init,
			(GtkObjectInitFunc) gb_lexer_stream_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		lex_type = gtk_type_unique (GTK_TYPE_OBJECT, &lex_info);

	}

	return lex_type;
}

GBLexerStream *
gb_lexer_stream_new (void)
{
	g_warning ("This is an abstract base class; "
		   "see gb-mmap-lexer.h");
	return NULL;
}
