/*
 * GNOME Basic main parser routines.
 *
 * Authors:
 *    Jody Goldberg (jgoldberg@home.com)
 *    Michael Meeks (michael@helixcode.com)
 *
 * Copyright 1999, 2000, Helix Code, Inc.
 */

#ifndef GB_MAIN_H
#define GB_MAIN_H

#include <gb/gb.h>
#include <gb/gb-type.h>
#include <gb/gb-expr.h>
#include <gb/gb-class.h>

typedef enum { 
	GB_COMPARE_BINARY,
	GB_COMPARE_TEXT,
	GB_COMPARE_DATABASE
} GBOptionCompare;

struct _GBOptions {
	gboolean explicit;
	gboolean private_module;
	int base;
	int compare;
};


/* Function/Subroutine */
struct _GBRoutine {
	const gchar *name;
	const gchar *as_type;
	gboolean     is_private;
	gboolean     is_function;

	/* For class properties */
	GBClassMethodType prop_type;

	/* Argument descriptions */
	GSList      *args;

	/* Statements in the routine */
	GSList      *body;
	
	/* variables local to the routine */
	GHashTable  *variables;
};

/* The GB file being parsed */
struct _GBParseData {
	/* The source of the source */
	GBLexerStream *ls;

	/* An evaluation context for const_exprs & exceptions */
	GBEvalContext *ec;

	GBOptions       options;

	/* Single expression */

		const GBExpr *expr;

	/* Block of statements */

		GSList       *stmts;

	/* Elements for full parsing */

		/* All new types declared in this scope GBType *'s */
		GSList       *types;

		/* Set of routines declared within the file hashed by name */
		GHashTable   *routines;

		/* All constants declared within the file hashed by name */
		GHashTable   *constants;
	
		/* Set of global variables hashed by name */
		GHashTable   *variables;
	
		/* The current routine */
		GBRoutine    *current_routine;
		
		/* Any associated form data */
		GBFormItem    *form;
	
		/* Any associated class data */
		GSList        *class;
	
		/* Support nested with statements */
		int with_depth;

	        /* Attributes */
	        GSList        *attrs; 
};

/* Call before doing anything  */
void             gb_init          (void);
/* Frees up internal resources */
void             gb_shutdown      (void);

/* Return TRUE on error */
gboolean         gb_routine_start   (GBParseData        *module,
				     const char         *name,
				     GSList             *args,
				     const char         *as_type,
				     gboolean            is_function,
				     GBClassMethodType prop_type);

GBRoutine const *gb_routine_finish  (GBParseData *module,
				     gboolean  is_private,
				     gboolean  is_function,
				     GSList   *body);
void             gb_routine_print   (FILE *sink, const GBRoutine *routine);

GBVar           *gb_var_lookup      (GBParseData *module, const char *name);


void             gb_parse_data_start_form (GBParseData  *module);
void             gb_parse_data_end_form   (GBParseData  *module,
					   GBFormItem   *form);

void             gb_parse_data_end_class  (GBParseData  *module,
					   GSList       *class);

void             gb_parse_data_begin_routine (GBParseData *module);

void             gb_parse_data_inside_dim (GBParseData *module);
void             gb_parse_data_dim_finish (GBParseData *module);

void             gb_parse_data_parse_basic (GBParseData *module);

void             gb_parse_data_set_expr   (GBParseData  *module,
					   const GBExpr *expr);

void             gb_parse_data_set_stmts  (GBParseData  *module,
					   GSList       *stmts);

void             gb_parse_data_add_type   (GBParseData  *module,
					   GBType       *type);

gboolean         gb_parse_data_add_var    (GBParseData  *module,
					   GBVar        *var);

void             gb_parse_data_add_const  (GBParseData  *module,
					   const char   *name,
					   const GBExpr *expr);

void             gb_module_with_depth_inc (GBParseData  *module,
					   const GBExpr *base_obj);

void             gb_parse_data_set_option_explicit       (GBParseData  *module);
void             gb_parse_data_set_option_private_module (GBParseData  *module);
void             gb_parse_data_set_option_base           (GBParseData  *module,
							  int           base);

void		gb_parse_data_set_option_compare         (GBParseData  *module,
							  /*GBOptionCompare compare);*/
							  int compare);

struct _GBIndex {
	const GBExpr *min;
	const GBExpr *max;
};

GBIndex *gb_index_new    (const GBExpr    *min,
			  const GBExpr    *max);

void     gb_options_init (GBOptions       *options);

void     gb_options_copy (GBOptions       *dest,
			  const GBOptions *src);

struct _GBAttribute {
	const char *name;
	GBValue    *value;
};

GBAttribute *gb_parse_data_attribute_new    (GBParseData  *module,
					     const char   *name,
					     const GBExpr *expr);

void         gb_parse_data_add_attr         (GBParseData       *module,
					     const GBAttribute *attr);


#endif /* GB_MAIN_H */
