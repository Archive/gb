/**
 * gb-frx-get
 *   Get/oad data from the binary frx file.
 *
 * Author:
 *	Frank Chiulli	(fc-linux@home.com)
 *
 * Copyright 2000, Helix Code, Inc.
 **/

GPtrArray	*gb_get_frx_text (const GBValue *v);
GPtrArray	*gb_get_frx_list (const GBValue *v);
