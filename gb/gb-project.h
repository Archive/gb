/*
 * GNOME Basic Project support
 *
 * Author:
 *    Michael Meeks (michael@helixcode.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#ifndef GB_PROJECT_H
#define GB_PROJECT_H

#include <gb/gb.h>

struct _GBProjectPair {
	char *name;
	char *filename;
};

typedef GSList GBProjectPairList;

struct _GBProject {
	GBProjectPairList *modules;
	GBProjectPairList *classes;

	/*  Lists of string values */
	GSList            *forms;

	char       *startup;
	
	/* A hash for as yet un-interpreted values */
	GHashTable *values;
};

GBProject  *gb_project_new         (GBEvalContext *ec, GBLexerStream *ls);
void        gb_project_destroy     (GBProject *proj);

const char *gb_project_startup_get (GBProject *proj);

#endif /* GB_PROJECT_H */
