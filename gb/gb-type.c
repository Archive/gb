/*
 * GNOME Basic User defined type bits
 *
 * Author:
 *    Michael Meeks <mmeeks@gnu.org>
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <gb/gb-value.h>
#include <gb/gb-type.h>
#include <gb/gb-eval.h>

GBType *
gb_type_new (gboolean public,
	     const char *name,
	     GSList     *vars)
{
	GBType *type = g_new0 (GBType, 1);

	type->name = g_strdup (name);
	type->vars = vars;

	return type;
}

GBVar *
gb_var_new (const char *var_name,
	    gboolean    object,
	    gboolean    is_array,
	    GSList     *indices,
	    const char *type_name)
{
	GBVar *var = g_new0 (GBVar, 1);

	var->name     = g_strdup (var_name);
	var->object   = object;
	var->type     = g_strdup (type_name);
	var->is_array = is_array;
	var->indices  = indices;

	return var;
}

void
gb_type_destroy (GBType *type)
{
	if (type) {
		GSList *l;

		for (l = type->vars; l; l = l->next)
			gb_var_destroy (l->data);
		g_slist_free (type->vars);
		g_free (type->name);
		g_free (type);
	}
}

void
gb_var_destroy (GBVar *var)
{
	if (var) {
		g_free ((gpointer) var->name);
		g_free ((gpointer) var->type);
		g_free (var);
	}
}

GBConst *
gb_const_new (GBEvalContext *ec,
	      const char    *name,
	      GtkType        t,
	      const GBExpr  *expr)
{
	GBConst *cons;
	GBValue *ans, *tmp;

	if (expr) {
		tmp = gb_eval_context_eval (GB_EVAL_CONTEXT (ec), expr);
		if (!tmp || gb_eval_exception (ec))
			return NULL;

		ans = gb_value_promote (GB_EVAL_CONTEXT (ec), t, tmp);
		gb_value_destroy (tmp);

		if (!ans)
			return NULL;
	} else
		ans = gb_value_new_default (ec, t);

	cons = g_new0 (GBConst, 1);
	cons->name  = g_strdup (name);
	cons->value = ans;

	return cons;
}

void
gb_const_destroy (GBConst *cons)
{
	if (cons) {
		g_free           (cons->name);
		gb_value_destroy (cons->value);
		g_free (cons);
	}
}
