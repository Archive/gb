/*
 * GNOME Basic Values
 *
 * Author:
 *    Michael Meeks <mmeeks@gnu.org>
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#include "gb.h"
#include "gb-frx.h"
#include "gb-value.h"
#include "gb-expr.h"
#include "gb-object.h"
#include "gb-eval.h"
#include "gb-frx-get.h"

/* Useful for using the standard comparison function */

#define SECS_IN_DAY 86400
#define SECS_IN_HR  3600
#define SECS_IN_MIN 60

const GBValue *GBTrue_value;
const GBValue *GBFalse_value;

GHashTable *type_hash     = NULL;

GHashTable *type_def_hash = NULL;

GtkType gb_type_int;
GtkType gb_type_long;
GtkType gb_type_single;
GtkType gb_type_double;
GtkType gb_type_string;
GtkType gb_type_boolean;
GtkType gb_type_list;

GBValue *
gb_value_new_empty (void)
{
	GBValue *v = g_new (GBValue, 1);

	v->gtk_type= gb_gtk_type_from_value (GB_VALUE_EMPTY);
	v->v.l     = 0xcafebabe;

	return v;
}

GBValue *
gb_value_new_null (void)
{
	GBValue *v = g_new (GBValue, 1);

	v->gtk_type= gb_gtk_type_from_value (GB_VALUE_NULL);
	v->v.l     = 0xdeadbeef;

	return v;
}

GBValue *
gb_value_new_boolean (GBBoolean b)
{
	GBValue *v = g_new (GBValue, 1);

	v->gtk_type= gb_gtk_type_from_value (GB_VALUE_BOOLEAN);
	v->v.bool  = b;

	return v;
}

GBValue *
gb_value_new_byte (GBByte b)
{
	GBValue *v = g_new (GBValue, 1);

	v->gtk_type= gb_gtk_type_from_value (GB_VALUE_BYTE);
	v->v.byte  = b;

	return v;
}

GBValue *
gb_value_new_int (GBInt i)
{
	GBValue *v = g_new (GBValue, 1);

	v->gtk_type= gb_gtk_type_from_value (GB_VALUE_INT);
	v->v.i     = i;

	return v;
}

GBValue *
gb_value_new_long (GBLong l)
{
	GBValue *v = g_new (GBValue, 1);

	v->gtk_type= gb_gtk_type_from_value (GB_VALUE_LONG);
	v->v.l     = l;

	return v;
}

GBValue *
gb_value_new_single (GBSingle f)
{
	GBValue *v = g_new (GBValue, 1);

	v->gtk_type= gb_gtk_type_from_value (GB_VALUE_SINGLE);
	v->v.f     = f;

	return v;
}

GBValue *
gb_value_new_double (GBDouble d)
{
	GBValue *v = g_new (GBValue, 1);

	v->gtk_type= gb_gtk_type_from_value (GB_VALUE_DOUBLE);
	v->v.d     = d;

	return v;
}

GBValue *
gb_value_new_string (const GBString s)
{
	GBValue *v = g_new (GBValue, 1);

	g_return_val_if_fail (s != NULL, NULL);

	v->gtk_type= gb_gtk_type_from_value (GB_VALUE_STRING);
	v->v.s     = g_string_new (s->str);

	return v;
}

GBValue *
gb_value_new_string_chars (const char *s)
{
	GBValue *v = g_new (GBValue, 1);

	g_return_val_if_fail (s != NULL, NULL);

	v->gtk_type = gb_gtk_type_from_value (GB_VALUE_STRING);
	v->v.s      = g_string_new (s);

	return v;
}

GBValue *
gb_value_new_object (GBObject *obj)
{
	GBValue *v;

	g_return_val_if_fail (GB_IS_OBJECT (obj), NULL);

	v = g_new (GBValue, 1);
	v->gtk_type = GTK_OBJECT (obj)->klass->type;
	v->v.obj    = obj;

	return v;
}

GBValue *
gb_value_new_date (const GBDate date)
{
	GBValue *v = g_new (GBValue, 1);

	v->gtk_type = gb_gtk_type_from_value (GB_VALUE_DATE);
	v->v.date   = date;

	return v;
}

GBValue *
gb_value_new_date_gdate (const GDate *date)
{
	GBValue *v   = g_new (GBValue, 1);
	GDate   *tmp = g_date_new ();
	GDate   *d = (GDate *) date;
	guint32 val; 

	/* Take care MS' brain dead ranges. 
	   Start from 01 Jan 100 */
	g_date_set_dmy (tmp, 1, 1, 100);
	val = g_date_julian (d) - g_date_julian (tmp);
	
	/* Negative values are for dates before 1 Jan 1900 */
	g_date_set_dmy (tmp, 1, 1, 1900);
	val -= g_date_julian (tmp);

	v->gtk_type = gb_gtk_type_from_value (GB_VALUE_DATE);
	v->v.date   = val;
	
	g_date_free (tmp);

	return v;
}	

GBValue *
gb_value_new_date_from_str (const char *str)
{
	GBValue *v;
	char    *d = NULL;
	char    *t = NULL;
	int      h = 0, m = 0, s = 0;
	GDate   *date = g_date_new ();

	/* First we determine if the string is date/time or both */
	
	/* 
	 * FIXME: As of now we don't parse strings such as "Jan 1, 2001" etc 
	 * We're assuming that dates and times are in the numbers-separated 
	 * by-slashes-or-colons format 
	 */

	if (strlen (str) == 8) {
		if (str [2] == ':') {
			t = g_strdup (str);
			d = NULL;
		} 
	} 

	if (strlen (str) == 10) {
		if (str [2] == '/') {
			d = g_strdup (str);
			t = NULL;
		} 
	}
	
	if (!t && !d)
		return NULL; /* Unrecognized format */
	  
	if (t) {
		h = (t [0] - 48) * 10 + t[1] - 48; /* 0-9 are mapped to ASCII 48-57 */
		m = (t [3] - 48) * 10 + t[4] - 48;
		s = (t [6] - 48) * 10 + t[7] - 48;
		
		g_free (t);
	}

	if (d) {
		g_date_set_parse (date, d);
		if (!g_date_valid (date))
			return NULL;
		
		g_free (d);
	}
	
	if (d && t)
		v = gb_value_new_date_time ((const GDate *) date, h, m, s);
	else if (d)
		v = gb_value_new_date_gdate ((const GDate *) date);
	else if (t)
		v = gb_value_new_time (h, m, s);

	g_date_free (date);

	return v; 

}

GBValue *
gb_value_new_time (int hr, int min, int sec)
{
	GBValue *v = g_new (GBValue, 1);
	
	v->gtk_type = gb_gtk_type_from_value (GB_VALUE_DATE);
	v->v.date   = (double) ((hr * SECS_IN_HR) + (min * SECS_IN_MIN) + sec) / SECS_IN_DAY;

	return v;
}

GBValue *
gb_value_new_date_time (const GDate *date, int hr, int min, int sec)
{
	GBValue *v   = g_new (GBValue, 1);
	GDate   *tmp = g_date_new ();
	GDate   *d = (GDate *) date;
	guint32 val; 
	
	/* Take care MS' brain dead ranges. 
	   Start from 01 Jan 100 */
	g_date_set_dmy (tmp, 1, 1, 100);
	val = g_date_julian (d) - g_date_julian (tmp);
	
	/* Negative values are for dates before 1 Jan 1900 */
	g_date_set_dmy (tmp, 1, 1, 1900);
	val -= g_date_julian (tmp);

	v->gtk_type = gb_gtk_type_from_value (GB_VALUE_DATE);
	v->v.date   = val;
	
	/* Time is the fraction */
	v->v.date += (double) ((hr * SECS_IN_HR) + (min * SECS_IN_MIN) + sec) / SECS_IN_DAY;
	
	g_date_free (tmp);
	
	return v;
}


/**
 * gb_value_new_frx
 *  @fn_offset	A string containing a filename in quotes and an offset.
 *
 **/
GBValue *
gb_value_new_frx (const char *fn_offset)
{
	GBValue		*v;
	
	gchar		*name;
	gchar		**tokens;

	guint32		 offset;
	glong		 tmp;

	
	if (fn_offset != NULL) {
		tokens = g_strsplit (fn_offset, ":", -1);
		g_return_val_if_fail (tokens != NULL, NULL);	

		name   = g_strdup (tokens[0]);
		name   = g_strdup (&name[1]);
		name   = g_strndup (name, strlen(name)-1);
		tmp    = strtol (tokens[1], NULL, 16);
		g_strfreev (tokens);

		if (tmp == LONG_MIN) 
			g_error ("Underflow processing %s\nError:%s\n", fn_offset,
			         strerror (errno));

		if (tmp < 0) 
			g_error ("Negative number read when non-negative number expected processing %s\n", fn_offset);


		if (tmp == LONG_MAX)
			g_error ("Overflow processing %s\nError:%s\n", fn_offset,
			         strerror (errno));

		offset = tmp;

		v                  = g_new (GBValue, 1);
		v->gtk_type        = gb_gtk_type_from_value (GB_VALUE_FRX);
		v->v.frx	   = g_new (GBFrx, 1);
		v->v.frx->offset   = offset;
		v->v.frx->filename = g_strdup (name);

	} else {
		v                  = g_new (GBValue, 1);
		v->gtk_type        = gb_gtk_type_from_value (GB_VALUE_FRX);
		v->v.frx	   = g_new (GBFrx, 1);
		v->v.frx->offset   = offset;
		v->v.frx->filename = NULL;
	
	}

	return v;
}


GBValue *
gb_value_new_list (GPtrArray *a)
{
	GBValue *v = g_new (GBValue, 1);

	v->gtk_type= gb_gtk_type_from_value (GB_VALUE_LIST);
	v->v.list  = a;

	return v;
}


void
gb_value_destroy (GBValue *v)
{
	if (!v)
		return;

	switch (gb_value_from_gtk_type (v->gtk_type)) {
	case GB_VALUE_STRING:
		g_string_free (v->v.s, TRUE);
		break;

	case GB_VALUE_OBJECT:
		gb_object_unref (v->v.obj);
		break;
	default:
		break;
	}

	g_free ((GBValue *)v);
}

GBValue *
gb_value_copy (GBEvalContext *ec, const GBValue *v)
{
	GBValue *val;

	g_return_val_if_fail (v != NULL, NULL);

	val = g_new (GBValue, 1);

	val->gtk_type = v->gtk_type;
	switch (gb_value_from_gtk_type (v->gtk_type)) {
	case GB_VALUE_STRING:
		val->v.s = g_string_new (v->v.s->str);
		break;
		
	case GB_VALUE_OBJECT:
		val->v.obj = gb_object_ref (v->v.obj);
		break;

	case GB_VALUE_EMPTY:
	case GB_VALUE_NULL:
		break;

	case GB_VALUE_ERROR:
	case GB_VALUE_DATA_OBJECT:
	case GB_VALUE_DECIMAL:
	case GB_VALUE_USER_DEF:
	case GB_VALUE_CURRENCY:
		g_warning ("Exotic value type in copy %d",
			   gb_value_from_gtk_type (v->gtk_type));
		break;

	case GB_VALUE_VARIANT:
		g_warning ("No value should have type variant!");
		break;
		
	case GB_VALUE_INT:
	case GB_VALUE_LONG:
	case GB_VALUE_SINGLE:
	case GB_VALUE_DOUBLE:
	case GB_VALUE_DATE:
	case GB_VALUE_BOOLEAN:
	case GB_VALUE_BYTE:
	default:
		memcpy (val, v, sizeof (GBValue));
		break;
	}

	return val;
}

GBValue *
gb_value_copy_byval (GBEvalContext *ec, const GBValue *v)
{
	g_return_val_if_fail (v != NULL, NULL);
	g_return_val_if_fail (ec != NULL, NULL);

	if (v->gtk_type != gb_gtk_type_from_value (GB_VALUE_OBJECT))
		return gb_value_copy (ec, v);
	else
		return gb_eval_exception_fire (ec, _("Object copy unimplemented"));
}

void
gb_value_print (FILE *sink, const GBValue *v)
{
	g_return_if_fail (v != NULL);
	g_return_if_fail (sink != NULL);

	switch (gb_value_from_gtk_type (v->gtk_type)) {
	case GB_VALUE_EMPTY:
		fprintf (sink, "Empty");
		break;
	case GB_VALUE_NULL:
		fprintf (sink, "Null");
		break;
	case GB_VALUE_INT:
		fprintf (sink, "%d", v->v.i);
		break;
	case GB_VALUE_LONG:
		fprintf (sink, "%d", v->v.l);
		break;
	case GB_VALUE_SINGLE:
		fprintf (sink, "%f", v->v.f);
		break;
	case GB_VALUE_DOUBLE:
		fprintf (sink, "%g", v->v.d);
		break;
	case GB_VALUE_STRING:
		fprintf (sink, "%s", v->v.s->str);
		break;
	case GB_VALUE_CURRENCY:
	case GB_VALUE_DATE:
		g_warning ("Unimplemented");
		break;
	case GB_VALUE_OBJECT:
		fprintf (sink, "Object(%s)",
			 gtk_type_name (v->gtk_type));
	default:
		g_warning ("Too wierd type %d(%s)",
			   v->gtk_type, gtk_type_name (v->gtk_type));
		break;
	}
}

GBDouble
gb_value_get_as_double (const GBValue *v)
{
	g_return_val_if_fail (v != NULL, 0.0);

	switch (gb_value_from_gtk_type (v->gtk_type)) {
	case GB_VALUE_INT:
		return v->v.i;
	case GB_VALUE_LONG:
		return v->v.l;
	case GB_VALUE_SINGLE:
		return v->v.f;
	case GB_VALUE_DOUBLE:
		return v->v.d;
	case GB_VALUE_DATE:
		return v->v.date;
	case GB_VALUE_BYTE:
		return v->v.byte;
	case GB_VALUE_BOOLEAN:
		if (v->v.bool)
			return -1.0;
		else
			return 0.0;
	case GB_VALUE_EMPTY:
		return 0.0;
	default:
		g_warning ("Unhandled type as double %d",
			   gb_value_from_gtk_type (v->gtk_type));
		break;
	}
	return 0.0;
}

GBInt
gb_value_get_as_int (const GBValue *v)
{
	g_return_val_if_fail (v != NULL, 0);

	switch (gb_value_from_gtk_type (v->gtk_type)) {
	case GB_VALUE_INT:
		return (GBInt)v->v.i;
	case GB_VALUE_LONG:
		return (GBInt)v->v.l;
	case GB_VALUE_SINGLE:
		return (GBInt)v->v.f;
	case GB_VALUE_DOUBLE:
		return (GBInt)v->v.d;
	case GB_VALUE_BYTE:
		return (GBInt)v->v.byte;
	case GB_VALUE_BOOLEAN:
		return v->v.bool ? -1 : 0;
	default:
		g_warning ("Unhandled type as Int");
		break;
	}
	return 0;
}


GBLong
gb_value_get_as_long (const GBValue *v)
{
	g_return_val_if_fail (v != NULL, 0);

	switch (gb_value_from_gtk_type (v->gtk_type)) {
	case GB_VALUE_INT:
		return (GBLong)v->v.i;
	case GB_VALUE_LONG:
		return (GBLong)v->v.l;
	case GB_VALUE_SINGLE:
		return (GBLong)v->v.f;
	case GB_VALUE_DOUBLE:
		return (GBLong)v->v.d;
	case GB_VALUE_BYTE:
		return (GBLong)v->v.byte;
	case GB_VALUE_BOOLEAN:
		return v->v.bool ? -1 : 0;
	default:
		g_warning ("Unhandled type as long");
		break;
	}
	return 0;
}

GBString
gb_value_get_as_string (const GBValue *v)
{
	GString *str;

	g_return_val_if_fail (v != NULL, g_string_new (""));

	switch (gb_value_from_gtk_type (v->gtk_type)) {

	case GB_VALUE_EMPTY:
	case GB_VALUE_NULL:
		return g_string_new ("");

	case GB_VALUE_INT:
		str = g_string_sized_new (7);
		g_string_sprintf (str, "%d", v->v.i);
		break;

	case GB_VALUE_BYTE:
		str = g_string_sized_new (3);
		g_string_sprintf (str, "%d", v->v.byte);
		break;

	case GB_VALUE_LONG:
		str = g_string_sized_new (7);
		g_string_sprintf (str, "%d", v->v.l);
		break;

	case GB_VALUE_SINGLE:
		str = g_string_sized_new (15);
		g_string_sprintf (str, "%f", v->v.f);
		break;

	case GB_VALUE_DOUBLE:
		str = g_string_sized_new (15);
		g_string_sprintf (str, "%g", v->v.d);
		break;
		
	case GB_VALUE_STRING:
		str = g_string_new (v->v.s->str);
		break;

	case GB_VALUE_BOOLEAN:
		if (v->v.bool)
			str = g_string_new ("True");
		else
			str = g_string_new ("False");
		break;

	case GB_VALUE_OBJECT:
		str = g_string_new ("");
		if (v->v.obj && v->v.obj->object.klass)
			g_string_sprintf (str, "Object : %s",
					  gtk_type_name (v->v.obj->object.klass->type));
		else
			g_string_sprintf (str, "Object : [Null]");
		break;

	case GB_VALUE_CURRENCY:
	case GB_VALUE_DATE:
	default:
		g_warning ("Unhandled value as string");
		str = g_string_new ("");
		g_string_sprintf (str, "error value 0x%x",
				  gb_value_from_gtk_type (v->gtk_type));
	}
	return str;
}

/**
 * gb_value_get_as_boolean:
 * @v: a GBValue
 * 
 * checks if @v is false.
 *  that is :
 *  empty : false
 *  for numbers : 0 = false
 *  for strings : "" = false
 * 
 * Return value: FALSE if @v is false.
 **/
GBBoolean
gb_value_get_as_boolean (const GBValue* v)
{
	g_return_val_if_fail (v != NULL, GBFalse);

	switch (gb_value_from_gtk_type (v->gtk_type)) {

	case GB_VALUE_EMPTY :
		return GBFalse;

	case GB_VALUE_NULL:
		/* error */
		g_warning ("Can't cast an Null reference to boolean");
		return GBFalse;

	case GB_VALUE_INT:
		return (GBBoolean)(v->v.i != 0);

	case GB_VALUE_LONG:
		return (GBBoolean)(v->v.l != 0);

	case GB_VALUE_SINGLE:
		return (GBBoolean)(v->v.f != 0.0);

	case GB_VALUE_DOUBLE:
		return (GBBoolean)(v->v.d != 0.0);

	case GB_VALUE_CURRENCY:
		return (GBBoolean)(v->v.c != 0);

	case GB_VALUE_DATE:
		/*
		 * FIXME: should read if date was the right type
		 * return (GBBoolean)(v->v.date!=0.0);
		 */
		return (GBBoolean)(v->v.date != 0);

	case GB_VALUE_STRING:
		g_warning ("gb_value_get_as_boolean : string not implemented");
		return GBFalse;	  

	case GB_VALUE_BYTE:
		return (GBBoolean)(v->v.byte != 0);

	case GB_VALUE_BOOLEAN:
		return v->v.bool;

	case GB_VALUE_DECIMAL:
		g_warning ("gb_value_get_as_boolean : decimal not implemented");
		return GBFalse;

	default:
		g_warning ("Unhandled value as boolean");
		return GBFalse;
	}
	return GBFalse;
}

GBDate 
gb_value_get_as_date (const GBValue *v)
{
	GBDate d;
	
	g_return_val_if_fail (v != NULL, d);

	switch (gb_value_from_gtk_type (v->gtk_type)) {

	case GB_VALUE_DATE:
		d = v->v.date;
		break;

	case GB_VALUE_DOUBLE:
		d = v->v.d;
		break;
	default:
		g_warning ("gb_value_get_as_date: "
			   "Conversion from other types not implemented");
		break;
	}

	return d;
}

GDate *
gb_value_get_as_gdate (const GBValue *v)
{
	GDate   *tmp = g_date_new ();
	GDate   *d   = g_date_new ();
	guint32 val;

	g_return_val_if_fail (v != NULL, d);

	switch (gb_value_from_gtk_type (v->gtk_type)) {
		
	case GB_VALUE_DATE:
		g_date_set_dmy (tmp, 1, 1, 100);
		val = v->v.date + g_date_julian (tmp);
		g_date_set_dmy (tmp, 1, 1, 1900);
		val += g_date_julian (tmp);
		g_date_set_julian (d, val);
		break;
	
	default:
		g_warning ("gb_value_get_as_gdate:"
			   "Conversion from other types not implemented");
		break;
	}

	return d;
}

GBInt 
gb_value_time_get_hr (const GBValue *v)
{
	gdouble tmp, i;

	tmp = modf (v->v.date, &i);

	return (GBInt) floor (floor (tmp * SECS_IN_DAY) / SECS_IN_HR);  
}

GBInt
gb_value_time_get_min (const GBValue *v)
{
	gdouble i;
	gdouble tmp = modf (v->v.date, &i);

	tmp = floor (tmp * SECS_IN_DAY);
	tmp = tmp - (SECS_IN_HR * gb_value_time_get_hr (v));
	
	return (GBInt) floor (tmp / SECS_IN_MIN);
}
 
GBInt 
gb_value_time_get_sec (const GBValue *v)
{
	gdouble i;
	gdouble tmp = modf (v->v.date, &i);

	tmp = floor (tmp * SECS_IN_DAY);

	tmp = tmp - (SECS_IN_HR * gb_value_time_get_hr (v))  
		  - (SECS_IN_MIN * gb_value_time_get_min (v));
	
	return (GBInt) tmp;
}

/*
 *   Returns a newly allocated Value promoted to type 'to',
 * if possible. If promotion invalid returns NULL and sets
 * up exception.
 */
GBValue *
gb_value_promote (GBEvalContext *ec,
		  GtkType        to,
		  const GBValue *v)
{
	GBValue    *ret = NULL;
	GBValue    *nv;
	GBValueType from;

	g_return_val_if_fail (v != NULL, NULL);
	g_return_val_if_fail (ec != NULL, NULL);

	if (v->gtk_type == to ||
	    gtk_type_is_a (v->gtk_type, to) ||
	    to == gb_gtk_type_from_value (GB_VALUE_VARIANT))

		return gb_value_copy (ec, v);

	from = gb_value_from_gtk_type (v->gtk_type);

	switch (gb_value_from_gtk_type (to)) {
	case GB_VALUE_BOOLEAN:
		switch (from) {
			/*
			 * FIXME: anything wrong or missing?
			 * This is basically how C handles booleans
			 */
		case GB_VALUE_STRING:
			if (!g_strcasecmp (v->v.s->str, "True"))
				ret = gb_value_new_boolean (TRUE);
			else if (!g_strcasecmp (v->v.s->str, "False"))
				ret = gb_value_new_boolean (FALSE);
			break;

		case GB_VALUE_INTEGER:
		        nv = gb_value_promote (ec, gb_gtk_type_from_value (GB_VALUE_LONG), v);
			if (nv && nv->v.l != 0)
				ret = gb_value_new_boolean (TRUE);
			else
		 	        ret = gb_value_new_boolean (FALSE);
			gb_value_destroy (nv);
			break;
			
		case GB_VALUE_FLOAT:
		        nv = gb_value_promote (ec, gb_gtk_type_from_value (GB_VALUE_DOUBLE), v);
			if (nv && nv->v.d != 0.0)
				ret = gb_value_new_boolean (TRUE);
			else
				ret = gb_value_new_boolean (FALSE);
			gb_value_destroy (nv);
			break;
			
		case GB_VALUE_EMPTY:
		case GB_VALUE_NULL:
		        ret = gb_value_new_boolean (FALSE);
			break;

		default:
			break;
		}
		break;

	/* Chain up then check */
	case GB_VALUE_BYTE:
		nv = gb_value_promote (ec, gb_gtk_type_from_value (GB_VALUE_LONG), v);
		if (nv &&
		    nv->v.l <= GB_VALUE_BYTE_MAX &&
		    nv->v.l >= GB_VALUE_BYTE_MIN)
			ret = gb_value_new_byte (nv->v.l);
		gb_value_destroy (nv);
		break;

	case GB_VALUE_INT:
		nv = gb_value_promote (ec, gb_gtk_type_from_value (GB_VALUE_LONG), v);
		if (nv &&
		    nv->v.l <= GB_VALUE_INT_MAX &&
		    nv->v.l >= GB_VALUE_INT_MIN)
			ret = gb_value_new_int (nv->v.l);
		gb_value_destroy (nv);
		break;

	case GB_VALUE_LONG:
		switch (from) {
		case GB_VALUE_EMPTY:
		case GB_VALUE_NULL:
			ret = gb_value_new_long (0);
			break;

		case GB_VALUE_INT:
			ret = gb_value_new_long (v->v.i);
			break;

		case GB_VALUE_BYTE:
			ret = gb_value_new_long (v->v.byte);
			break;

		case GB_VALUE_STRING:
		{
			GBLong   l;
			char    *end;
			GString *str = v->v.s;

			/* FIXME: is this right ? */
			if (str->str && str->str [0] == '\0')
				ret = gb_value_new_long (0);

			l = strtol (str->str, &end, 0);
			if (str->str [0] != '\0' && *end == '\0')
				ret = gb_value_new_long (l);
			break;
		}
		
		case GB_VALUE_SINGLE:
			/* FIXME: should this be a trunc / ceil / floor ? */
			ret = gb_value_new_long (v->v.f);
			break;

		case GB_VALUE_DOUBLE:
			/* FIXME: should this be a trunc / ceil / floor ? */
			ret = gb_value_new_long (v->v.d);
			break;

		case GB_VALUE_BOOLEAN:
			/* FIXME: test: is this correct ? */
			if (v->v.bool)
				ret = gb_value_new_long (-1);
			else
				ret = gb_value_new_long (0);
			break;

		default:
			break;
		}
		break;

	case GB_VALUE_SINGLE:
		nv = gb_value_promote (ec, gb_gtk_type_from_value (GB_VALUE_DOUBLE), v);
		if (nv)
/*
 * FIXME: Need some sensible bounds, or to know if we just loose precision.
 *		    v->v.l <= GB_VALUE_INT_MAX && *		    v->v.l >= GB_VALUE_INT_MIN)
 */
			ret = gb_value_new_single (nv->v.d);
		gb_value_destroy (nv);
		break;

	case GB_VALUE_DOUBLE:
		switch (from) {
		case GB_VALUE_INTEGER:
			ret = gb_value_new_double (gb_value_get_as_long (v));
			break;
		case GB_VALUE_SINGLE:
			ret = gb_value_new_double (v->v.f);
			break;
		case GB_VALUE_STRING:
		{
			GBDouble d;
			char    *end;
			GString *str = v->v.s;

			/* FIXME: is this right ? */
			if (str->str && str->str [0] == '\0')
				ret = gb_value_new_double (0.0);

			d = strtod (str->str, &end);
			if (str->str [0] != '\0' && *end == '\0')
				ret = gb_value_new_double (d);
			break;
		}
		case GB_VALUE_DATE:
			ret = gb_value_new_double (v->v.date);
			break;
		case GB_VALUE_BOOLEAN:
			/* FIXME: test: is this correct ? */
			if (v->v.bool)
				ret = gb_value_new_double (-1.0);
			else
				ret = gb_value_new_double (0.0);
			break;
		case GB_VALUE_EMPTY:
			ret = gb_value_new_double (0.0);
			break;
		default:
			break;
		}
		
		break;

	case GB_VALUE_STRING:
		/* FIXME: should promotions to strings happen ? */
		switch (from) {
		case GB_VALUE_BOOLEAN:
			if (v->v.bool)
				ret = gb_value_new_string_chars ("True");
			else
				ret = gb_value_new_string_chars ("False");
			break;

		case GB_VALUE_EMPTY:
			ret = gb_value_new_string_chars ("");
			break;

		case GB_VALUE_NULL:
			ret = gb_value_new_string_chars ("Null");
			break;

		case GB_VALUE_BYTE:
		case GB_VALUE_INT:
			nv = gb_value_promote (ec, gb_gtk_type_from_value (GB_VALUE_LONG), v);
			ret = gb_value_promote (ec, gb_gtk_type_from_value (GB_VALUE_STRING), nv);
			gb_value_destroy (nv);
			break;

		case GB_VALUE_LONG:
		{
			char *str = g_strdup_printf ("%ld", (long)v->v.l);

			ret = gb_value_new_string_chars (str);

			g_free (str);
			break;
		}

		case GB_VALUE_SINGLE:
			nv = gb_value_promote (ec, gb_gtk_type_from_value (GB_VALUE_DOUBLE), v);
			ret = gb_value_promote (ec, gb_gtk_type_from_value (GB_VALUE_STRING), nv);
			gb_value_destroy (nv);
			break;

		case GB_VALUE_DOUBLE:
		{
			char *str = g_strdup_printf ("%g", v->v.d);

			ret = gb_value_new_string_chars (str);

			g_free (str);
			break;
		}

		case GB_VALUE_DATE:
		{
			GDate *d;
			double tmp;
			char   *str = NULL;
			char   *t   = NULL;
			char   *res = NULL;

			/* FIXME: The format should be locale specific */

			if (floor (v->v.date) != 0) {
				d = gb_value_get_as_gdate (v);
				str = g_strdup_printf ("%.4d-%.2d-%.2d", g_date_year (d),
						       g_date_month (d), g_date_day (d));
			}

			if (modf (v->v.date, &tmp) != 0) 
				t = g_strdup_printf ("%.2d:%.2d:%.2d", gb_value_time_get_hr (v),
						     gb_value_time_get_min (v), gb_value_time_get_sec (v));
			if (str && t) { 
				res = g_strjoin (" ", str, t, NULL);
				g_free (str);
				g_free (t);
			} else if (str) {
				res = g_strdup (str);
				g_free (str);
			} else {
				res = g_strdup (t);
				g_free (t);
			}
			ret = gb_value_new_string_chars (res);
			g_free (res);
			
			break;
			
		}
		
		case GB_VALUE_FRX:
		{
			GPtrArray *textptr;

			textptr = gb_get_frx_text (v);

			ret = gb_value_new_string_chars (g_ptr_array_index (textptr, 0));
			
			g_ptr_array_free (textptr, FALSE);
		}

		default:
			break;
		}
		break;
		
	case GB_VALUE_DATE:

		switch (from) {
		case GB_VALUE_STRING:
		{
			ret = gb_value_new_date_from_str (v->v.s->str);

			if (!ret) 
				fprintf (stderr, _("gb_value_promote : String->Date failed on %s"), v->v.s->str);
			
			break;
		}
		case GB_VALUE_DOUBLE:
			ret = gb_value_new_date (v->v.date);
			break;

		default:
			g_warning ("Only promotion from string,double->date implemented");
			break;
		}
		break;			

	case GB_VALUE_LIST:
		switch (from) {
		case GB_VALUE_FRX:
		{
			ret = gb_value_new_list (gb_get_frx_list (v));
		
			break;
		}

		default:
			break;
		}
	
	default:
		break;
	}
	
	if (ret)
		return ret;

	return gb_eval_exception_firev (ec, _("(gb_value_promote) Can't promote %s to %s"),
					gtk_type_name (gb_gtk_type_from_value (from)),
					gtk_type_name (to));
}

gboolean
gb_value_is_integer (const GBValue *v)
{
	g_return_val_if_fail (v != NULL, FALSE);

	switch (gb_value_from_gtk_type (v->gtk_type)) {
	case GB_VALUE_INTEGER:
		return TRUE;
	default:
		return FALSE;
	}
}

GBValue *
gb_value_new_default (GBEvalContext *ec, GtkType t)
{
	GBValue *val;

	g_return_val_if_fail (type_def_hash != NULL, NULL);

	if (gtk_type_is_a (t, gb_fundamental_type ())) {
		val = g_hash_table_lookup (type_def_hash, GINT_TO_POINTER (t));
		if (!val) 
			return NULL;

		return gb_value_copy (ec, val);
	} else /* Create a new instance and hope */
		return gb_value_new_object (gtk_type_new (t));
}

GtkType
gb_gtk_type_from_name (const char *vb_type_name)
{
	GtkType t;
	char   *name;

	g_return_val_if_fail (vb_type_name != NULL, 0);

	name = gb_gtk_type_name (vb_type_name);

	t = gtk_type_from_name (name);

	g_free (name);

	return t;
}

typedef struct {
	const char *name;
	GBValueType t;
} find_closure_t;

void
find_name_cb (gpointer key, gpointer value, gpointer user_data)
{
	find_closure_t *fct = user_data;

	if (GPOINTER_TO_INT (value) == fct->t)
		fct->name = key;
}

static void
add_type_map (const char *name, GBValue *val)
{
	g_hash_table_insert (type_hash, g_strdup (name),
			     GINT_TO_POINTER (val->gtk_type));
	g_hash_table_insert (type_def_hash, GINT_TO_POINTER (val->gtk_type), val);
}

static void
add_type_map_type (const char *name, GBValue *val, GtkType gtk_type)
{
	g_hash_table_insert (type_hash, g_strdup (name),
			     GINT_TO_POINTER (gtk_type));
	g_hash_table_insert (type_def_hash, GINT_TO_POINTER (gtk_type), val);
}

gint
gb_strcase_equal (gconstpointer v, gconstpointer v2)
{
	return g_strcasecmp ((const gchar*) v, (const gchar*)v2) == 0;
}

/* a char* hash function from ASU */
guint
gb_strcase_hash (gconstpointer v)
{
	const unsigned char *s = (const unsigned char *)v;
	const unsigned char *p;
	guint h = 0, g;

	for (p = s; *p != '\0'; p += 1) {
		h = ( h << 4 ) + tolower (*p);
		if ( ( g = h & 0xf0000000 ) ) {
			h = h ^ (g >> 24);
			h = h ^ g;
		}
	}

	return h /* % M */;
}

typedef struct {
	const char *name;
	GtkType     created_type;
} GBBuiltinType;

static GBBuiltinType
builtin_types [] = {
	{ "gb-empty" },		/* 0 */
	{ "gb-null" },		/* 1 */
	{ "gb-integer" },	/* 2 */
	{ "gb-long" },		/* 3 */
	{ "gb-single" },	/* 4 */
	{ "gb-double" },	/* 5 */
	{ "gb-currency" },	/* 6 */
	{ "gb-date" },		/* 7 */
	{ "gb-string" },	/* 8 */
	{ "gb-object" },	/* 9 */
	{ "gb-error" },		/* 10 */
	{ "gb-boolean" },	/* 11 */
	{ "gb-variant" },	/* 12 */
	{ "gb-dataobject" },	/* 13 */
	{ "gb-decimal" },	/* 14 */
	{ NULL },		/* 15 */
	{ NULL },		/* 16 */
	{ "gb-byte" },	        /* 17 */
	{ NULL },		/* 18 */
	{ NULL },		/* 19 */
	{ NULL },		/* 20 */
	{ NULL },		/* 21 */
	{ NULL },		/* 22 */
	{ NULL },		/* 23 */
	{ NULL },		/* 24 */
	{ NULL },		/* 25 */
	{ NULL },		/* 26 */
	{ NULL },		/* 27 */
	{ NULL },		/* 28 */
	{ NULL },		/* 29 */
	{ NULL },		/* 30 */
	{ NULL },		/* 31 */
	{ NULL },		/* 32 */
	{ NULL },		/* 33 */
	{ NULL },		/* 34 */
	{ NULL },		/* 35 */
	{ "gb-userdef" },	/* 36 */
	{ "gb-frx" },		/* 37 */
	{ "gb-list" },		/* 38 */
};

GtkType
gb_fundamental_type ()
{
	static int type = 0;
	
	if (!type) {
		GtkTypeInfo info = { "gb-fundamental-type" };
		type = gtk_type_unique (0, &info);
	}

	return type;
}

GtkType
gb_gtk_type_from_value (GBValueType t)
{
	if (t >= 0 && t < sizeof (builtin_types) / sizeof (GBBuiltinType))
		return builtin_types [t].created_type;
	else
		return builtin_types [GB_TYPE_OBJECT].created_type;
}

GBValueType
gb_value_from_gtk_type (GtkType t)
{
	int i;

	for (i = 0; i < sizeof (builtin_types) / sizeof (GBBuiltinType); i++) {
		if (builtin_types [i].created_type == t)
			return i;
	}

	return GB_TYPE_OBJECT;
}

char *
gb_gtk_type_name (const char *gb_name)
{
	char *name;

	g_return_val_if_fail (gb_name != NULL, NULL);

	name = g_strconcat ("gb-", gb_name, NULL);

	g_strdown (name);

	return name;
}

char *
gb_type_name_from_gtk (const char *gtk_name)
{
	if (gtk_name) {
		if (strncmp (gtk_name, "gb-", 3))
			return g_strdup (gtk_name + 3);
		else
			return g_strdup (gtk_name);
	} else
		return NULL;
}

void
gb_value_init (void)
{
	int    i;
	GtkTypeInfo info;

	/* Register Gtk Types */
	for (i = 0; i < sizeof (builtin_types) / sizeof (GBBuiltinType); i++) {
		info.object_size = 0;
		info.class_size  = 8;
		info.class_init_func  = NULL;
		info.object_init_func = NULL;
		info.reserved_1 = NULL;
		info.reserved_2 = NULL;
		info.base_class_init_func = NULL;

		if ((info.type_name = (char *)builtin_types [i].name)) {
			builtin_types [i].created_type =
				gtk_type_unique (gb_fundamental_type (), &info);
			
			if (!builtin_types [i].created_type)
				g_warning ("Failed to create type '%s'", info.type_name);
		}
	}

	gb_type_int     = builtin_types [GB_VALUE_INT].created_type;
	gb_type_long    = builtin_types [GB_VALUE_LONG].created_type;
	gb_type_single  = builtin_types [GB_VALUE_SINGLE].created_type;
	gb_type_double  = builtin_types [GB_VALUE_DOUBLE].created_type;
	gb_type_string  = builtin_types [GB_VALUE_STRING].created_type;
	gb_type_boolean = builtin_types [GB_VALUE_BOOLEAN].created_type;
	gb_type_list    = builtin_types [GB_VALUE_LIST].created_type;

	builtin_types [GB_VALUE_OBJECT].created_type =
		gb_object_get_type ();
/*	builtin_types [GB_VALUE_VARIANT].created_type = 0;*/

	/* Register various internal types */
	GBTrue_value  = gb_value_new_boolean (GBTrue);
	GBFalse_value = gb_value_new_boolean (GBFalse);

	/* Register type name mappings */
	type_hash     = g_hash_table_new (gb_strcase_hash, gb_strcase_equal);
	type_def_hash = g_hash_table_new (NULL, NULL); /* hear it type */

	add_type_map ("Null",    gb_value_new_null    ());
	add_type_map ("Empty",   gb_value_new_empty   ());

	add_type_map ("Integer", gb_value_new_int     (0));
	add_type_map ("Long",    gb_value_new_long    (0));
	add_type_map ("Single",  gb_value_new_single  (0.0));
	add_type_map ("Double",  gb_value_new_double  (0.0));
	/* Currency */
	
	add_type_map ("Date",    gb_value_new_time (0,0,0));

	add_type_map ("String",  gb_value_new_string_chars (""));
	/* Object */

	add_type_map ("Object",  gb_value_new_object (gtk_type_new (GB_TYPE_OBJECT)));

	/* Error */
	add_type_map ("Boolean", gb_value_new_boolean (FALSE));
	add_type_map_type ("Variant", gb_value_new_empty (), 
			   builtin_types [GB_VALUE_VARIANT].created_type);
	/* Data object */
	/* Decimal */
	add_type_map ("Byte",    gb_value_new_byte (0));
	add_type_map ("Frx",     gb_value_new_frx  (NULL));
	add_type_map ("List",    gb_value_new_list (NULL));
}

static void
kill_name_type_map (gpointer key, gpointer value, gpointer user_data)
{
	if (key)
		g_free (key);

}

static void
kill_type_def_map (gpointer key, gpointer value, gpointer user_data)
{
	if (value)
		gb_value_destroy (value);
}

void
gb_value_shutdown (void)
{
	gb_value_destroy ((GBValue *)GBTrue_value);
	gb_value_destroy ((GBValue *)GBFalse_value);

	g_hash_table_foreach (type_def_hash, kill_type_def_map, NULL);
	g_hash_table_destroy (type_def_hash);
	g_hash_table_foreach (type_hash, kill_name_type_map, NULL);
	g_hash_table_destroy (type_hash);
}
