/*
 * GNOME Basic Statements
 *
 * Authors:
 *    Jody Goldberg (jgoldberg@home.com)
 *    Ravi Pratap (ravi_pratap@email.com)
 *
 * Copyright 1999, 2000, Helix Code, Inc.
 */

#include <gb/gb-lex.h>
#include <gb/gb-statement.h>

static GBStatement *
stmt_new (GBParseData *m)
{
	GBStatement *s = g_new (GBStatement, 1);

	s->line = gb_lexer_stream_line (m->ls);

	return s;
}

const GBStatement *
gb_stmt_new_assignment (GBParseData *m, const GBExpr *dest, const GBExpr *val)
{
	GBStatement *res;

	g_return_val_if_fail (dest != NULL, NULL);
	g_return_val_if_fail (val != NULL, NULL);

	res = stmt_new (m);
	res->type = GBS_ASSIGN;
	res->parm.assignment.dest = dest;
	res->parm.assignment.val = val;

	return res;
}
 
const GBStatement *
gb_stmt_new_call (GBParseData *m, const GBExpr *func)
{
	GBStatement *res;

	g_return_val_if_fail (func != NULL, NULL);
	g_return_val_if_fail (func->type == GB_EXPR_OBJREF, NULL);

	res = stmt_new (m);
	res->type = GBS_CALL;
	res->parm.func.call = func;

	return res;
}

const GBStatement *
gb_stmt_new_forloop (GBParseData  *m, const char *name,
		     const GBExpr *from, const GBExpr *to,
		     const GBExpr *step,
		     GSList *body)
{
	GBStatement *res;

	g_return_val_if_fail (to != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);
	g_return_val_if_fail (from != NULL, NULL);

	res = stmt_new (m);
	res->type = GBS_FOR;
	res->parm.forloop.var = name;
	res->parm.forloop.from = from;
	res->parm.forloop.to = to;
	res->parm.forloop.step = step;
	res->parm.forloop.body = g_slist_reverse (body);

	return res;
}

const GBStatement *
gb_stmt_new_foreach (GBParseData  *m, const char *name,
		     const GBExpr *collection,
		     GSList *body)
{
	GBStatement *res;

	g_return_val_if_fail (name != NULL, NULL);
	g_return_val_if_fail (collection != NULL, NULL);

	res = stmt_new (m);
	res->type = GBS_FOREACH;
	res->parm.foreach.var = name;
	res->parm.foreach.collection = collection;
	res->parm.foreach.body = g_slist_reverse (body);

	return res;
}
const GBStatement *
gb_stmt_new_with (GBParseData  *m,
		  const GBExpr *base_obj, GSList *body)
{
	GBStatement *res;

	g_return_val_if_fail (base_obj != NULL, NULL);
	g_return_val_if_fail (m->with_depth > 0, NULL);

	res = g_new0 (GBStatement, 1);
	res->type = GBS_WITH;
	res->parm.with.base_obj = base_obj;
	res->parm.with.body = g_slist_reverse (body);

	/* Decrement the module->with_depth */
	res->parm.with.depth = m->with_depth--;

	return res;
}

const GBStatement *
gb_stmt_new_do_while (GBParseData  *m,
		      const GBExpr *while_expr, const GBExpr *until_expr,
		      GSList *body)
{
	GBStatement *res;

	g_return_val_if_fail ((while_expr == NULL) || (until_expr == NULL), NULL);

	res = stmt_new (m);
	if (until_expr != NULL) {
		res->type = GBS_DO;
		res->parm.do_while.expr = until_expr;
	} else {
		res->type = GBS_WHILE;
		res->parm.do_while.expr = while_expr;
	}
	res->parm.do_while.body = g_slist_reverse (body);

	return res;
}

const GBStatement *
gb_stmt_if_set_cond (const GBStatement *stmt,
		     const GBExpr *condition)
{
	GBStatement *s = (GBStatement *)stmt;

	g_return_val_if_fail (stmt != NULL, stmt);
	g_return_val_if_fail (stmt->type == GBS_IF, stmt);
	g_return_val_if_fail (stmt->parm.if_stmt.condition == NULL, stmt);

	s->parm.if_stmt.condition = condition;

	return stmt;
}

const GBStatement *
gb_stmt_new_if (GBParseData *m, GSList *body, GSList *else_body)
{
	GBStatement *res;

	res = stmt_new (m);
	res->type = GBS_IF;
	res->parm.if_stmt.condition = NULL;
	res->parm.if_stmt.body = g_slist_reverse (body);
	res->parm.if_stmt.else_body = else_body;

	return res;
}


const GBStatement *
gb_stmt_new_select (GBParseData  *module,
		    const GBExpr *expr,
		    GSList       *case_stmts)
{
	GBStatement *stmt = stmt_new (module);

	stmt->type = GBS_SELECT;

	stmt->parm.select.test_expr = expr;
	stmt->parm.select.cases     = case_stmts;

	return stmt;
}

const GBStatement *
gb_stmt_new_set (GBParseData  *module, 
		 const GBExpr *var,
		 gboolean      new, 
		 const GBExpr *objref)
{
	GBStatement *stmt = stmt_new (module);
	
	stmt->type = GBS_SET;

	stmt->parm.set.var    = var;
	stmt->parm.set.new    = new;
	stmt->parm.set.objref = objref;

	return stmt;

}

GBCaseExpr *
gb_select_case_new_expr (const GBExpr *expr)
{
	GBCaseExpr *e = g_new (GBCaseExpr, 1);

	e->type = GB_CASE_EXPR;
	e->u.expr = expr;

	return e;
}

GBCaseExpr *
gb_select_case_new_expr_to_expr (const GBExpr *from,
				 const GBExpr *to)
{
	GBCaseExpr *e = g_new (GBCaseExpr, 1);

	e->type = GB_CASE_EXPR_TO_EXPR;
	e->u.expr_to_expr.from = from;
	e->u.expr_to_expr.to   = to;

	return e;
}

GBCaseExpr *
gb_select_case_new_comparison (GBExprType    op,
			       const GBExpr *to)
{
	GBCaseExpr *e = g_new (GBCaseExpr, 1);

	e->type = GB_CASE_COMPARISON;
	e->u.comparison.op = op;
	e->u.comparison.to = to;

	return e;
}

GBSelectCase *
gb_select_case_new_else (GSList *statements)
{
	GBSelectCase *sc = g_new (GBSelectCase, 1);
	GBCaseExpr   *e  = g_new (GBCaseExpr, 1);

	e->type   = GB_CASE_ELSE;
	e->u.expr = NULL;

	sc->case_exprs = g_slist_prepend (NULL, (gpointer) e);
	sc->statements = statements;
	
	return sc;
}

GBSelectCase *
gb_select_case_new (GSList *case_exprs,
		    GSList *statements)
{
	GBSelectCase *sc = g_new (GBSelectCase, 1);

	sc->case_exprs = case_exprs;
	sc->statements = statements;

	return sc;
}

static void
gb_cases_destroy (GSList *l)
{
	while (l) {
		GBSelectCase *sc = l->data;
		GSList       *tmp;
		
		for (tmp = sc->case_exprs; tmp; tmp = tmp->next) {
			GBCaseExpr *e = tmp->data;
		
			if (e->type == GB_CASE_COMPARISON)
				gb_expr_destroy (e->u.comparison.to);
			else if (e->type == GB_CASE_EXPR_TO_EXPR) {
				gb_expr_destroy (e->u.expr_to_expr.from);
				gb_expr_destroy (e->u.expr_to_expr.to);
			} else if (e->type == GB_CASE_EXPR)
				gb_expr_destroy (e->u.expr);
		}
		gb_stmts_destroy (sc->statements);
		
		l = g_slist_remove (l, l->data);
	}
}

const GBStatement *
gb_stmt_new_randomize (GBParseData  *module,
		       const GBExpr *expr)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);
 
	stmt = stmt_new (module);
	stmt->type           = GBS_RANDOMIZE;
	stmt->parm.randomize = expr;

	return stmt;
}

const GBStatement *
gb_stmt_new_load (GBParseData *module, const GBExpr *expr)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type      = GBS_LOAD;
	stmt->parm.load = expr;

	return stmt;
}

const GBStatement *
gb_stmt_new_unload (GBParseData *module, const GBExpr *expr)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type        = GBS_UNLOAD;
	stmt->parm.unload = expr;

	return stmt;
}

const GBStatement *
gb_stmt_new_redim (GBParseData *module,
		   const char  *var_name,
		   GSList      *indices,
		   gboolean     preserve)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type                = GBS_REDIM;
	stmt->parm.redim.var_name = var_name;
	stmt->parm.redim.preserve = preserve;
	stmt->parm.redim.indices  = indices;

	return stmt;
}

const GBStatement *
gb_stmt_new_exit (GBParseData   *module,
		  GBSExitNesting exit)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type      = GBS_EXIT;
	stmt->parm.exit = exit;

	return stmt;
}

const GBStatement *
gb_stmt_new_erase (GBParseData *module,
		   const char  *var_name)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type                = GBS_ERASE;
	stmt->parm.erase.var_name = var_name;

	return stmt;
}

const GBStatement *
gb_stmt_new_chdir (GBParseData  *module,
		   const GBExpr *path)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);

	stmt                  = stmt_new (module);
	stmt->type            = GBS_CHDIR;
	stmt->parm.chdir.path = path;
	
	return stmt;
}

const GBStatement *
gb_stmt_new_filecopy (GBParseData  *module,
		      const GBExpr *source,
		      const GBExpr *dest)
{
	GBStatement *stmt;
	
	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type = GBS_FILECOPY;
	stmt->parm.filecopy.source = source;
	stmt->parm.filecopy.dest   = dest;

	return stmt;
}

const GBStatement *
gb_stmt_new_kill (GBParseData   *module,
		  const GBExpr  *path)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type = GBS_KILL;
	stmt->parm.kill.path = path;

	return stmt;
}

const GBStatement *
gb_stmt_new_mkdir (GBParseData   *module,
		   const GBExpr  *path)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type = GBS_MKDIR;
	stmt->parm.mkdir.path = path;
	
	return stmt;
}

const GBStatement *
gb_stmt_new_reset (GBParseData   *module)
{
	GBStatement *stmt;
	
	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type = GBS_RESET;

	return stmt;
}

const GBStatement *
gb_stmt_new_rmdir (GBParseData   *module,
		   const GBExpr  *path)
{
	GBStatement *stmt;
	
	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type = GBS_RMDIR;
	stmt->parm.rmdir.path = path;

	return stmt;
}

const GBStatement *
gb_stmt_new_setattr (GBParseData   *module,
		     const GBExpr  *file,
		     const GBExpr  *attrs)
{
	GBStatement *stmt;
	
	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type = GBS_SETATTR;
	stmt->parm.setattr.file = file;
	stmt->parm.setattr.attrs = attrs;

	return stmt;
}



/************** File handling related functions **************************/

const GBStatement *
gb_stmt_new_open (GBParseData  *module,
		  const GBExpr *filename,
		  GBSOpenMode   mode,
		  GBSAccessMode access,
		  GBSLockMode   lock,
		  const GBExpr *handle,
		  const GBExpr *recordlen)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type = GBS_OPEN;
	stmt->parm.open.filename  = filename;
	stmt->parm.open.mode      = mode;
	stmt->parm.open.access    = access;
	stmt->parm.open.lock      = lock;
	stmt->parm.open.handle    = handle;
	stmt->parm.open.recordlen = recordlen;

	return stmt;
}

const GBStatement *
gb_stmt_new_input (GBParseData *module, const GBExpr *handle,
		   GBExprList *objrefs)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type = GBS_INPUT;
	stmt->parm.input.handle  = handle;
	stmt->parm.input.objrefs = objrefs;

	return stmt;
}

const GBStatement *
gb_stmt_new_print (GBParseData *module, const GBExpr *handle,
		   GBExprList *items)
{
	GBStatement *stmt;
	
	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type = GBS_PRINT;
	stmt->parm.print.handle  = handle;
	stmt->parm.print.items = items;

	return stmt;
}


const GBStatement *
gb_stmt_new_line_input (GBParseData *module, const GBExpr *handle,
			const GBExpr *objref)
{
	GBStatement *stmt;
	
	g_return_val_if_fail (module != NULL, NULL);
	
	stmt = stmt_new (module);
	stmt->type = GBS_LINE_INPUT;
	stmt->parm.line_input.handle = handle;
	stmt->parm.line_input.objref = objref;

	return stmt;
}

const GBStatement *
gb_stmt_new_close (GBParseData *module, GBExprList *handles)
{
	GBStatement *stmt;
	
	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type = GBS_CLOSE;
	stmt->parm.close.handles = handles;

	return stmt;
}

const GBStatement *
gb_stmt_new_on_error_next (GBParseData *module)
{
	GBStatement *stmt;
	
	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type = GBS_ON_ERROR;
	stmt->parm.on_error.type  = GB_ON_ERROR_NEXT;
	stmt->parm.on_error.label = NULL;

	return stmt;
}

const GBStatement *
gb_stmt_new_on_error_goto (GBParseData *module,
			   const char  *label)
{
	GBStatement *stmt;
	
	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type = GBS_ON_ERROR;
	stmt->parm.on_error.type  = GB_ON_ERROR_GOTO;
	stmt->parm.on_error.label = label;

	return stmt;
}

const GBStatement *
gb_stmt_new_on_expr (GBParseData  *module,
		   const GBExpr *expr,
		   GSList      *indices)
{
	GBStatement *stmt;
	
	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type = GBS_ON_EXPR;

	return stmt;
}

const GBStatement *
gb_stmt_new_goto (GBParseData *module, const char *label)
{
	GBStatement *stmt;
	
	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type       = GBS_GOTO;
	stmt->parm.label = label;

	return stmt;
}

const GBStatement *
gb_stmt_new_label (GBParseData *module, const char *label)
{
	GBStatement *stmt;
	
	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type       = GBS_LABEL;
	stmt->parm.label = label;

	return stmt;
}

const GBStatement *
gb_stmt_new_get (GBParseData  *module, const GBExpr *handle, const GBExpr *recordnum,
		 const GBExpr *objref)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type = GBS_GET;
	stmt->parm.get.handle = handle;
	stmt->parm.get.recordnum = recordnum;
	stmt->parm.get.objref = objref;

	return stmt;
}

const GBStatement *
gb_stmt_new_put (GBParseData  *module, const GBExpr *handle, const GBExpr *recordnum,
		 const GBExpr *objref)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);
	
	stmt = stmt_new (module);
	stmt->type = GBS_PUT;
	stmt->parm.put.handle = handle;
	stmt->parm.put.recordnum = recordnum;
	stmt->parm.put.objref = objref;

	return stmt;
}

const GBStatement *
gb_stmt_new_seek (GBParseData *module, const GBExpr *handle, const GBExpr *pos)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);

	stmt = stmt_new (module);
	stmt->type = GBS_SEEK;
	stmt->parm.seek.handle = handle;
	stmt->parm.seek.pos = pos;

	return stmt;
}

const GBStatement *
gb_stmt_new_resume_default (GBParseData  *module)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);
	
	stmt = stmt_new (module);
	stmt->type = GBS_RESUME;
	stmt->parm.on_error.type  = GB_RESUME_DEFAULT;
	stmt->parm.on_error.label = NULL;

	return stmt;
}

const GBStatement *
gb_stmt_new_gosub (GBParseData  *module, const char *label)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);
	
	stmt = stmt_new (module);
	stmt->type = GBS_GOSUB;
	stmt->parm.label = label;

	return stmt;
}

const GBStatement *
gb_stmt_new_resume_next (GBParseData  *module)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);
	
	stmt = stmt_new (module);
	stmt->type = GBS_RESUME;
	stmt->parm.on_error.type  = GB_RESUME_NEXT;
	stmt->parm.on_error.label = NULL;

	return stmt;
}

const GBStatement *
gb_stmt_new_resume_goto (GBParseData  *module, const char *label)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);
	
	stmt = stmt_new (module);
	stmt->type = GBS_RESUME;
	stmt->parm.on_error.type  = GB_RESUME_GOTO;
	stmt->parm.on_error.label = label;

	return stmt;
}

const GBStatement *
gb_stmt_new_return (GBParseData  *module)
{
	GBStatement *stmt;

	g_return_val_if_fail (module != NULL, NULL);
	
	stmt = stmt_new (module);
	stmt->type = GBS_RESUME;

	return stmt;
}


/***************** End of file handling related functions ******************/

GSList *
gb_stmt_accumulate (GSList *list, const GBStatement *next)
{
	if (next != NULL)
		return g_slist_prepend (list, (gpointer)next);

	return list;
}

void
gb_stmts_destroy (GSList *l)
{
	while (l) {
		gb_stmt_destroy (l->data);
		l = g_slist_remove (l, l->data);
	}
}

void
gb_stmt_destroy (GBStatement *stmt)
{
	switch (stmt->type)
	{
	case GBS_ASSIGN:
		gb_expr_destroy (stmt->parm.assignment.dest);
		gb_expr_destroy (stmt->parm.assignment.val);
		break;

	case GBS_CALL:
		gb_expr_destroy (stmt->parm.func.call);
		break;

	case GBS_WITH:
		gb_expr_destroy  (stmt->parm.with.base_obj);
		gb_stmts_destroy (stmt->parm.with.body);
		break;

	case GBS_FOR:
		gb_expr_destroy  (stmt->parm.forloop.from);
		gb_expr_destroy  (stmt->parm.forloop.to);
		gb_expr_destroy  (stmt->parm.forloop.step);
		gb_stmts_destroy (stmt->parm.forloop.body);
		break;

	case GBS_FOREACH:
		gb_expr_destroy  (stmt->parm.foreach.collection);
		gb_stmts_destroy (stmt->parm.foreach.body);
		break;

	case GBS_WHILE:
	case GBS_DO:
		gb_expr_destroy  (stmt->parm.do_while.expr);
		gb_stmts_destroy (stmt->parm.do_while.body);
		break;

	case GBS_IF:
		gb_expr_destroy  (stmt->parm.if_stmt.condition);
		gb_stmts_destroy (stmt->parm.if_stmt.body);
		gb_stmts_destroy (stmt->parm.if_stmt.else_body);
		break;

	case GBS_SELECT:
		gb_expr_destroy  (stmt->parm.select.test_expr);
		gb_cases_destroy (stmt->parm.select.cases);
		break;

	default:
		g_warning ("Can't destroy stmt type %d", stmt->type);
		return;
	};
}

const char *
gb_stmt_type (const GBStatement *stmt)
{
	switch (stmt->type) {
	case GBS_ASSIGN:
		return "Assign";
	case GBS_CALL:
		return "Call";
	case GBS_WITH:
		return "With";
	case GBS_FOR:
		return "For";
	case GBS_FOREACH:
		return "ForEach";
	case GBS_WHILE:
		return "While";
	case GBS_DO:
		return "Do";
	case GBS_IF:
		return "If";
	case GBS_SELECT:
		return "Select";
	case GBS_RANDOMIZE:
		return "Randomize";
	case GBS_LOAD:
		return "Load";
	case GBS_UNLOAD:
		return "Unload";
	case GBS_OPEN:
		return "Open";
	case GBS_INPUT:
		return "Input";
	case GBS_LINE_INPUT:
		return "LineInput";
	case GBS_CLOSE:
		return "Close";
	case GBS_ON_ERROR:
		return "OnError";
	case GBS_GOTO:
		return "Goto";
	case GBS_LABEL:
		return "Label";
	case GBS_GET:
		return "Get";
	case GBS_PUT:
		return "Put";
	case GBS_SEEK:
		return "Seek";
	case GBS_PRINT:
		return "Print";
	case GBS_SET:
		return "Set";
	case GBS_REDIM:
		return "ReDim";
	case GBS_EXIT:
		return "Exit";
	case GBS_ERASE:
		return "Erase";
	case GBS_CHDIR:
		return "ChDir";
	case GBS_FILECOPY:
		return "FileCopy";
	case GBS_KILL:
		return "Kill";
	case GBS_MKDIR:
		return "MkDir";
	case GBS_RESET:
		return "Reset";
	case GBS_RMDIR:
		return "RmDir";
	case GBS_SETATTR:
		return "SetAttr";
 	case GBS_GOSUB:
 		return "Gosub";
 	case GBS_RESUME:
 		return "Resume";
 	case GBS_RETURN:
 		return "Return";
	case GBS_ON_EXPR:
		return "On <expr>";
	}

	return "Unknown";
}

void
gb_stmt_print (FILE *sink, const GBStatement *stmt)
{
	switch (stmt->type)
	{
	case GBS_ASSIGN:
		gb_objref_print (sink, stmt->parm.assignment.dest);
		fprintf (sink, " = ");
		gb_expr_print (sink, stmt->parm.assignment.val);
		fputc (';', sink);
		break;

	case GBS_CALL:
		fprintf (sink, "Call ");
		gb_objref_print (sink, stmt->parm.func.call);
		fputc (';', sink);
		break;

	case GBS_WITH:
		fprintf (sink, "{\nvoid * with_var_%d = ", stmt->parm.with.depth);
		gb_objref_print (sink, stmt->parm.with.base_obj);
		fputs ("; /* TODO : get type correct */", sink);
		gb_stmts_print (sink, stmt->parm.with.body, FALSE);
		fputs ("\n}", sink);
		break;

	case GBS_FOR:
		/* TODO : test how frequently VB evals the bounds.
		 *        test when it decides on the step
		 */
		fprintf (sink, "for %s = ", stmt->parm.forloop.var);
		gb_expr_print (sink, stmt->parm.forloop.from);
		fprintf (sink, " to ");
		gb_expr_print (sink, stmt->parm.forloop.to);
		if (stmt->parm.forloop.step) {
			fprintf (sink, " Step ");
			gb_expr_print (sink, stmt->parm.forloop.step);
		}

		gb_stmts_print (sink, stmt->parm.forloop.body, TRUE);
		break;

	case GBS_FOREACH:
		fprintf (sink, "foreach %s in ", stmt->parm.foreach.var);
		gb_objref_print (sink, stmt->parm.foreach.collection);
		gb_stmts_print (sink, stmt->parm.foreach.body, TRUE);
		break;

	case GBS_WHILE:
		fputs ("while (", sink);
		if (stmt->parm.do_while.expr)
			gb_expr_print (sink, stmt->parm.do_while.expr);
		else
			fputs ("1", sink);
		fputc (')', sink);
		if (stmt->parm.do_while.body)
			gb_stmts_print (sink, stmt->parm.do_while.body, TRUE);
		else
			fputs ("\n;\n", sink);
		break;

	case GBS_DO:
		fputs ("do", sink);
		gb_stmts_print (sink, stmt->parm.do_while.body, TRUE);
		fputs ("\nwhile (!(", sink);
		gb_expr_print (sink, stmt->parm.do_while.expr);
		fputc (')', sink);
		break;

	case GBS_IF:
		if (stmt->parm.if_stmt.condition != NULL) {
			fputs ("if (", sink);
			gb_expr_print (sink, stmt->parm.if_stmt.condition);
			fputc (')', sink);
		}
		gb_stmts_print (sink, stmt->parm.if_stmt.body, TRUE);
		if (stmt->parm.if_stmt.else_body != NULL) {
			fputs ("else ", sink);
			gb_stmts_print (sink, stmt->parm.if_stmt.else_body, TRUE);
		}
		break;

	case GBS_REDIM:
		if (stmt->parm.redim.preserve)
			fputs ("Preserve ", sink);

		fputs ("Some indices\n", sink);
		break;

	case GBS_ERASE:
		fputs ("Some indices\n", sink);
		break;

	default:
		g_warning ("Unknown stmt type %d", stmt->type);
		return;
	};

	fputc ('\n', sink);
}

void
gb_stmts_print (FILE *sink, GSList const *stmts, gboolean nest)
{
	fputc ('\n', sink);
	if (nest)
		fprintf (sink, "{\n");

	for (; stmts != NULL; stmts = stmts->next)
		gb_stmt_print (sink, stmts->data);

	if (nest)
		fprintf (sink, "}");
}
