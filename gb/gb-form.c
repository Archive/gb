/*
 * GNOME Basic Forms
 *
 * Author:
 *    Michael Meeks (michael@helixcode.com)
 *    Ravi Pratap   (ravi@che.iitm.ac.in)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <gb/gb-expr.h>
#include <gb/gb-form.h>
#include <gb/gb-value.h>
#include <gb/gb-main.h>
#include <gb/gb-eval.h>

GBFormItem *
gb_form_item_new (const char *type, const char *name)
{
	GBFormItem *fi;

	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);

	fi             = g_new0 (GBFormItem, 1);
	fi->type       = g_strdup (type);
	fi->name       = g_strdup (name);
	fi->children   = NULL;
	fi->properties = NULL;

	return fi;
}

GBFormProperty *
gb_form_property_new (GBParseData  *module,
		      const char   *prop_name,
		      const GBExpr *expr)
{
	GBFormProperty *prop;
	GBValue        *val;

	g_return_val_if_fail (expr != NULL, NULL);
	g_return_val_if_fail (prop_name != NULL, NULL);

	val = gb_eval_context_eval (module->ec, expr);
       
	if (!val) {
		g_warning ("Can't evaluate '%s's value ", prop_name);
		return NULL;
	}

	prop = g_new (GBFormProperty, 1);

	prop->name     = g_strdup (prop_name);
	prop->value    = val;
	prop->subprop  = FALSE;
	prop->children = NULL;

	return prop;
}


GBFormProperty *
gb_form_property_subprop_new (const char *name,
			      GSList     *children)
{
	GBFormProperty *prop;

	g_return_val_if_fail (name != NULL, NULL);

	prop = g_new (GBFormProperty, 1);
	
	prop->name     = g_strdup (name);
	prop->subprop  = TRUE;
	prop->value    = NULL;
	prop->children = g_slist_prepend (NULL, children);
	
	return prop;

}


void
gb_form_property_destroy (GBFormProperty *prop)
{
	if (prop) {
		if (prop->name)
			g_free (prop->name);

		gb_value_destroy (prop->value);
		
		g_free (prop);
	}
}

void
gb_form_item_destroy (GBFormItem *fi)
{
	if (fi) {
		GSList *l;

		for (l = fi->children; l; l = l->next)
			gb_form_item_destroy (l->data);

		if (fi->children)
			g_slist_free (fi->children);
		fi->children = NULL;

		for (l = fi->properties; l; l = l->next)
			gb_form_property_destroy (l->data);
		
		if (fi->properties)
			g_slist_free (fi->properties);
		fi->properties = NULL;

		if (fi->type)
			g_free (fi->type);
		fi->type = NULL;

		if (fi->name)
			g_free (fi->name);
		fi->name = NULL;
	}
}

void
gb_form_item_add_children (GBFormItem *fi, GSList *children)
{
	g_return_if_fail (fi != NULL);

	if (!children)
		return;

	fi->children = g_slist_concat (fi->children, children);
}

void
gb_form_item_add_props (GBFormItem *fi, GSList *props)
{
	GSList *l;

	g_return_if_fail (fi != NULL);

	for (l = props; l; l = l->next) {
		GBFormProperty *prop = l->data;

		if (!prop)
			continue;

		fi->properties = g_slist_append (fi->properties, prop);
	}

	if (props)
		g_slist_free (props);
}
