/*
 * The Gnome Basic Frx processor.
 *
 * Authors:
 *	Frank Chiulli  <fc-linux@home.com>
 *
 * Copyright 2000, Helix Code, Inc.
 */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "gb-statement.h"
#include "gb-expr.h"
#include "grammar.h"
#include "gb-frx.h"

#undef GB_FRX_DEBUG

/**
 * gb_frx_getu8
 *   @frx	GBFrx structure
 *    offset	The offset in the frx file where the unsigned integer is.
 *
 * Returns a 1-byte unsigned integer at offset in the frx file.
 *
 **/
guint8
gb_frx_getu8 (GBFrx *frx, const guint offset)
{
	guint8		 ans;

	GBFrxClass	*klass;

		
	g_return_val_if_fail (frx != NULL, 0);

	klass = GB_FRX_CLASS (GTK_OBJECT (frx)->klass);
	g_return_val_if_fail (klass != NULL, 0);

	ans = klass->s_getu8 (frx, offset);
	
	return ans;
}


/**
 * gb_frx_getu16
 *   @frx	GBFrx structure
 *    offset	The offset in the frx file where the unsigned integer is.
 *
 * Returns a 2-byte unsigned integer at offset in the frx file.
 *
 **/
guint16
gb_frx_getu16 (GBFrx *frx, const guint offset)
{
	guint16		 ans;

	GBFrxClass	*klass;
	
		
	g_return_val_if_fail (frx != NULL, 0);

	klass = GB_FRX_CLASS (GTK_OBJECT (frx)->klass);
	g_return_val_if_fail (klass != NULL, 0);

	ans = klass->s_getu16 (frx, offset);
	
	return ans;
}


/**
 * gb_frx_getcn
 *   @frx	GBFrx structure
 *    offset	The offset in the frx file where the characters start
 *    item_len  The number of characters
 *
 * Returns a null-terminated character string extracted from the frx
 * file at offset.
 *
 **/
char  *
gb_frx_getcn (GBFrx *frx, const guint offset, const guint32 item_len)
{
	char		*ans;

	GBFrxClass	*klass;

	
	g_return_val_if_fail (frx != NULL, 0);

	klass = GB_FRX_CLASS (GTK_OBJECT (frx)->klass);
	g_return_val_if_fail (klass != NULL, 0);

	ans = klass->s_getcn (frx, offset, item_len);

	return ans;
}



/**
 * gb_frx_class_init
 *   @klass	A GBFrxClass structure
 **/
static void
gb_frx_class_init (GBFrxClass *klass)
{
	klass->s_getu8   = NULL;
	klass->s_getu16  = NULL;
	klass->s_getcn   = NULL;
}


/**
 * gb_frx_init
 *   @frx	GBFrx structure
 **/
static void
gb_frx_init (GBFrx *frx)
{
	frx->filename = NULL;
	frx->offset   = 0;
}


/**
 * gb_frx_get_type
 **/
GtkType
gb_frx_get_type (void)
{
	static GtkType frx_type = 0;

	
	if (!frx_type) {
		static const GtkTypeInfo frx_info = {
			"GBFrx",
			sizeof (GBFrx),
			sizeof (GBFrxClass),
			(GtkClassInitFunc) gb_frx_class_init,
			(GtkObjectInitFunc) gb_frx_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		frx_type = gtk_type_unique (GTK_TYPE_OBJECT, &frx_info);
	}

	return frx_type;
}


/**
 * gb_frx_new
 **/
GBFrx *
gb_frx_new (void)
{
	g_warning ("This is an abstract base class; "
		   "see gb-mmap-frx.h");
	return NULL;
}
