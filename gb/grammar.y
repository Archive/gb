%{
/*
 * GNOME Basic Line Parser
 *
 * Author:
 *    Jody Goldberg (jgoldberg@home.com)
 *    Michael Meeks (michael@ximian.com)
 *    Ravi Pratap   (ravi@che.iitm.ac.in) 
 */
#include <stdlib.h>
#include <string.h>

#include <gb/gb.h>
#include <gb/gb-lex.h>
#include <gb/gb-expr.h>
#include <gb/gb-eval.h>
#include <gb/gb-form.h>
#include <gb/gb-class.h>
#include <gb/gb-statement.h>
#include <gb/gb-type.h>
#include <gb/gb-main.h>

#define YYDEBUG         1
#define YYPARSE_PARAM	buffer
#define YYLEX_PARAM	buffer
#define YYERROR_VERBOSE 1

extern int gb_lex (void *res, void *closure);
/*
 *   The fact that gb_error is not passed the closure
 * makes this unutterably hideous.
 */
static GBParseData *yacc_hack = NULL;
extern void gb_error (const char *msg);
%}
%pure_parser

%union {
	const char      *v_str;
	double	         v_float;
	int		 v_int;
	gboolean	 v_bool;
	GSList	        *list;

	struct {
		const char *name;
		gboolean    as_new;
	} v_dim_type;

	struct {
		gboolean    is_array;
		GSList     *indices;
	} v_array;
	GBIndex            *v_index;

	GBValue		   *value;
	const GBExpr	   *expr;
	const GBStatement  *stmt;
	GBObjRef           *objr;
	GBArgDesc          *v_arg;
	GBSelectCase       *v_case;
	GBVar              *v_var;
	
	GBFormItem         *v_form_item;
	GBFormProperty     *v_form_prop;
	GBClassProperty    *v_class_prop;

	GBAttribute        *v_attr;
	GBCaseExpr         *v_case_expr;
}

%token <v_str> NAME STRING FRX GB_GOTO_LABEL GB_OBJ_NAME_LPAR
%token <v_float> FLOAT SINGLE
%token <v_int> INTEGER LONG
%token GB_GE GB_LE GB_MOD GB_ARG_ASSIGN GB_IS GB_EQV GB_LIKE GB_IMP
%token GB_AND GB_OR GB_XOR GB_NOT

%token GB_LEXTYPE_ASP GB_LEXTYPE_FILE GB_LEXTYPE_EXPR GB_LEXTYPE_STATEMENTS

%token GB_ACTIVATE GB_ALIAS GB_AS GB_ATTRIBUTE GB_APPEND GB_BASE GB_BEGIN GB_BEEP GB_CALL GB_CASE
%token GB_CLASS GB_CLOSE GB_COMPARE GB_CONST GB_DATE GB_DECLARE
%token GB_DEFTYPE GB_DELETESETTING GB_DIM GB_DO GB_EACH GB_ELSE GB_END
%token GB_ELSE_IF GB_ERASE GB_ERROR GB_EXIT GB_EXPLICIT GB_FALSE GB_FILECOPY GB_FOR
%token GB_FUNCTION GB_GET GB_GLOBAL GB_GOSUB GB_GOTO GB_IF GB_INPUT GB_KILL GB_LSET GB_LET
%token GB_LIB GB_LEN GB_LINE GB_LOAD GB_LOCK GB_LOOP GB_MID GB_MKDIR GB_NAME GB_NEXT GB_ON
%token GB_OPEN GB_OPTION GB_OUTPUT GB_PRINT GB_PRIVATE GB_PROPERTY GB_PUBLIC GB_PUT
%token GB_RSET GB_RANDOM GB_RANDOMIZE GB_REDIM GB_REM GB_RESET GB_RESUME GB_RETURN
%token GB_RMDIR GB_RND GB_SAVESETTING GB_SCALE GB_SEEK GB_SELECT GB_SENDKEYS GB_SET GB_SETATTR
%token GB_STATIC GB_STOP GB_SUB GB_THEN GB_TIME GB_TRUE GB_TYPE GB_UNLOAD GB_UNLOCK GB_VERSION
%token GB_WEND GB_WHILE GB_WIDTH GB_WITH GB_WITH_EVENTS GB_WRITE GB_NEW GB_TO
%token GB_STEP GB_PARAM_ARRAY GB_BYREF GB_BYVAL GB_OPTIONAL GB_NOTHING GB_IN
%token GB_BINARY GB_TEXT GB_DATABASE GB_MODULE GB_UNTIL GB_PRESERVE 
%token GB_CHDIR GB_CHDRIVE GB_APPACTIVATE GB_EVENT GB_IMPLEMENTS GB_RAISEEVENT GB_FRIEND
%token GB_ACCESS GB_READ GB_SHARED

/* ASP specific tokens */
%token GB_ASP_START GB_ASP_START_AT GB_ASP_START_EXPR GB_ASP_END

/* Form specific tokens */
%token GB_BEGIN_PROPERTY GB_END_PROPERTY GB_OBJECT

/* To make the lexer nicer */
%token GB_KEEP_LOOKING

/* Magic GB_SPACE_DOT which flags a '.' preceded whitespace */
%token GB_SPACE_DOT

%type <v_bool> opt_new opt_events opt_static pub_private opt_parmarray opt_pub_private
%type <v_bool> opt_optional opt_by_val_or_ref opt_preserve end_routine

%type <v_int> open_mode access_mode lock_mode nesting
%type <v_int> compare_types 

%type <v_str> obj_name type as_type var_name

%type <v_case> case_stmt
%type <v_case_expr> case_expr

%type <v_dim_type> dim_type
%type <v_arg>   arg
%type <v_array> opt_subscripts
%type <v_index> subscript
%type <v_var>   type_var_decl
 
%type <value> value

%type <expr> object_list expr parm const_expr opt_step opt_while_until
%type <expr> opt_default sub_call opt_expr opt_open_len 

%type <list> parms opt_parms statements
%type <list> object_refs handles case_stmts expr_csv case_exprs
%type <list> else_body else_end opt_arglist arglist subscripts
%type <list> type_var_decls label_list

%type <stmt> statement if_body on_error resume
%type <objr> object_ref sub_ref prop_ref method_array_ref 

/* Form bits */
%type <list>        form_items form_item_props class_props class_block
%type <v_form_item> form_item 
%type <v_form_prop> form_item_prop form_item_subprop 
%type <v_str>       form_item_name

/* Class bits */
%type <v_class_prop> class_prop

%type <v_attr> attribute_item


%left '&'
%left GB_XOR
%left GB_OR
%left GB_AND
%left GB_NOT
%left '<' '>' '=' GB_NE GB_GE GB_LE GB_LIKE GB_IS
%left '-' '+' 
%left '*' '/'
%left UNARY_NEG UNARY_POS
%left '^'
%left '\\'
%left GB_MOD
%left GB_EQV
%left GB_IMP
%%

Start : GlobalStatements
      | GB_LEXTYPE_FILE GlobalStatements
      | GB_LEXTYPE_ASP asp
      | GB_LEXTYPE_EXPR expr             { gb_parse_data_set_expr  (buffer, $2); }
      | GB_LEXTYPE_STATEMENTS statements { gb_parse_data_set_stmts (buffer, $2); }
      ;

GlobalStatements : GlobalStatements GlobalStatement
		 | GlobalStatements eol
		 |
		 ;

eostmt : '\n'  /* End of Statement */
       | ':'
       ;

eol : '\n'     /* End of Line */
    ;

/* ASP code */

asp : GB_ASP_START_AT expr GB_ASP_END '\n' asp_stmts
    ;

asp_stmts : asp_stmts asp_stmt 
	  | asp_stmt
          ;

asp_stmt : STRING { }
	 | GB_ASP_START statements GB_ASP_END {}
	 | GB_ASP_START_EXPR expr GB_ASP_END {}
	 | '\n'

/* End of ASP code */

/* Form code */
form_version : GB_VERSION FLOAT eol
			{
				if ($2 != 5.0) {
					yyerror ("Unsupported version");
					YYERROR;
				}
			}
             ;

form_item_subprop : GB_BEGIN_PROPERTY NAME eol form_item_props GB_END_PROPERTY eol 
                    { $$ = gb_form_property_subprop_new ($2, $4); }
                  ;

form_item_prop : NAME '=' const_expr eol { $$ = gb_form_property_new (buffer, $1, $3); }
               | form_item_subprop       { $$ = $1; }
               ;

form_item_props : form_item_props form_item_prop { $$ = g_slist_append ($1, $2); }
	        | form_item_prop                 { $$ = g_slist_append (NULL, $1); }
	        ;

form_item_name  : form_item_name '.' NAME { $$ = g_strconcat ($1, ".", $3, NULL); }
	        | NAME                    { $$ = g_strdup ($1); }
	        ;

form_item  : GB_BEGIN form_item_name NAME eol form_item_props form_items GB_END eol
	   {
		$$ = gb_form_item_new     ($2, $3);
		gb_form_item_add_children ($$, $6);
		gb_form_item_add_props    ($$, $5);
	   }
	   ;

form_items : form_items form_item                { $$ = g_slist_append ($1, $2); } 
           | form_items eol                      { $$ = $1;   }
           | GB_OBJECT '=' STRING ';' STRING eol { $$ = NULL; }
           |                                     { $$ = NULL; }
           ;

/* End of Form code */

/* Class Code */

class_version : GB_VERSION FLOAT GB_CLASS eol
                         { 
				 if ($2 != 1.0) {
					 yyerror ("Unsupported class version");
					 YYERROR;
				 }
			 }
              ; 

class_prop : NAME '=' const_expr eol { $$ = gb_class_property_new (buffer, $1, $3); }
           ; 

class_props : class_props class_prop { $$ = g_slist_prepend ($1, $2); }
            | class_prop             { $$ = g_slist_prepend (NULL, $1); }

class_block : GB_BEGIN eol class_props GB_END eol  { $$ = $3;  } 
           ;

/* End of Class Code */

/* Module-level statements */

GlobalStatement : '#' PreProc					{ }
		| form_version form_item 
		  { gb_parse_data_end_form (buffer, $2);  }
                | class_version class_block
                  { gb_parse_data_end_class (buffer, $2); }
		| attribute_item                               
                  { gb_parse_data_add_attr (buffer, $1); }
                | gb_option_statement				{ }
		| opt_pub_private const_decl  		        { }
                | GB_GLOBAL const_decl                          { }
                | pub_private var_decls                         { }
                | GB_PUBLIC GB_EVENT NAME opt_arglist           { }
                | GB_IMPLEMENTS NAME                            { }
                | type_statement                                { }
		| declare_statement	       			{ }
                | dim_statement					{ gb_parse_data_parse_basic (buffer); }
                | routine_decl_block                            { }
                ;

attribute_item : GB_ATTRIBUTE NAME '=' const_expr { $$ = gb_parse_data_attribute_new (buffer, $2, $4); }
               ;        
      
routine_decl_block :  opt_pub_private opt_static declare_routine eol { gb_parse_data_begin_routine (buffer); }
		          statements
		      GB_END end_routine
		       { gb_routine_finish (buffer, $1, $8, $6); }
                   ;

declare_routine : GB_SUB NAME opt_arglist
		    { gb_routine_start (buffer, $2, $3, NULL, FALSE, GB_CLASS_NONE); }
		| GB_FUNCTION NAME opt_arglist as_type
		    { gb_routine_start (buffer, $2, $3, $4, TRUE, GB_CLASS_NONE); }
                | GB_PROPERTY GB_GET NAME opt_arglist as_type
                    { gb_routine_start (buffer, $3, $4, $5, TRUE, GB_CLASS_PROPERTY_GET); }
                | GB_PROPERTY GB_LET NAME opt_arglist
                    { gb_routine_start (buffer, $3, $4, NULL, TRUE, GB_CLASS_PROPERTY_LET); }
                ;


end_routine : GB_SUB		{ $$ = TRUE;  }
	    | GB_FUNCTION	{ $$ = FALSE; }
            | GB_PROPERTY       { $$ = FALSE;  } 	    
            ;

PreProc : GB_CONST NAME '=' const_expr
        | GB_IF const_expr GB_THEN
        | GB_ELSE_IF const_expr GB_THEN
        | GB_ELSE
        | GB_END GB_IF
        ;

gb_option_statement : GB_OPTION GB_EXPLICIT	         { gb_parse_data_set_option_explicit (buffer); }
                    | GB_OPTION GB_PRIVATE GB_MODULE     { gb_parse_data_set_option_private_module (buffer); }
	            | GB_OPTION GB_BASE INTEGER	         { gb_parse_data_set_option_base (buffer, $3); }
	            | GB_OPTION GB_COMPARE compare_types { gb_parse_data_set_option_compare (buffer, $3); }
  	            ;

compare_types : GB_BINARY				{ $$ = GB_COMPARE_BINARY;   }
 	      | GB_TEXT					{ $$ = GB_COMPARE_TEXT;     }
	      | GB_DATABASE				{ $$ = GB_COMPARE_DATABASE; }
  	      ;
    
declare_statement : opt_pub_private GB_DECLARE GB_SUB      NAME GB_LIB STRING opt_alias opt_arglist         { }
                  | opt_pub_private GB_DECLARE GB_FUNCTION NAME GB_LIB STRING opt_alias opt_arglist as_type { }
	          ;


type_statement : opt_pub_private GB_TYPE NAME eol type_var_decls GB_END GB_TYPE
		{ gb_parse_data_add_type (buffer, gb_type_new ($1, $3, $5)); }
               ;

type_var_decl : obj_name opt_subscripts dim_type
		{ $$ = gb_var_new ($1, $3.as_new, $2.is_array,
				   $2.indices, $3.name); }
              ;

type_var_decls : type_var_decls type_var_decl eol { $$ = g_slist_prepend ($1, $2); }
               | type_var_decls eol               { $$ = $1; }
	       |                                  { $$ = NULL; }
               ; 

/* End of Module-level statements */

statements : statements statement eostmt
	     { $$ = gb_stmt_accumulate ($1, $2); }
	   | statements eol		{ $$ = $1; }
	   |                            { $$ = NULL }
           ;

statement : sub_call
	    { $$ = gb_stmt_new_call (buffer, $1) }
	  | object_list '=' expr                       
            { $$ = gb_stmt_new_assignment (buffer, $1, $3); }
	  | GB_LET object_list '=' expr                       
            { $$ = gb_stmt_new_assignment (buffer, $2, $4); }
	  | dim_statement				{ $$ = NULL; }
          | static_statement                            { $$ = NULL; }
          | GB_REDIM { gb_parse_data_inside_dim (buffer); } 
		  opt_preserve var_name '(' subscripts ')'
            { 
		    gb_parse_data_dim_finish (buffer);
		    $$ = gb_stmt_new_redim (buffer, $4, $6, $3); 
	    }
	  | GB_ERASE var_name				{ $$ = gb_stmt_new_erase (buffer, $2); }
	  | GB_SET object_list '=' opt_new object_list	{ $$ = gb_stmt_new_set (buffer, $2, $4, $5); }
	  | GB_ON GB_ERROR on_error			{ $$ = $3; }
	  | GB_ON expr GB_GOTO label_list   { $$ = gb_stmt_new_on_expr (buffer, $2, $4); }
	  | GB_EXIT nesting				{ $$ = gb_stmt_new_exit (buffer, $2); }

	  | GB_DO opt_while_until eostmt
		statements
	    GB_LOOP opt_while_until
	    { $$ = gb_stmt_new_do_while (buffer, $2, $6, $4); }

	  | GB_SELECT GB_CASE expr eostmt
		case_stmts
	    GB_END GB_SELECT
	    { $$ = gb_stmt_new_select   (buffer, $3, $5); }

	  | GB_WHILE expr eostmt
		statements
	    GB_WEND
	    { $$ = gb_stmt_new_do_while (buffer, $2, NULL, $4); }

	  | GB_FOR NAME '=' expr GB_TO expr opt_step eostmt
		statements
	    GB_NEXT opt_name /* TODO : add check for NAME == opt_name */
	    { $$ = gb_stmt_new_forloop (buffer, $2, $4, $6, $7, $9); }

	  | GB_FOR GB_EACH NAME GB_IN object_list eostmt
		statements
	    GB_NEXT opt_name /* TODO : add check for NAME == opt_name */
	    { $$ = gb_stmt_new_foreach (buffer, $3, $5, $7); }

	  | GB_WITH object_list eostmt { gb_module_with_depth_inc (buffer, $2); }
		statements
	    GB_END GB_WITH
	    { $$ = gb_stmt_new_with (buffer, $2, $5); }

	  | GB_IF expr GB_THEN if_body
	    { $$ = gb_stmt_if_set_cond ((GBStatement *)$4, $2); }

	  | GB_BEEP					{ $$ = NULL; }
	  | GB_DATE '=' expr				{ $$ = NULL; }
	  | GB_RANDOMIZE opt_expr { $$ = gb_stmt_new_randomize (buffer, $2); }

          | GB_LOAD expr   { $$ = gb_stmt_new_load (buffer, $2); }  
          | GB_UNLOAD expr { $$ = gb_stmt_new_unload (buffer, $2); }

	  /* THIS LOOKS EXTREMELY BROKEN ! */
          | object_refs '.' GB_LINE '(' expr ',' expr ')' '-' opt_line_step '(' expr ',' expr ')' ',' expr ',' expr
            { }
          
          | object_refs '.' GB_LINE '(' expr ',' expr ')' '-' opt_line_step '(' expr ',' expr ')' ',' expr 
            { }

          | object_refs '.' GB_SCALE '(' expr ',' expr ')' '-' '(' expr ',' expr ')' 
            { }
      
          /* File handling related statements */
          | GB_OPEN expr GB_FOR open_mode access_mode lock_mode GB_AS expr opt_open_len          
            { $$ = gb_stmt_new_open (buffer, $2, $4, $5, $6, $8, $9); }
          | GB_OPEN expr GB_FOR open_mode access_mode lock_mode GB_AS '#' expr opt_open_len
            { $$ = gb_stmt_new_open (buffer, $2, $4, $5, $6, $9, $10); }
          | GB_INPUT '#' expr ',' expr_csv                     
            { $$ = gb_stmt_new_input (buffer, $3, $5); }
          | GB_LINE GB_INPUT '#' expr ',' expr                 
            { $$ = gb_stmt_new_line_input (buffer, $4, $6); }
          | GB_CLOSE expr_csv                                  
            { $$ = gb_stmt_new_close (buffer, $2); }
          | GB_CLOSE handles                        
            { $$ = gb_stmt_new_close (buffer, $2); }
          | GB_GET '#' expr ',' opt_expr ',' expr
            { $$ = gb_stmt_new_get (buffer, $3, $5, $7); }
          | GB_PUT '#' expr ',' opt_expr ',' expr
            { $$ = gb_stmt_new_put (buffer, $3, $5, $7); }
          | GB_SEEK '#' expr ',' expr 
            { $$ = gb_stmt_new_seek (buffer, $3, $5); }
          | GB_PRINT '#' expr ',' expr_csv 
            { $$ = gb_stmt_new_print (buffer, $3, $5); }
          /* End of File handling related statements */
	  
          | GB_GOTO NAME
            { $$ = gb_stmt_new_goto (buffer, $2); }
          | GB_GOTO_LABEL
            { $$ = gb_stmt_new_label (buffer, $1); }
          | GB_GOSUB NAME
            { $$ = gb_stmt_new_gosub (buffer, $2); }
          | GB_RETURN
            { $$ = gb_stmt_new_return (buffer); }
          | GB_RESUME resume
            { $$ = $2; }
          | GB_CHDIR expr
            { $$ = gb_stmt_new_chdir (buffer, $2); }   
          | GB_CHDRIVE expr
            { }
          | GB_FILECOPY expr ',' expr
            { $$ = gb_stmt_new_filecopy (buffer, $2, $4); }
          | GB_KILL expr
            { $$ = gb_stmt_new_kill (buffer, $2); } 
          | GB_MKDIR expr
            { $$ = gb_stmt_new_mkdir (buffer, $2); }
          | GB_RESET
            { $$ = gb_stmt_new_reset (buffer); }
          | GB_RMDIR expr
            { $$ = gb_stmt_new_rmdir (buffer, $2); }
          | GB_SETATTR expr ',' expr
            { $$ = gb_stmt_new_setattr (buffer, $2, $4); }
          | GB_APPACTIVATE expr ',' expr 
            { }
          | GB_DELETESETTING expr ',' expr ',' expr
            { }
          | GB_SAVESETTING expr ',' expr ',' expr ',' expr
            { }
          | GB_SENDKEYS expr ',' expr
            { }
          | GB_RAISEEVENT NAME expr_csv
            { }
          ;


handles : handles ',' '#' expr { $$ = g_slist_append ($1, (gpointer) $4); }
        | '#' expr             { $$ = g_slist_append (NULL, (gpointer) $2); }
        |                      { $$ = NULL; }
        ;

open_mode : GB_INPUT  { $$ = GB_OPEN_INPUT; }
          | GB_OUTPUT { $$ = GB_OPEN_OUTPUT; }
          | GB_APPEND { $$ = GB_OPEN_APPEND; }
          | GB_BINARY { $$ = GB_OPEN_BINARY; }
          | GB_RANDOM { $$ = GB_OPEN_RANDOM; }
          ;

access_mode : GB_ACCESS GB_READ          { $$ = GB_ACCESS_READ; }
			| GB_ACCESS GB_WRITE         { $$ = GB_ACCESS_WRITE; }
			| GB_ACCESS GB_READ GB_WRITE { $$ = GB_ACCESS_READ_WRITE; }
			|                            { $$ = GB_ACCESS_READ_WRITE; }
			;

lock_mode : GB_SHARED                { $$ = GB_LOCK_SHARED; }
          | GB_LOCK GB_READ          { $$ = GB_LOCK_READ; }
		  | GB_LOCK GB_WRITE         { $$ = GB_LOCK_WRITE; }
		  | GB_LOCK GB_READ GB_WRITE { $$ = GB_LOCK_READ_WRITE; }
		  |                          { $$ = GB_LOCK_READ_WRITE; }
		  ;

if_body : eostmt statements else_body
	  { $$ = gb_stmt_new_if (buffer, $2, $3); }
	| statement	
          { $$ = gb_stmt_new_if (buffer, gb_stmt_accumulate (NULL, $1), NULL); }
	;

else_body : GB_ELSE_IF expr GB_THEN if_body
	    { $$ = g_slist_prepend (NULL,
				    (gpointer)gb_stmt_if_set_cond ($4, $2)); }
	  | GB_ELSE else_end 
	    { $$ = g_slist_reverse($2); }
	  | GB_END GB_IF
	    { $$ = NULL; }
	  ;

else_end : eostmt statements GB_END GB_IF
           { $$ = $2; }
         | statement
           { $$ = gb_stmt_accumulate (NULL, $1); }
         ;

nesting : GB_DO       { $$ = GBS_EXIT_DO;       }
	| GB_FOR      { $$ = GBS_EXIT_FOR;      }
	| GB_FUNCTION { $$ = GBS_EXIT_FUNCTION; }
	| GB_PROPERTY { $$ = GBS_EXIT_PROPERTY; }
	| GB_SUB      { $$ = GBS_EXIT_SUB;      }
	; 

label_list : label_list ',' NAME { $$ = g_slist_append ($1, (gpointer) $3); }
		| NAME                   { $$ = g_slist_append (NULL, (gpointer) $1); }
		;

on_error : GB_RESUME GB_NEXT { $$ = gb_stmt_new_on_error_next (buffer);     }
	 | GB_GOTO INTEGER   { $$ = NULL; /* FIXME: urk - 0 is a special case here */ }
	 | GB_GOTO NAME      { $$ = gb_stmt_new_on_error_goto (buffer, $2); }
         ;

resume : GB_NEXT { $$ = gb_stmt_new_resume_next (buffer);     }
	 | INTEGER   { $$ = gb_stmt_new_resume_default (buffer); /* FIXME: 0 has special meaning, non-0 numerical labels are not allowed */ }
	 | NAME      { $$ = gb_stmt_new_resume_goto (buffer, $1); }
	 |           { $$ = gb_stmt_new_resume_default (buffer); }
         ;

case_expr : GB_IS '=' expr   { $$ = gb_select_case_new_comparison (GB_EXPR_EQ, $3); }
          | GB_IS '<' expr   { $$ = gb_select_case_new_comparison (GB_EXPR_LT, $3); }
          | GB_IS '>' expr   { $$ = gb_select_case_new_comparison (GB_EXPR_GT, $3); }
          | GB_IS GB_LE expr { $$ = gb_select_case_new_comparison (GB_EXPR_LE, $3); }
          | GB_IS GB_GE expr { $$ = gb_select_case_new_comparison (GB_EXPR_GE, $3); }
          | expr GB_TO expr  { $$ = gb_select_case_new_expr_to_expr ($1, $3); }
          | expr             { $$ = gb_select_case_new_expr ($1); }
          ;

case_exprs : case_exprs ',' case_expr { $$ = g_slist_append  ($1, (gpointer) $3); }
           | case_expr                { $$ = g_slist_prepend (NULL, $1); }
           ;

case_stmt : GB_CASE case_exprs eostmt statements  { $$ = gb_select_case_new ($2, $4); }
          | GB_CASE GB_ELSE eostmt statements     { $$ = gb_select_case_new_else ($4); }
          ;

case_stmts : case_stmts case_stmt { $$ = g_slist_append ($1, $2); }
	   |                      { $$ = NULL;                    }
	   ;


/* Declarations */

dim_statement : GB_DIM    { gb_parse_data_inside_dim (buffer); } 
                var_decls { gb_parse_data_dim_finish (buffer); }
              ;

static_statement : GB_STATIC { gb_parse_data_inside_dim (buffer); }
                   var_decls { gb_parse_data_dim_finish (buffer); }
                 ;

const_decl : GB_CONST NAME as_type '=' const_expr { gb_parse_data_add_const (buffer, $2, $5); }
	   ;

var_decl : opt_events var_name opt_subscripts dim_type
	   { gb_parse_data_add_var (buffer, gb_var_new ($2, $4.as_new, $3.is_array,
							$3.indices, $4.name)); }
         ;

var_name : NAME { $$ = $1; }
         ;

var_decls : var_decls ',' var_decl
          | var_decl
          ;

subscript : expr		{ $$ = gb_index_new (NULL, $1); }
	  | expr GB_TO expr	{ $$ = gb_index_new ($1, $3);   }
          ;

subscripts : subscripts ',' subscript { $$ = g_slist_append ($1, $3); }
	   | subscript		      { $$ = g_slist_append (NULL, $1); }
	   ;

/*
 * NOTE : This silliness is required because MS uses keywords as
 *        types and property names ...
 */
type : GB_DATE	{ $$ = g_strdup ("Date"); }
     | NAME
     {
	 extern char const * gb_is_type (char const * str, unsigned int len);
/*	 char const * typ;*/
/* Needs fixing, nice to have type checking here, but need 'As' object */
/*	 if ((typ = gb_is_type ($1,strlen($1))) == NULL)
	 {
	     yyerror ("Expecting a type\n");
	     YYERROR;
	     }*/
	 $$ = g_strdup ($1);
     }
     ;

dim_type : GB_AS opt_new type { $$.name = $3; $$.as_new = $2; }
         |	              { $$.name = g_strdup ("Variant"); $$.as_new = FALSE; }
	 ;

arglist : arglist ',' arg       { $$ = g_slist_append ($1, $3);   }
	| arg			{ $$ = g_slist_append (NULL, $1); }
        ;

arg : opt_optional opt_by_val_or_ref opt_parmarray NAME is_array as_type opt_default
       { $$ = gb_arg_desc_new ($4, $6, $7, $2, $1); /* FIXME: opt_parmarray, is_array */ }
    ;

as_type : GB_AS type { $$ = $2; }
        |            { $$ = g_strdup ("Variant"); }
        ;

is_array : '(' ')'
         |
         ;
 

/* Begin Object Reference stuff */

/* 
 * VB has a nasty habit of using keywords as property/function names, bah!
 * FIXME: we need to solve this problem more elegantly
 * essentially it is not possible to have a routine called these things,
 * only a dereferenced object method etc.
 */
obj_name : NAME	       { $$ = $1; }
         | GB_TYPE     { $$ = "Type"; }  
         | GB_NAME     { $$ = "Name"; }  
         | GB_SELECT   { $$ = "Select"; }
         | GB_LEN      { $$ = "Len"; }   
         | GB_SEEK     { $$ = "Seek"; } 
         | GB_PRINT    { $$ = "Print"; }
         | GB_NOTHING  { $$ = "Nothing"; }
         | GB_PROPERTY { $$ = "Property"; }
         | GB_WRITE    { $$ = "Write"; }
         ;

parm  : expr { $$ = $1 }
      ;

parms : parms ',' parm		{ $$ = g_slist_prepend ($1, (gpointer) $3); }
      |	parm 			{ $$ = g_slist_prepend (NULL, (gpointer) $1); }
      ;

/*
 * NOTE : An array reference is handled like a method invocation.
 * Only, the method name in this case is NULL
 */

prop_ref : obj_name { $$ = gb_obj_ref_new ($1, FALSE, NULL); }
         ;

/* GB_OBJ_NAME_LPAR is equivalent to obj_name '(' */

method_array_ref : GB_OBJ_NAME_LPAR opt_parms ')' { $$ = gb_obj_ref_new ($1, TRUE, $2); }
                 ;

object_ref  : prop_ref             { $$ = $1; }
            | method_array_ref     { $$ = $1; }
            ;

object_refs : object_refs '.' object_ref { $$ = g_slist_prepend ($1, $3);   }
            | object_refs '!' obj_name   
              { 
		      $$ = g_slist_prepend ($1, gb_obj_ref_new (NULL, TRUE, 
								g_slist_prepend (NULL, (gpointer) gb_expr_new_string ($3)))); 
	      }
            | object_ref                 { $$ = g_slist_prepend (NULL, $1); }
	    | GB_SPACE_DOT object_ref    { $$ = g_slist_prepend (NULL, $2); }
            ;

sub_ref  : obj_name parms { $$ = gb_obj_ref_new ($1, TRUE, $2); }
         ;

sub_call : sub_ref				{ $$ = gb_expr_new_obj_list (g_slist_prepend (NULL, $1)); }
         | GB_CALL object_refs  		{ $$ = gb_expr_new_obj_list_call ($2); }
         | GB_CALL sub_ref                      { $$ = gb_expr_new_obj_list_call (g_slist_prepend (NULL, $2)); }
	 | GB_SPACE_DOT sub_ref			{ $$ = gb_expr_new_obj_list (g_slist_prepend (NULL, $2)); }
	 | object_refs '.' sub_ref              { $$ = gb_expr_new_obj_list (g_slist_prepend ($1,   $3)); }
         | object_refs                          { $$ = gb_expr_new_obj_list ($1); }        
         ;

object_list : object_refs { $$ = gb_expr_new_obj_list ($1); }
            ;


/* End of Object Reference stuff */


/* Expressions, values etc. */

expr_csv : expr_csv ',' expr { $$ = g_slist_append ($1, (gpointer) $3); }
         | expr              { $$ = g_slist_append (NULL, (gpointer) $1); }
         ;

expr : value            { $$ = gb_expr_new_value ($1); } 
     | object_list	{ $$ = $1; }
     | '+' expr		{ $$ = gb_expr_new_unary (GB_EXPR_POSITIVE, $2);}
     | '-' expr		{ $$ = gb_expr_new_unary (GB_EXPR_NEGATIVE, $2);}
     | GB_NOT expr	{ $$ = gb_expr_new_unary (GB_EXPR_NOT, $2);}
     | '(' expr ')'	{ $$ = gb_expr_new_unary (GB_EXPR_PAREN, $2); }
     | expr '&' expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_CONCAT, $3); }
     | expr GB_AND expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_AND, $3); }
     | expr GB_OR expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_OR, $3); }
     | expr GB_XOR expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_XOR, $3); }
     | expr '>' expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_GT, $3); }
     | expr GB_GE expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_GE, $3); }
     | expr '=' expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_EQ, $3); }
     | expr GB_IS expr  { $$ = gb_expr_new_binary ($1, GB_EXPR_EQ, $3); }
     | expr GB_NE expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_NE, $3); }
     | expr GB_LE expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_LE, $3); }
     | expr '<' expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_LT, $3); }
     | expr '-' expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_SUB, $3); }
     | expr '+' expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_ADD, $3); }
     | expr '*' expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_MULT, $3); }
     | expr '/' expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_DIV, $3); }
     | expr '\\' expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_INT_DIV, $3); }
     | expr '^' expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_POW, $3); }
     | expr GB_EQV expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_EQV, $3); }
     | expr GB_IMP expr	{ $$ = gb_expr_new_binary ($1, GB_EXPR_IMP, $3); }
     ;

value : GB_FALSE		{ $$ = gb_value_new_boolean (FALSE); }
      | GB_TRUE			{ $$ = gb_value_new_boolean (TRUE); }
      | INTEGER			{ $$ = gb_value_new_int ($1); }
      | LONG			{ $$ = gb_value_new_long ($1); }
      | SINGLE			{ $$ = gb_value_new_single ($1); }
      | FLOAT			{ $$ = gb_value_new_double ($1); }
      | STRING			{ $$ = gb_value_new_string_chars ($1); }
      | FRX			{ $$ = gb_value_new_frx ($1); }
      ;

const_expr : value              { $$ = gb_expr_new_value ($1); }
	   | '-' const_expr	%prec UNARY_NEG { $$ = gb_expr_new_unary (GB_EXPR_NEGATIVE, $2);}
	   | '+' const_expr	%prec UNARY_POS { $$ = gb_expr_new_unary (GB_EXPR_POSITIVE, $2);}
	   | GB_NOT const_expr	{ $$ = gb_expr_new_unary (GB_EXPR_NOT, $2);}
	   | '(' const_expr ')'	{ $$ = gb_expr_new_unary (GB_EXPR_PAREN, $2); }
	   | const_expr '&' const_expr
		{ $$ = gb_expr_new_binary ($1, GB_EXPR_CONCAT, $3); }
	   | const_expr GB_AND const_expr
		{ $$ = gb_expr_new_binary ($1, GB_EXPR_AND, $3); }
	   | const_expr GB_OR const_expr
		{ $$ = gb_expr_new_binary ($1, GB_EXPR_OR, $3); }
	   | const_expr GB_XOR const_expr
		{ $$ = gb_expr_new_binary ($1, GB_EXPR_XOR, $3); }
	   | const_expr '>' const_expr
		{ $$ = gb_expr_new_binary ($1, GB_EXPR_GT, $3); }
	   | const_expr GB_GE const_expr
		{ $$ = gb_expr_new_binary ($1, GB_EXPR_GE, $3); }
	   | const_expr '=' const_expr
		{ $$ = gb_expr_new_binary ($1, GB_EXPR_EQ, $3); }
	   | const_expr GB_NE const_expr
		{ $$ = gb_expr_new_binary ($1, GB_EXPR_NE, $3); }
	   | const_expr GB_LE const_expr
		{ $$ = gb_expr_new_binary ($1, GB_EXPR_LE, $3); }
	   | const_expr '<' const_expr
		{ $$ = gb_expr_new_binary ($1, GB_EXPR_LT, $3); }
	   | const_expr '-' const_expr
		{ $$ = gb_expr_new_binary ($1, GB_EXPR_SUB, $3); }
	   | const_expr '+' const_expr
		{ $$ = gb_expr_new_binary ($1, GB_EXPR_ADD, $3); }
	   | const_expr '*' const_expr
		{ $$ = gb_expr_new_binary ($1, GB_EXPR_MULT, $3); }
	   | const_expr '/' const_expr
		{ $$ = gb_expr_new_binary ($1, GB_EXPR_DIV, $3); }
	   | const_expr '\\' const_expr
		{ $$ = gb_expr_new_binary ($1, GB_EXPR_INT_DIV, $3); }
	   | const_expr '^' const_expr
		{ $$ = gb_expr_new_binary ($1, GB_EXPR_POW, $3); }
           ;


/* Optional stuff -  opt_* tokens */

opt_line_step : GB_STEP 
              | 
              ;

opt_open_len : GB_LEN '=' expr { $$ = $3; } 
             |                 { $$ = NULL; }
             ;

opt_alias : GB_ALIAS STRING { }
	  |                 { }
          ;

opt_while_until : GB_WHILE expr		{ $$ = $2; }
		| GB_UNTIL expr		{ $$ = gb_expr_new_unary (GB_EXPR_NOT, $2); }
	        |			{ $$ = NULL; }
		;

opt_name : NAME		{ }
         |		{ }
	 ;

opt_preserve : GB_PRESERVE				{ $$ = TRUE; }
	     |						{ $$ = FALSE; }
	     ;

opt_step : GB_STEP expr					{ $$ = $2; }
         |						{ $$ = NULL; }
	 ;

opt_subscripts : '(' subscripts ')'           { $$.is_array  = TRUE; $$.indices = $2; }
	       /* A dynamic array to be set later */
	       | '(' ')'		      { $$.is_array = TRUE; $$.indices = NULL;  }
	       /* not an array */
	       |			      { $$.is_array = FALSE; $$.indices = NULL; }
               ;

opt_arglist : '(' arglist ')'   { $$ = $2; }
	    | '(' ')'		{ $$ = NULL; }
            |			{ $$ = NULL; }
            ;

opt_default : '=' const_expr	{ $$ = $2;   }
	    |			{ $$ = NULL; }
            ;

opt_by_val_or_ref : GB_BYREF	{ $$ = FALSE; }
                  | GB_BYVAL	{ $$ = TRUE;  }
                  |		{ $$ = TRUE;  }
                  ;

opt_optional : GB_OPTIONAL	{ $$ = TRUE; }
             |			{ $$ = FALSE; }
             ;

opt_parmarray : GB_PARAM_ARRAY	{ $$ = TRUE; }
              |			{ $$ = FALSE; }
              ;

opt_pub_private : pub_private   { $$ = $1; }
                |		{ $$ = FALSE; }
                ;

pub_private : GB_PRIVATE { $$ = TRUE; }
            | GB_PUBLIC  { $$ = FALSE; }
            | GB_FRIEND  { $$ = TRUE; }
            ;

opt_static : GB_STATIC		{ $$ = TRUE; }
           |			{ $$ = FALSE; }
           ;

opt_events : GB_WITH_EVENTS	{ $$ = TRUE; }
           |			{ $$ = FALSE; }
           ;

opt_new : GB_NEW		{ $$ = TRUE; }
        |			{ $$ = FALSE; }
	;

opt_parms : parms    { $$ = $1; }
          |          { $$ = NULL; }          
          ;


opt_expr : expr { $$ = $1;   }
	 |      { $$ = NULL; }
	 ;

/* End of optional stuff */


%%

/**
 * gb_parse_gb:
 * @module: The un-parsed module.
 * 
 * parses the Module's stream data into the module.
 * 
 * Return value: whether it failed, TRUE on error.
 **/
gboolean
gb_parse_gb (GBParseData *module)
{
	gboolean ans;

	yacc_hack = module;

	ans = gb_parse (module);

	yacc_hack = NULL;

	return ans;
}

void
gb_error (const char *msg)
{
	g_return_if_fail (yacc_hack != NULL);
	g_return_if_fail (yacc_hack->ec != NULL);

	gb_eval_context_set_line (yacc_hack->ec,
				  gb_lexer_stream_line (yacc_hack->ls));
	gb_eval_exception_fire   (yacc_hack->ec, msg?msg:"no text");
}
