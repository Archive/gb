/*
 * GNOME Basic Forms
 *
 * Author:
 *    Michael Meeks (michael@helixcode.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#ifndef GB_FORM_H
#define GB_FORM_H

#include <gb/gb.h>
#include <gb/gb-value.h>

struct _GBFormItem {
	char       *type;
	char       *name;

	GSList     *properties;
	GSList     *children;
};

struct _GBFormProperty {
	char       *name;
	GBValue    *value;
	gboolean    subprop;  /* Does this have sub-properties ? */
	GSList     *children; /* Only for subprops */
};

GBFormItem     *gb_form_item_new          (const char *type, const char *name);
void            gb_form_item_destroy      (GBFormItem *fi);

void            gb_form_item_add_children (GBFormItem *fi, GSList *children);
void            gb_form_item_add_props    (GBFormItem *fi, GSList *props);

GBFormProperty *gb_form_property_new      (GBParseData  *module,
					   const char   *prop_name,
					   const GBExpr *expr);

GBFormProperty *gb_form_property_subprop_new (const char *name,
					      GSList     *children);  

void            gb_form_property_destroy  (GBFormProperty *prop);



#endif /* GB_FORM_H */
