/*
 * GNOME Basic Memory Mapped Lexer Stream
 *
 * Author:
 *    Michael Meeks <mmeeks@gnu.org>
 *
 * Copyright 2000, Helix Code, Inc.
 */
#ifndef GB_MMAP_LEX_H
#define GB_MMAP_LEX_H

#include <gb/gb.h>
#include <gb/gb-lex.h>

#define GB_TYPE_MMAP_STREAM            (gb_mmap_stream_get_type ())
#define GB_MMAP_STREAM(obj)            (GTK_CHECK_CAST ((obj), GB_TYPE_MMAP_STREAM, GBMMapStream))
#define GB_MMAP_STREAM_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GB_TYPE_MMAP_STREAM, GBMMapStreamClass))
#define GB_IS_MMAP_STREAM(obj)	       (GTK_CHECK_TYPE ((obj), GB_TYPE_MMAP_STREAM))
#define GB_IS_MMAP_STREAM_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GB_TYPE_MMAP_STREAM))

struct _GBMMapStream {
	GBLexerStream  stream;

	/* Information on what is being parsed */
	unsigned  length;
	char	 *start, *cur, *end;
	int	  line;
};

struct _GBMMapStreamClass {
	GBLexerStreamClass klass;
};

GtkType        gb_mmap_stream_get_type (void);
GBLexerStream *gb_mmap_stream_new      (char *data, unsigned length);
GBLexerStream *gb_mmap_stream_copy     (GBLexerStream *ls);

#endif
