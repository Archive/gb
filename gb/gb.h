/*
 * GNOME Basic Internal header file.
 *
 * Author:
 *    Michael Meeks <michael@imaginator.com>
 *
 * Copyright 2000, Helix Code, Inc.
 */
#ifndef GB_GB_H
#define GB_GB_H

#include <stdio.h>
#include <glib.h>
#include <gtk/gtk.h>

/* Putting these here sucks */
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>

/* gb-value.h: structures */
typedef struct _GBValue      GBValue;
typedef struct _InternalDec  InternalDec;
typedef struct _InternalCur  InternalCur;

/* gb-value.h: types */
typedef guint8               GBByte;
typedef gboolean             GBBoolean;
typedef gint16               GBInt;
typedef gint32               GBLong;
typedef gfloat               GBSingle;
typedef gdouble              GBDouble;
typedef const InternalCur   *GBCurrency;
typedef const InternalDec   *GBDecimal;
#define GBDate               double
#define GBString             GString *
typedef const void          *InternalUserDefined;

/* gb-eval.h */
typedef struct _GBOnError           GBOnError;
typedef struct _GBEvalContext       GBEvalContext;
typedef struct _GBEvalContextClass  GBEvalContextClass;
typedef struct _GBResume  			GBResume;

/* gb-object.h */
typedef struct _GBObject            GBObject;
typedef struct _GBObjectClass       GBObjectClass;

/* gb-expr.h */
typedef struct _GBExpr     GBExpr;
typedef GSList             GBExprList;
typedef struct _GBObjRef   GBObjRef;
typedef struct _GBArgDesc  GBArgDesc;

/* gb-exception.h */
typedef struct _GBException         GBException;

/* gb-project.h */
typedef struct _GBProject           GBProject;
typedef struct _GBProjectPair       GBProjectPair;

/* gb-form.h */
typedef struct _GBFormItem          GBFormItem;
typedef struct _GBFormProperty      GBFormProperty;


/* gb-class.h */
typedef struct _GBClassProperty     GBClassProperty;

/* gb-main.h */
typedef struct _GBParseData GBParseData;
typedef struct _GBRoutine   GBRoutine;
typedef struct _GBVar       GBVar;
typedef struct _GBIndex     GBIndex;
typedef struct _GBOptions   GBOptions;
typedef struct _GBAttribute GBAttribute;

/* gb-statement.h */
typedef struct _GBStatement  GBStatement;
typedef struct _GBSelectCase GBSelectCase;
typedef struct _GBCaseExpr   GBCaseExpr;

/* gb-type.h */
typedef struct _GBType       GBType;
typedef struct _GBTypeElem   GBTypeElem;
typedef struct _GBConst      GBConst;

/* gb-lex.h */
typedef struct _GBLexerStream      GBLexerStream;
typedef struct _GBLexerStreamClass GBLexerStreamClass;

/* gb-mmap-lex.h */
typedef struct _GBMMapStream       GBMMapStream;
typedef struct _GBMMapStreamClass  GBMMapStreamClass;

/* gb-frx.h */
typedef struct _GBFrx      GBFrx;
typedef struct _GBFrxClass GBFrxClass;

/* gb-mmap-frx.h */
typedef struct _GBMMapFrx       GBMMapFrx;
typedef struct _GBMMapFrxClass  GBMMapFrxClass;

#endif
