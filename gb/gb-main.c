/*
 * Gnome Basic parse utilities.
 *
 * Author:
 *     Jody Goldberg(jgoldberg@home.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <libgb.h>
#include <gb/gb-main.h>
#include <gb/gb-eval.h>
#include <gb/gb-form.h>
#include <gb/gb-statement.h>
#include <gb/gb-mmap-lex.h>

GBIndex *
gb_index_new (const GBExpr *min, const GBExpr *max)
{
	GBIndex *i;
       
	g_return_val_if_fail (max != NULL, NULL);

   	i = g_new (GBIndex, 1);
	
	if (min)
		i->min = min;
	else
		i->min = gb_expr_new_value (gb_value_new_int (0));

	i->max = max;

	return i;
}

GBAttribute *
gb_parse_data_attribute_new (GBParseData  *module,
			     const char   *name,
			     const GBExpr *expr)
{
	GBAttribute  *attr;
	GBValue      *val;

	g_return_val_if_fail (name != NULL, NULL);
	g_return_val_if_fail (expr != NULL, NULL);

	val = gb_eval_context_eval (module->ec, expr);

	if (!val) {
		g_warning ("Can't evaluate '%s's value ", name);
		return NULL;
	}
	
	attr = g_new (GBAttribute, 1);
	attr->name  = name;
	attr->value = val;

	return attr;
	
}

static GHashTable *
gb_priv_new_hash (void)
{
	return g_hash_table_new (&g_str_hash, g_str_equal);
}

static gpointer
gb_priv_lookup (GHashTable *hash, const char * const name)
{
	return g_hash_table_lookup (hash, name);
}

/***************************************************************************/
/* Variable utilities */

GBVar *
gb_var_lookup (GBParseData * module, const char * const name)
{
	GHashTable * variables = (module->current_routine != NULL)
		? module->current_routine->variables
		: module->variables;

	return gb_priv_lookup (variables, name);
}

gboolean
gb_decl_var (GBParseData *parse_data,
	     const char  *name,
	     gboolean     obj,
	     gboolean     is_array,
	     GSList      *indices,
	     const char  *type)
{
	GBVar * new_var = NULL;
	GHashTable ** variables = (parse_data->current_routine != NULL)
		? &parse_data->current_routine->variables
		: &parse_data->variables;

	if (!*variables)
		*variables = gb_priv_new_hash ();

	else if ((new_var = gb_priv_lookup (*variables, name))) {
		g_warning ("%s : Already declared in this context",
			   new_var->name);
		return TRUE;
	}

	new_var = g_new (GBVar, 1);

	new_var->name     = name;
	new_var->object   = obj;
	new_var->type     = type;
	new_var->is_array = is_array;
	new_var->indices  = indices;

	g_hash_table_insert (*variables, (char *)new_var->name, new_var);

	return FALSE;
}

void
gb_module_with_depth_inc (GBParseData * module, GBExpr const * base_obj)
{
    g_return_if_fail (module != NULL);

    /* TODO : make symetric */
    /* Is decremented in gb_stmt_new_with */
    ++module->with_depth;
}

/***************************************************************************/
/* Module utilities */

gboolean
gb_routine_start (GBParseData        *module,
		  const char         *name,
		  GSList             *args,
		  const char         *as_type,
		  gboolean const      is_function,
		  GBClassMethodType prop_type)
{
	GBRoutine *routine;

	g_return_val_if_fail (module != NULL, TRUE);

	if (module->current_routine) {
		g_warning ("GB : Nested %s are not permitted",
			   is_function ? "functions" : "subroutines");
		return TRUE;
	}

	if (!module->routines)
		module->routines = gb_priv_new_hash ();

	else if ((routine = gb_priv_lookup (module->routines, name)) &&
		 prop_type == GB_CLASS_NONE) {
		
		/* 
		 * Disallow subs/funcs with the same names unless they're 
		 * class property procedures in which case, they usually 
		 * are get/let procs with the same name
		 */

		g_warning ("%s : Already declared", routine->name);

		return TRUE; /* ERROR : already declared */
	}

	routine = g_new0 (GBRoutine, 1);
	routine->name        = name;
	routine->is_function = is_function;
	routine->prop_type   = prop_type;
	routine->as_type     = as_type;
	routine->variables   = NULL;
	routine->args        = args;

	g_hash_table_insert (module->routines, (gpointer)routine->name,
			     routine);
	module->current_routine = routine;

	return FALSE;
}

static void
cb_routine_print (gpointer key, gpointer value, gpointer user_data)
{
	/* Its ok to print names from the hash.  We null terminate them
	 * when they go in */
	GBVar *var = value;
	printf ("\t%s %s;\n", var->type, var->name);
}

GBRoutine const *
gb_routine_finish (GBParseData * module, gboolean is_private,
		   gboolean is_function, GSList * body)
{
    GBRoutine * routine;

    g_return_val_if_fail (module != NULL, NULL);
    g_return_val_if_fail (module->current_routine != NULL, NULL);
    g_return_val_if_fail (module->current_routine->is_function != is_function,
			  NULL);
  
    routine                 = module->current_routine;
    routine->is_private     = is_private;
    routine->body           = g_slist_reverse (body);
    module->current_routine = NULL;

    gb_lexer_stream_state_set (module->ls, GB_PARSING_BASIC);

    /* HACK HACK HACK : quicky debug thingy */
   /*   gb_routine_print (stdout,  routine);   */

    return routine;
}

void
gb_routine_print (FILE *sink, const GBRoutine *routine)
{
	g_return_if_fail (routine != NULL);

	/* Lets pretend to do something */
	if (routine->is_private)
		fputs ("static ", sink);
	fprintf (sink, "void\n%s ()\n{\n", routine->name);

	/* Dump the variables */
	if (routine->variables != NULL)
		g_hash_table_foreach (routine->variables,
				      cb_routine_print, NULL);

	/* Dump the statements */
	gb_stmts_print (sink, routine->body, FALSE);

	printf ("\n}\n");
}

static gboolean
cb_routine_destroy (gpointer key, gpointer value, gpointer user_data)
{
	GBRoutine * routine = value;

	g_free ((char *) (routine->name));

	return FALSE;
}

static gboolean
cb_constant_destroy (gpointer key, gpointer value, gpointer user_data)
{
	gb_const_destroy (value);

	return FALSE;
}

static gboolean
cb_variable_destroy (gpointer key, gpointer value, gpointer user_data)
{
	GBVar * var = value;

	/* no need to free types.  Those managed elsewhere */
	g_free ((char *) (var->name));

	return FALSE;
}

static void
gb_var_dict_free (GHashTable * dict)
{
	g_hash_table_foreach_remove (dict, &cb_variable_destroy, NULL);
	g_hash_table_destroy (dict);
}

void
gb_parse_data_destroy (const GBParseData * const_module)
{
	/* cast away const */
	GBParseData *module = (GBParseData *) const_module;
	GSList      *l;

	g_return_if_fail (module != NULL);

	if (module->expr)
		gb_expr_destroy (module->expr);
	module->expr = NULL;


	if (module->stmts)
		gb_stmts_destroy (module->stmts);
	module->stmts = NULL;


	for (l = module->types; l; l = l->next)
		gb_type_destroy (l->data);
	g_slist_free (module->types);
	module->types = NULL;

	if (module->routines) {
		g_hash_table_foreach_remove (module->routines,
					     &cb_routine_destroy,
					     NULL);
		g_hash_table_destroy (module->routines);
		module->routines = NULL;
	}

	if (module->constants) {
		g_hash_table_foreach_remove (module->constants,
					     &cb_constant_destroy,
					     NULL);
		g_hash_table_destroy (module->constants);
		module->constants = NULL;
	}

	if (module->variables)
		gb_var_dict_free (module->variables);
	module->variables = NULL;

	module->current_routine = NULL;

	if (module->form)
		gb_form_item_destroy (module->form);
	module->form = NULL;

	for (l = module->class; l; l = l->next)
		gb_form_property_destroy (l->data);
	g_slist_free (module->class);
	module->class = NULL;
}

extern int gb_parse (void *);

const GBParseData *
gb_parse_stream (GBEvalContext *ec, GBLexerStream *ls)
{
	GBParseData *module;
	
	g_return_val_if_fail (GB_IS_EVAL_CONTEXT (ec), NULL);
	g_return_val_if_fail (GB_IS_LEXER_STREAM (ls), NULL);

	module = g_new0 (GBParseData, 1);

	module->ls         = ls;
	module->routines   = NULL;
	module->constants  = NULL;
	module->variables  = NULL;
	module->with_depth = 0;
	module->ec         = ec;
	module->form       = NULL;
	module->current_routine = NULL;

	gb_options_init (&module->options);

	if (gb_parse_gb (module)) {
		gb_parse_data_destroy (module);

		return NULL;
	}

	return module;
}

void
gb_parse_data_end_form (GBParseData *module, GBFormItem *form)
{
	module->form = form;
	gb_lexer_stream_state_set (module->ls, GB_PARSING_BASIC);
}

void
gb_parse_data_end_class (GBParseData *module, GSList *class)
{
	module->class = class;
	gb_lexer_stream_state_set (module->ls, GB_PARSING_BASIC);
}

void
gb_parse_data_start_form (GBParseData *module)
{
	gb_lexer_stream_state_set (module->ls, GB_PARSING_FORM);
}

void
gb_parse_data_set_expr (GBParseData  *module,
			const GBExpr *expr)
{
	module->expr = expr;
}

void
gb_parse_data_set_stmts (GBParseData *module,
			 GSList      *stmts)
{
	module->stmts = g_slist_reverse (stmts);
}

void
gb_parse_data_begin_routine (GBParseData *module)
{
	gb_lexer_stream_state_set (module->ls, GB_PARSING_INSIDE_SUB);
}

void
gb_parse_data_inside_dim (GBParseData *module)
{
	gb_lexer_stream_state_set (module->ls, GB_PARSING_INSIDE_DIM);
}

void
gb_parse_data_dim_finish (GBParseData *module)
{
	gb_lexer_stream_state_set (module->ls, GB_PARSING_INSIDE_SUB);
}

void
gb_parse_data_parse_basic (GBParseData *module)
{
	gb_lexer_stream_state_set (module->ls, GB_PARSING_BASIC);
}

void
gb_parse_data_add_type (GBParseData  *module,
			GBType       *type)
{
	g_return_if_fail (type != NULL);
	g_return_if_fail (module != NULL);

	module->types = g_slist_prepend (module->types, type);
}

gboolean
gb_parse_data_add_var (GBParseData *parse_data,
		       GBVar       *var)
{
	GBVar       *new_var   = NULL;
	GHashTable **variables = (parse_data->current_routine != NULL)
		? &parse_data->current_routine->variables
		: &parse_data->variables;

	if (!*variables)
		*variables = gb_priv_new_hash ();

	else if ((new_var = gb_priv_lookup (*variables, var->name))) {
		g_warning ("%s : Already declared in this context",
			   new_var->name);
		return TRUE;
	}

	g_hash_table_insert (*variables, (char *)var->name, var);

	return FALSE;
}

void
gb_parse_data_add_const (GBParseData  *parse_data,
			 const char   *name,
			 const GBExpr *expr)
{
	GBConst *cons;
	GBValue *val;

	g_return_if_fail (name != NULL);
	g_return_if_fail (expr != NULL);

	val = gb_eval_context_eval (parse_data->ec, expr);
	
	if (!val) {
	       g_warning ("Can't evaluate '%s's value ", name);
	       return;
	}
	
	cons = gb_const_new (parse_data->ec, name, val->gtk_type, expr);
	
	g_return_if_fail (cons != NULL);

	if (!parse_data->constants)
		parse_data->constants = gb_priv_new_hash ();

	g_hash_table_insert (parse_data->constants, cons->name, cons);
}

void
gb_init (void)
{
	gb_value_init ();
}

void
gb_shutdown (void)
{
	gb_value_shutdown ();
}

void
gb_parse_data_set_option_explicit (GBParseData *module)
{
	module->options.explicit = TRUE;
}

void
gb_parse_data_set_option_private_module (GBParseData *module)
{
	module->options.private_module = TRUE;
}

void
gb_parse_data_set_option_base (GBParseData *module,
			       int          base)
{
	module->options.base = base; 
}

void
gb_parse_data_set_option_compare (GBParseData *module,
				  int          compare)
{
	module->options.compare = compare; 
}

void
gb_options_init (GBOptions *options)
{
	options->explicit = FALSE;
	options->private_module = FALSE;
	options->base = 0;
	options->compare = GB_COMPARE_BINARY;
}

void
gb_options_copy (GBOptions       *dest,
                 const GBOptions *src)
{
	dest->explicit       = src->explicit;
	dest->private_module = src->private_module;
	dest->base           = src->base;
	dest->compare        = src->compare;
}

void
gb_parse_data_add_attr (GBParseData       *module,
		        const GBAttribute *attr)
{
	g_return_if_fail (attr != NULL);

	module->attrs = g_slist_prepend (module->attrs, (gpointer) attr);
}
