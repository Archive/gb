/*
 * GNOME Basic Memory Mapped Lexer Stream
 *
 * Author:
 *    Michael Meeks <mmeeks@gnu.org>
 *
 * Copyright 2000, Helix Code, Inc.
 */
#ifndef GB_MMAP_FRX_H
#define GB_MMAP_FRX_H

#include <gb/gb.h>
#include <gb/gb-frx.h>

#define GB_TYPE_MMAP_FRX            (gb_mmap_frx_get_type ())
#define GB_MMAP_FRX(obj)            (GTK_CHECK_CAST ((obj), GB_TYPE_MMAP_FRX, GBMMapFrx))
#define GB_MMAP_FRX_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GB_TYPE_MMAP_FRX, GBMMapFrxClass))
#define GB_IS_MMAP_FRX(obj)	    (GTK_CHECK_TYPE ((obj), GB_TYPE_MMAP_FRX))
#define GB_IS_MMAP_FRX_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GB_TYPE_MMAP_FRX))

struct _GBMMapFrx {
	GBFrx		 frx;

	unsigned  	 length;
	char		*start;
	char		*cur;
	char		*end;
};

struct _GBMMapFrxClass {
	GBFrxClass	 klass;
};

GtkType  gb_mmap_frx_get_type (void);
GBFrx   *gb_mmap_frx_new      (char *data, unsigned length);

#endif
