/*
 * GNOME Basic Lexer Stream interface
 *
 * Author:
 *    Michael Meeks <mmeeks@gnu.org>
 *
 * Copyright 2000, Helix Code, Inc.
 */
#ifndef GB_LEX_H
#define GB_LEX_H

#include <gb/gb.h>

extern int      gb_is_keyword (char const  *str, unsigned const len);
extern int      gb_is_function(char const  *str, unsigned const len);

extern gboolean gb_parse_gb   (GBParseData *module);

#define GB_TYPE_LEXER_STREAM            (gb_lexer_stream_get_type ())
#define GB_LEXER_STREAM(obj)            (GTK_CHECK_CAST ((obj), GB_TYPE_LEXER_STREAM, GBLexerStream))
#define GB_LEXER_STREAM_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GB_TYPE_LEXER_STREAM, GBLexerStreamClass))
#define GB_IS_LEXER_STREAM(obj)		(GTK_CHECK_TYPE ((obj), GB_TYPE_LEXER_STREAM))
#define GB_IS_LEXER_STREAM_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GB_TYPE_LEXER_STREAM))

typedef enum {
	GB_PARSING_BASIC,
	GB_PARSING_FORM,
	GB_PARSING_PROJECT,
	GB_PARSING_CLASS,

	GB_PARSING_ASP,

	GB_PARSING_EXPR,
	GB_PARSING_STATEMENTS,

	GB_PARSING_INSIDE_SUB,
	GB_PARSING_INSIDE_DIM

} GBParsingState;

struct _GBLexerStream {
	GtkObject      object;

	int            first_symbol;
	int            line;
	gboolean       incr_line;
	GBParsingState state;
	char           c;
	char           lastc;
};

struct _GBLexerStreamClass {
	GtkObjectClass klass;

	void     (*state_set)   (GBLexerStream *ls, GBParsingState state);

	gboolean (*s_eof)       (GBLexerStream *ls);
	char     (*s_getc)      (GBLexerStream *ls);
	char     (*s_peek)      (GBLexerStream *ls);
	char    *(*s_gets)      (GBLexerStream *ls, char c);
};

GtkType        gb_lexer_stream_get_type (void);
GBLexerStream *gb_lexer_stream_new      (void);


/* Is this a string character ? */
gboolean       gb_lexer_is_string_char          (GBLexerStream *ls,
						 char           c);

/* General bits - concrete */
#define        gb_lexer_stream_line_inc(ls) ((ls)->line++)
#define        gb_lexer_stream_line(ls)     ((ls)->line)
#define        gb_lexer_stream_state(ls)    ((ls)->state)
void           gb_lexer_stream_state_set    (GBLexerStream *ls, GBParsingState state);

/* Stream bits - abstract */
gboolean       gb_lexer_stream_eof      (GBLexerStream *ls);
char           gb_lexer_stream_getc     (GBLexerStream *ls);
char           gb_lexer_stream_peek     (GBLexerStream *ls);
char          *gb_lexer_stream_gets     (GBLexerStream *ls, char c);

int            gb_lex_real              (gpointer res, GBLexerStream *ls);

#endif
