#
# The Gnome Basic Makefile.
# 
# Author:
#	Jody Goldberg <jgoldberg@home.com>
#	Michael Meeks <michael@imaginator.com>
# 

LDADD        = @GLIB_LIBS@
INCLUDES     = -I$(top_srcdir) @GLIB_CFLAGS@

noinst_LTLIBRARIES = libgb.la

GB_GPERF_FILES =		\
	function.gperf		\
	keywords.gperf		\
	types.gperf

GB_GPERF_OUTPUT = $(GB_GPERF_FILES:.gperf=.c)

BUILT_SOURCES = grammar.h grammar.c gb-constants.h gb-constants.c

EXTRA_DIST = $(BUILT_SOURCES) gb-constants.gba $(GB_GPERF_OUTPUT) \
	$(GB_GPERF_FILES)

CLEANFILES += $(BUILT_SOURCES) $(GB_GPERF_OUTPUT)

libgb_la_SOURCES =  		\
	grammar.y		\
	grammar.h		\
	$(GB_GPERF_OUTPUT)	\
	gb.h			\
	gb-constants.c		\
	gb-constants.h		\
	gb-eval.c		\
	gb-expr.c		\
	gb-expr.h		\
	gb-form.c		\
	gb-form.h		\
        gb-class.h		\
	gb-class.c		\
	gb-frx.h		\
	gb-frx.c		\
	gb-frx-get.h		\
	gb-frx-get.c		\
	gb-lex.h		\
	gb-lex.c		\
	gb-main.c		\
	gb-main.h		\
	gb-mmap-frx.c		\
	gb-mmap-frx.h		\
	gb-mmap-lex.c		\
	gb-mmap-lex.h		\
	gb-object.c		\
	gb-object.h		\
	gb-project.c		\
	gb-project.h		\
	gb-statement.c		\
	gb-statement.h		\
	gb-type.c		\
	gb-type.h		\
	gb-value.c		\
	gb-value.h		\
	libgb.h

libgb_la_LIBADD = -lm @GLIB_LIBS@ $(EXTRA_GNOME_LIBS)
#
#  gperf flags
#    -c		Generates C code that uses the strncmp function to perform string
#		comparisons.
#    -k		Allows selection of the character key positions used in the 
#		keywords' hash function.
#    -l		Compare key lengths before trying a string comparison.
#    -t		Allows inclusion of a struct type declaration for generated code.
#    -C		Make the contents of all generted lookup tables constant.
#    -D		Handle keywords whose key position sets hash to duplicate values.
#    -L		Instructs gperf to generate code in the language specified by the
#		option's argument.
#    -N		Allows you to specify the name for the generated lookup function.
#
GB_keywords_GPERF_FLAGS		:= -k '1-6,$$' -t
GB_types_GPERF_FLAGS		:= -k '1-3,$$'
GB_function_GPERF_FLAGS		:= -k '1-3,7' -t
GB_method_GPERF_FLAGS		:= -k '1,3,5,9,10,$$' -t
GB_object_GPERF_FLAGS           := -k '1-4,6,$$' -t
GB_properties_GPERF_FLAGS	:= -k '1-5,8-9,12,15,$$' -D -t

CFLAGS += -g -DYYDEBUG -Wall $(EXTRA_GNOME_CFLAGS)

gb-constants.h: gb-constants.gba
	sed -e "s/'[02].*//" $(srcdir)/gb-constants.gba | sed -e "s/'1//" | perl -pe 's/^const[ \t]+/\#define GB_C_/;s/GB_C_gb/GB_C_/' > gb-constants.h

gb-constants.c: gb-constants.gba
	sed -e "s/'[01].*//" $(srcdir)/gb-constants.gba | sed -e "s/'2//" | perl -pe 's/^const\s+(\S+)\s+(\S+)/        { "\1", \2 }, /;s/"gb/"vb/' > gb-constants.c

%.c : %.gperf
	@-rm -f $@
	gperf $(GB_$*_GPERF_FLAGS) -L 'ANSI-C' -l -C -c -N $(addsuffix _lookup, $*) $^ > $@

YFLAGS += -d -v -p gb_

libgbincludedir = $(includedir)/gb
libgbinclude_HEADERS =		\
		gb.h		\
		gb-class.h	\
		gb-constants.h	\
		gb-eval.h	\
		gb-expr.h	\
		gb-lex.h	\
		gb-main.h	\
		gb-mmap-lex.h	\
		gb-object.h	\
		gb-project.h	\
		gb-statement.h	\
		gb-type.h	\
		gb-value.h	\
		libgb.h

