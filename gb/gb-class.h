/*
 * GNOME Basic Classes
 *
 * Author:
 *    Ravi Pratap M  (ravi_pratap@email.com)
 *
 * Copyright 2001, Helix Code, Inc.
 */

#ifndef GB_CLASS_H
#define GB_CLASS_H

#include <gb/gb.h>
#include <gb/gb-value.h>

struct _GBClassProperty {
	char    *name;
	GBValue *value;
};

typedef enum { 
	GB_CLASS_PROPERTY_GET,
	GB_CLASS_PROPERTY_LET,
	GB_CLASS_NONE
} GBClassMethodType;

GBClassProperty *gb_class_property_new      (GBParseData  *module,
					     const char   *prop_name,
					     const GBExpr *expr);

void             gb_class_property_destroy  (GBClassProperty *prop);

#endif /* GB_CLASS_H */
