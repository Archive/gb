/*
 * GNOME Basic Expressions
 *
 * Author:
 *    Jody Goldberg (jgoldberg@home.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <gb/gb-expr.h>
#include <gb/gb-value.h>

static inline GBExpr const *
gb_expr_new_value_doit (GBValue *v)
{
    GBExpr *res     = g_new (GBExpr, 1);

    res->type       = GB_EXPR_VALUE;
    res->parm.value = v;
    return res;
}

GBExpr const *
gb_expr_new_boolean (GBBoolean bool)
{
	return gb_expr_new_value_doit (gb_value_new_boolean(bool));
}

GBExpr const *
gb_expr_new_int (int val)
{
	if (val < 0x7FFF)
		return gb_expr_new_value_doit (gb_value_new_int (val));
	else
		return gb_expr_new_value_doit (gb_value_new_long (val));
}

GBExpr const *
gb_expr_new_float (double val)
{
	return gb_expr_new_value_doit (gb_value_new_double (val));
}

GBExpr const *
gb_expr_new_string (char const * str)
{
	return gb_expr_new_value_doit (gb_value_new_string_chars (str));
}

GBExpr const *
gb_expr_new_frx (char const * str)
{
	return gb_expr_new_value_doit (gb_value_new_frx (str));
}

GBExpr const *
gb_expr_new_value (GBValue *v)
{
	return gb_expr_new_value_doit (v);
}

GBExpr const *
gb_expr_new_unary (GBExprType type, GBExpr const * sub)
{
	GBExpr *res = g_new (GBExpr, 1);

	res->type = type;
	res->parm.unary.sub = sub;

	return res;
}

GBExpr const *
gb_expr_new_binary (GBExpr const * left, GBExprType type, GBExpr const * right)
{
	GBExpr *res = g_new (GBExpr, 1);

	res->type = type;
	res->parm.binary.left = left;
	res->parm.binary.right = right;

	return res;
}

GBExpr const *
gb_expr_new_obj_list (GSList *objref)
{
	GBExpr *res = g_new (GBExpr, 1);

	res->type = GB_EXPR_OBJREF;
	res->parm.objref = g_slist_reverse (objref);

	return res;
}

GBExpr const *
gb_expr_new_obj_list_call (GSList *objref)
{
	GBExpr   *res = g_new (GBExpr, 1);
	GBObjRef *ref;

	res->type = GB_EXPR_OBJREF;

	if (objref && objref->data) {
		ref = objref->data;
		ref->method = TRUE;
	}

	res->parm.objref = g_slist_reverse (objref);

	return res;
}

void
gb_objref_print (FILE *sink, const GBExpr *e)
{
	GSList *l;
	
	for (l = e->parm.objref; l; l = l->next)
		gb_obj_ref_print (sink, l->data);
}

void
gb_objref_destroy (GSList *objlist)
{
	GSList *l;

 	for (l = objlist; l; l = l->next)
		gb_obj_ref_destroy (l->data);

	g_slist_free (objlist);
}

void
gb_expr_destroy (const GBExpr *expr)
{
	if (!expr)
		return;

	switch (expr->type) {

	case GB_EXPR_OBJREF :
		gb_objref_destroy (expr->parm.objref);
		break;

	case GB_EXPR_VALUE:
		gb_value_destroy (expr->parm.value);
		return;

		/* Unary */
	case GB_EXPR_PAREN :
	case GB_EXPR_POSITIVE : case GB_EXPR_NEGATIVE : case GB_EXPR_NOT :
		gb_expr_destroy (expr->parm.unary.sub);
		return;

		/* Binary */
	case GB_EXPR_CONCAT :
	case GB_EXPR_AND : case GB_EXPR_OR : case GB_EXPR_XOR :
	case GB_EXPR_GT : case GB_EXPR_GE : case GB_EXPR_EQ : case GB_EXPR_NE :
	case GB_EXPR_LE : case GB_EXPR_LT :
	case GB_EXPR_SUB : case GB_EXPR_ADD :
	case GB_EXPR_MULT : case GB_EXPR_DIV : case GB_EXPR_INT_DIV :
	case GB_EXPR_POW : case GB_EXPR_EQV : case GB_EXPR_IMP :
		gb_expr_destroy (expr->parm.binary.left);
		gb_expr_destroy (expr->parm.binary.right);
		break;

	default :
		g_warning ("Unkown GBExprType %d", expr->type);
	};

	g_free ((GBExpr *) expr);
}

void
gb_expr_print (FILE * sink, GBExpr const * expr)
{
	/*
	 *  See gb-expr.h for correct order.
	 */
	static char const * const oper_names [] = {
		NULL, 		/* GB_EXPR_VALUE */
		NULL,		/* GB_EXPR_OBJREF */
		"+ ", 		/* GB_EXPR_POSITIVE */
		"- ", 		/* GB_EXPR_NEGATIVE */
		"! ",		/* GB_EXPR_NOT */
		NULL, 		/* GB_EXPR_PAREN */
		" & ", 		/* GB_EXPR_CONCAT */
		" And ", 	/* GB_EXPR_AND */
		" Or ", 	/* GB_EXPR_OR */
		" Xor ",	/* GB_EXPR_XOR */
		" > ", 		/* GB_EXPR_GT */
		" >= ", 	/* GB_EXPR_GE */
		" == ", 	/* GB_EXPR_EQ */
		" != ", 	/* GB_EXPR_NE */
		" <= ", 	/* GB_EXPR_LE */
		" < ",		/* GB_EXPR_LT */
		" - ", 		/* GB_EXPR_SUB */
		" + ", 		/* GB_EXPR_ADD */
		" * ", 		/* GB_EXPR_MULT */
		" / ", 		/* GB_EXPR_DIV */
		" \\ ",		/* GB_EXPR_INT_DIV */
		" ^ ", 		/* GB_EXPR_POW */
		" Eqv ", 	/* GB_EXPR_EQV */
		" Imp "		/* GB_EXPR_IMP */
	};
	const GBExpr *next = NULL;

	g_return_if_fail (expr != NULL);

	switch (expr->type) {

	case GB_EXPR_OBJREF:
		gb_objref_print (sink, expr);
		return;

	case GB_EXPR_VALUE:
		gb_value_print (sink, expr->parm.value);
		return;

		/* Unary */
	case GB_EXPR_PAREN :
		fputc ('(', sink);
		gb_expr_print (sink, expr->parm.unary.sub);
		fputc (')', sink);
		return;

	case GB_EXPR_POSITIVE : case GB_EXPR_NEGATIVE : case GB_EXPR_NOT :
		next = expr->parm.unary.sub;
		break;

		/* Binary */
	case GB_EXPR_CONCAT :
	case GB_EXPR_AND : case GB_EXPR_OR : case GB_EXPR_XOR :
	case GB_EXPR_GT : case GB_EXPR_GE : case GB_EXPR_EQ : case GB_EXPR_NE :
	case GB_EXPR_LE : case GB_EXPR_LT :
	case GB_EXPR_SUB : case GB_EXPR_ADD :
	case GB_EXPR_MULT : case GB_EXPR_DIV : case GB_EXPR_INT_DIV :
	case GB_EXPR_POW : case GB_EXPR_EQV : case GB_EXPR_IMP :
		gb_expr_print (sink, expr->parm.binary.left);
		next = expr->parm.binary.right;
		break;

	default :
		g_warning ("Unknown GBExprType %d", expr->type);
	};
	g_return_if_fail (oper_names [expr->type] != NULL);
	fprintf (sink,  oper_names [expr->type]);
	gb_expr_print (sink, next);
}

/***************** ObjRef Functions ************************************/

GBObjRef *
gb_obj_ref_new (char const *name, gboolean method,
		GSList * parms)
{
	GBObjRef *res;

	res = g_new (GBObjRef, 1);

	if (name)
		res->name = g_strdup (name);
	else
		res->name = NULL;

	res->method = method;
	res->parms  = g_slist_reverse (parms);

	return res;
}

void
gb_obj_ref_print (FILE *sink, GBObjRef const *objref)
{
	GSList *tmp;
    
	fprintf (sink, "%s", objref->name);
	if (objref->parms == NULL)
		return;

	if (!(tmp = objref->parms))
		return;

	fprintf (sink,"(");
	while (tmp) {
		gb_expr_print (sink, (GBExpr *)tmp->data);
	
		if (tmp->next != NULL)
			fprintf (sink, ",");
	
		tmp = tmp->next;
	}
	fprintf (sink,")");
}

void
gb_obj_ref_destroy (const GBObjRef *obj)
{
	GSList * tmp = obj->parms;
 
	while (tmp != NULL) {
		GSList * next = tmp->next;
		g_slist_free (tmp);
		tmp = next;
	}
    
	g_free ((gpointer)obj);
}

GBArgDesc *
gb_arg_desc_new (const char   *name,
		 const char   *type_name,
		 const GBExpr *def_value,
		 gboolean      by_value,
		 gboolean      optional)
{
	GBArgDesc *ad;

	g_return_val_if_fail (name != NULL, NULL);
	g_return_val_if_fail (type_name != NULL, NULL);

	ad = g_new0 (GBArgDesc, 1);

	ad->name      = g_strdup (name);
	ad->type_name = g_strdup (type_name);
	ad->def_value = (GBExpr *)def_value;
	if (by_value)
		ad->by_value = 1;
	if (optional)
		ad->optional = 1;

	return ad;
}
