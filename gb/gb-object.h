/*
 * GNOME Object
 *
 * Author:
 *    Michael Meeks <michael@imaginator.com>
 *
 * Copyright 2000, Helix Code, Inc.
 */

#ifndef GB_OBJECT_H
#define GB_OBJECT_H

#include <gb/gb.h>

#define GB_TYPE_OBJECT            (gb_object_get_type ())
#define GB_OBJECT(obj)            (GTK_CHECK_CAST ((obj), GB_TYPE_OBJECT, GBObject))
#define GB_OBJECT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GB_TYPE_OBJECT, GBObjectClass))
#define GB_IS_OBJECT(obj)         (GTK_CHECK_TYPE ((obj), GB_TYPE_OBJECT))
#define GB_IS_OBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GB_TYPE_OBJECT))
#define GB_IS_AN_OBJECT(t)        (gtk_type_is_a (t, GB_TYPE_OBJECT))

/*
 * An abstract base class
 */
struct _GBObject {
	GtkObject object;
};

struct _GBObjectClass {
	GtkObjectClass klass;

	void     (*copy)   (GBEvalContext  *ec,
			    GBObject       *src,
			    GBObject       *dest);

	gboolean (*assign) (GBEvalContext  *ec,
			    GBObject       *object,
			    const GBObjRef *ref,
			    GBValue        *value,
			    gboolean        try_assign);

	/*
	 *  A NULL ref->name signifies using the 'default'
	 * method / property.
	 */
	GBValue *(*deref)  (GBEvalContext  *ec,
			    GBObject       *object,
			    const GBObjRef *ref,
			    gboolean        try_deref);
};

GtkType   gb_object_get_type (void);

GBObject *gb_object_copy     (GBEvalContext  *ec,
			      GBObject       *object);

gboolean  gb_object_assign   (GBEvalContext  *ec,
			      GBObject       *object,
			      const GBObjRef *ref,
			      GBValue        *value,
			      gboolean        try_assign);

GBValue  *gb_object_deref    (GBEvalContext  *ec,
			      GBObject       *object,
			      const GBObjRef *ref,
			      gboolean        try_deref);

GBObject *gb_object_ref      (GBObject       *object);

#define   gb_object_unref(o) (gtk_object_unref (GTK_OBJECT (o)))

#endif /* GB_OBJECT_H */
