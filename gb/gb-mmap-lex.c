/*
 * GNOME Basic memory mapped lexer stream
 *
 * Author:
 *    Michael Meeks (michael@helixcode.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */
#include <ctype.h>
#include <string.h>

#include <gb.h>

#include "gb-lex.h"
#include "gb-mmap-lex.h"

static GtkObjectClass *lex_object_parent = NULL;

static gboolean
s_eof (GBLexerStream *ls)
{
	GBMMapStream *ms = GB_MMAP_STREAM (ls);

	if (ms->end - ms->cur > 0)
		return FALSE;
	else
		return TRUE;
}

static char
s_getc (GBLexerStream *ls)
{
	GBMMapStream *ms = GB_MMAP_STREAM (ls);

	if (!s_eof (ls))
		return *(ms->cur++);
	else {
		g_warning ("Overrunning stream");
		return '\'';
	}
}

static char
s_peek (GBLexerStream *ls)
{
	GBMMapStream *ms = GB_MMAP_STREAM (ls);
	
	g_return_val_if_fail (!s_eof (ls), '\0');

	return *ms->cur;
}

static char *
s_gets (GBLexerStream *ls, char c)
{
        GBMMapStream *ms = GB_MMAP_STREAM (ls);

	if (s_eof (ls))
	        return g_strdup ("");

	else {
	        const char *s = ms->cur;
		char       *ans = NULL;
		int         n = 1;

		while (ms->end - ms->cur > 0) {
		        char nxt = *ms->cur;

			if (!gb_lexer_is_string_char (ls, nxt))
				break;
			n++; ms->cur++;
		}

		ans = g_new (char, n + 1);
		ans [0] = c;
		strncpy (ans + 1, s, n - 1);
		ans [n] = '\0';

		return ans;
	}
}

static void
gb_mmap_stream_destroy (GtkObject *object)
{
	GBMMapStream *stream = GB_MMAP_STREAM(object);

	if (stream && stream->start) {
		g_free (stream->start);
		stream->start = NULL;
	}
	lex_object_parent->destroy (object);
}

static void
gb_mmap_stream_class_init (GBMMapStreamClass *klass)
{
	GBLexerStreamClass *lex_class;
	GtkObjectClass     *object_class;

	lex_object_parent = gtk_type_class (GB_TYPE_LEXER_STREAM);

	object_class = GTK_OBJECT_CLASS (klass);
	object_class->destroy = gb_mmap_stream_destroy;
	
	lex_class = GB_LEXER_STREAM_CLASS (klass);

	lex_class->s_getc   = s_getc;
	lex_class->s_peek   = s_peek;
	lex_class->s_gets   = s_gets;
	lex_class->s_eof    = s_eof;
}

static void
gb_mmap_stream_init (GBMMapStream *ls)
{
}

GtkType
gb_mmap_stream_get_type (void)
{
	static GtkType lex_type = 0;

	if (!lex_type) {
		static const GtkTypeInfo lex_info = {
			"GBMMapStream",
			sizeof (GBMMapStream),
			sizeof (GBMMapStreamClass),
			(GtkClassInitFunc) gb_mmap_stream_class_init,
			(GtkObjectInitFunc) gb_mmap_stream_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		lex_type = gtk_type_unique (GB_TYPE_LEXER_STREAM, &lex_info);
	}

	return lex_type;
}

GBLexerStream *
gb_mmap_stream_new (char *data, unsigned length)
{
	GBMMapStream *ms = GB_MMAP_STREAM (gtk_type_new (GB_TYPE_MMAP_STREAM));

	ms->start  = data;
	ms->length = length;

	ms->end    = ms->start + ms->length;
	ms->cur    = data;

	return GB_LEXER_STREAM (ms);
}

GBLexerStream *
gb_mmap_stream_copy (GBLexerStream *ls)
{
	GBMMapStream *newms = GB_MMAP_STREAM (gtk_type_new (GB_TYPE_MMAP_STREAM));
	GBMMapStream *ms;

	g_return_val_if_fail (ls != NULL, NULL);

	ms = GB_MMAP_STREAM (ls);
	
	newms->start  = ms->start;
	newms->length = ms->length;
	newms->end    = ms->end;
	newms->cur    = ms->cur;

	return GB_LEXER_STREAM (newms);
}
