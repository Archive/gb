/*
 * GNOME Basic Classes
 *
 * Author:
 *    Ravi Pratap M  (ravi_pratap@email.com)
 *
 * Copyright 2001, Helix Code, Inc.
 */

#include <gb/gb-expr.h>
#include <gb/gb-class.h>
#include <gb/gb-value.h>
#include <gb/gb-main.h>
#include <gb/gb-eval.h>

GBClassProperty *
gb_class_property_new (GBParseData  *module,
		       const char   *prop_name,
		       const GBExpr *expr)
{
	GBClassProperty *prop;
	GBValue         *val;

	g_return_val_if_fail (expr != NULL, NULL);
	g_return_val_if_fail (prop_name != NULL, NULL);

	val = gb_eval_context_eval (module->ec, expr);
       
	if (!val) {
		g_warning ("Can't evaluate '%s's value ", prop_name);
		return NULL;
	}

	prop = g_new (GBClassProperty, 1);

	prop->name = g_strdup (prop_name);
	prop->value = val;

	return prop;
}

void
gb_class_property_destroy (GBClassProperty *prop)
{
	if (prop) {
		if (prop->name)
			g_free (prop->name);

		gb_value_destroy (prop->value);
		
		g_free (prop);
	}
}
