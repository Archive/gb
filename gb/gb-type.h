/*
 * GNOME Basic User defined type bits
 *
 * Author:
 *    Michael Meeks <mmeeks@gnu.org>
 *
 * Copyright 2000, Helix Code, Inc.
 */
#ifndef GB_TYPE_H
#define GB_TYPE_H

#include <gb/gb.h>

struct _GBType {
	gboolean public;
	char    *name;
	GSList  *vars;
};

/* Variable */
struct _GBVar {
	const char *name;
	gboolean    object;
	gboolean    is_array;
	GSList     *indices;
	const char *type;
};

/* Constant */
struct _GBConst {
	char    *name;
	GBValue *value;
};

GBType     *gb_type_new          (gboolean       public,
				  const char    *name,
				  GSList        *vars);

GBVar      *gb_var_new           (const char    *var_name,
				  gboolean       object,
				  gboolean       is_array,
				  GSList        *indices,
				  const char    *type_name);

GBConst    *gb_const_new         (GBEvalContext *ec,
				  const char    *name,
				  GtkType        t,
				  const GBExpr  *expr);

void        gb_const_destroy     (GBConst       *cons);

void        gb_type_destroy      (GBType        *type);

void        gb_var_destroy       (GBVar         *var);

#endif
