#ifndef LIBGB_H
#define LIBGB_H
/*
 * GNOME Basic Parser
 *
 * Author:
 *    Jody Goldberg (jgoldberg@home.com)
 */

#include <glib.h>

#include <gb/gb-main.h>
#include <gb/gb-eval.h>
#include <gb/gb-lex.h>
#include <gb/gb-project.h>

const GBParseData *gb_parse_stream       (GBEvalContext *ec, GBLexerStream *ls);
void               gb_parse_data_destroy (const GBParseData *);

#endif /* LIBGB_H */
