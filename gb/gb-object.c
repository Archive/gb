/*
 * GNOME Object
 *
 * Author:
 *    Michael Meeks <michael@imaginator.com>
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <gb/gb.h>
#include <gb/gb-object.h>

static void
gb_object_copy_unimplemented (GBEvalContext  *ec,
			      GBObject       *src,
			      GBObject       *dest)
{
	g_warning ("Copy method not implemented for '%s'",
		   gtk_type_name (GTK_OBJECT (src)->klass->type));
}

GBObject *
gb_object_copy (GBEvalContext *ec, GBObject *object)
{
	GBObject       *copy;
	GtkObjectClass *klass;

	g_return_val_if_fail (object != NULL, NULL);

	klass = GTK_OBJECT (object)->klass;
	g_return_val_if_fail (klass != NULL, NULL);

	object = gtk_type_new (klass->type);
	g_return_val_if_fail (object != NULL, NULL);

	GB_OBJECT_CLASS (klass)->copy (ec, object, copy);

	return copy;
}
				     
static gboolean
gb_object_assign_unimplemented (GBEvalContext  *ec,
				GBObject       *object,
				const GBObjRef *ref,
				GBValue        *value,
				gboolean        try_assign)
{
	g_warning ("Assign method not implemented for '%s'",
		   gtk_type_name (GTK_OBJECT (object)->klass->type));
	return FALSE;
}

gboolean
gb_object_assign (GBEvalContext  *ec,
		  GBObject       *object,
		  const GBObjRef *ref,
		  GBValue        *value,
		  gboolean        try_assign)
{
	GtkObjectClass *klass;

	klass = GTK_OBJECT (object)->klass;
	g_return_val_if_fail (klass != NULL, FALSE);

	return GB_OBJECT_CLASS (klass)->assign (ec, object, ref, value, try_assign);
}

static GBValue *
gb_object_deref_unimplemented (GBEvalContext  *ec,
			       GBObject       *object,
			       const GBObjRef *ref,
			       gboolean        try_deref)
{
	g_warning ("Deref method not implemented for '%s'",
		   gtk_type_name (GTK_OBJECT (object)->klass->type));
	return NULL;
}

GBValue *
gb_object_deref (GBEvalContext  *ec,
		 GBObject       *object,
		 const GBObjRef *ref,
		 gboolean        try_deref)
{
	GtkObjectClass *klass;

	klass = GTK_OBJECT (object)->klass;
	g_return_val_if_fail (klass != NULL, FALSE);

	return GB_OBJECT_CLASS (klass)->deref (ec, object, ref, try_deref);
}

static void
gb_object_class_init (GBObjectClass *klass)
{
	klass->copy   = gb_object_copy_unimplemented;
	klass->assign = gb_object_assign_unimplemented;
	klass->deref  = gb_object_deref_unimplemented;
}

GtkType
gb_object_get_type (void)
{
	static GtkType object_type = 0;

	if (!object_type) {
		static const GtkTypeInfo object_info = {
			"GBObject",
			sizeof (GBObject),
			sizeof (GBObjectClass),
			(GtkClassInitFunc)  gb_object_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		object_type = gtk_type_unique (GTK_TYPE_OBJECT, &object_info);
		gtk_type_class (object_type);
	}

	return object_type;	
}

GBObject *
gb_object_ref (GBObject *object)
{
	gtk_object_ref (GTK_OBJECT (object));

	return object;
}
