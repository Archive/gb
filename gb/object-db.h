#ifndef GB_OBJECT_DB_H
#define GB_OBJECT_DB_H
/*
 * GNOME Basic Object registry
 *
 * Author:
 *    Jody Goldberg (jgoldberg@home.com)
 */

#include "gb-main.h"

extern GBPropertyIndex const AddIn_properties[];
extern GBMethodIndex const AddIn_methods[];

extern GBPropertyIndex const AddIns_properties[];
extern GBMethodIndex const AddIns_methods[];

extern GBPropertyIndex const Adjustments_properties[];
extern GBMethodIndex const Adjustments_methods[];

extern GBPropertyIndex const Application_properties[];
extern GBMethodIndex const Application_methods[];

extern GBPropertyIndex const Areas_properties[];
extern GBMethodIndex const Areas_methods[];

extern GBPropertyIndex const AutoCorrect_properties[];
extern GBMethodIndex const AutoCorrect_methods[];

extern GBPropertyIndex const Axes_properties[];
extern GBMethodIndex const Axes_methods[];

extern GBPropertyIndex const Axes_properties[];
extern GBMethodIndex const Axes_methods[];

extern GBPropertyIndex const Axis_properties[];
extern GBMethodIndex const Axis_methods[];

extern GBPropertyIndex const AxisTitle_properties[];
extern GBMethodIndex const AxisTitle_methods[];

extern GBPropertyIndex const Border_properties[];
extern GBMethodIndex const Border_methods[];

extern GBPropertyIndex const Borders_properties[];
extern GBMethodIndex const Borders_methods[];

extern GBPropertyIndex const CalculatedFields_properties[];
extern GBMethodIndex const CalculatedFields_methods[];

extern GBPropertyIndex const CalculatedItems_properties[];
extern GBMethodIndex const CalculatedItems_methods[];

extern GBPropertyIndex const CalloutFormat_properties[];
extern GBMethodIndex const CalloutFormat_methods[];

extern GBPropertyIndex const Characters_properties[];
extern GBMethodIndex const Characters_methods[];

extern GBPropertyIndex const Chart_properties[];
extern GBMethodIndex const Chart_methods[];

extern GBPropertyIndex const ChartArea_properties[];
extern GBMethodIndex const ChartArea_methods[];

extern GBPropertyIndex const ChartColorFormat_properties[];
extern GBMethodIndex const ChartColorFormat_methods[];

extern GBPropertyIndex const ChartFillFormat_properties[];
extern GBMethodIndex const ChartFillFormat_methods[];

extern GBPropertyIndex const ChartGroup_properties[];
extern GBMethodIndex const ChartGroup_methods[];

extern GBPropertyIndex const ChartGroups_properties[];
extern GBMethodIndex const ChartGroups_methods[];

extern GBPropertyIndex const ChartObject_properties[];
extern GBMethodIndex const ChartObject_methods[];

extern GBPropertyIndex const ChartObjects_properties[];
extern GBMethodIndex const ChartObjects_methods[];

extern GBPropertyIndex const ChartTitle_properties[];
extern GBMethodIndex const ChartTitle_methods[];

extern GBPropertyIndex const Charts_properties[];
extern GBMethodIndex const Charts_methods[];

extern GBPropertyIndex const ColorFormat_properties[];
extern GBMethodIndex const ColorFormat_methods[];

extern GBPropertyIndex const Comment_properties[];
extern GBMethodIndex const Comment_methods[];

extern GBPropertyIndex const Comments_properties[];
extern GBMethodIndex const Comments_methods[];

extern GBPropertyIndex const ConnectorFormat_properties[];
extern GBMethodIndex const ConnectorFormat_methods[];

extern GBPropertyIndex const ControlFormat_properties[];
extern GBMethodIndex const ControlFormat_methods[];

extern GBPropertyIndex const CustomView_properties[];
extern GBMethodIndex const CustomView_methods[];

extern GBPropertyIndex const CustomViews_properties[];
extern GBMethodIndex const CustomViews_methods[];

extern GBPropertyIndex const DataLabel_properties[];
extern GBMethodIndex const DataLabel_methods[];

extern GBPropertyIndex const DataLabels_properties[];
extern GBMethodIndex const DataLabels_methods[];

extern GBPropertyIndex const DataTable_properties[];
extern GBMethodIndex const DataTable_methods[];

extern GBPropertyIndex const Dialog_properties[];
extern GBMethodIndex const Dialog_methods[];

extern GBPropertyIndex const Dialogs_properties[];
extern GBMethodIndex const Dialogs_methods[];

extern GBPropertyIndex const DocumentProperties_properties[];
extern GBMethodIndex const DocumentProperties_methods[];

extern GBPropertyIndex const DownBars_properties[];
extern GBMethodIndex const DownBars_methods[];

extern GBPropertyIndex const DropLines_properties[];
extern GBMethodIndex const DropLines_methods[];

extern GBPropertyIndex const Err_properties[];
extern GBMethodIndex const Err_methods[];

extern GBPropertyIndex const ErrorBars_properties[];
extern GBMethodIndex const ErrorBars_methods[];

extern GBPropertyIndex const FillFormat_properties[];
extern GBMethodIndex const FillFormat_methods[];

extern GBPropertyIndex const Floor_properties[];
extern GBMethodIndex const Floor_methods[];

extern GBPropertyIndex const Font_properties[];
extern GBMethodIndex const Font_methods[];

extern GBPropertyIndex const FormatCondition_properties[];
extern GBMethodIndex const FormatCondition_methods[];

extern GBPropertyIndex const FormatConditions_properties[];
extern GBMethodIndex const FormatConditions_methods[];

extern GBPropertyIndex const FreeformBuilder_properties[];
extern GBMethodIndex const FreeformBuilder_methods[];

extern GBPropertyIndex const Gridlines_properties[];
extern GBMethodIndex const Gridlines_methods[];

extern GBPropertyIndex const GroupShapes_properties[];
extern GBMethodIndex const GroupShapes_methods[];

extern GBPropertyIndex const HPageBreak_properties[];
extern GBMethodIndex const HPageBreak_methods[];

extern GBPropertyIndex const HPageBreaks_properties[];
extern GBMethodIndex const HPageBreaks_methods[];

extern GBPropertyIndex const HiLoLines_properties[];
extern GBMethodIndex const HiLoLines_methods[];

extern GBPropertyIndex const Hyperlink_properties[];
extern GBMethodIndex const Hyperlink_methods[];

extern GBPropertyIndex const Hyperlinks_properties[];
extern GBMethodIndex const Hyperlinks_methods[];

extern GBPropertyIndex const Interior_properties[];
extern GBMethodIndex const Interior_methods[];

extern GBPropertyIndex const LeaderLines_properties[];
extern GBMethodIndex const LeaderLines_methods[];

extern GBPropertyIndex const Legend_properties[];
extern GBMethodIndex const Legend_methods[];

extern GBPropertyIndex const LegendEntries_properties[];
extern GBMethodIndex const LegendEntries_methods[];

extern GBPropertyIndex const LegendEntry_properties[];
extern GBMethodIndex const LegendEntry_methods[];

extern GBPropertyIndex const LegendKey_properties[];
extern GBMethodIndex const LegendKey_methods[];

extern GBPropertyIndex const LineFormat_properties[];
extern GBMethodIndex const LineFormat_methods[];

extern GBPropertyIndex const LinkFormat_properties[];
extern GBMethodIndex const LinkFormat_methods[];

extern GBPropertyIndex const Mailer_properties[];
extern GBMethodIndex const Mailer_methods[];

extern GBPropertyIndex const Name_properties[];
extern GBMethodIndex const Name_methods[];

extern GBPropertyIndex const Names_properties[];
extern GBMethodIndex const Names_methods[];

extern GBPropertyIndex const ODBCError_properties[];
extern GBMethodIndex const ODBCError_methods[];

extern GBPropertyIndex const ODBCErrors_properties[];
extern GBMethodIndex const ODBCErrors_methods[];

extern GBPropertyIndex const OLEFormat_properties[];
extern GBMethodIndex const OLEFormat_methods[];

extern GBPropertyIndex const OLEObject_properties[];
extern GBMethodIndex const OLEObject_methods[];

extern GBPropertyIndex const OLEObjects_properties[];
extern GBMethodIndex const OLEObjects_methods[];

extern GBPropertyIndex const Outline_properties[];
extern GBMethodIndex const Outline_methods[];

extern GBPropertyIndex const PageSetup_properties[];
extern GBMethodIndex const PageSetup_methods[];

extern GBPropertyIndex const Pane_properties[];
extern GBMethodIndex const Pane_methods[];

extern GBPropertyIndex const Panes_properties[];
extern GBMethodIndex const Panes_methods[];

extern GBPropertyIndex const Parameter_properties[];
extern GBMethodIndex const Parameter_methods[];

extern GBPropertyIndex const Parameters_properties[];
extern GBMethodIndex const Parameters_methods[];

extern GBPropertyIndex const Phonetic_properties[];
extern GBMethodIndex const Phonetic_methods[];

extern GBPropertyIndex const PictureFormat_properties[];
extern GBMethodIndex const PictureFormat_methods[];

extern GBPropertyIndex const PivotCache_properties[];
extern GBMethodIndex const PivotCache_methods[];

extern GBPropertyIndex const PivotCaches_properties[];
extern GBMethodIndex const PivotCaches_methods[];

extern GBPropertyIndex const PivotField_properties[];
extern GBMethodIndex const PivotField_methods[];

extern GBPropertyIndex const PivotFields_properties[];
extern GBMethodIndex const PivotFields_methods[];

extern GBPropertyIndex const PivotFormula_properties[];
extern GBMethodIndex const PivotFormula_methods[];

extern GBPropertyIndex const PivotFormulas_properties[];
extern GBMethodIndex const PivotFormulas_methods[];

extern GBPropertyIndex const PivotItem_properties[];
extern GBMethodIndex const PivotItem_methods[];

extern GBPropertyIndex const PivotItems_properties[];
extern GBMethodIndex const PivotItems_methods[];

extern GBPropertyIndex const PivotTable_properties[];
extern GBMethodIndex const PivotTable_methods[];

extern GBPropertyIndex const PivotTables_properties[];
extern GBMethodIndex const PivotTables_methods[];

extern GBPropertyIndex const PlotArea_properties[];
extern GBMethodIndex const PlotArea_methods[];

extern GBPropertyIndex const Point_properties[];
extern GBMethodIndex const Point_methods[];

extern GBPropertyIndex const Points_properties[];
extern GBMethodIndex const Points_methods[];

extern GBPropertyIndex const QueryTable_properties[];
extern GBMethodIndex const QueryTable_methods[];

extern GBPropertyIndex const QueryTables_properties[];
extern GBMethodIndex const QueryTables_methods[];

extern GBPropertyIndex const Range_properties[];
extern GBMethodIndex const Range_methods[];

extern GBPropertyIndex const RecentFile_properties[];
extern GBMethodIndex const RecentFile_methods[];

extern GBPropertyIndex const RecentFiles_properties[];
extern GBMethodIndex const RecentFiles_methods[];

extern GBPropertyIndex const RecentFiles_properties[];
extern GBMethodIndex const RecentFiles_methods[];

extern GBPropertyIndex const RoutingSlip_properties[];
extern GBMethodIndex const RoutingSlip_methods[];

extern GBPropertyIndex const Scenario_properties[];
extern GBMethodIndex const Scenario_methods[];

extern GBPropertyIndex const Scenarios_properties[];
extern GBMethodIndex const Scenarios_methods[];

extern GBPropertyIndex const Series_properties[];
extern GBMethodIndex const Series_methods[];

extern GBPropertyIndex const SeriesCollection_properties[];
extern GBMethodIndex const SeriesCollection_methods[];

extern GBPropertyIndex const SeriesLines_properties[];
extern GBMethodIndex const SeriesLines_methods[];

extern GBPropertyIndex const ShadowFormat_properties[];
extern GBMethodIndex const ShadowFormat_methods[];

extern GBPropertyIndex const Shape_properties[];
extern GBMethodIndex const Shape_methods[];

extern GBPropertyIndex const ShapeNode_properties[];
extern GBMethodIndex const ShapeNode_methods[];

extern GBPropertyIndex const ShapeNodes_properties[];
extern GBMethodIndex const ShapeNodes_methods[];

extern GBPropertyIndex const ShapeRange_properties[];
extern GBMethodIndex const ShapeRange_methods[];

extern GBPropertyIndex const Shapes_properties[];
extern GBMethodIndex const Shapes_methods[];

extern GBPropertyIndex const Sheets_properties[];
extern GBMethodIndex const Sheets_methods[];

extern GBPropertyIndex const SoundNote_properties[];
extern GBMethodIndex const SoundNote_methods[];

extern GBPropertyIndex const Style_properties[];
extern GBMethodIndex const Style_methods[];

extern GBPropertyIndex const Styles_properties[];
extern GBMethodIndex const Styles_methods[];

extern GBPropertyIndex const UserForm_properties[];
extern GBMethodIndex const UserForm_methods[];

extern GBPropertyIndex const UserForms_properties[];
extern GBMethodIndex const UserForms_methods[];

extern GBPropertyIndex const VPageBreak_properties[];
extern GBMethodIndex const VPageBreak_methods[];

extern GBPropertyIndex const VPageBreak_properties[];
extern GBMethodIndex const VPageBreak_methods[];

extern GBPropertyIndex const VPageBreaks_properties[];
extern GBMethodIndex const VPageBreaks_methods[];

extern GBPropertyIndex const Walls_properties[];
extern GBMethodIndex const Walls_methods[];

extern GBPropertyIndex const Window_properties[];
extern GBMethodIndex const Window_methods[];

extern GBPropertyIndex const Windows_properties[];
extern GBMethodIndex const Windows_methods[];

extern GBPropertyIndex const Workbook_properties[];
extern GBMethodIndex const Workbook_methods[];

extern GBPropertyIndex const Workbooks_properties[];
extern GBMethodIndex const Workbooks_methods[];

extern GBPropertyIndex const Worksheet_properties[];
extern GBMethodIndex const Worksheet_methods[];

extern GBPropertyIndex const WorksheetFunction_properties[];
extern GBMethodIndex const WorksheetFunction_methods[];

extern GBPropertyIndex const Worksheets_properties[];
extern GBMethodIndex const Worksheets_methods[];

#endif /* GB_OBJECT_DB_H */
