/*
 * GNOME Constant expression evaluation
 *
 * Author:
 *    Michael Meeks <michael@imaginator.com>
 *
 * Copyright 2000, Helix Code, Inc.
 */

#include <config.h>
#include <errno.h>
#include <math.h>
#include <setjmp.h>
#include <string.h>
#include <stdlib.h>

#include <gb/gb.h>
#include <gb/gb-eval.h>
#include <gb/gb-value.h>
#include <gb/gb-expr.h>
#include <gb/gb-constants.h>

static GtkObject *gb_eval_context_parent = NULL;
/*
 * FIXME: These need lots more work.
 */
GBValue *
gb_eval_sub (const GBValue *l, const GBValue *r)
{
	GBDouble ld, rd;

	g_return_val_if_fail (l != NULL, NULL);
	g_return_val_if_fail (r != NULL, NULL);

	ld = gb_value_get_as_double (l);
	rd = gb_value_get_as_double (r);

	return gb_value_new_double (ld - rd);
}

GBValue *
gb_eval_add (const GBValue *l, const GBValue *r)
{
	GBDouble ld, rd;

	g_return_val_if_fail (l != NULL, NULL);
	g_return_val_if_fail (r != NULL, NULL);

	if (gb_value_from_gtk_type (r->gtk_type) <
	    gb_value_from_gtk_type (l->gtk_type)) {
		const GBValue *tmp = l;
		l = r;
		r = tmp;
	}	

	ld = gb_value_get_as_double (l);
	rd = gb_value_get_as_double (r);

	return gb_value_new_double (ld + rd);
}

GBValue *
gb_eval_mult (const GBValue *l, const GBValue *r)
{
	GBDouble ld, rd;

	g_return_val_if_fail (l != NULL, NULL);
	g_return_val_if_fail (r != NULL, NULL);

	if (gb_value_from_gtk_type (r->gtk_type) <
	    gb_value_from_gtk_type (l->gtk_type)) {
		const GBValue *tmp = l;
		l = r;
		r = tmp;
	}	

	ld = gb_value_get_as_double (l);
	rd = gb_value_get_as_double (r);

	return gb_value_new_double (ld * rd);
}

GBValue *
gb_eval_div (const GBValue *l, const GBValue *r)
{
	GBDouble ld, rd;

	g_return_val_if_fail (l != NULL, NULL);
	g_return_val_if_fail (r != NULL, NULL);

	ld = gb_value_get_as_double (l);
	rd = gb_value_get_as_double (r);

	return gb_value_new_double (ld / rd);
}

GBValue *
gb_eval_int_div (const GBValue *l, const GBValue *r)
{
	GBLong ll, rl;

	g_return_val_if_fail (l != NULL, NULL);
	g_return_val_if_fail (r != NULL, NULL);

	ll = gb_value_get_as_long (l);
	rl = gb_value_get_as_long (r);

	return gb_value_new_long (ll / rl);
}

GBValue *
gb_eval_pow (const GBValue *l, const GBValue *r)
{
	GBDouble ld, rd;

	g_return_val_if_fail (l != NULL, NULL);
	g_return_val_if_fail (r != NULL, NULL);

	ld = gb_value_get_as_double (l);
	rd = gb_value_get_as_double (r);

	return gb_value_new_double (pow (ld, rd));
}

GBValue *
gb_eval_concat (const GBValue *l, const GBValue *r)
{
	GString *str, *tmp;

	g_return_val_if_fail (l != NULL, NULL);
	g_return_val_if_fail (r != NULL, NULL);

	if (l->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL) &&
	    r->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL))
		return gb_value_new_null ();

	str = (GString *)gb_value_get_as_string (l);
	tmp = (GString *)gb_value_get_as_string (r);
	g_string_append (str, tmp->str);
	g_string_free (tmp, TRUE);

	return gb_value_new_string (str);
}

GBValue *
gb_eval_eqv (const GBValue *l, const GBValue *r)
{
	GBLong ll, rl;

	g_return_val_if_fail (l != NULL, NULL);
	g_return_val_if_fail (r != NULL, NULL);

	if (l->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL) &&
	    r->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL))
		return gb_value_new_null ();
	
	if (l->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN) &&
	    r->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN)) {
		if (l->v.bool == r->v.bool)
			return gb_value_new_boolean (GBTrue);
		else
			return gb_value_new_boolean (GBFalse);
	}

	ll = gb_value_get_as_long (l);
	rl = gb_value_get_as_long (r);

	return gb_value_new_long (~(ll ^ rl));
}

GBValue *
gb_eval_and (const GBValue *l, const GBValue *r)
{
	GBLong ll, rl;

	g_return_val_if_fail (l != NULL, NULL);
	g_return_val_if_fail (r != NULL, NULL);

	if (l->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN) &&
	    r->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN)) /* Most common we hope */
		return gb_value_new_boolean (l->v.bool && r->v.bool);

	if (l->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL) &&
	    r->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL))
		return gb_value_new_null ();

	if ((l->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL) &&
	     r->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN)) ||
	    (r->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL) &&
	     l->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN))) { /* Boolean and Null: ug ! */

		if ((l->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN) && l->v.bool) ||
		    (r->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN) && r->v.bool))

			return gb_value_new_null ();
		else
			return gb_value_new_boolean (GBFalse);
	}
	    
	ll = gb_value_get_as_long (l);
	rl = gb_value_get_as_long (r);
	
	return gb_value_new_long (ll & rl);
}

GBValue *
gb_eval_or (const GBValue *l, const GBValue *r)
{
	GBLong ll, rl;

	g_return_val_if_fail (l != NULL, NULL);
	g_return_val_if_fail (r != NULL, NULL);

	if (l->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN) &&
	    r->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN)) /* Most common we hope */
		return gb_value_new_boolean (l->v.bool || r->v.bool);

	if (l->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL) ||
	    r->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL)) {
		g_warning ("Fixme: is this correct ?");
		return gb_value_new_null ();
	}
	    
	ll = gb_value_get_as_long (l);
	rl = gb_value_get_as_long (r);

	return gb_value_new_long (ll | rl);
}

GBValue *
gb_eval_xor (const GBValue *l, const GBValue *r)
{
	GBLong ll, rl;

	g_return_val_if_fail (l != NULL, NULL);
	g_return_val_if_fail (r != NULL, NULL);

	if (l->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN) &&
	    r->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN)) /* Most common we hope */
		return gb_value_new_boolean (l->v.bool != r->v.bool);

	if (l->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL) ||
	    r->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL))
		return gb_value_new_null ();
	    
	ll = gb_value_get_as_long (l);
	rl = gb_value_get_as_long (r);

	return gb_value_new_long (ll ^ rl);
}

GBValue *
gb_eval_imp (const GBValue *l, const GBValue *r)
{
	GBLong ll, rl;

	g_return_val_if_fail (l != NULL, NULL);
	g_return_val_if_fail (r != NULL, NULL);

/* FIXME: Imp users have damaged brains; commutativity is golden. */

	/* This really sucks */
	if (l->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN) && l->v.bool) {

		if (r->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN) && l->v.bool)
			return gb_value_new_boolean (GBTrue);

		else if (r->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN) && !l->v.bool)
			return gb_value_new_boolean (GBFalse);

		else if (r->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL))
			return gb_value_new_null ();

	} else if (l->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN) && !l->v.bool) {

		if (r->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN) && l->v.bool)
			return gb_value_new_boolean (GBTrue);

		else if (r->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN) && !l->v.bool)
			return gb_value_new_boolean (GBTrue);

		else if (r->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL))
			return gb_value_new_boolean (GBTrue);

	} else if (l->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL)) {

		if (r->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN) && l->v.bool)
			return gb_value_new_boolean (GBTrue);

		else if (r->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN) && !l->v.bool)
			return gb_value_new_null ();

		else if (r->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL))
			return gb_value_new_null ();
	}
	    
	ll = gb_value_get_as_long (l);
	rl = gb_value_get_as_long (r);

	return gb_value_new_long ((ll & rl) | ~ll);
}


/* ----------------- Unary operations ----------------- */


GBValue *
gb_eval_not (const GBValue *v)
{
	GBLong vl;

	g_return_val_if_fail (v != NULL, NULL);

	if (v->gtk_type == gb_gtk_type_from_value (GB_VALUE_NULL))
		return gb_value_new_null ();

	if (v->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN))
		return gb_value_new_boolean (!v->v.bool);
	
	vl = gb_value_get_as_long (v);

	return gb_value_new_long (~vl);
}

GBValue *
gb_eval_neg (const GBValue *v)
{
	g_return_val_if_fail (v != NULL, NULL);

	switch (gb_value_from_gtk_type (v->gtk_type)) {

	case GB_VALUE_NULL:
		return gb_value_new_null ();
		
	case GB_VALUE_BOOLEAN:
		return gb_value_new_boolean (!v->v.bool);

	case GB_VALUE_INT:
		return gb_value_new_int    (- v->v.i);
		
	case GB_VALUE_LONG:
		return gb_value_new_long   (- v->v.l);

	case GB_VALUE_SINGLE:
		return gb_value_new_single (- v->v.f);

	case GB_VALUE_DOUBLE:
		return gb_value_new_double (- v->v.d);

	default:
		g_warning ("unhandled negated value");
		return gb_value_new_long (- gb_value_get_as_long (v));
	}
}

static void
fire (GBEvalContext *ec, const char *txt)
{
	g_return_if_fail (ec != NULL);
	g_return_if_fail (txt != NULL);

	ec->exception = g_strdup (txt);

#if RUNTIME_DEBUG
	g_warning ("An exception was fired '%s'",
		   gb_eval_context_get_text (ec));
#endif
}

gboolean
gb_eval_exception (GBEvalContext *ec)
{
	return ec->exception != NULL;
}

GBValue *
gb_eval_exception_fire (GBEvalContext *ec, const char *txt)
{
	GB_EVAL_CONTEXT_CLASS (GTK_OBJECT (ec)->klass)->fire (ec, txt);

	return NULL;
}

GBValue *
gb_eval_exception_firev (GBEvalContext *ec, const char *format, ...)
{
	va_list             args;
	char               *txt;

	va_start (args, format);

	txt = g_strdup_vprintf (format, args);

	GB_EVAL_CONTEXT_CLASS (GTK_OBJECT (ec)->klass)->fire (ec, txt);
	g_free (txt);

	va_end (args);

	return NULL;
}

static char *
get_text (GBEvalContext *ec)
{
	char *txt;

	g_return_val_if_fail (ec != NULL, g_strdup ("Unknown context"));
	g_return_val_if_fail (ec->exception != NULL, g_strdup ("Unknown exception"));

	txt = g_strdup_printf ("Error: before line %d - '%s'", ec->line, 
			       ec->exception);

	return txt;
}

char *
gb_eval_context_get_text (GBEvalContext *ec)
{
	GBEvalContextClass *class;

	g_return_val_if_fail (ec != NULL, g_strdup ("No context"));

	class = GB_EVAL_CONTEXT_CLASS (GTK_OBJECT (ec)->klass);

	g_return_val_if_fail (class->get_text != NULL, g_strdup ("No context"));

	return class->get_text (ec);
}

static void
reset (GBEvalContext *ec)
{
	g_return_if_fail (ec != NULL);

	if (ec->exception)
		g_free (ec->exception);

	ec->exception = NULL;
}

void
gb_eval_context_reset (GBEvalContext *ec)
{
	GBEvalContextClass *class;

	g_return_if_fail (ec != NULL);

	class = GB_EVAL_CONTEXT_CLASS (GTK_OBJECT (ec)->klass);

	g_return_if_fail (class->get_text != NULL);

	class->reset (ec);
}

static void
gb_eval_context_destroy (GtkObject *object)
{
	GBEvalContext *ec;

	g_return_if_fail (GB_IS_EVAL_CONTEXT (object));

	ec = GB_EVAL_CONTEXT (object);

	if (ec->exception)
		g_free (ec->exception);

	GTK_OBJECT_CLASS (gb_eval_context_parent)->destroy (object);
}

static void
gb_eval_context_class_init (GBEvalContextClass *klass)
{
	GtkObjectClass *object_class;

	gb_eval_context_parent = gtk_type_class (GTK_TYPE_OBJECT);

	object_class = (GtkObjectClass*) klass;
	object_class->destroy = gb_eval_context_destroy;

	klass->eval     = NULL;

	klass->fire     = fire;
	klass->get_text = get_text;
	klass->reset    = reset;
}

static void
gb_eval_context_init (GBEvalContext *ec)
{
	ec->exception = NULL;
}

GtkType
gb_eval_context_get_type (void)
{
	static GtkType eval_type = 0;

	if (!eval_type) {
		static const GtkTypeInfo eval_info = {
			"GBEvalContext",
			sizeof (GBEvalContext),
			sizeof (GBEvalContextClass),
			(GtkClassInitFunc) gb_eval_context_class_init,
			(GtkObjectInitFunc) gb_eval_context_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		eval_type = gtk_type_unique (GTK_TYPE_OBJECT, &eval_info);
	}

	return eval_type;	
}

GBEvalContext *
gb_eval_context_new (void)
{
	return GB_EVAL_CONTEXT (gtk_type_new (GB_TYPE_EVAL_CONTEXT));
}

gboolean
gb_eval_compare (GBEvalContext *ec,
		 const GBValue *l, GBExprType t,
		 const GBValue *r, GBBoolean *result)
{
	GBBoolean ans = GBFalse;

	g_return_val_if_fail (l, GBFalse);
	g_return_val_if_fail (r, GBFalse);
	g_return_val_if_fail (GB_EXPR_COMPARE(t), GBFalse);

	if (l->gtk_type == gb_gtk_type_from_value (GB_VALUE_STRING) &&
	    r->gtk_type == gb_gtk_type_from_value (GB_VALUE_STRING)) {
		GBLong l_num;
		GBLong r_num;

		/* FIXME: may need expanding to chomp whitespace away ? */

		l_num = strcmp (l->v.s->str, r->v.s->str);
		r_num = 0;

		switch (t) {
		case GB_EXPR_GT:
			ans = (l_num >  r_num);
			break;

		case GB_EXPR_GE:
			ans = (l_num >= r_num);
			break;

		case GB_EXPR_EQ:
			ans = (l_num == r_num);
			break;

		case GB_EXPR_NE:
			ans = (l_num != r_num);
			break;

		case GB_EXPR_LE:
			ans = (l_num <= r_num);
			break;

		case GB_EXPR_LT:
			ans = (l_num <  r_num);
			break;
		default:
			g_warning ("Extra unhandled compare type added");
			break;
		}
	} else if ((gb_value_is_integer (l) || l->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN)) &&
		   (gb_value_is_integer (r) || r->gtk_type == gb_gtk_type_from_value (GB_VALUE_BOOLEAN))) {

		GBValue *v;
		GBLong   l_num;
		GBLong   r_num;

		v = gb_value_promote (ec, gb_gtk_type_from_value (GB_VALUE_LONG), l);
		if (!v)
			return FALSE;
                l_num = v->v.l;
                gb_value_destroy (v);

		v = gb_value_promote (ec, gb_gtk_type_from_value (GB_VALUE_LONG), r);
		if (!v)
			return FALSE;
                r_num = v->v.l;
                gb_value_destroy (v);

		switch (t) {
		case GB_EXPR_GT:
			ans = (l_num >  r_num);
			break;

		case GB_EXPR_GE:
			ans = (l_num >= r_num);
			break;

		case GB_EXPR_EQ:
			ans = (l_num == r_num);
			break;

		case GB_EXPR_NE:
			ans = (l_num != r_num);
			break;

		case GB_EXPR_LE:
			ans = (l_num <= r_num);
			break;

		case GB_EXPR_LT:
			ans = (l_num <  r_num);
			break;
		default:
			g_warning ("Extra unhandled compare type added");
			break;
		}
	} else {
		GBValue *v;
		GBDouble l_num;
		GBDouble r_num;

		v = gb_value_promote (ec, gb_gtk_type_from_value (GB_VALUE_DOUBLE), l);
		if (!v)
			return FALSE;
		l_num = v->v.d;
		gb_value_destroy (v);

		v = gb_value_promote (ec, gb_gtk_type_from_value (GB_VALUE_DOUBLE), r);
		if (!v)
			return FALSE;
		r_num = v->v.d;
		gb_value_destroy (v);

		switch (t) {
		case GB_EXPR_GT:
			ans = (l_num >  r_num);
			break;

		case GB_EXPR_GE:
			ans = (l_num >= r_num);
			break;

		case GB_EXPR_EQ:
			ans = (l_num == r_num);
			break;

		case GB_EXPR_NE:
			ans = (l_num != r_num);
			break;

		case GB_EXPR_LE:
			ans = (l_num <= r_num);
			break;

		case GB_EXPR_LT:
			ans = (l_num <  r_num);
			break;
		default:
			g_warning ("Extra unhandled compare type added");
			break;
		}
	}

	*result = ans;
	return TRUE;
}

GBValue *
gb_eval_binary (GBEvalContext *ec,
		const GBValue *l, GBExprType t, const GBValue *r)
{
	GBValue *val = NULL;

	if (GB_EXPR_COMPARE (t)) {
		GBBoolean res;
		if (gb_eval_compare (ec, l, t, r, &res))
			return gb_value_new_boolean (res);
		else
			return NULL;
	}

	/* Basic type checks */
 	if (GB_EXPR_BINARY_NUMERIC (t)) {
		switch (gb_value_from_gtk_type (l->gtk_type)) {
		case GB_VALUE_NUMBER:
			break;
		default:
			switch (gb_value_from_gtk_type (r->gtk_type)) {
			case GB_VALUE_NUMBER:
				break;
			default:
				g_warning ("FIXME: Error handling ?, mixed types");
				return NULL;
			}
			break;
		}
	}

	switch (t) {

	case GB_EXPR_SUB:
		return gb_eval_sub (l, r);

	case GB_EXPR_ADD:
		return gb_eval_add (l, r);

	case GB_EXPR_MULT:
		return gb_eval_mult (l, r);

	case GB_EXPR_DIV:
		return gb_eval_div (l, r);

	case GB_EXPR_INT_DIV:
		return gb_eval_int_div (l, r);

	case GB_EXPR_POW:
		return gb_eval_pow (l, r);

	case GB_EXPR_CONCAT:
		return gb_eval_concat (l, r);

	case GB_EXPR_EQV:
		return gb_eval_eqv (l, r);

	case GB_EXPR_AND:
		return gb_eval_and (l, r);

	case GB_EXPR_OR:
		return gb_eval_or (l, r);

	case GB_EXPR_XOR:
		return gb_eval_xor (l, r);

	case GB_EXPR_IMP:
		return gb_eval_imp (l, r);

	default:
		g_warning ("Unimplemented binary op %d", t);
		break;
	}

	return val;
}

GBValue *
gb_eval_unary (GBEvalContext *ec,
	       const GBValue *v, GBExprType t)
{
	g_return_val_if_fail (v != NULL, NULL);

	switch (t) {
	case GB_EXPR_PAREN:
	case GB_EXPR_POSITIVE:
		return gb_value_copy (ec, v);

	case GB_EXPR_NEGATIVE:
		return gb_eval_neg (v);

	case GB_EXPR_NOT:
		return gb_eval_not (v);

	default:
		g_warning ("Unhandled unary type %d", t);
		return NULL;
	}
}

GBValue *
gb_eval_frx (GBEvalContext *ec,
	     const GBValue *v) {

	return gb_value_new_frx (v->v.s->str);
}

static void
add_constants (GBEvalContext *ec, GHashTable *consts)
{
	const GBBuiltinConstInt *i;

	/* Integer / long constants */
	for (i = gb_constant_table; i && i->name; i++)
		g_hash_table_insert (consts, (char *)i->name,
				     gb_value_new_long (i->value));

	/* Ugly special cases for now */
	g_hash_table_insert (consts, (char *) "vbCrLf",
			     gb_value_new_string_chars ("\r\n"));
}

static GBValue *
gb_constant_lookup (GBEvalContext *ec,
		    GBObjRef      *ref)
{
	static GHashTable *consts = NULL;
	GBValue           *val;

	if (ref->parms)
		return NULL;

	if (!consts) {
		consts = g_hash_table_new (g_str_hash, g_str_equal);
		add_constants (ec, consts);
	}

	if ((val = g_hash_table_lookup (consts, ref->name)))
		return gb_value_copy (ec, val);
	else
		return NULL;
}

GBValue *
gb_eval_context_eval (GBEvalContext *ec,
		      const GBExpr  *e)
{
	GBValue  *val   = NULL;
	gboolean  chain = FALSE;

	g_return_val_if_fail (e != NULL, NULL);
	g_return_val_if_fail (ec != NULL, NULL);

	/* Move the code here ! */
	if (GB_EXPR_BINARY (e->type)) {
		GBValue *l, *r;

		l = gb_eval_context_eval (ec, e->parm.binary.left);
		r = gb_eval_context_eval (ec, e->parm.binary.right);
		
		if (!l || !r)
			return NULL;
		else
			val = gb_eval_binary (ec, l, e->type, r);

		gb_value_destroy (l);
		gb_value_destroy (r);
	} else if (GB_EXPR_UNARY (e->type)) {
		GBValue *v;

		v = gb_eval_context_eval (ec, e->parm.unary.sub);

		if (!v)
			return NULL;
		else
			val = gb_eval_unary (ec, v, e->type);

		gb_value_destroy (v);
	} else if (e->type == GB_EXPR_VALUE) {
		switch (gb_value_from_gtk_type (e->parm.value->gtk_type)) {
		case GB_VALUE_NUMBER:
			val = gb_value_copy (ec, e->parm.value);
			break;

		case GB_VALUE_STRING:
			if ((e->parm.value->v.s->str[0] == '"')     &&
			     strstr(e->parm.value->v.s->str, "\":")    ) {
				val = gb_eval_frx (ec, e->parm.value);
			} else
				val = gb_value_copy (ec, e->parm.value);
			break;

		default:
			chain = TRUE;
			break;
		}
	} else {
		g_assert (e->type == GB_EXPR_OBJREF);

		if (!e->parm.objref ||
		    g_slist_length (e->parm.objref) > 1 ||
		    !(val = gb_constant_lookup (ec, e->parm.objref->data)))
			chain = TRUE;
	}

	if (chain) {
		GBEvalContextClass *class;

		class = GB_EVAL_CONTEXT_CLASS (GTK_OBJECT (ec)->klass);

		if (class->eval)
			val = class->eval (ec, e);
		else
			val = gb_eval_exception_firev (ec, _("Cannot evaluate "
						       "non constant expression"));
	}

	return val;	
}
