/*
 * GNOME Basic memory mapped frx stream
 *
 * Author:
 *	Frank Chiulli  (fc-linux@home.com)
 *
 * Adapted from gb-mmap-lex.c by
 *    Michael Meeks (michael@helixcode.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */
#include <ctype.h>
#include <string.h>

#include <gb.h>

#include "gb-frx.h"
#include "gb-mmap-frx.h"

#define GET_GUINT8(p)            (*((const guint8 *)(p) + 0)      )
#define GET_GUINT16(p) (guint16)((*((const guint8 *)(p) + 0)      ) | \
				 (*((const guint8 *)(p) + 1) <<  8)   )
#define GET_GUINT32(p) (guint32)((*((const guint8 *)(p) + 0)      ) | \
				 (*((const guint8 *)(p) + 1) <<  8) | \
				 (*((const guint8 *)(p) + 2) << 16) | \
				 (*((const guint8 *)(p) + 3) << 24)   )

static GtkObjectClass *frx_object_parent = NULL;


/**
 * s_getu8
 *   @frx	A GBFrx structure
 *    offset	The offset within the frx file of the unsigned integer
 **/
static guint8
s_getu8 (GBFrx *frx, const guint offset)
{
	guint8		 ans;

        GBMMapFrx	*mfrx = GB_MMAP_FRX (frx);


	if (offset <= mfrx->length)
		ans = GET_GUINT8 (mfrx->start + offset);
	else {
		g_warning ("s_get8: Attempt to read past stream EOF");
		ans = 0;
	}

	return ans;
}


/**
 * s_getu16
 *   @frx	A GBFrx structure
 *    offset	The offset within the frx file of the unsigned integer
 **/
static guint16
s_getu16 (GBFrx *frx, const guint offset)
{
	guint16		 ans;

        GBMMapFrx	*mfrx = GB_MMAP_FRX (frx);


	if ((offset + 1) <= mfrx->length)
		ans = GET_GUINT16 (mfrx->start + offset);
	else  {
		g_warning ("s_getu16: Attempt to read past stream EOF");
		ans = 0;
	}

	return ans;
}


/**
 * s_getucn
 *   @frx	A GBFrx structure
 *    offset	The offset within the frx file of the first character
 *    n         The number of characters to retrieve
 **/
static char *
s_getcn (GBFrx *frx, const guint offset, const guint n)
{
	char		*ans = NULL;

        GBMMapFrx	*mfrx = GB_MMAP_FRX (frx);


	if ((offset + n) <= mfrx->length) {
		ans = g_new (char, n + 1);
		strncpy (ans, mfrx->start + offset, n);
		ans[n] = '\0';

	} else 
		g_warning ("s_getcn: Attempt to read past stream EOF");


	return ans;
}


/**
 * gb_mmap_frx_destroy
 *   @object	Object to destroy
 **/
static void
gb_mmap_frx_destroy (GtkObject *object)
{
	GBMMapFrx *mfrx = GB_MMAP_FRX (object);

	if (mfrx && mfrx->start) {
		g_free (mfrx->start);
		mfrx->start = NULL;
	}

	frx_object_parent->destroy (object);
}


/**
 * gb_mmap_frx_class_init
 *   @klass	Klass object to initialize
 **/
static void
gb_mmap_frx_class_init (GBMMapFrxClass *klass)
{
	GBFrxClass	*frx_class;

	GtkObjectClass	*object_class;


	frx_object_parent = gtk_type_class (GB_TYPE_FRX);

	object_class          = GTK_OBJECT_CLASS (klass);
	object_class->destroy = gb_mmap_frx_destroy;
	
	frx_class = GB_FRX_CLASS (klass);

	frx_class->s_getu8  = s_getu8;
	frx_class->s_getu16 = s_getu16;
	frx_class->s_getcn  = s_getcn;
}


/**
 * gb_mmap_frx_init
 *   @mfrx	Mapped frx instance to initialize
 **/
static void
gb_mmap_frx_init (GBMMapFrx *mfrx)
{
}


/**
 * gb_mmap_frx_get_type
 **/
GtkType
gb_mmap_frx_get_type (void)
{
	static GtkType frx_type = 0;


	if (!frx_type) {
		static const GtkTypeInfo frx_info = {
			"GBMMapFrx",
			sizeof (GBMMapFrx),
			sizeof (GBMMapFrxClass),
			(GtkClassInitFunc) gb_mmap_frx_class_init,
			(GtkObjectInitFunc) gb_mmap_frx_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		frx_type = gtk_type_unique (GB_TYPE_FRX, &frx_info);
	}

	return frx_type;
}


/**
 * gb_mmap_frx_new
 *   @data	data to store in GBFrx structure
 *    length	number of characters in data
 **/
GBFrx *
gb_mmap_frx_new (char *data, unsigned length)
{
	GBMMapFrx *mfrx = GB_MMAP_FRX (gtk_type_new (GB_TYPE_MMAP_FRX));


	mfrx->start  = data;
	mfrx->length = length;

	mfrx->end    = mfrx->start + mfrx->length;
	mfrx->cur    = data;

	return GB_FRX (mfrx);
}
