/*
 * Gnome Basic controller.
 *
 * Authors:
 *     Michael Meeks (mmeeks@gnu.org)
 *     Jody Goldberg (jgoldberg@home.com)
 *
 * Copyright 2000, Helix Code, Inc.
 */
#include "config.h"

#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <gnome.h>
#include <gb/libgb.h>
#include <gb/gb-mmap-lex.h>
#include <gbrun/libgbrun.h>
#include <gbrun/gbrun-global.h>
#include <gbrun/gbrun-statement.h>
#include <gbrun/objects/libgbobj.h>

int gb_debug = 0;
poptContext ctx;

const struct poptOption gb_popts [] = {
	{ "debug", '\0', POPT_ARG_INT, &gb_debug, 0,
	  N_("Enables some debugging functions"), N_("LEVEL") },

	{ NULL, '\0', 0, NULL, 0 }
};

static GBLexerStream *
file_to_stream (const char *filename)
{
	guint8            *data;
	struct stat        sbuf;
	int                fd, len;
	GBLexerStream     *ans;

	fd = open (filename, O_RDONLY);
	if (fd < 0 || fstat (fd, &sbuf) < 0) {
		fprintf (stderr, "gb: %s : %s\n", filename, strerror (errno));

		if (fd >= 0)
			close (fd);
		return NULL;
	}

	if ((len = sbuf.st_size) <= 0) {
		fprintf (stderr, "%s : empty file\n", filename);
		close (fd);
		return NULL;
	}

	data = g_new (guint8, len);
	if (read (fd, data, len) != len) {
		fprintf (stderr, "Short read error on '%s'\n", filename);
		g_free (data);
		return NULL;
	}

	if (!isspace (data [len - 1])) {
		fprintf (stderr, "File must end in whitespace");
		g_free (data);
		return NULL;
	}

	ans = gb_mmap_stream_new (data, len);

	close (fd);

	return ans;
}

#if 0
static const GBParseData *
parse_file (const char *filename)
{
	GBLexerStream     *ls;
	const GBParseData *module;
	GBEvalContext     *ec;

	if (!(ls = file_to_stream (filename)))
		return NULL;

	ec = gb_eval_context_new ();
	module = gb_parse_stream (ec, ls);
	if (!module) {
  		fputs (gb_eval_context_get_text (ec), stderr);
  		fputs ("\n", stderr);
  	}

	gtk_object_destroy (GTK_OBJECT (ec));
	gtk_object_destroy (GTK_OBJECT (ls));

	return module;
}
#endif

static GBLexerStream *
stream_provider (GBRunEvalContext *ec,
		 const char       *name,
		 gpointer          user_data)
{
	GBLexerStream *ls;

	ls = file_to_stream (name);
	if (!ls) {
		gb_eval_exception_firev (GB_EVAL_CONTEXT (ec),
					 "no such file '%s'", name);
		return NULL;
	}

	return ls;
}

static void
exec_project (const char *filename)
{
	GBLexerStream    *ls;
	GBProject        *gb_proj;
	GBRunEvalContext *ec;
	GBRunProject     *proj;

	if (!(ls = file_to_stream (filename)))
		return;

	ec = gbrun_eval_context_new (filename, GBRUN_SEC_NONE);
	gb_proj = gb_project_new (GB_EVAL_CONTEXT (ec), ls);
	if (!gb_proj) {
  		fputs (gb_eval_context_get_text (GB_EVAL_CONTEXT (ec)), stderr);
  		fputs ("\n", stderr);
  	}

	proj = gbrun_project_new (ec, gb_proj, stream_provider, NULL);

	if (!proj || !gbrun_project_execute (ec, proj))
		g_warning ("%s", gb_eval_context_get_text (GB_EVAL_CONTEXT (ec)));

	if (proj)
		gtk_object_unref (GTK_OBJECT (proj));
	gtk_object_unref (GTK_OBJECT (ec));
	gtk_object_unref (GTK_OBJECT (ls));
}

static gchar *
get_to_asp_term (GBLexerStream *ls)
{
	GString *str = g_string_new ("");
	char *s;
	char  c;
	
	while (!gb_lexer_stream_eof (ls)) {
		c = gb_lexer_stream_getc (ls);

		if (c == '%' && gb_lexer_stream_peek (ls) == '>') {
			c = gb_lexer_stream_getc (ls);
			goto ret_str;
		}
		
		g_string_append_c (str, c);
	}

 ret_str:
	s = str->str;
	g_string_free (str, FALSE);

	return s;
}

static GString *
output_writes_for (GString *output, GString *html)
{
	if (!html)
		return NULL;

	g_string_sprintfa (output, "Response.Write \"%s\"\n", html->str);

	return NULL;
}

static GBLexerStream *
hack_preprocess_asp (GBLexerStream *ls)
{
	GBLexerStream *out;
	char c;
	GString *str, *html = NULL;

	str = g_string_new ("");

	while (!gb_lexer_stream_eof (ls)) {
		c = gb_lexer_stream_getc (ls);

		if (c == '<' && gb_lexer_stream_peek (ls) == '%') {
			char *chunk;

			html = output_writes_for (str, html);

			c = gb_lexer_stream_getc (ls);

			if (gb_lexer_stream_peek (ls) == '=') {
				c = gb_lexer_stream_getc (ls);
				chunk = get_to_asp_term (ls);
				g_string_sprintfa (str, "Response.Write %s\n",
						   chunk);
				g_free (chunk);
			} else if (gb_lexer_stream_peek (ls) == '@') {
				/* Ignore it */
				c = gb_lexer_stream_getc (ls);
				chunk = get_to_asp_term (ls);
				g_free (chunk);
			} else { /* Standard VBScript */
				chunk = get_to_asp_term (ls);
				g_string_append (str, chunk);
				g_free (chunk);
				g_string_append_c (str, '\n');
			}
		} else {
			if (c == '\n')
				html = output_writes_for (str, html);
			else {
				if (!html)
					html = g_string_new ("");
				g_string_append_c (html, c);
			}
		}
	}

	html = output_writes_for (str, html);

/*	g_warning ("Preproccesed to '%s'", str->str);*/

	out = gb_mmap_stream_new (str->str, strlen (str->str));
	g_string_free (str, FALSE);

	gtk_object_unref (GTK_OBJECT (ls));

	return out;
}

static GBValue *
gbrun_sub_write (GBRunEvalContext *ec,
		 GBRunObject      *object,
		 GSList           *expr)
{
	FILE *stream = stdout;

	for (; expr; expr = expr->next) {
		GBValue *v;

		v = gbrun_eval_as (ec, expr->data, GB_VALUE_STRING);
		if (!v)
			return NULL;

		fprintf (stream, "%s", v->v.s->str);

		gb_value_destroy (v);
	}

	fprintf (stream, "\n");

	return gb_value_new_empty ();
}

static void
setup_response_object (GBRunEvalContext *ec)
{
	GBRunObject      *gba_object;
	GBRunObjectClass *gba;

	gba_object = gtk_type_new (
		gbrun_object_subclass_simple (GBRUN_TYPE_OBJECT, "response"));
	gbrun_global_add (GB_OBJECT (gba_object), "response");

	gba = GBRUN_OBJECT_GET_CLASS (gba_object);

	gbrun_object_add_method_var (gba, "sub;write;...;n", gbrun_sub_write);
}

/*
 *   It seems we can preprocess this with a quick evil hack,
 * instead of doing work in the grammer / lexer.
 */
static void
exec_asp (const char *filename)
{
	GBLexerStream     *ls;
	GBRunEvalContext  *ec;
	const GBParseData *pd;

	if (!(ls = file_to_stream (filename)))
		return;

	ls = hack_preprocess_asp (ls);

	ec = gbrun_eval_context_new (filename, GBRUN_SEC_NONE);

	gb_lexer_stream_state_set (ls, GB_PARSING_STATEMENTS);

	pd = gb_parse_stream (GB_EVAL_CONTEXT (ec), ls);
	if (!pd)
		g_error ("An exception occured\n%s",
			   gb_eval_context_get_text (GB_EVAL_CONTEXT (ec)));

	setup_response_object (ec);

	if (pd->stmts)
		gbrun_stmts_evaluate (ec, pd->stmts);
	else
		g_error ("No statements");

	if (gbrun_eval_context_exception (ec))
		g_warning ("An exception occured\n%s",
			   gb_eval_context_get_text (GB_EVAL_CONTEXT (ec)));

	gtk_object_unref (GTK_OBJECT (ec));
	gtk_object_unref (GTK_OBJECT (ls));
}

int
main (int argc, char * argv[])
{
	const char **args;

	free (malloc (8)); /* kick -lefence */

	gnome_init_with_popt_table (PACKAGE, VERSION, argc, argv,
				    gb_popts, 0, &ctx);
	gb_init ();

	{ /* Setup the runtime */
		GBEvalContext *ec;

		ec = gb_eval_context_new ();

		gbrun_init (ec);

		if (gb_eval_exception (ec)) {
			g_warning ("%s", gb_eval_context_get_text (ec));
			gtk_object_unref (GTK_OBJECT (ec));
			return -1;
		}
		gtk_object_unref (GTK_OBJECT (ec));
	}

	args = poptGetArgs (ctx);
	if (args) {
		int i;

		for (i = 0; args [i]; i++) {
#if 0
			const GBParseData *pd;
			GBRunEvalContext  *ec;
			GBRunObject       *obj;
#endif
			char              *s;

			if ((s = strrchr (args [i], '.')) &&
			    !g_strcasecmp (s + 1, "asp"))
				exec_asp (args [i]);

			else if ((s = strrchr (args [i], '.')) &&
				 !g_strcasecmp (s + 1, "vbp"))
				exec_project (args [i]);

			else {
			        if (s) { /* s already == strrchr (args [i], '.') */
				        int diff_len = strlen (args [i]) - strlen (s);
					char *str1   = g_strndup (args [i], diff_len);
					char *str2   = g_strconcat (str1, ".vbp", NULL);
					
					exec_project (str2);
					
					g_free (str2);
					g_free (str1);
				} else {
					char *str1 = g_strconcat (args [i], ".vbp", NULL);
					
					exec_project (str1);
					
					g_free (str1);
				}
			}

#if 0			
			pd = parse_file (args [i]);
			if (!pd)
				continue;

			obj = gbrun_object_from_data (ec, "Module", pd);

			if (args [i + 1]) {
				GBValue *val =
					gbrun_routine (ec, obj, args [i + 1], NULL);
				if (!val)
					g_warning ("An exception occured\n%s",
						   gb_eval_context_get_text (GB_EVAL_CONTEXT (ec)));
				else {
					fprintf (stdout, "%s returned ", args [i + 1]);
					gb_value_print (stdout, val);
					fprintf (stdout, "\n");

					gb_value_destroy (val);
				}
				i++;
			} else {
				if (obj && GBRUN_IS_FORM (obj))
					gbrun_context_exec (ec, obj);
				else
					g_warning ("Can't decide what to do with this object");
			}

			gtk_object_unref (GTK_OBJECT (ec));
			gb_object_unref (obj);
			gb_parse_data_destroy (pd);
			
			if (!args [i])
				break;
#endif
		}

	} else { /* No args */
		GBRunEvalContext *ec;
		const char test [] = 
			"print \"*Testing string compilation*\"\n"
			"Dim j As Long\n"
			"Dim i As Long\n" 
			"j = 1\n"
			"For i = 1 To 6\n"
			"	j = j * i\n"
			"Next i\n"
			"print \"6 factorial = \", j";

		ec = gbrun_eval_context_new ("builtin", GBRUN_SEC_NONE);

		gbrun_exec_str (ec, NULL, test);

		gtk_object_unref (GTK_OBJECT (ec));
	}

	gbrun_shutdown ();

	poptFreeContext (ctx);

	gb_shutdown ();

	return 0;
}

