VERSION 5.00
Begin VB.Form frmToolBar 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Controls"
   ClientHeight    =   6345
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   1650
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6345
   ScaleWidth      =   1650
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdTool 
      Caption         =   "ListBox"
      Height          =   495
      Index           =   9
      Left            =   120
      TabIndex        =   9
      Top             =   5760
      Width           =   1400
   End
   Begin VB.CommandButton cmdTool 
      Caption         =   "ComboBox"
      Height          =   495
      Index           =   8
      Left            =   120
      TabIndex        =   8
      Top             =   5160
      Width           =   1400
   End
   Begin VB.CommandButton cmdTool 
      Caption         =   "Pointer"
      Default         =   -1  'True
      Height          =   495
      Index           =   0
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   1400
   End
   Begin VB.CommandButton cmdTool 
      Caption         =   "CheckBox"
      Height          =   495
      Index           =   6
      Left            =   120
      TabIndex        =   6
      Top             =   3960
      Width           =   1400
   End
   Begin VB.CommandButton cmdTool 
      Caption         =   "CommandButton"
      Height          =   495
      Index           =   5
      Left            =   120
      TabIndex        =   5
      Top             =   3360
      Width           =   1400
   End
   Begin VB.CommandButton cmdTool 
      Caption         =   "Frame"
      Height          =   495
      Index           =   4
      Left            =   120
      TabIndex        =   4
      Top             =   2760
      Width           =   1400
   End
   Begin VB.CommandButton cmdTool 
      Caption         =   "TextBox"
      Height          =   495
      Index           =   3
      Left            =   120
      TabIndex        =   3
      Top             =   2160
      Width           =   1400
   End
   Begin VB.CommandButton cmdTool 
      Caption         =   "Label"
      Height          =   495
      Index           =   2
      Left            =   120
      TabIndex        =   2
      Top             =   1560
      Width           =   1400
   End
   Begin VB.CommandButton cmdTool 
      Caption         =   "Picture"
      Height          =   495
      Index           =   1
      Left            =   120
      TabIndex        =   1
      Top             =   960
      Width           =   1400
   End
   Begin VB.CommandButton cmdTool 
      Caption         =   "RadioButton"
      Height          =   495
      Index           =   7
      Left            =   120
      TabIndex        =   0
      Top             =   4560
      Width           =   1400
   End
End
Attribute VB_Name = "frmToolBar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' GB Designer
' (c) Per.Winkvist@UK.COM, 2000
' Distributed under the terms of GPL v2 or later
'

Private iCurrentTool As Integer
Private Tools() As ToolType

Private Type ToolType
    Count As Integer
    Type As String
    Name As String
    Property As String
End Type

Public Property Get ControlCount() As Integer
    ControlCount = Tools(iCurrentTool).Count
End Property

Public Property Get ControlType() As String
    ControlType = Tools(iCurrentTool).Type
End Property

Public Property Get ControlName() As String
    ControlName = Tools(iCurrentTool).Name & Tools(iCurrentTool).Count
End Property

Public Property Get ControlProperty() As String
    ControlProperty = Tools(iCurrentTool).Property
End Property

Public Sub IncreaseControlCount()
    Tools(iCurrentTool).Count = Tools(iCurrentTool).Count + 1
End Sub

Public Function GetProperty(ByRef iTool As Integer)
    GetProperty = Tools(iTool).Property
End Function

Public Property Get ToolIndex()
    ToolIndex = iCurrentTool
End Property

Public Sub SetPointer()
    cmdTool_Click (0)
End Sub

Private Sub cmdTool_Click(Index As Integer)
    iCurrentTool = Index
    
    If (cmdTool(iCurrentTool).Caption = "Pointer") Then
        frmDesign.MousePointer = vbDefault '0
    Else
        frmDesign.MousePointer = vbCrosshair '2
    End If
End Sub

Private Sub SetupToolBar()
    Dim i As Integer

    ReDim Tools(cmdTool.Count)

    Tools(0).Name = "Pointer"
    Tools(0).Type = ""
    Tools(0).Property = ""

    Tools(1).Name = "Picture"
    Tools(1).Type = "PictureBox"
    Tools(1).Property = "Top Left Width Height "

    Tools(2).Name = "Label"
    Tools(2).Type = "Label"
    Tools(2).Property = "Top Left Width Height Caption"

    Tools(3).Name = "TextBox"
    Tools(3).Type = "TextBox"
    Tools(3).Property = "Top Left Width Height Text"

    Tools(4).Name = "Frame"
    Tools(4).Type = "Frame"
    Tools(4).Property = "Top Left Width Height Caption"

    Tools(5).Name = "Button"
    Tools(5).Type = "CommandButton"
    Tools(5).Property = "Top Left Width Height Caption"

    Tools(6).Name = "CheckBox"
    Tools(6).Type = "CheckBox"
    Tools(6).Property = "Top Left Width Height Caption"

    Tools(7).Name = "RadioButton"
    Tools(7).Type = "OptionButton"
    Tools(7).Property = "Top Left Width Height Caption"

    Tools(8).Name = "ComboBox"
    Tools(8).Type = "ComboBox"
    Tools(8).Property = "Top Left Width Text List"   ' ComboBox has "Height" as read-only

    Tools(9).Name = "ListBox"
    Tools(9).Type = "ListBox"
    Tools(9).Property = "Top Left Width Height List"

    For i = 1 To cmdTool.Count - 1
        Tools(i).Count = 1
        cmdTool.Item(i).Caption = Tools(i).Name
    Next i
End Sub


Private Sub Form_Load()
    SetupToolBar
    iCurrentTool = 0
End Sub
