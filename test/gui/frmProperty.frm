VERSION 5.00
Begin VB.Form frmProperty 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Properties"
   ClientHeight    =   5775
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3045
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5775
   ScaleWidth      =   3045
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtProperty 
      Height          =   285
      Index           =   8
      Left            =   960
      TabIndex        =   17
      Tag             =   "Tag"
      Top             =   4440
      Width           =   2000
   End
   Begin VB.CommandButton cmdUpdate 
      Caption         =   "&Update"
      Height          =   375
      Left            =   1440
      TabIndex        =   16
      Top             =   5280
      Width           =   1455
   End
   Begin VB.TextBox txtProperty 
      Height          =   1725
      Index           =   7
      Left            =   960
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   15
      Tag             =   "List"
      Top             =   2640
      Width           =   2000
   End
   Begin VB.TextBox txtProperty 
      Height          =   285
      Index           =   6
      Left            =   960
      TabIndex        =   14
      Tag             =   "Text"
      Top             =   2280
      Width           =   2000
   End
   Begin VB.TextBox txtProperty 
      Height          =   285
      Index           =   5
      Left            =   960
      TabIndex        =   13
      Tag             =   "Caption"
      Top             =   1920
      Width           =   2000
   End
   Begin VB.TextBox txtProperty 
      Height          =   285
      Index           =   4
      Left            =   960
      TabIndex        =   12
      Tag             =   "Width"
      Top             =   1560
      Width           =   2000
   End
   Begin VB.TextBox txtProperty 
      Height          =   285
      Index           =   3
      Left            =   960
      TabIndex        =   11
      Tag             =   "Height"
      Top             =   1200
      Width           =   2000
   End
   Begin VB.TextBox txtProperty 
      Height          =   285
      Index           =   2
      Left            =   960
      TabIndex        =   10
      Tag             =   "Left"
      Top             =   840
      Width           =   2000
   End
   Begin VB.TextBox txtProperty 
      Height          =   285
      Index           =   1
      Left            =   960
      TabIndex        =   9
      Tag             =   "Top"
      Top             =   480
      Width           =   2000
   End
   Begin VB.TextBox txtProperty 
      Height          =   285
      Index           =   0
      Left            =   960
      TabIndex        =   8
      Tag             =   "Name"
      Top             =   120
      Width           =   2000
   End
   Begin VB.Label lblProperty 
      Caption         =   "Tag"
      Height          =   255
      Index           =   8
      Left            =   120
      TabIndex        =   18
      Tag             =   "List"
      Top             =   4440
      Width           =   735
   End
   Begin VB.Label lblProperty 
      Caption         =   "List items"
      Height          =   255
      Index           =   7
      Left            =   120
      TabIndex        =   7
      Tag             =   "List"
      Top             =   2640
      Width           =   735
   End
   Begin VB.Label lblProperty 
      Caption         =   "Text"
      Height          =   255
      Index           =   6
      Left            =   120
      TabIndex        =   6
      Tag             =   "Text"
      Top             =   2280
      Width           =   495
   End
   Begin VB.Label lblProperty 
      Caption         =   "Caption"
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   5
      Tag             =   "Caption"
      Top             =   1920
      Width           =   735
   End
   Begin VB.Label lblProperty 
      Caption         =   "Name"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   4
      Tag             =   "Name"
      Top             =   120
      Width           =   615
   End
   Begin VB.Label lblProperty 
      Caption         =   "Height"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   3
      Tag             =   "Height"
      Top             =   1200
      Width           =   615
   End
   Begin VB.Label lblProperty 
      Caption         =   "Left"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   2
      Tag             =   "Left"
      Top             =   840
      Width           =   615
   End
   Begin VB.Label lblProperty 
      Caption         =   "Top"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   1
      Tag             =   "Top"
      Top             =   480
      Width           =   615
   End
   Begin VB.Label lblProperty 
      Caption         =   "Width"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   0
      Tag             =   "Width"
      Top             =   1560
      Width           =   615
   End
End
Attribute VB_Name = "frmProperty"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' GB Designer
' (c) Per.Winkvist@UK.COM, 2000
' Distributed under the terms of GPL v2 or later
'

Dim oCurrentControl As Object


Public Sub ViewControl(ByRef oControl As Object, ByRef sProperty As String)
    Dim i As Integer
    
    Set oCurrentControl = oControl
    
    txtProperty(0).Text = frmDesign.GetControlName(oControl.Tag)
    txtProperty(8).Text = frmDesign.GetControlTag(oControl.Tag)
    For i = 1 To 7
        If (InStr(1, sProperty, lblProperty(i).Tag, vbTextCompare)) Then
            lblProperty(i).Visible = True
            txtProperty(i).Visible = True
            If (txtProperty(i).Tag <> "List") Then
                'vbGet = 2
                txtProperty(i).Text = CallByName(oControl, txtProperty(i).Tag, VbGet)
            Else
                txtProperty(i).Text = GetList(oControl)
            End If
        Else
            lblProperty(i).Visible = False
            txtProperty(i).Visible = False
        End If
    Next i
End Sub


Private Function GetList(oControl As Object)
    Dim sList As String
    Dim i As Integer
    
    For i = 0 To oControl.ListCount - 1
        sList = sList & oControl.List(i)
    Next i
    
    GetList = sList
End Function


Private Sub SetList(oControl As Object, sList As String)
    Dim i As Integer
    Dim j As Integer
    Dim sItem As String
    
    oControl.Clear
    i = 1
    ' vbcrlf = chr(13) + chr(10)
    ' vbcr = chr(13)
    ' vbTextCompare = 1
    j = InStr(i, sList, vbCrLf, vbTextCompare)
    While (j > 0)
        sItem = Mid(sList, i, j - i)
        oControl.AddItem sItem
        
        i = j + 2
        j = InStr(i, sList, vbCrLf, vbTextCompare)
    Wend
End Sub


Private Sub cmdUpdate_Click()
    Dim i As Integer
    Dim oControl As Object
    
    Set oControl = oCurrentControl
    
    ' Yehh... Do it backwards instead !
    ' When setting List Items for ComboBox we use .Clear
    ' This also removes the visible text which needs to be added afterwards then :)
    For i = 7 To 1 Step -1
        If (txtProperty(i).Visible) Then
            If (txtProperty(i).Tag <> "List") Then
                'vbLet = 4
                Call CallByName(oControl, txtProperty(i).Tag, VbLet, txtProperty(i).Text)
            Else
                Call SetList(oControl, txtProperty(i).Text)
            End If
        End If
    Next i
        
    Call frmDesign.SetControlName(oControl.Tag, txtProperty(0).Text)
    Call frmDesign.SetControlTag(oControl.Tag, txtProperty(8).Text)

End Sub
