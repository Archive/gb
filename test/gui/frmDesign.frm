VERSION 5.00
Begin VB.Form frmDesign 
   Caption         =   "GB Designer"
   ClientHeight    =   6165
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   7365
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   411
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   491
   StartUpPosition =   2  'CenterScreen
   Tag             =   "0"
   Begin VB.ListBox ListBoxControl 
      Height          =   255
      Index           =   0
      ItemData        =   "frmDesign.frx":0000
      Left            =   120
      List            =   "frmDesign.frx":0007
      TabIndex        =   8
      Top             =   4080
      Width           =   1455
      Visible         =   0   'False
   End
   Begin VB.ComboBox ComboBoxControl 
      Height          =   315
      Index           =   0
      Left            =   120
      TabIndex        =   7
      Text            =   "ComboBoxControl"
      Top             =   3600
      Width           =   1455
      Visible         =   0   'False
   End
   Begin VB.OptionButton OptionButtonControl 
      Caption         =   "OptionButtonControl"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   6
      Top             =   3120
      Width           =   1815
      Visible         =   0   'False
   End
   Begin VB.CheckBox CheckBoxControl 
      Caption         =   "CheckBoxControl"
      Height          =   375
      Index           =   0
      Left            =   120
      TabIndex        =   5
      Top             =   2520
      Width           =   1095
      Visible         =   0   'False
   End
   Begin VB.Frame FrameControl 
      Caption         =   "FrameControl"
      Height          =   375
      Index           =   0
      Left            =   120
      TabIndex        =   4
      Top             =   1560
      Width           =   1575
      Visible         =   0   'False
   End
   Begin VB.TextBox TextBoxControl 
      Height          =   375
      Index           =   0
      Left            =   120
      TabIndex        =   3
      Text            =   "TextBoxControl"
      Top             =   1080
      Width           =   1575
      Visible         =   0   'False
   End
   Begin VB.PictureBox PictureBoxControl 
      Height          =   255
      Index           =   0
      Left            =   120
      ScaleHeight     =   195
      ScaleWidth      =   1755
      TabIndex        =   1
      Top             =   360
      Width           =   1815
      Visible         =   0   'False
   End
   Begin VB.CommandButton CommandButtonControl 
      Caption         =   "CommandButtonControl"
      Height          =   375
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   2040
      Width           =   1815
      Visible         =   0   'False
   End
   Begin VB.Label LabelControl 
      Caption         =   "LabelControl"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   2
      Top             =   720
      Width           =   1575
      Visible         =   0   'False
   End
   Begin VB.Shape ToolFrame 
      Height          =   135
      Left            =   240
      Tag             =   "00"
      Top             =   120
      Width           =   6735
      Visible         =   0   'False
   End
   Begin VB.Menu menuFile 
      Caption         =   "&File"
      Begin VB.Menu menuitemLoad 
         Caption         =   "&Load"
      End
      Begin VB.Menu menuItemSave 
         Caption         =   "&Save"
      End
      Begin VB.Menu menuitemQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu menuEdit 
      Caption         =   "&Edit"
   End
   Begin VB.Menu menuView 
      Caption         =   "&View"
      Begin VB.Menu menuitemViewToolBar 
         Caption         =   "View &Toolbar"
      End
      Begin VB.Menu menuitemViewPropertyEditor 
         Caption         =   "View &Property Editor"
      End
   End
End
Attribute VB_Name = "frmDesign"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' GB Designer
' (c) Per.Winkvist@UK.COM, 2000
' Distributed under the terms of GPL v2 or later
'

Const MIN_CONTROL_WIDTH = 100
Const MIN_CONTROL_HEIGHT = 20
Const CONTROL_PREFIX = "VB."
Const TWIPS_PER_PIXEL_X = 15
Const TWIPS_PER_PIXEL_Y = 15

Dim arrControlProperty() As ControlPropertyType

Private Type ControlPropertyType
    Name As String
    Tag As String
    Type As String
    ToolID As Integer
    ControlIndex As Integer
    Deleted As Boolean
End Type

Private Sub MoveControlByKey(Key As Integer)
    If (Key = vbKeyUp) Then ' 38
        If (ActiveControl.Top < 0) Then
            ActiveControl.Top = 0
        Else
            ActiveControl.Top = ActiveControl.Top - 1
        End If
    End If
    
    If (Key = vbKeyDown) Then ' 40
        ActiveControl.Top = ActiveControl.Top + 1
    End If
    
    If (Key = vbKeyLeft) Then   '37
        If (ActiveControl.Left < 0) Then
            ActiveControl.Left = 0
        Else
            ActiveControl.Left = ActiveControl.Left - 1
        End If
    End If
    
    If (Key = vbKeyRight) Then ' 39
        ActiveControl.Left = ActiveControl.Left + 1
    End If

End Sub

Private Sub Form_Click()
    Call frmProperty.ViewControl(frmDesign, "Caption Width Height")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    Call MoveControlByKey(KeyAscii)
End Sub

Private Sub Form_Load()
    'frmToolBar.Left = frmDesign.Left - frmToolBar.Width - 10
    frmToolBar.Show
    frmProperty.Show
    
    ReDim arrControlProperty(1)
    arrControlProperty(0).ControlIndex = 1
    arrControlProperty(0).Name = "frmMain"
    arrControlProperty(0).ToolID = 0
    arrControlProperty(0).Type = CONTROL_PREFIX & "Form"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim sProperty As String
    Dim iTag As Integer
    
    If Not (Screen.ActiveControl Is Nothing) Then
        
        ' Control is selected and will be deleted
        If (KeyCode = vbKeyDelete) Then 'vbKeyDelete = 46
            iTag = Screen.ActiveControl
            Unload Screen.ActiveControl
            arrControlProperty(iTag).Deleted = True
        End If
    
    End If
    
    If (KeyCode = vbKeyEscape) Then 'vbKeyEscape = 27
        frmToolBar.SetPointer
    End If
        
    Call MoveControlByKey(KeyCode)
    
End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If (frmToolBar.ControlType = "") Then ' Pointer control selected
        Exit Sub
    End If
    
    With ToolFrame
        .Left = X
        .Top = Y
        .Width = 0
        .Height = 0
        .Visible = True
    End With
End Sub

Private Sub Form_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim oTool As Object
    
    ToolFrame.Visible = False
    If (frmToolBar.ControlType <> "") Then
        If (ToolFrame.Width < MIN_CONTROL_WIDTH) Then ToolFrame.Width = MIN_CONTROL_WIDTH
        If (ToolFrame.Height < MIN_CONTROL_HEIGHT) Then ToolFrame.Height = MIN_CONTROL_HEIGHT
        
        Call AddControl(frmToolBar.ControlType, frmToolBar.ControlName, frmToolBar.ControlProperty)
        
        frmToolBar.IncreaseControlCount
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call menuitemQuit_Click
End Sub

Private Sub menuitemLoad_Click()
    Call LoadFile("c:\testform.frm")
End Sub

Private Sub menuitemQuit_Click()
    ' End
End Sub

Private Sub menuItemSave_Click()
    Call SaveFile("c:\testform.frm")
    Call MsgBox("File was saved to c:\testform.frm")
End Sub

Private Sub menuitemViewPropertyEditor_Click()
    If (frmProperty.Visible) Then
        frmProperty.Hide
    Else
        frmProperty.Show
    End If
End Sub

Private Sub menuitemViewToolBar_Click()
    If (frmToolBar.Visible) Then
        frmToolBar.Hide
    Else
        frmToolBar.Show
    End If
End Sub

Private Sub ResizeToolBarFrame(MouseX As Single, MouseY As Single)
    Dim X As Integer
    Dim Y As Integer
    Dim X2 As Integer
    Dim Y2 As Integer
    Dim OldX2 As Integer
    Dim OldY2 As Integer
    Dim bSetX As Boolean
    Dim bSetY As Boolean
    Dim bNegX As Boolean
    Dim bNegY As Boolean
    
    With ToolFrame
        OldX2 = .Width + .Left
        OldY2 = .Top + .Height
        bSetX = True
        bSetY = True
        bNegX = (Left(.Tag, 1) = "1")
        bNegY = (Right(.Tag, 1) = "1")
        
        If (MouseX > .Left And bNegX And MouseX < OldX2) Then
            X = MouseX
            X2 = OldX2
        ElseIf (MouseX > .Left) Then
            If (bNegX) Then
                X = OldX2
                .Tag = "0" + Right(.Tag, 1)
            Else
                X = .Left
            End If
            X2 = MouseX
        ElseIf (MouseX < .Left) Then
            X = MouseX
            If (bNegX) Then
                X2 = OldX2
            Else
                X2 = .Left
                .Tag = "1" + Right(.Tag, 1)
            End If
        Else
            bSetX = False
        End If
        
        If (MouseY > .Top And bNegY And MouseY < OldY2) Then
            Y = MouseY
            Y2 = OldY2
        ElseIf (MouseY > .Top) Then
            If (bNegY) Then
                Y = OldY2
                .Tag = Left(.Tag, 1) + "0"
            Else
                Y = .Top
            End If
            Y2 = MouseY
        ElseIf (MouseY < .Top) Then
            Y = MouseY
            If (bNegY) Then
                Y2 = OldY2
            Else
                Y2 = .Top
                .Tag = Left(.Tag, 1) + "1"
            End If
        Else
            bSetY = False
        End If
    
        If (bSetX) Then
            .Left = X
            .Width = X2 - X
        End If
        
        If (bSetY) Then
            .Top = Y
            .Height = Y2 - Y
        End If
    End With
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    If (Button = vbLeftButton) Then
        Call ResizeToolBarFrame(X, Y)
    End If
End Sub

Private Function GetControl(sControltype As String) As Object
    Select Case sControltype
        Case CONTROL_PREFIX & "PictureBox"
            Set GetControl = PictureBoxControl
        Case CONTROL_PREFIX & "Label"
            Set GetControl = LabelControl
        Case CONTROL_PREFIX & "TextBox"
            Set GetControl = TextBoxControl
        Case CONTROL_PREFIX & "Frame"
            Set GetControl = FrameControl
        Case CONTROL_PREFIX & "CommandButton"
            Set GetControl = CommandButtonControl
        Case CONTROL_PREFIX & "CheckBox"
            Set GetControl = CheckBoxControl
        Case CONTROL_PREFIX & "OptionButton"
            Set GetControl = OptionButtonControl
        Case CONTROL_PREFIX & "ComboBox"
            Set GetControl = ComboBoxControl
        Case CONTROL_PREFIX & "ListBox"
            Set GetControl = ListBoxControl
        Case Else
            Call MsgBox("Control Type : " & sControltype & "was not found!")
            Exit Function
    End Select

End Function

Private Sub AddControl(sControltype As String, sControlName As String, sProperty As String)
    Dim oControl As Object
    Dim i As Integer
    Dim j As Integer
    
    Set oControl = GetControl(CONTROL_PREFIX & sControltype)
        
    j = UBound(arrControlProperty)
    ReDim Preserve arrControlProperty(j + 1)
    
    i = oControl.UBound + 1
    Load oControl(i)
    ' vbTextCompare = 1
    If (InStr(1, sProperty, "left", vbTextCompare)) Then oControl(i).Left = ToolFrame.Left
    If (InStr(1, sProperty, "Top", vbTextCompare)) Then oControl(i).Top = ToolFrame.Top
    If (InStr(1, sProperty, "Width", vbTextCompare)) Then oControl(i).Width = ToolFrame.Width
    If (InStr(1, sProperty, "Height", vbTextCompare)) Then oControl(i).Height = ToolFrame.Height
    If (InStr(1, sProperty, "Caption", vbTextCompare)) Then oControl(i).Caption = sControlName
    If (InStr(1, sProperty, "List", vbTextCompare)) Then oControl(i).List(0) = sControlName
    If (InStr(1, sProperty, "Text", vbTextCompare)) Then oControl(i).Text = sControlName
        
    arrControlProperty(j).Name = sControlName
    arrControlProperty(j).ToolID = frmToolBar.ToolIndex
    arrControlProperty(j).Type = CONTROL_PREFIX & sControltype
    arrControlProperty(j).ControlIndex = i
    arrControlProperty(j).Deleted = False
    
    oControl(i).MousePointer = vbSizeAll ' vbSizeAll = 15 (Move Pointer like 'X')
    oControl(i).Tag = j 'Store the index in the arrControlPropery as the control's tag
    oControl(i).Visible = True
End Sub

Private Sub ViewProperty(oControl As Object)
    Dim sProperty As String
    
    sProperty = frmToolBar.GetProperty(GetControlID(oControl.Tag))
    Call frmProperty.ViewControl(oControl, sProperty)
End Sub

Public Function GetControlName(iIndex As Integer) As String
    GetControlName = arrControlProperty(iIndex).Name
End Function

Public Sub SetControlName(iIndex As Integer, sNewName As String)
    arrControlProperty(iIndex).Name = sNewName
End Sub


Public Function GetControlType(iIndex As Integer) As String
    GetControlType = arrControlProperty(iIndex).Type
End Function

Public Sub SetControlType(iIndex As Integer, sNewType As String)
    arrControlProperty(iIndex).Type = sNewType
End Sub


Public Function GetControlTag(iIndex As Integer) As String
    GetControlTag = arrControlProperty(iIndex).Tag
End Function

Public Sub SetControlTag(iIndex As Integer, sNewTag As String)
    arrControlProperty(iIndex).Tag = sNewTag
End Sub

Public Function GetControlID(iIndex As Integer) As Integer
    GetControlID = arrControlProperty(iIndex).ToolID
End Function


' ###########################################################
' Load/Save routines.
'

Public Sub WriteForm(sFile As String)
    Print #1, "VERSION 5.00"
    Print #1, "Begin VB.Form " & arrControlProperty(0).Name
    Print #1, " Caption = """ & frmDesign.Caption & """"
    Print #1, " ClientHeight = " & frmDesign.Height
    Print #1, " ClientLeft = " & frmDesign.Left
    Print #1, " ClientTop = " & frmDesign.Top
    Print #1, " ClientWidth = " & frmDesign.Width
    Print #1, " ScaleHeight = " & frmDesign.ScaleHeight
    Print #1, " ScaleMode = " & frmDesign.ScaleMode
    Print #1, " ScaleWidth = " & frmDesign.ScaleWidth
End Sub


Public Sub WriteAttribute(sFile As String)
    Print #1, "Attribute VB_Name = """ & arrControlProperty(0).Name & """"
    Print #1, "Attribute VB_GlobalNameSpace = False"
    Print #1, "Attribute VB_Creatable = False"
    Print #1, "Attribute VB_PredeclaredId = True"
    Print #1, "Attribute VB_Exposed = False"
End Sub

Public Sub WriteControl(sFile As String)
    Dim i As Integer
    Dim Max As Integer
    Dim oControl As Object
    Dim sProperty As String
            
    Max = UBound(arrControlProperty)
    ' 0 = The form that is saved in WriteForm
    For i = 1 To Max - 1
        If (arrControlProperty(i).Deleted = False) Then
            ' Set oControl = GetControl(arrControlProperty(i).Type)(arrControlProperty(i).ControlIndex)
            sProperty = frmToolBar.GetProperty(arrControlProperty(i).ToolID)
            
            Print #1, " Begin " & arrControlProperty(i).Type & " " & arrControlProperty(i).Name
            If (InStr(1, sProperty, "Caption", vbTextCompare)) Then Print #1, "  Caption = """ & oControl.Caption & """"
            If (InStr(1, sProperty, "Height", vbTextCompare)) Then Print #1, "  Height = " & oControl.Height * TWIPS_PER_PIXEL_Y
            If (InStr(1, sProperty, "left", vbTextCompare)) Then Print #1, "  Left = " & oControl.Left * TWIPS_PER_PIXEL_X
            If (InStr(1, sProperty, "Top", vbTextCompare)) Then Print #1, "  Top = " & oControl.Top * TWIPS_PER_PIXEL_Y
            If (InStr(1, sProperty, "Width", vbTextCompare)) Then Print #1, "  Width = " & oControl.Width * TWIPS_PER_PIXEL_X
            If (InStr(1, sProperty, "Text", vbTextCompare)) Then Print #1, "  Text = """ & oControl.Text & """"
            'If (InStr(1, sProperty, "List", vbTextCompare)) Then Print #1, "  List = " & oControl.List
            Print #1, " End"
        End If
    Next i
End Sub


Public Sub SaveFile(sFile As String)
    Open sFile For Output As #1
    
    Call WriteForm(sFile)
    Call WriteControl(sFile)
    Print #1, "End"             ' Form End tag
    Call WriteAttribute(sFile)
    
    Close #1
End Sub


Public Sub LoadFile(sFile As String)
    MsgBox ("Yehh... Feel free to implement this easy thing!")
End Sub



' ###########################################################
' Click Events for all controls
'

Private Sub PictureBoxControl_Click(Index As Integer)
    Call ViewProperty(PictureBoxControl(Index))
End Sub

Private Sub LabelControl_Click(Index As Integer)
    Call ViewProperty(LabelControl(Index))
End Sub

Private Sub TextBoxControl_Click(Index As Integer)
    Call ViewProperty(TextBoxControl(Index))
End Sub

Private Sub FrameControl_Click(Index As Integer)
    Call ViewProperty(FrameControl(Index))
End Sub

Private Sub CommandButtonControl_Click(Index As Integer)
    Call ViewProperty(CommandButtonControl(Index))
End Sub

Private Sub CheckBoxControl_Click(Index As Integer)
    Call ViewProperty(CheckBoxControl(Index))
End Sub

Private Sub OptionButtonControl_Click(Index As Integer)
    Call ViewProperty(OptionButtonControl(Index))
End Sub

Private Sub ComboBoxControl_Click(Index As Integer)
    Call ViewProperty(ComboBoxControl(Index))
End Sub

Private Sub ComboBoxControl_GotFocus(Index As Integer)
    Call ViewProperty(ComboBoxControl(Index))
End Sub

Private Sub ListBoxControl_Click(Index As Integer)
    Call ViewProperty(ListBoxControl(Index))
End Sub

Private Sub ListBoxControl_GotFocus(Index As Integer)
    Call ViewProperty(ListBoxControl(Index))
End Sub


