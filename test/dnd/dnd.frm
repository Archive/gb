' * Drag 'n Drop
'
' Program to test dnd functionality
'
' Author, Per Winkvist
' (c) HelixCode Inc, 2000.
'           

VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3210
   ClientLeft      =   60
   ClientTop       =   330
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3210
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdQuit 
      Caption         =   "&Quit"
      Height          =   375
      Left            =   3840
      TabIndex        =   4
      Top             =   2640
      Width           =   735
   End
   Begin VB.CommandButton cmdTarget 
      Caption         =   "Drag 'n Drop target"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   1800
      Width           =   4455
   End
   Begin VB.CommandButton cmdRightEye 
      Caption         =   "Right eye"
      DragMode        =   1  'Automatic
      Height          =   375
      Index           =   1
      Left            =   2760
      TabIndex        =   1
      Top             =   240
      Width           =   1815
   End
   Begin VB.CommandButton cmdLeftEye 
      Caption         =   "Left eye"
      DragMode        =   1  'Automatic
      Height          =   375
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   1455
   End
   Begin VB.Label lblStatus 
      Caption         =   "Nothing"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   2880
      Width           =   3255
   End
   Begin VB.Line Line2 
      X1              =   1320
      X2              =   3120
      Y1              =   1440
      Y2              =   1440
   End
   Begin VB.Line Line1 
      X1              =   2160
      X2              =   2160
      Y1              =   240
      Y2              =   1440
   End
End

Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub cmdQuit_Click()
'  End
End Sub

Private Sub cmdTarget_DragDrop(Source As Control, X As Single, Y As Single)
  If (Source.Name <> "cmdQuit") Then
    lblStatus.Caption = "Accepted the drop of " & Source.Name
  End If
End Sub

Private Sub cmdTarget_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
  lblStatus.Caption = "Dragging " & Source.Name & " over target"
End Sub
