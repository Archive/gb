' * Timer callback control
'
' Starts a timer and a callback which scrolls 
' text across the screen.
'
' Author, Per Winkvist
' (c) HelixCode Inc 2000.
'           

VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3210
   ClientLeft      =   60
   ClientTop       =   330
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3210
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Tag             =   "0"
   Begin VB.Timer tmrMoveControl 
      Interval        =   500
      Left            =   4080
      Top             =   120
   End
   Begin VB.CommandButton cmdQuit 
      Caption         =   "Quit"
      Height          =   615
      Left            =   1440
      TabIndex        =   2
      Tag             =   "0"
      Top             =   1320
      Width           =   615
   End
   Begin VB.TextBox txtInterval 
      Height          =   375
      Left            =   1440
      TabIndex        =   0
      Text            =   "10"
      Top             =   120
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "ControlSpeed :"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   1095
   End
End

Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub cmdQuit_Click()
  Unload Me
End Sub

Private Sub txtInterval_Change()
  If (IsNumeric(txtInterval.Text)) Then
    tmrMoveControl.Interval = CInt(txtInterval.Text)
  End If
End Sub

Private Sub Form_Load()
  cmdQuit.Tag = 0
  tmrMoveControl.Interval = CInt(txtInterval.Text)
End Sub

'
' Use the controls TAG property to control the movement
'
Private Sub tmrMoveControl_Timer()
' FIXME: implement the 'Tag' property ...
  
  If (cmdQuit.Tag = 0) Then
    cmdQuit.Left = cmdQuit.Left - 1
    If (cmdQuit.Left < 0 Or cmdQuit.Left = 0) Then
      cmdQuit.Left = 0
      cmdQuit.Tag = 1
    End If
  Else
    cmdQuit.Left = cmdQuit.Left + 1
    If (cmdQuit.Left + cmdQuit.Width > Form1.Width) Then
      cmdQuit.Left = Form1.Width - cmdQuit.Width
      cmdQuit.Tag = 0
    End If
  End If
End Sub
