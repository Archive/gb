'
' GNOME Basic test game
'
' Author:
'	 G Wassenaar (zaphod@zonnet.nl)
'
' Copyright 2000, Helix Code, Inc
'
' ./gb frmNumberGame.frm
'

VERSION 5.00
Begin VB.Form frmNumberGame
   Caption         =   "Guess what!"
   ClientHeight    =   3945
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3135
   LinkTopic       =   "Form1"
   ScaleHeight     =   3945
   ScaleWidth      =   3135
   StartUpPosition =   3  'Windows Default
   Begin VB.Menu mnReady
      Caption         =   "Ready"
   End
   Begin VB.Menu mnClear
      Caption         =   "Clear"
   End
   Begin VB.Menu mnNew
      Caption         =   "New"
   End
   Begin VB.Menu mnExit
      Caption         =   "Exit"
   End
   Begin VB.TextBox txtScore
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   960
      TabIndex        =   20
      Text            =   "0"
      Top             =   960
      Width           =   855
   End
   Begin VB.TextBox txtTurn
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   2400
      TabIndex        =   19
      Text            =   "1"
      Top             =   960
      Width           =   375
   End
   Begin VB.TextBox txtDigit2
      Alignment       =   2  'Center
      BeginProperty Font
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   840
      TabIndex        =   18
      Top             =   240
      Width           =   495
   End
   Begin VB.TextBox txtDigit3
      Alignment       =   2  'Center
      BeginProperty Font
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1320
      TabIndex        =   17
      Top             =   240
      Width           =   495
   End
   Begin VB.TextBox txtDigit4
      Alignment       =   2  'Center
      BeginProperty Font
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1800
      TabIndex        =   16
      Top             =   240
      Width           =   495
   End
   Begin VB.TextBox txtDigit5
      Alignment       =   2  'Center
      BeginProperty Font
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2280
      TabIndex        =   15
      Top             =   240
      Width           =   495
   End
   Begin VB.TextBox txtDigit1
      Alignment       =   2  'Center
      BeginProperty Font
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   14
      Top             =   240
      Width           =   495
   End
   Begin VB.CommandButton cmdExit
      Caption         =   "E&xit"
      Height          =   495
      Left            =   2160
      TabIndex        =   13
      Top             =   3240
      Width           =   735
   End
   Begin VB.CommandButton cmdReady
      Caption         =   "&Ready"
      Height          =   495
      Left            =   2160
      TabIndex        =   12
      Top             =   1440
      Width           =   735
   End
   Begin VB.CommandButton cmdClear
      Caption         =   "&Clear"
      Height          =   495
      Left            =   2160
      TabIndex        =   11
      Top             =   2040
      Width           =   735
   End
   Begin VB.CommandButton cmd0
      Caption         =   "0"
      Height          =   495
      Left            =   840
      TabIndex        =   9
      Top             =   3240
      Width           =   495
   End
   Begin VB.CommandButton cmd3
      Caption         =   "3"
      Height          =   495
      Left            =   1440
      TabIndex        =   8
      Top             =   2640
      Width           =   495
   End
   Begin VB.CommandButton cmd2
      Caption         =   "2"
      Height          =   495
      Left            =   840
      TabIndex        =   7
      Top             =   2640
      Width           =   495
   End
   Begin VB.CommandButton cmd1
      Caption         =   "1"
      Height          =   495
      Left            =   240
      TabIndex        =   6
      Top             =   2640
      Width           =   495
   End
   Begin VB.CommandButton cmd6
      Caption         =   "6"
      Height          =   495
      Left            =   1440
      TabIndex        =   5
      Top             =   2040
      Width           =   495
   End
   Begin VB.CommandButton cmd5
      Caption         =   "5"
      Height          =   495
      Left            =   840
      TabIndex        =   4
      Top             =   2040
      Width           =   495
   End
   Begin VB.CommandButton cmd4
      Caption         =   "4"
      Height          =   495
      Left            =   240
      TabIndex        =   3
      Top             =   2040
      Width           =   495
   End
   Begin VB.CommandButton cmd9
      Caption         =   "9"
      Height          =   495
      Left            =   1440
      TabIndex        =   2
      Top             =   1440
      Width           =   495
   End
   Begin VB.CommandButton cmd8
      Caption         =   "8"
      Height          =   495
      Left            =   840
      TabIndex        =   1
      Top             =   1440
      Width           =   495
   End
   Begin VB.CommandButton cmd7
      Caption         =   "7"
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   1440
      Width           =   495
   End
   Begin VB.CommandButton cmdNewGame
      Caption         =   "&New"
      Height          =   495
      Left            =   2160
      TabIndex        =   10
      Top             =   2640
      Width           =   735
   End
   Begin VB.Label Label2
      Caption         =   "Turn:"
      Height          =   375
      Left            =   1920
      TabIndex        =   22
      Top             =   960
      Width           =   495
   End
   Begin VB.Label Label1
      Caption         =   "Score:"
      Height          =   255
      Left            =   360
      TabIndex        =   21
      Top             =   960
      Width           =   495
   End
End
Attribute VB_Name = "frmNumberGame"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Visual Basic constants used in this form
'vbBlack = 0
'vbRed = 255
'vbGreen = 65280
'vbBlue = 16711680
'vbYesNo = 3
'vbInformation = 64

Dim mstrsRandomDigits(1 To 5) As String
' FIXME: make redim work
' Dim mstrsEnteredDigits() As String
Dim mstrsEnteredDigits(1 To 5) As String
Dim mbytEnterDigit As Byte
Dim mbytTurn As Byte

Private Sub cmd0_Click()

    EnterDigit 0
    cmd0.Enabled = False

End Sub

Private Sub cmd1_Click()

    EnterDigit 1
    cmd1.Enabled = False

End Sub

Private Sub cmd2_Click()

    EnterDigit 2
    cmd2.Enabled = False

End Sub

Private Sub cmd3_Click()

    EnterDigit 3
    cmd3.Enabled = False

End Sub

Private Sub cmd4_Click()

    EnterDigit 4
    cmd4.Enabled = False

End Sub

Private Sub cmd5_Click()

    EnterDigit 5
    cmd5.Enabled = False

End Sub

Private Sub cmd6_Click()

    EnterDigit 6
    cmd6.Enabled = False

End Sub

Private Sub cmd7_Click()

    EnterDigit 7
    cmd7.Enabled = False

End Sub

Private Sub cmd8_Click()

    EnterDigit 8
    cmd8.Enabled = False

End Sub

Private Sub cmd9_Click()

    EnterDigit 9
    cmd9.Enabled = False

End Sub

Private Sub cmdClear_Click()

' FIXME: make redim work.
'    ReDim mstrsEnteredDigits(1 To 5)
    For i = 1 To 5
        mstrsEnteredDigits(i) = 0
    Next i
    mbytEnterDigit = 1

    txtDigit1.Text = ""
    txtDigit1.ForeColor = vbBlack
    txtDigit2.Text = ""
    txtDigit2.ForeColor = vbBlack
    txtDigit3.Text = ""
    txtDigit3.ForeColor = vbBlack
    txtDigit4.Text = ""
    txtDigit4.ForeColor = vbBlack
    txtDigit5.Text = ""
    txtDigit5.ForeColor = vbBlack

    cmd0.Enabled = True
    cmd1.Enabled = True
    cmd2.Enabled = True
    cmd3.Enabled = True
    cmd4.Enabled = True
    cmd5.Enabled = True
    cmd6.Enabled = True
    cmd7.Enabled = True
    cmd8.Enabled = True
    cmd9.Enabled = True

    cmdReady.Enabled = False
    mnReady.Enabled = False

    txtTurn.Text = mbytTurn

End Sub

Private Sub cmdExit_Click()
    Unload frmNumberGame
End Sub

Private Sub cmdNewGame_Click()

    Dim bytIndex As Byte
    Dim bytArrayIndex As Byte
    Dim intRandom As Integer
    Dim fUnique As Boolean
    Dim strRandomDigit As String

    mbytTurn = 1

    Randomize

    For bytIndex = 1 To 5
        fUnique = False
        Do Until fUnique = True
' FIXME: we should accept functions with no args as variables.
'            intRandom = Int(10 * Rnd)
            intRandom = Int(10 * Rnd())
            strRandomDigit = CStr(intRandom)
' FIXME: prune this debug
            print "Digit ", intRandom, " cstr-> ", strRandomDigit
            fUnique = True
            For bytArrayIndex = 1 To 5
                If strRandomDigit = mstrsRandomDigits(bytArrayIndex) Then
                    fUnique = False
                End If
            Next
            mstrsRandomDigits(bytIndex) = strRandomDigit
        Loop
    Next

'    Call cmdClear_Click
     cmdClear_Click

' A short delay to demo hide / show
    Me.hide

    For i = 1 To 100000
    Next i

    Me.show

End Sub

Private Sub cmdReady_Click()

    Dim bytEntryIndex As Byte
    Dim bytRandomIndex As Byte
    Dim fCorrectNumber As Boolean
    Dim fCorrectPlace As Boolean
    Dim bytCorrectNumbers As Byte
    Dim strSolved As String
    Dim lngColor As Long

    strSolved = "Congratulations! You've guessed all numbers in " & mbytTurn & " turns!"
    strSolved = strSolved & "Do you want to play again?"
    mbytTurn = mbytTurn + 1

    For bytEntryIndex = 1 To 5
        fCorrectNumber = False
        fCorrectPlace = False
        For bytRandomIndex = 1 To 5
            If CompareDigits(bytEntryIndex, bytRandomIndex) Then
                fCorrectNumber = True
                If bytEntryIndex = bytRandomIndex Then
                    fCorrectPlace = True
                End If
            End If
        Next

        If fCorrectNumber Then
            If fCorrectPlace Then
                lngColor = vbGreen
                Call SetColor(lngColor, bytEntryIndex)
                bytCorrectNumbers = bytCorrectNumbers + 1
            Else
                lngColor = vbBlue
                Call SetColor(lngColor, bytEntryIndex)
            End If
        Else
            lngColor = vbRed
            Call SetColor(lngColor, bytEntryIndex)
        End If
    Next

    If bytCorrectNumbers = 5 Then
        Call CalcScore
        If MsgBox(strSolved, vbYesNo, "Solved!!") = vbYes Then
            Call cmdNewGame_Click
        Else
            Call cmdClear_Click
            MsgBox "Your total score is: " & txtScore.Text
	    call cmdExit_Click
        End If
    Else
        MsgBox "Press Clear to continue", vbInformation
    End If


End Sub

Private Sub mnReady_Click ()
    Call cmdReady_Click
End Sub

Private Sub mnClear_Click ()
    Call cmdClear_Click
End Sub

Private Sub mnNew_Click ()
    Call cmdNewGame_Click
End Sub

Private Sub mnExit_Click ()
    Call cmdExit_Click
End Sub


Function CompareDigits(bytEntryIndex As Byte, bytRandomIndex As Byte) As Boolean

    If mstrsEnteredDigits(bytEntryIndex) = mstrsRandomDigits(bytRandomIndex) Then
        CompareDigits = True
    Else
        CompareDigits = False
    End If

End Function

Sub SetColor(lngColor As Long, bytDigitIndex As Byte)

    Select Case bytDigitIndex
        Case 1
            txtDigit1.ForeColor = lngColor
        Case 2
            txtDigit2.ForeColor = lngColor
        Case 3
            txtDigit3.ForeColor = lngColor
        Case 4
            txtDigit4.ForeColor = lngColor
        Case 5
            txtDigit5.ForeColor = lngColor
        Case Else
            ' This can't happen
    End Select

End Sub
Sub CalcScore()

    Dim lngScore As Long

    lngScore = CLng(txtScore.Text)

    If mbytTurn > 6 Then
        lngScore = lngScore + 1
    Else
        lngScore = lngScore + (2187 / 3 ^ mbytTurn)
    End If

    txtScore.Text = lngScore

End Sub

Private Sub Form_Resize()

	MsgBox "Blah"

End Sub

Private Sub Form_Load()

    Call cmdNewGame_Click

End Sub

Sub EnterDigit(strDigit As String)

    Select Case mbytEnterDigit
        Case 1
            txtDigit1.Text = strDigit
        Case 2
            txtDigit2.Text = strDigit
        Case 3
            txtDigit3.Text = strDigit
        Case 4
            txtDigit4.Text = strDigit
        Case 5
            txtDigit5.Text = strDigit
            cmdReady.Enabled = True
	    mnReady.Enabled = True
        Case Else
            MsgBox "Press Ready to continue", vbInformation
    End Select

    If mbytEnterDigit < 6 Then
        mstrsEnteredDigits(mbytEnterDigit) = strDigit
    End If

    mbytEnterDigit = mbytEnterDigit + 1

End Sub
