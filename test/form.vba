'
' Gnome Basic test program
' 
' In order to help debug use:
'
' ./gb form.gba
'
'

VERSION 5.00
Begin VB.Form testfrm 
   Caption         =   "My test program"
   ClientHeight    =   1200
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3960
   LinkTopic       =   "Form1"
   ScaleHeight     =   1200
   ScaleWidth      =   3960
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdGo 
      Caption         =   "Press me !"
      Height          =   495
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   3735
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3615
   End
End

Attribute VB_Name = "testfrm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim i As Integer

Private Sub Form_Load()
    i = 0
    print "I am executing"
    cmdGo.FillColor = vbBlue
End Sub

Private Sub cmdGo_Click()
    Dim s As String
    i = i + 1
    s = "I've been pressed " & i & IIf(i = 1, " time", " times")
    Label1.Caption = s

    If i > 5 Then
        Label1.ForeColor = vbGreen
        Label1.BackColor = vbYellow
    End If    

    s = Me.Caption
    s = left (s, len (s) - 1)
    Me.Caption = s
End Sub
