' * LostFocus Event
' * Control Arrays
'
' Program places some controls on a dialog
' which you can swap back and forth...
'
' Author, Per Winkvist
' (c) HelixCode Inc 2000.
'

VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   3210
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   3975
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3210
   ScaleWidth      =   3975
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdButtonArray 
      Caption         =   "Button1"
      Height          =   375
      Index           =   3
      Left            =   240
      TabIndex        =   9
      Top             =   1560
      Width           =   1455
   End
   Begin VB.TextBox txtInputArray 
      Height          =   375
      Index           =   3
      Left            =   2160
      TabIndex        =   8
      Text            =   "Text1"
      Top             =   1560
      Width           =   1575
   End
   Begin VB.CommandButton cmdButtonArray 
      Caption         =   "Button1"
      Height          =   375
      Index           =   2
      Left            =   240
      TabIndex        =   7
      Top             =   1080
      Width           =   1455
   End
   Begin VB.TextBox txtInputArray 
      Height          =   375
      Index           =   2
      Left            =   2160
      TabIndex        =   6
      Text            =   "Text1"
      Top             =   1080
      Width           =   1575
   End
   Begin VB.CommandButton cmdButtonArray 
      Caption         =   "Button1"
      Height          =   375
      Index           =   1
      Left            =   240
      TabIndex        =   5
      Top             =   600
      Width           =   1455
   End
   Begin VB.TextBox txtInputArray 
      Height          =   375
      Index           =   1
      Left            =   2160
      TabIndex        =   4
      Text            =   "Text1"
      Top             =   600
      Width           =   1575
   End
   Begin VB.CommandButton cmdFlip 
      Caption         =   "&Flip"
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   2640
      Width           =   1335
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "&Close"
      Height          =   375
      Left            =   2280
      TabIndex        =   2
      Top             =   2640
      Width           =   1455
   End
   Begin VB.CommandButton cmdButtonArray 
      Caption         =   "Button1"
      Height          =   375
      Index           =   0
      Left            =   240
      TabIndex        =   1
      Top             =   120
      Width           =   1455
   End
   Begin VB.TextBox txtInputArray 
      Height          =   375
      Index           =   0
      Left            =   2160
      TabIndex        =   0
      Text            =   "Text1"
      Top             =   120
      Width           =   1575
   End
   Begin VB.Label lblStatus 
      Caption         =   "No text field has focus..."
      Height          =   255
      Left            =   240
      TabIndex        =   10
      Top             =   2040
      Width           =   3495
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   3720
      Y1              =   2400
      Y2              =   2400
   End
End

Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub cmdClose_Click()
'  End
End Sub

Private Sub cmdFlip_Click()
  Dim x As Integer
  Dim y As Integer
  Dim cmdTemp As CommandButton
  
  For Each cmdTemp In cmdButtonArray
    x = cmdTemp.Left
    y = cmdTemp.Top
    
    ' Do something really stupied
    cmdTemp.Left = txtInputArray(cmdTemp.Index).Left
    cmdTemp.Top = txtInputArray(cmdTemp.Index).Top
            
    txtInputArray(cmdTemp.Index).Left = x
    txtInputArray(cmdTemp.Index).Top = y
  Next  ' perfectly legal

End Sub

Private Sub txtInputArray_LostFocus(Index As Integer)
  lblStatus = "Field " & Index & " lost focus!"
End Sub
