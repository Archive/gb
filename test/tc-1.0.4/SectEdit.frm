VERSION 5.00
Begin VB.Form dlgSectionEditor 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Section Editor"
   ClientHeight    =   1695
   ClientLeft      =   795
   ClientTop       =   2595
   ClientWidth     =   4425
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1695
   ScaleWidth      =   4425
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtBaseNum 
      Height          =   315
      Left            =   1350
      TabIndex        =   4
      Top             =   765
      Width           =   1395
   End
   Begin VB.TextBox txtRounds 
      Height          =   315
      Left            =   3090
      TabIndex        =   5
      Top             =   330
      Width           =   1035
   End
   Begin VB.OptionButton optGradSys 
      Caption         =   "BCF"
      Height          =   285
      Index           =   1
      Left            =   1350
      TabIndex        =   2
      Top             =   450
      Value           =   -1  'True
      Width           =   700
   End
   Begin VB.OptionButton optGradSys 
      Caption         =   "Elo"
      Height          =   285
      Index           =   2
      Left            =   2115
      TabIndex        =   3
      Top             =   450
      Width           =   700
   End
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2925
      TabIndex        =   7
      Top             =   1200
      Width           =   1410
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save Section"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2925
      TabIndex        =   6
      Top             =   795
      Width           =   1410
   End
   Begin VB.PictureBox picTC 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   0
      Picture         =   "SectEdit.frx":0000
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   1215
      Width           =   480
   End
   Begin VB.TextBox txtName 
      Height          =   315
      Left            =   675
      TabIndex        =   1
      Top             =   60
      Width           =   2145
   End
   Begin VB.Label lblBaseNum 
      AutoSize        =   -1  'True
      Caption         =   "Number from:"
      Height          =   195
      Left            =   315
      TabIndex        =   12
      Top             =   825
      Width           =   945
   End
   Begin VB.Label lblRounds 
      AutoSize        =   -1  'True
      Caption         =   "No.of Rounds:"
      Height          =   195
      Left            =   3090
      TabIndex        =   11
      Top             =   105
      Width           =   1035
   End
   Begin VB.Label lblGradSys 
      AutoSize        =   -1  'True
      Caption         =   "Grading System:"
      Height          =   195
      Left            =   105
      TabIndex        =   10
      Top             =   480
      Width           =   1155
   End
   Begin VB.Label lblInfo 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Information box"
      Height          =   465
      Left            =   525
      TabIndex        =   9
      Top             =   1185
      Width           =   2235
   End
   Begin VB.Label lblName 
      AutoSize        =   -1  'True
      Caption         =   "Name:"
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   465
   End
End

Attribute VB_Name = "dlgSectionEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'  Tournament Controller, a tool for chess tournament organisers
'  Copyright (C) 1997-99 Joe Orton <joe@orton.demon.co.uk>
'
'  This program is free software; you can redistribute it and/or modify
'  it under the terms of the GNU General Public License as published by
'  the Free Software Foundation; either version 2 of the License, or
'  (at your option) any later version.
'
'  This program is distributed in the hope that it will be useful,
'  but WITHOUT ANY WARRANTY; without even the implied warranty of
'  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'  GNU General Public License for more details.
'
'  You should have received a copy of the GNU General Public License
'  along with this program; if not, write to the Free Software
'  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
'
Option Explicit

' When the dialog is hidden, Cancelled will be
' set to True if the changes should be rejected
' else it will be False.

Public Cancelled As Boolean
Function InvalidData() As Boolean
' Returns True if any data entered is invalid
Dim RetValue As Boolean, sMessage As String
  RetValue = True
  ' Validate...
  If txtName = "" Then
    txtName.SetFocus
    sMessage = "You must specify a name for the section"
  ElseIf Val(txtRounds) < 4 Or Val(txtRounds) > 10 Then
    txtRounds.SetFocus
    sMessage = "You must specify a valid number of rounds between 4 and 10."
  ElseIf Val(txtBaseNum) = 0 Then
    txtBaseNum.SetFocus
    sMessage = "You must specify a valid base number."
  Else
    RetValue = False
  End If
  If RetValue Then
    ' Not valid, display message
    MsgBox sMessage, vbExclamation
  End If
  InvalidData = RetValue
End Function

Private Sub cmdClose_Click()
' 'Close' button clicked
  ' Focus back to name box ready for next open
  txtName.SetFocus
  ' Is a cancel
  Cancelled = True
  ' Hide the window
  Visible = False
End Sub

Private Sub cmdClose_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
' Mouse over 'Close' button
  lblInfo = "Close this window and lose changes"
End Sub

Private Sub cmdSave_Click()
' 'Save Section' button clicked
  ' Focus back to name box ready for next open
  txtName.SetFocus
  ' Not a cancel
  Cancelled = False
  ' Hide the window
  Visible = False
End Sub

Private Sub cmdSave_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
' Mouse moved over the 'Save Section' button
  lblInfo = "Save the section details"
End Sub

Private Sub optGradSys_GotFocus(Index As Integer)
' Either 'Grading System' option box gets the focus
  lblInfo = "Select the grading system for the section"
End Sub

Private Sub txtBaseNum_GotFocus()
' 'Number from' text box gets the focus
  lblInfo = "Enter the number from which players should be numbered."
End Sub

Private Sub txtName_GotFocus()
' 'Name' text box gets the focus
  lblInfo = "Enter the name of the section here"
End Sub

Private Sub txtRounds_GotFocus()
' 'No. of Rounds' text box go the focus'
  lblInfo = "Enter the number of rounds which will be played in the section"
End Sub
