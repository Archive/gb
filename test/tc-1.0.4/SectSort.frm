VERSION 5.00
Begin VB.Form dlgSectionSort 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Sort Section"
   ClientHeight    =   2580
   ClientLeft      =   1860
   ClientTop       =   2520
   ClientWidth     =   4575
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2580
   ScaleWidth      =   4575
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox picTC 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   -15
      Picture         =   "SectSort.frx":0000
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   2115
      Width           =   480
   End
   Begin VB.CommandButton cmdSort 
      Caption         =   "Sort"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2265
      TabIndex        =   8
      Top             =   2130
      Width           =   1100
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   3420
      TabIndex        =   7
      Top             =   2130
      Width           =   1100
   End
   Begin VB.Frame fraSortBy 
      Caption         =   "Sort players by..."
      Height          =   1935
      Left            =   45
      TabIndex        =   0
      Top             =   60
      Width           =   4485
      Begin VB.OptionButton optSort 
         Caption         =   "Score"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   3
         Left            =   105
         TabIndex        =   3
         Top             =   1275
         Width           =   1100
      End
      Begin VB.OptionButton optSort 
         Caption         =   "Grade"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   2
         Left            =   105
         TabIndex        =   2
         Top             =   735
         Width           =   1100
      End
      Begin VB.OptionButton optSort 
         Caption         =   "Surname"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   1
         Left            =   105
         TabIndex        =   1
         Top             =   270
         Value           =   -1  'True
         Width           =   1100
      End
      Begin VB.Label lblScore 
         Caption         =   "In descending order, highest - lowest. Players of equal score will be sorted by grading performance."
         Height          =   630
         Left            =   1230
         TabIndex        =   6
         Top             =   1155
         Width           =   3000
      End
      Begin VB.Label lblGrade 
         AutoSize        =   -1  'True
         Caption         =   "In descending order, highest - lowest."
         Height          =   195
         Left            =   1230
         TabIndex        =   5
         Top             =   780
         Width           =   3000
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblSurname 
         AutoSize        =   -1  'True
         Caption         =   "In ascending order, A - Z"
         Height          =   195
         Left            =   1230
         TabIndex        =   4
         Top             =   315
         Width           =   3000
      End
   End
   Begin VB.Label lblInfo 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Information box"
      Height          =   465
      Left            =   510
      TabIndex        =   10
      Top             =   2100
      Width           =   1635
   End
End

Attribute VB_Name = "dlgSectionSort"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'  Tournament Controller, a tool for chess tournament organisers
'  Copyright (C) 1997-99 Joe Orton <joe@orton.demon.co.uk>
'
'  This program is free software; you can redistribute it and/or modify
'  it under the terms of the GNU General Public License as published by
'  the Free Software Foundation; either version 2 of the License, or
'  (at your option) any later version.
'
'  This program is distributed in the hope that it will be useful,
'  but WITHOUT ANY WARRANTY; without even the implied warranty of
'  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'  GNU General Public License for more details.
'
'  You should have received a copy of the GNU General Public License
'  along with this program; if not, write to the Free Software
'  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
'
Option Explicit
' Cancelled is True if cancel was clicked
' to close the dialog.
Public Cancelled As Boolean
Public SortMethod As Integer
Sub SetMethod()
' Sets SortMethod to the selected method
' number
Dim numMethod As Integer
  ' Loop through the option boxes looking for
  ' the right one
  For numMethod = 1 To 3
    If optSort(numMethod).Value Then
      ' This one selected
      SortMethod = numMethod
      Exit For
    End If
  Next
End Sub

Private Sub cmdCancel_Click()
' Cancel button clicked
  Cancelled = True
  ' Focus back for next display
  optSort(1).SetFocus
  ' Hide the window
  Visible = False
End Sub

Private Sub cmdSort_Click()
' Sort button clicked
  Cancelled = False
  SetMethod
  ' Focus back for next display
  optSort(1).SetFocus
  ' Hide the window
  Visible = False
End Sub

Private Sub optSort_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
' Mouse is moved over a sort option box.
  lblInfo = "Selects the method by which players in the active section " + _
   "will be sorted."
End Sub
