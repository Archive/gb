VERSION 5.00
Begin VB.Form dlgTournEditor 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Tournament Editor"
   ClientHeight    =   1290
   ClientLeft      =   2085
   ClientTop       =   5940
   ClientWidth     =   5130
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1290
   ScaleWidth      =   5130
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   165
      TabIndex        =   3
      Top             =   885
      Width           =   1100
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1335
      TabIndex        =   4
      Top             =   885
      Width           =   1100
   End
   Begin VB.PictureBox picTC 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   4650
      Picture         =   "TrnEdit.frx":0000
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   810
      Width           =   480
   End
   Begin VB.TextBox txtShortName 
      Height          =   315
      Left            =   1005
      TabIndex        =   2
      Top             =   480
      Width           =   1560
   End
   Begin VB.TextBox txtName 
      Height          =   315
      Left            =   1005
      TabIndex        =   1
      Top             =   75
      Width           =   3990
   End
   Begin VB.Label lblInfo 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Information box"
      Height          =   465
      Left            =   2640
      TabIndex        =   7
      Top             =   780
      Width           =   1965
   End
   Begin VB.Label lblShortName 
      AutoSize        =   -1  'True
      Caption         =   "Short name:"
      Height          =   195
      Left            =   60
      TabIndex        =   5
      Top             =   540
      Width           =   855
   End
   Begin VB.Label lblName 
      AutoSize        =   -1  'True
      Caption         =   "Name:"
      Height          =   195
      Left            =   450
      TabIndex        =   0
      Top             =   120
      Width           =   465
   End
End

Attribute VB_Name = "dlgTournEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'  Tournament Controller, a tool for chess tournament organisers
'  Copyright (C) 1997-99 Joe Orton <joe@orton.demon.co.uk>
'
'  This program is free software; you can redistribute it and/or modify
'  it under the terms of the GNU General Public License as published by
'  the Free Software Foundation; either version 2 of the License, or
'  (at your option) any later version.
'
'  This program is distributed in the hope that it will be useful,
'  but WITHOUT ANY WARRANTY; without even the implied warranty of
'  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'  GNU General Public License for more details.
'
'  You should have received a copy of the GNU General Public License
'  along with this program; if not, write to the Free Software
'  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
'
Option Explicit
' When the dialog is hidden, Cancelled will be
' set to True if the changes should be rejected
' else it will be False.
Public Cancelled As Boolean
Function InvalidData() As Boolean
' Returns true if any boxes contain
' invalid data
Dim RetValue As Boolean
  If txtName = "" Then
    ' Needs a name
    MsgBox "You must specify a name for the tournament.", vbExclamation
    RetValue = True
  End If
End Function

Private Sub cmdCancel_Click()
' 'Cancel' button clicked
  ' Was cancelled...
  Cancelled = True
  ' Focus back to txtName
  txtName.SetFocus
  ' Hide the form
  Visible = False
End Sub

Private Sub cmdCancel_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
' Mouse is moved over the 'Cancel' Button
  lblInfo = "Lose the changes made to the names"
End Sub

Private Sub cmdOK_Click()
' 'OK' button clicked
  ' Check for valid data
  If InvalidData() Then Exit Sub
  ' Was not cancelled
  Cancelled = False
  ' Focus back to txtName
  txtName.SetFocus
  ' Hide the form
  Visible = False
End Sub

Private Sub cmdOK_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
' Mouse is moved over the 'OK' Button
  lblInfo = "Use these names for the tournament"
End Sub

Private Sub txtName_GotFocus()
' 'Name' text box gets the focus
  lblInfo = "Enter the full name of the tournament here"
End Sub

Private Sub txtShortName_GotFocus()
' 'Short Name' text box gets the focus
  lblInfo = "Enter a short name for the tournament here"
End Sub
