VERSION 5.00
Begin VB.Form frmMain 
   Caption         =   "Tournament Controller"
   ClientHeight    =   5910
   ClientLeft      =   690
   ClientTop       =   810
   ClientWidth     =   7845
   Icon            =   "Main2.frx":0000
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5910
   ScaleWidth      =   7845
   Begin VB.CommandButton cmdReCalculate 
      Caption         =   "Recalculate"
      Height          =   315
      Left            =   3960
      TabIndex        =   10
      Top             =   60
      Width           =   1200
   End
   Begin VB.PictureBox panSection 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   4575
      Left            =   120
      ScaleHeight     =   4575
      ScaleWidth      =   7575
      TabIndex        =   5
      Top             =   840
      Width           =   7575
      Begin VB.PictureBox picHolder 
         Height          =   4050
         Left            =   60
         ScaleHeight     =   266
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   384
         TabIndex        =   8
         Top             =   60
         Width           =   5820
         Begin VB.PictureBox picSection 
            AutoRedraw      =   -1  'True
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            Height          =   2580
            Left            =   0
            ScaleHeight     =   172
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   329
            TabIndex        =   9
            Top             =   0
            Width           =   4935
         End
      End
      Begin VB.VScrollBar scrSectVert 
         Height          =   3960
         Left            =   7290
         TabIndex        =   7
         Top             =   60
         Width           =   255
      End
      Begin VB.HScrollBar scrSectHorz 
         Height          =   255
         LargeChange     =   10
         Left            =   60
         Max             =   100
         SmallChange     =   10
         TabIndex        =   6
         Top             =   4155
         Value           =   50
         Width           =   4935
      End
   End
   Begin VB.CommandButton cmdPlayerEdit 
      Caption         =   "&Edit Player"
      Height          =   315
      Left            =   2610
      TabIndex        =   4
      Top             =   60
      Width           =   1200
   End
   Begin VB.CommandButton cmdPlayerDelete 
      Caption         =   "Delete Player"
      Height          =   315
      Left            =   1335
      TabIndex        =   3
      Top             =   60
      Width           =   1200
   End
   Begin VB.CommandButton cmdPlayerAdd 
      Caption         =   "&Add Player"
      Height          =   315
      Left            =   60
      TabIndex        =   2
      Top             =   60
      Width           =   1200
   End
   Begin TabDlg.SSTab tabSections 
      Height          =   5085
      Left            =   15
      TabIndex        =   0
      Top             =   420
      Width           =   9225
      _ExtentX        =   16272
      _ExtentY        =   8969
      _Version        =   327681
      Style           =   1
      Tabs            =   1
      TabsPerRow      =   10
      TabHeight       =   547
      TabCaption(0)   =   "Section Name"
      TabPicture(0)   =   "Main2.frx":0442
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).ControlCount=   0
   End
   Begin VB.PictureBox panMsg 
      Align           =   2  'Align Bottom
      BackColor       =   &H00C0C0C0&
      Height          =   285
      Left            =   0
      ScaleHeight     =   225
      ScaleWidth      =   7785
      TabIndex        =   1
      Top             =   5625
      Width           =   7845
   End
   Begin MSComDlg.CommonDialog cdgMain 
      Left            =   3405
      Top             =   5400
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
      DefaultExt      =   ".trn"
      Filter          =   "Tournament files (*.trn)|*.trn|All files (*.*)|*.*"
      Flags           =   3676164
      FontSize        =   3.54153e-38
   End
   Begin VB.Line linSep4 
      BorderColor     =   &H00808080&
      X1              =   5220
      X2              =   5220
      Y1              =   0
      Y2              =   400
   End
   Begin VB.Line linSep3 
      BorderColor     =   &H00E0E0E0&
      X1              =   5235
      X2              =   5235
      Y1              =   15
      Y2              =   400
   End
   Begin VB.Line linSep2 
      BorderColor     =   &H00E0E0E0&
      X1              =   3870
      X2              =   3870
      Y1              =   15
      Y2              =   400
   End
   Begin VB.Line linSep1 
      BorderColor     =   &H00808080&
      X1              =   3855
      X2              =   3855
      Y1              =   0
      Y2              =   400
   End
   Begin VB.Line linMenu2 
      BorderColor     =   &H00808080&
      X1              =   0
      X2              =   7755
      Y1              =   0
      Y2              =   0
   End
   Begin VB.Line linMenu1 
      BorderColor     =   &H00E0E0E0&
      X1              =   0
      X2              =   7770
      Y1              =   15
      Y2              =   15
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "&New"
      End
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open..."
         Shortcut        =   ^O
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuFileSaveAs 
         Caption         =   "Save &as..."
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuFileSepA 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileTournEdit 
         Caption         =   "&Tournament Details..."
      End
      Begin VB.Menu mnuFileSepB 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFilePrint 
         Caption         =   "&Print..."
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuFilePrinterSetup 
         Caption         =   "P&rinter Setup..."
      End
      Begin VB.Menu mnuFileRecSep 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileRecent 
         Caption         =   ""
         Index           =   1
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileRecent 
         Caption         =   ""
         Index           =   2
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileRecent 
         Caption         =   ""
         Index           =   3
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileRecent 
         Caption         =   ""
         Index           =   4
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileSepX 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileAbout 
         Caption         =   "&About..."
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuSection 
      Caption         =   "&Sections"
      Begin VB.Menu mnuSectionAdd 
         Caption         =   "&Add..."
      End
      Begin VB.Menu mnuSectionDelete 
         Caption         =   "&Delete..."
      End
      Begin VB.Menu mnuSectionEdit 
         Caption         =   "&Edit..."
      End
      Begin VB.Menu mnuSectionSepA 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSectionGameEnt 
         Caption         =   "&Game entry..."
      End
   End
   Begin VB.Menu mnuPlayers 
      Caption         =   "&Players"
      Begin VB.Menu mnuPlayerAdd 
         Caption         =   "&Add..."
      End
      Begin VB.Menu mnuPlayerDelete 
         Caption         =   "&Delete..."
      End
      Begin VB.Menu mnuPlayerEdit 
         Caption         =   "&Edit..."
      End
      Begin VB.Menu mnuPlayerSepA 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPlayersSort 
         Caption         =   "&Sort..."
      End
      Begin VB.Menu mnuPlayersRenumber 
         Caption         =   "&Renumber"
      End
      Begin VB.Menu mnuPlayersRenumZero 
         Caption         =   "Renumber from &zero"
      End
   End
End
' Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
' Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'  Tournament Controller, a tool for chess tournament organisers
'  Copyright (C) 1997-99 Joe Orton <joe@orton.demon.co.uk>
'
'  This program is free software; you can redistribute it and/or modify
'  it under the terms of the GNU General Public License as published by
'  the Free Software Foundation; either version 2 of the License, or
'  (at your option) any later version.
'
'  This program is distributed in the hope that it will be useful,
'  but WITHOUT ANY WARRANTY; without even the implied warranty of
'  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'  GNU General Public License for more details.
'
'  You should have received a copy of the GNU General Public License
'  along with this program; if not, write to the Free Software
'  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
'
Option Explicit

Public Sub Recent_Show()
' Shows the recently-used-files
Dim numRec As Integer
  If RecentFiles.Count = 0 Then
    ' Hide the menu seperator
    mnuFileRecSep.Visible = False
  Else
    ' Show the menu seperator
    mnuFileRecSep.Visible = True
  End If
  For numRec = 1 To RecentFiles.Count
    With mnuFileRecent(numRec)
      .Visible = True
      .Caption = "&" + Format$(numRec) + "  " + RecentFiles(numRec)
    End With
  Next
  For numRec = RecentFiles.Count + 1 To 4
    mnuFileRecent(numRec).Visible = False
  Next
End Sub

Private Sub cmdPlayerAdd_Click()
' 'Add Player' button clicked
  OpenTourn.Player_Add
End Sub

Private Sub cmdPlayerDelete_Click()
' 'Delete Player' button clicked
  OpenTourn.Player_Delete
End Sub

Private Sub cmdPlayerEdit_Click()
' 'Edit Player' button clicked
  OpenTourn.Player_Edit
End Sub

Private Sub cmdRed_Click()
  Display_Section
End Sub


Private Sub cmdReCalculate_Click()
' Recalculate button clicked
  OpenTourn.Section_GatherPlayers
  Display_Section
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
' Is it OK to unload the form? - has the tourn
' been saved?
  If UnloadMode <> vbFormCode Then
    ' Check whether it's ok finish...
    Cancel = Not FinishQuery()
  End If
End Sub

Private Sub Form_Resize()
' The form is being resized
  ResizeForm
End Sub

Private Sub Form_Unload(Cancel As Integer)
' The form has been closed
  Finish
End Sub

Private Sub mnuFileAbout_Click()
' File>About menu item clicked
  Display_AboutBox
End Sub

Private Sub mnuFileExit_Click()
' File>Exit menu item clicked
  Unload frmMain
End Sub

Private Sub mnuFileNew_Click()
' File>New menu item clicked
  Tourn_New
End Sub

Private Sub mnuFileOpen_Click()
' File>Open menu item clicked
  Tourn_Open
End Sub

Private Sub mnuFilePrint_Click()
' Prints the tournament
  OpenTourn.Tourn_Print
End Sub

Private Sub mnuFilePrinterSetup_Click()
  On Error Resume Next
  cdgMain.Flags = cdlPDPrintSetup
  cdgMain.ShowPrinter
End Sub

Private Sub mnuFileRecent_Click(Index As Integer)
' Opens a recently-used file
  Recent_Open Index
End Sub

Private Sub mnuFileSave_Click()
' File>Save menu item clicked
  Tourn_Save
End Sub

Private Sub mnuFileSaveAs_Click()
' File>Save As menu item clicked
  Tourn_SaveAs
End Sub


Private Sub mnuFileTournEdit_Click()
' File>Tournament Details menu item clicked
  OpenTourn.Tourn_Edit
End Sub

Private Sub mnuPlayerAdd_Click()
' Players>Add menu item clicked
  OpenTourn.Player_Add
End Sub

Private Sub mnuPlayerDelete_Click()
' Players>Delete menu item clicked
  OpenTourn.Player_Delete
End Sub

Private Sub mnuPlayerEdit_Click()
' Players>Edit menu item clicked
  OpenTourn.Player_Edit
End Sub

Private Sub mnuPlayersRenumber_Click()
  OpenTourn.Section_DoRenumber
End Sub

Private Sub mnuPlayersRenumZero_Click()
' Players>Renumber from Zero menu item clicked
  OpenTourn.Section_RenumberFromZero
End Sub

Private Sub mnuPlayersSort_Click()
' Players>Sort menu item clicked
  OpenTourn.Section_DoSort
End Sub

Private Sub mnuSectionAdd_Click()
' Sections>Add menu item clicked
  OpenTourn.Section_Add
End Sub

Private Sub mnuSectionDelete_Click()
' Section>Delete menu item clicked
  OpenTourn.Section_Delete
End Sub

Private Sub mnuSectionEdit_Click()
' Section>Edit menu item clicked
  OpenTourn.Section_Edit
End Sub

Private Sub mnuSectionGameEnt_Click()
' Show the Game Entry box
  frmGameEntry.Show
End Sub

Private Sub picSection_DblClick()
' Double click on the section box
  If OpenTourn.CurrentPlayerIndex > 0 Then
    OpenTourn.Player_Edit
  End If
End Sub

Private Sub picSection_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
' Mouse button down on the section box
  ' Select a player
  OpenTourn.Section_Click Y
  If Button = 2 Then
    ' Display a pop-up menu
    PopupMenu mnuPlayers, 2
  End If
End Sub

Private Sub scrSectHorz_Change()
' Move the section box
  Display_SectionPosition
End Sub

Private Sub scrSectHorz_Scroll()
' Move the section box
  Display_SectionPosition
End Sub

Private Sub scrSectVert_Change()
' Move the section box
  Display_SectionPosition
End Sub

Private Sub scrSectVert_Scroll()
' Move the section box
  Display_SectionPosition
End Sub

Private Sub tabSections_Click(PreviousTab As Integer)
' A section tab was clicked
  OpenTourn.Section_TabSelect
End Sub
