Attribute VB_Name = "basGlobal"
'  Tournament Controller, a tool for chess tournament organisers
'  Copyright (C) 1997-99 Joe Orton <joe@orton.demon.co.uk>
'
'  This program is free software; you can redistribute it and/or modify
'  it under the terms of the GNU General Public License as published by
'  the Free Software Foundation; either version 2 of the License, or
'  (at your option) any later version.
'
'  This program is distributed in the hope that it will be useful,
'  but WITHOUT ANY WARRANTY; without even the implied warranty of
'  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'  GNU General Public License for more details.
'
'  You should have received a copy of the GNU General Public License
'  along with this program; if not, write to the Free Software
'  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
'
Option Explicit

' The Filename of the INI File used in INI File handling
Public fnIni As String

Sub Clear(AnyCollection As Collection)
' Clears the contents of the given collection
Dim nCount As Long
  For nCount = 1 To AnyCollection.Count
    AnyCollection.Remove 1
  Next
End Sub
Function fnStripExtension(fnAny As String) As String
' Returns the filename without its extension
' eg 'c:\foo.exe' -> 'c:\foo'
Dim lChar As Long, sRet As String, lLength As Long
  If fnAny = "" Then
    fnStripExtension = ""
    Exit Function
  End If
  lLength = Len(fnAny)
  For lChar = lLength To 1 Step -1
    If Mid$(fnAny, lChar, 1) = "." Then
      fnStripExtension = Left$(fnAny, lChar - 1)
      Exit Function
    End If
  Next
  fnStripExtension = fnAny
End Function

Function fnPath(fn As String) As String
' Returns the path of fn without trailing '\'
Dim nChar As Integer
  For nChar = Len(fn) To 1 Step -1
    If Mid$(fn, nChar, 1) = "\" Then
      If nChar = 1 Then Exit Function
      fnPath = Left$(fn, nChar - 1)
      Exit Function
    End If
  Next
End Function

Function fnExists(fnAny As String) As Integer
' File Exists?
' Returns:
' 1:  if file exists,
' 0:  no file (path is ok)
' -1: fnAny contains wildcards or bad characters, or is empty
' -2: bad path
' -3: bad drive specification
' -4: Hardware error (ie Disk Not Ready)
' -5: Something else
Dim ret As Integer
  If fnAny = "" Then
    ' No fnAny given
    fnExists = -1
    Exit Function
  End If
  On Error GoTo Exists_Bad
  If Dir$(fnAny) <> "" Then
    ' At least one file fits the description...
    If Dir$ <> "" Then
      ' More than one file fits the description
      ret = -1
    Else
      ' Only one file fits the description
      ret = 1
    End If
  Else
    ' No files fit the description
    If Dir$(fnPath(fnAny), 16) <> "" Then
      ' The directory exists
      ret = 0
    Else
      ' The directory does NOT exist
      ret = -2
    End If
  End If
  fnExists = ret
  Exit Function
Exists_Bad:
  Select Case Err
    Case 52, 53 : ret = -1 ' Bad fnAny
    Case 75, 76: ret = -2 ' Bad Path
    Case 68: ret = -3     ' Bad drive
    Case 51, 57, 71: ret = -4  ' Hardware
    Case Else: ret = -5   ' Something else...
  End Select
  fnExists = ret
  Exit Function
End Function

' *** INI File Handling Procedures ***

Function iniRead(sSection As String, sKeyName As String, Optional sDefault) As String
' Reads a string from the INI file
Dim sBuffer As String, ret As Long
  If IsMissing(sDefault) Then
    ' If no default string specified, use ""
    sDefault = ""
  End If
  ret = GetPrivateProfileString(sSection, sKeyName, sDefault, sBuffer, 2048, fnIni)
  iniRead = Left$(sBuffer, ret)
End Function

Function iniReadNum(sSection As String, sKeyName As String, Optional nDefault) As Variant
' Reads a number from the INI File
  iniReadNum = Val(iniRead(sSection, sKeyName, nDefault))
End Function
Sub iniWrite(sSection As String, sKeyName As String, vValue As Variant)
' Writes a string to the INI file
Dim ret As Long, sValue As String
  sValue = CStr(vValue)
  ret = WritePrivateProfileString(sSection, sKeyName, sValue, fnIni)
End Sub


Sub iniLoadForm(sFormName As String, frmForm As Form)
' Loads the position of named form from the INI file
Dim numVal As Long
  With frmForm
'    .WindowState = iniReadNum("Windows", sFormName + ".State", .WindowState)
    If .WindowState = 0 Then
      ' Only save position+size if form is not minimized or maximized
      .Left = iniReadNum("Windows", sFormName + ".Left", 200)
      .Top = iniReadNum("Windows", sFormName + ".Top", 200)
      If .BorderStyle = 2 Or .BorderStyle = 5 Then
        ' Only save width and height of form if it is resizable
        .Width = iniReadNum("Windows", sFormName + ".Width", .Width)
        .Height = iniReadNum("Windows", sFormName + ".Height", .Height)
      End If
    End If
  End With
End Sub

Sub iniSaveForm(sFormName As String, frmForm As Form)
' Saves the position of named form to the INI file
  With frmForm
    iniWrite "Windows", sFormName + ".State", Format$(.WindowState)
    If .WindowState = 0 Then
      ' Only save position+size if form is not minimized or maximized
      iniWrite "Windows", sFormName + ".Left", .Left
      iniWrite "Windows", sFormName + ".Top", .Top
      If .BorderStyle = 2 Or .BorderStyle = 5 Then
        ' Only save width and height of form if it is resizable
        iniWrite "Windows", sFormName + ".Width", .Width
        iniWrite "Windows", sFormName + ".Height", .Height
      End If
    End If
  End With
End Sub

Sub CenterForm(frmAny As Form)
' Places given form in middle of screen
  frmAny.Left = (Screen.Width - frmAny.Width) / 2
  frmAny.Top = (Screen.Height - frmAny.Height) / 2
End Sub

Sub PrintCell(ByRef vObject As Variant, ByVal XPos As Integer, ByVal Width As Integer, ByVal Margin As Integer, ByVal vPrint As Variant, bBold As Boolean, iAlign As Integer, BGColor)
' Prints vPrint to the Printer vObject in bold text bBold in cell background
' color BGColor with cell width Width and inner margin Margin Margin.
' Cell Left Coord = XPos, Top Coord = CurrentY
' if iAlign=0, then left align text in cell
' if iAlign=1, then middle align text in cell
' if iAlign=2, then right align text in cell
' // Hacked for v4.02 to work with HiMetrics rather than fucking
' around with all the stupid stuff v4.00 did. Which is nice.
' // Use 'Virtual Pixels' ... give the proc pixel coords and dims and it will
' hopefully convert everything into Himetrics to send to printer.
Dim StoreY As Long, TWidth As Long, sText As String
Dim himRowHeight As Long
  ' Erm, ok, SP2 is supposed to fixed ScaleMode stuff
  ShowCell vObject, XPos, Width, Margin, vPrint, bBold, iAlign, BGColor
  Exit Sub
  ' Pixels -> HiMetric
  With vObject
    XPos = .ScaleX(XPos, vbPixels, vbUser)
    Width = .ScaleX(Width, vbPixels, vbUser)
    himRowHeight = .ScaleY(himRowHeight, vbPixels, vbUser)
    Margin = .ScaleX(Margin, vbPixels, vbUser)
  End With
  'himRowHeight = RowHeight
  ' Set the bold value
  vObject.Font.Bold = bBold
  ' Store Y value
  StoreY = vObject.CurrentY
  ' Blank out the cell with white rectangle
  vObject.Line (XPos, StoreY)-(XPos + Width, StoreY + himRowHeight), BGColor, BF
  ' Where to print the text...
  sText = CStr(vPrint) ' Convert to string
  TWidth = vObject.TextWidth(sText) ' Width of text
  ' This Could be the problem.
  ' TWidth is real dimensions, whereas XPos, Width etc
  ' are virtual dimensions... Hmmm. TWidth -> Pixels...
'  TWidth = vObject.ScaleX(TWidth, vbUser, vbPixels)
  Select Case iAlign
    Case 1 ' Middle align
      XPos = XPos + (Width - TWidth) / 2
    Case 2 ' Right align
      XPos = XPos + Width - (TWidth + Margin)
    Case 0 ' Left align
      XPos = XPos + Margin
  End Select
  ' Set the X position
  vObject.CurrentX = XPos
  vObject.CurrentY = StoreY
  ' Print the text
  vObject.Print sText
  ' Recall the Y position
  vObject.CurrentY = StoreY
End Sub

Sub ShowCell(ByRef vObject As Variant, ByVal XPos As Integer, Width As Integer, Margin As Integer, vPrint As Variant, bBold As Boolean, iAlign As Integer, BGColor)
' Prints a vText on vObject in bold text bBold in cell background color BGColor with Width
' if iAlign=0, then left align text in cell
' if iAlign=1, then middle align text in cell
' if iAlign=2, then right align text in cell
Dim StoreY As Integer, TWidth As Integer, sText As String
  ' Set the bold value
  vObject.FontBold = bBold
  ' Store Y value
  StoreY = vObject.CurrentY
  ' Blank out the cell with white rectangle
  vObject.Line (XPos, StoreY)-(XPos + Width, StoreY + RowHeight), BGColor, BF
  ' Where to print the text...
  sText = CStr(vPrint) ' Convert to string
  TWidth = vObject.TextWidth(sText) ' Width of text
  Select Case iAlign
    Case 1 ' Middle align
      XPos = XPos + (Width - TWidth) / 2
    Case 2 ' Right align
      XPos = XPos + Width - (TWidth + Margin)
    Case 0 ' Left align
      XPos = XPos + Margin
  End Select
  ' Set the X position
  vObject.CurrentX = XPos
  vObject.CurrentY = StoreY
  ' Print the text
  vObject.Print CStr(sText)
  ' Recall the Y position
  vObject.CurrentY = StoreY
End Sub

Sub PrintFont(vObject As Variant, FontName As String, FontSize As Integer, FontBold As Boolean)
' Sets the print font of vObject
  vObject.Font.Name = FontName
  vObject.Font.Size = FontSize
  vObject.Font.Bold = FontBold
End Sub

Sub PrintCenter(vObject As Variant, Text As String)
' Prints text in center of line of vObject
  vObject.CurrentX = (vObject.ScaleWidth - vObject.TextWidth(Text)) / 2
  vObject.Print Text
End Sub

Function ReadLine(fh As Integer) As String
' Reads a line from file of given file handle
' and returns it as a string
Dim sLine As String
  ' Ignore an error - this will return a blank string
  On Error Resume Next
  Line Input #fh, sLine
  ReadLine = sLine
End Function

