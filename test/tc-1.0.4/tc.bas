Attribute VB_Name = "basTC"
'  Tournament Controller, a tool for chess tournament organisers
'  Copyright (C) 1997-99 Joe Orton <joe@orton.demon.co.uk>
'
'  This program is free software; you can redistribute it and/or modify
'  it under the terms of the GNU General Public License as published by
'  the Free Software Foundation; either version 2 of the License, or
'  (at your option) any later version.
'
'  This program is distributed in the hope that it will be useful,
'  but WITHOUT ANY WARRANTY; without even the implied warranty of
'  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'  GNU General Public License for more details.
'
'  You should have received a copy of the GNU General Public License
'  along with this program; if not, write to the Free Software
'  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

' Tournament Controller
' ---------------------
'
' Main Module

Option Explicit ' All Variables must be dimensioned before use

Public Const TC_VERSION As String = "1.0.4"

' *** Constants ***

' Grading Systems
Public Const BCFSystem = 0, EloSystem = 1
' Display of Section
Public Const RowHeight = 15, HeaderHeight = 17
Public Const GameResWidth = 10, GameOppWidth = 28
' Sort Methods
Public Const SortName = 1, SortGrade = 2, SortScore = 3
' Print Modes: Scopes
Public Const PrintAll = 0, PrintSingle = 1
' And Modes
Public Const PrintFull = 0, PrintSimple = 1
' *** Temporary Storage ***

' Contains the recently-used-files list
Public RecentFiles As New Collection

' Elo Performance Calculation Tables
Public EloTable(4 To 10, 0 To 20) As Integer
Public bGotEloTable As Boolean ' True if EloTable has been filled

' *** Data Storage ***

' OpenTourn contains the Currently Open Tournament
Public OpenTourn As Tournament

Public SavedX As Integer, SavedY As Integer

' *** Tournament Controller Procedures ***

Sub ResizeForm()
' Resizes the main form
Static bHiddenGE As Boolean
  If frmMain.WindowState <> 1 Then
    ' Only if the form is not minimized
    If bHiddenGE Then
      ' The Game Entry window was hidden
      ' after a minimize
      frmGameEntry.Visible = True
      bHiddenGE = False
    End If
    With frmMain
      ' Resize the seperator line
      .linMenu1.X2 = .ScaleWidth
      .linMenu2.X2 = .ScaleWidth
      If .ScaleWidth > 555 Then
        ' Resize width of tab + section holder panel if there is room
        .tabSections.Width = .ScaleWidth - 30
        .panSection.Width = .ScaleWidth - 180
        ' Resize the section holder box and the scrollbars
        .picHolder.Width = .ScaleWidth - 555
        .scrSectHorz.Width = .ScaleWidth - 555
        .scrSectVert.Left = .ScaleWidth - 480
      End If
      If .ScaleHeight > 1590 Then
        ' Resize height of tab + section holder box if there is room
        .tabSections.Height = .ScaleHeight - 750
        .panSection.Height = .ScaleHeight - 1215
        ' Resize the section holder box and the scrollbars
        .picHolder.Height = .ScaleHeight - 1590
        .scrSectVert.Height = .ScaleHeight - 1590
        .scrSectHorz.Top = .ScaleHeight - 1515
      End If
    End With
    Display_ScrollBars
  Else
    ' Main form is minimized
    If frmGameEntry.Visible Then
      ' Hide the Game entry window
      frmGameEntry.Hide
      bHiddenGE = True
    End If
  End If
End Sub

Sub Main()

  print "Hit Main..."

' First called when program is run
Dim sMessage As String
  ' Show hourglass mouse pointer
  Screen.MousePointer = vbHourglass
  ' Show the Loader window
  Loader_Show
  ' Load Elo Performance Calculation Tables
  Init_LoadTables
  ' INI File
  Init_FindINI
  Recent_Read
  ' Load forms and open new tourn
  Init_LoadForms
  Init_Show
  ' Hide the Loader window
  Loader_Hide
  ' Display error if EloTables not found
  If bGotEloTable = False Then
    sMessage = "The file elotable.csv was not found." + vbCrLf + _
      "Elo Grading Performances cannot be calculated without this file."
    MsgBox sMessage, vbExclamation
  End If
  ' Normal mouse pointer
  Screen.MousePointer = vbDefault
  frmMain.SetFocus
End Sub

Sub Loader_Show()
' Shows the Loader window
  Load frmLoader
  CenterForm frmLoader
  frmLoader.Show
  frmLoader.Refresh
  ' Set it ON TOP of all other windows
  SetWindowPos frmLoader.hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE
End Sub

Sub Loader_Hide()
' Hides the Loader window
  SetWindowPos frmLoader.hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE
  Unload frmLoader
End Sub

Sub Init_FindINI()
' Finds the tc.ini file
Dim sFind As String
  sFind = App.Path + "\tc.ini"
  ' If no ini file in app path
  ' then use it in default directory
  ' eg c:\windows
  If fnExists(sFind) < 1 Then sFind = "tc.ini"
  fnIni = sFind
End Sub

Sub Init_LoadForms()
' Loads the forms at startup
  Load frmGameEntry
  Load dlgPlayerEditor
  Load dlgSectionEditor
  Load dlgSectionSort
  Load dlgTournEditor
  Load dlgPrint
  Load frmMain
  ' Load positions from ini file
  iniLoadForm "Results", frmGameEntry
  iniLoadForm "Main", frmMain
  iniLoadForm "SectionEditor", dlgSectionEditor
  iniLoadForm "SectionSort", dlgSectionSort
  iniLoadForm "TournEditor", dlgTournEditor
  iniLoadForm "PlayerEditor", dlgPlayerEditor
  iniLoadForm "Print", dlgPrint
End Sub

Sub Init_Show()
' All initializing done, so show the forms...
  frmMain.Show
  frmMain.Recent_Show
  ' Start a new tournament
  Tourn_New
  ' Make sure the window is displayed
  ' properly
  frmMain.Refresh
End Sub

Sub Init_LoadTables()
' Loads Elo Performance Calculation Tables
Dim fnTables As String, fhTables As Integer
Dim Points As Integer, numRound As Integer
  ' Set the filename (in same dir as app)
  fnTables = App.Path + "\elotable.csv"
  If fnExists(fnTables) <> 1 Then
    ' Could not find file
    bGotEloTable = False
    Exit Sub
  End If
  ' Read the file
  fhTables = FreeFile
  ' Set error handler
  On Error GoTo Init_LoadTables_Error
  ' Open the file
  Open fnTables For Input As fhTables
  ' Read the data
    For Points = 0 To 20
      For numRound = 4 To 10
        ' Read next value
        Input #fhTables, EloTable(numRound, Points)
      Next
    Next
  ' Close the file
  Close fhTables
  ' Ok - got the values
  bGotEloTable = True
  Exit Sub
Init_LoadTables_Error:
  ' Error when reading...
  bGotEloTable = False
  Exit Sub
End Sub

Sub Finish()
' Called when user closes application
  Recent_Write
  Finish_UnloadForms
End Sub

Function FinishQuery() As Boolean
' Returns True if it's ok to finish
  FinishQuery = Tourn_QueryClose()
End Function

Sub Finish_UnloadForms()
' Unloads all the forms which were loaded, saving
' their positions first
  ' Save positions of forms
  iniSaveForm "Main", frmMain
  iniSaveForm "Results", frmGameEntry
  iniSaveForm "PlayerEditor", dlgPlayerEditor
  iniSaveForm "SectionEditor", dlgSectionEditor
  iniSaveForm "SectionSort", dlgSectionSort
  iniSaveForm "TournEditor", dlgTournEditor
  iniSaveForm "Print", dlgPrint
  ' Unload the forms
  Unload frmGameEntry
  Unload dlgPlayerEditor
  Unload dlgSectionEditor
  Unload dlgSectionSort
  Unload dlgTournEditor
  Unload dlgPrint
End Sub

Sub Tourn_New()
' Start a new tournament
Dim NewID As Long
  Set OpenTourn = New Tournament
  With OpenTourn
    ' Set defaults for new tournament
    .Name = "Untitled tournament"
    .ShortName = "Untitled"
  End With
  ' Create a default section
  NewID = OpenTourn.Section_New()
  ' Switch to new section
  OpenTourn.Section_Switch NewID
  ' and display everything
  Display_Update
  Display_Tabs
End Sub

Sub Tourn_Open()
' Opens an existing tournament
  ' Check to save open tournament...
  With frmMain.cdgMain
    ' Sets Common Dialog options
    .DialogTitle = "Open tournament..."
    ' Flags for the dialog:
    '   The filename returned must exist
    '   The 'Read Only' checkbox is hidden
    .Flags = cdlOFNFileMustExist + cdlOFNHideReadOnly
    ' Generate an error if user clicks 'Cancel'
    .CancelError = True
    ' Error Handler
    On Error GoTo Tourn_Open_Error
    ' And show an 'Open File...' type dialog
    .ShowOpen
    Tourn_DoOpen .Filename
  End With
  Exit Sub
Tourn_Open_Error:
' An error has occurred
'  MsgBox "An error occurred while reading the file." + _
 '   vbCrLf + Error$, vbExclamation
  Exit Sub
End Sub

Sub Tourn_DoOpen(fnTourn As String)
' Opens tournament of given filename
Dim DummyTourn As Tournament
  ' Check whether ok to lose open tournament
  If Not Tourn_QueryClose() Then
    ' User doesn't want to carry on
    Exit Sub
  End If
  ' Create a new tournament
  Set DummyTourn = New Tournament
  ' Set its filename
  DummyTourn.Filename = fnTourn
  ' Try to read it
  If Not DummyTourn.File_Read Then
    ' Read error
    MsgBox "Error reading file.", vbExclamation
    Exit Sub
  End If
  Recent_Add fnTourn
  ' Successfull read, use DummyTourn as
  ' OpenTourn
  Set OpenTourn = DummyTourn
  ' Switch to first section
  OpenTourn.Section_Switch OpenTourn.Sections(1).SecID
  ' and display everything
  Display_Update
  Display_Tabs
End Sub

Function Tourn_GetSave() As Boolean
' Saves the open tournament, returns True if save went OK,
' else False.
  If OpenTourn.Saved Then
    ' The file has been saved already
    Tourn_GetSave = Tourn_DoSave()
  Else
    ' Do as 'Save As...', as the tournament has never been
    ' saved before so a filename needs to be picked.
    Tourn_GetSave = Tourn_SaveAs()
  End If
End Function

Function Tourn_DoSave() As Boolean
' Actually does the save, returns True if successfull
' Displays dialog box with error if necessary
  If OpenTourn.File_Write() = False Then
    ' Error whilst saving.
    MsgBox "The file could not be saved.", vbInformation
    ' So return false
    Tourn_DoSave = False
  Else
    ' Saved to file ok
    ' Add to recently-used list
    Recent_Add OpenTourn.Filename
    Recent_Write
    ' Return True
    Tourn_DoSave = True
  End If
End Function

Sub Tourn_Save()
' Saves the tournament, does not return a value
Dim DummyValue As Boolean
  DummyValue = Tourn_GetSave
  ' DummyValue is not used
End Sub

Function Tourn_SaveAs() As Boolean
' Prompts the user for a filename for the tournament, then
' saves it using that filename. Returns True if the save was
' successful
  If OpenTourn.Saved = False Then
    ' Show Tournament Editor dialog
    OpenTourn.Tourn_Edit
    If dlgTournEditor.Cancelled Then Exit Function
  End If
  With frmMain.cdgMain
    ' Set the Common Dialog options
    .DialogTitle = "Save tournament as..."
    ' Flags of the dialog:
    '   Filename must have a valid path +
    '   The 'Read Only' checkbox is hidden
    .Flags = cdlOFNPathMustExist + cdlOFNHideReadOnly
    ' Raise an error if user clicks cancel
    .CancelError = True
    ' Error handler
    On Error GoTo Tourn_SaveAs_Error
    ' And show a 'Save file...' type dialog
    .ShowSave
    ' Set the tournament filename to be the one chosen
    OpenTourn.Filename = .Filename
    ' Return whether the save went ok...
    Tourn_SaveAs = Tourn_DoSave()
  End With
  Exit Function
Tourn_SaveAs_Error:
  Tourn_SaveAs = False
  Exit Function
End Function

Function Tourn_QueryClose() As Boolean
' Returns true if it is ok to close the tournament
' eg checks whether changes have been made since last
' save.
Dim retID As Integer, sMessage As String, ReturnValue As Integer
  If OpenTourn.Changed Then
    ' The tourn has been changed since last save,
    ' so ask whether user wants to cancel operation,
    ' (eg close app), save the tourn now then carry
    ' on with operation, or carry on and lose changes
    ' made.
    sMessage = "The tournament has been changes since " + _
      "it was last saved. " + vbCrLf + "Do you wish to save the changes " + _
      "made before continuing?"
    retID = MsgBox(sMessage, vbYesNoCancel + vbInformation, "Tournament Controller")
    Select Case retID
      Case vbYes:     ' Save changes before carrying on
        Tourn_QueryClose = Tourn_GetSave()
      Case vbNo:      ' Continue without saving changes
        Tourn_QueryClose = True
      Case vbCancel:  ' Do NOT continue
        Tourn_QueryClose = False
    End Select
  Else
    ' No changes made since last save, so Ok to carry on
    Tourn_QueryClose = True
  End If
End Function

Sub Display_AboutBox()
' Shows the About box
  Load frmAbout
  frmAbout!lblBuild.Caption = _
    "Build: " + Format$(App.Revision, "0000")
  CenterForm frmAbout
  frmAbout.Show vbModal
End Sub

Sub Recent_Read()
' Reads the recently-used-files list from the ini file
Dim numRec As Integer, sFile As String
  ' Clear the contents of the RecentFiles collection
  Clear RecentFiles
  ' Read the files from the ini
  For numRec = 1 To 4
    sFile = iniRead("TC", "RecentFile" + Format$(numRec))
    If sFile <> "" Then
      ' If this is a file, add a new item to the collection
      RecentFiles.Add sFile, sFile
    End If
  Next
End Sub
Sub Recent_Open(numFile As Integer)
' The menu item was clicked
  Tourn_DoOpen RecentFiles(numFile)
End Sub
Sub Recent_Write()
' Writes the recently-used-files list to the ini file
Dim numRec As Integer, sWrite As String
  For numRec = 1 To 4
    If numRec <= RecentFiles.Count Then
      sWrite = RecentFiles(numRec)
    Else
      sWrite = ""
    End If
    iniWrite "TC", "RecentFile" + Format$(numRec), sWrite
  Next
End Sub
Sub Recent_Add(sNewFile As String)
' Adds a filename to the recently-used-files list
Dim numRec As Integer
  ' Check to see whether it is already in the list
  For numRec = 1 To RecentFiles.Count
    If RecentFiles(numRec) = sNewFile Then
      ' If it is, then remove it
      RecentFiles.Remove numRec
      Exit For
    End If
  Next
  If RecentFiles.Count > 0 Then
    ' Add new filename before first item (ie At top)
    RecentFiles.Add sNewFile, sNewFile, 1
  Else
    ' Add it to the collection
    RecentFiles.Add sNewFile, sNewFile
  End If
  If RecentFiles.Count > 4 Then
    RecentFiles.Remove 5
  End If
  frmMain.Recent_Show
End Sub


Sub Display_Tabs()
' Displays the sections tabs
Dim vSection As Variant
  With frmMain.tabSections
    If .Tabs <> OpenTourn.Sections.Count Then
      ' Only change number of tabs if
      ' necessary
      .Tabs = OpenTourn.Sections.Count
    End If
    For Each vSection In OpenTourn.Sections
      .TabCaption(vSection.TabNumber) = vSection.Name
    Next
  End With
End Sub

Sub Display_Section()
' Displays the active section
  ' Clear the box
  ' Set the width and height of the section box
  frmMain.picSection.Cls
  OpenTourn.Section_DisplaySetSize frmMain.picSection
  ' Update the scroll bars
  Display_ScrollBars
  ' Display the section
  OpenTourn.Section_Display frmMain.picSection
  ' Show the current player selection box
  Display_ShowCurrentPlayer
End Sub
Sub Display_ShowCurrentPlayer()
  OpenTourn.Section_ShowCurrentPlayer frmMain.picSection
End Sub
Sub Display_Caption()
' Displays the caption of the main form
  frmMain.Caption = OpenTourn.Name + " - Tournament Controller"
End Sub
Sub Display_Update()
' Updates all of the display
'  Display_Tabs    ' Updates the section tabs
  Display_Caption ' Update the caption for the form
  Display_Section ' Updates the active section
End Sub

Sub Display_ScrollBars()
' Updates the scroll bars
  With frmMain
    'Display_ShowScrollBar .picHolder.ScaleWidth, .picSection.Width, _
       .scrSectHorz
    'Display_ShowScrollBar .picHolder.ScaleHeight, .picSection.Height, _
       .scrSectVert
End With
End Sub
Sub Display_ShowScrollBar(iViewSize As Integer, iRealSize As Integer, vScrollBar As Variant)
' Show a scroll bar
Dim iDiffSize As Integer
  iDiffSize = iRealSize - iViewSize ' Difference in size
  If iDiffSize > 0 Then
    ' Some of the SectionSize is covered up
    With vScrollBar
      .Enabled = True ' Enable the scroll bar
      ' Min value is 0
      .Min = 0
      ' Max value is difference in size
      .Max = iDiffSize
      ' Set changes
      .SmallChange = Int(iDiffSize / 5) + 1
      .LargeChange = iDiffSize
      ' And current value to 0
      .Value = 0
    End With
  Else
    ' Disable the horizontal scroller
    vScrollBar.Enabled = False
  End If
End Sub
Sub Display_SectionPosition()
' Moves the section box according to value of
' scroll bars
  With frmMain
    ' X position according to horz scrollbar value
    If .scrSectHorz.Enabled Then .picSection.Left = -.scrSectHorz.Value
    ' Y position according to vert scrollbar value
    If .scrSectVert.Enabled Then .picSection.Top = -.scrSectVert.Value
  End With
End Sub


Function GetGradSysName(GradSystem As Integer) As String
' Returns the name of given grading system
  If GradSystem = EloSystem Then
    GetGradSysName = "Elo"
  Else
    GetGradSysName = "BCF"
  End If
End Function

Sub Display_RememberPos()
  SavedX = frmMain.picSection.Left
  SavedY = frmMain.picSection.Top
End Sub

Sub Display_RecallPos()
  frmMain.picSection.Left = SavedX
  frmMain.picSection.Top = SavedY
End Sub
