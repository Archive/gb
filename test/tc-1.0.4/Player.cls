VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Player"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'  Tournament Controller, a tool for chess tournament organisers
'  Copyright (C) 1997-99 Joe Orton <joe@orton.demon.co.uk>
'
'  This program is free software; you can redistribute it and/or modify
'  it under the terms of the GNU General Public License as published by
'  the Free Software Foundation; either version 2 of the License, or
'  (at your option) any later version.
'
'  This program is distributed in the hope that it will be useful,
'  but WITHOUT ANY WARRANTY; without even the implied warranty of
'  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'  GNU General Public License for more details.
'
'  You should have received a copy of the GNU General Public License
'  along with this program; if not, write to the Free Software
'  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
'

' 'Player' Class

Option Explicit

' *** Class Properties **

Public PlayID As Long ' Player ID
Attribute PlayID.VB_VarUserMemId = 0
Public SecID As Long  ' Section ID
Public Number As Integer ' Player Number
Public Surname As String ' Surname of player
Public FName As String   ' First name of player
Public Club As String    ' Player's club
Public Grade As Integer  ' Player's grade (Elo or BCF)
Public numPlayed As Integer ' Number of games completed
Public numGames As Integer  ' Number of games stored
Public Games As New Collection  ' Games

' *** Temporary Properties **

Public Points As Single    ' Total Points
Public GradPerf As String ' Grading Performance
Public GradAvg As String  ' Grading Average

' *** Class Functions and Methods **

Sub File_Write(fh As Integer)
' Writes the player to file using given file handle
Dim numGame As Integer
  ' Start marker
  Print #fh, "PLAYER"
  ' Write the fields, converting to strings if nec.
  Print #fh, CStr(PlayID)
  Print #fh, CStr(SecID)
  Print #fh, CStr(Number)
  Print #fh, Surname
  Print #fh, FName
  Print #fh, Club
  Print #fh, CStr(Grade)
  Print #fh, CStr(numPlayed)
  Print #fh, CStr(numGames)
  ' Write the games
  For numGame = 1 To numGames
    Games(numGame).File_Write fh
  Next
  ' End marker
  Print #fh, "ENDPLAYER"
End Sub

Function File_Read(fh As Integer) As Boolean
' Reads the player from a file using given file handle
' Returns true if successfull
Dim sRead As String, numGame As Integer, NewGame As Game
  ' Got a valid start marker?
  If ReadLine(fh) <> "PLAYER" Then
    ' No valid start marker - return false
    File_Read = False
    Exit Function
  End If
  ' Read fields, converting to numbers if necessary
  PlayID = Val(ReadLine(fh))
  SecID = Val(ReadLine(fh))
  Number = Val(ReadLine(fh))
  Surname = ReadLine(fh)
  FName = ReadLine(fh)
  Club = ReadLine(fh)
  Grade = Val(ReadLine(fh))
  numPlayed = Val(ReadLine(fh))
  numGames = Val(ReadLine(fh))
  ' Now read the games
  Clear Games ' Clear the collection ready to be filled...
  For numGame = 1 To numGames
    Set NewGame = New Game ' Create a new game object
    ' Now try to read it
    If NewGame.File_Read(fh) Then
      ' Read game successfully
      Games.Add NewGame, CStr(numGame)
    Else
      ' Read error
      File_Read = False
      Exit Function
    End If
  Next
  ' Got an valid end marker?
  If ReadLine(fh) = "ENDPLAYER" Then
    ' Valid end marker - so return True
    File_Read = True
  Else
    ' No valid end marker - return false
    File_Read = False
  End If
End Function

Sub Display(ByRef vObject As Variant)
' Displays the player on vObject
Dim numGame As Integer, XPos As Integer
  ShowCell vObject, 0, 39, 3, Number, True, 2, vbWhite
  ShowCell vObject, 40, 69, 5, Surname, False, 0, vbWhite
  ShowCell vObject, 110, 69, 5, FName, False, 0, vbWhite
  ShowCell vObject, 180, 99, 5, Club, False, 0, vbWhite
  ShowCell vObject, 280, 39, 5, Grade, False, 2, vbWhite
  ' Games Info
  For numGame = 1 To numGames
    ' Print Result
    XPos = 320 + (GameResWidth + GameOppWidth) * (numGame - 1)
    ShowCell vObject, XPos, GameResWidth, 2, Games(numGame).ResultText(), False, 1, vbWhite
    ' Print Opponent number
    XPos = XPos + GameResWidth
    ShowCell vObject, XPos, GameOppWidth, 2, Games(numGame).OppText(), False, 0, vbWhite
  Next
  XPos = XPos + GameOppWidth
  ' Now show points/perf/avg
  ShowCell vObject, XPos, 39, 3, Format$(Points, "#0.0"), True, 1, vbWhite
  ShowCell vObject, XPos + 40, 39, 5, GradPerf, False, 2, vbWhite
  ShowCell vObject, XPos + 80, 39, 5, GradAvg, False, 2, vbWhite
End Sub

Sub doPrint(Mode As Integer)
' Displays the player on Printer
Dim numGame As Integer, XPos As Integer
 ' Display Printer
 ' Exit Sub
  PrintCell Printer, 0, 39, 3, Number, True, 2, vbWhite
  PrintCell Printer, 40, 89, 5, Surname, False, 0, vbWhite
  PrintCell Printer, 130, 69, 5, FName, False, 0, vbWhite
  PrintCell Printer, 200, 79, 5, Club, False, 0, vbWhite
  PrintCell Printer, 280, 39, 5, Grade, False, 2, vbWhite
  ' Games Info
  If Mode = PrintFull Then
    ' Print everything
    For numGame = 1 To numGames
      ' Print Result
      XPos = 320 + (GameResWidth + GameOppWidth) * (numGame - 1)
      PrintCell Printer, XPos, GameResWidth, 2, Games(numGame).ResultText(), False, 1, vbWhite
      ' Print Opponent number
      XPos = XPos + GameResWidth
      PrintCell Printer, XPos, GameOppWidth, 2, Games(numGame).OppText(), False, 0, vbWhite
    Next
    XPos = XPos + GameOppWidth
    ' Now show points/perf/avg
    PrintCell Printer, XPos, 39, 3, Format$(Points, "#0.0"), True, 1, vbWhite
    PrintCell Printer, XPos + 40, 39, 5, GradPerf, False, 2, vbWhite
    PrintCell Printer, XPos + 80, 39, 5, GradAvg, False, 2, vbWhite
  End If
End Sub


Sub ChangeRounds(numRounds As Integer)
' The number of rounds in the section has changed
' Make this player have the right number of 'game' objects
Dim numCount As Integer, NewGame As Game
  If numRounds > numGames Then
    ' More rounds than games, so add more games
    For numCount = numGames To numRounds
      Set NewGame = New Game
      Games.Add NewGame
    Next
  ElseIf numRounds < numGames Then
    ' Less rounds than games, so delete last games
    For numCount = numRounds + 1 To numGames
      ' Delete the extra game object
      Games.Remove numRounds + 1
    Next
  End If
  numGames = numRounds
End Sub
