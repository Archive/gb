
Tournament Controller
=====================

http://www.orton.demon.co.uk/projects/tc.html

A tool for chess tournament organisers. Supports the British
Chess Federation and Elo grading systems.

This program is licensed under the GNU GPL, see the 
Copying.txt file for details.

Please let me know if you find this program useful. Since the
program is no longer under active development, if you have 
fixed bugs or added new features, feel free to take over as
new maintainer.

Joe Orton
<joe@orton.demon.co.uk>
