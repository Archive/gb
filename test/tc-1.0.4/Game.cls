VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Game"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'  Tournament Controller, a tool for chess tournament organisers
'  Copyright (C) 1997-99 Joe Orton <joe@orton.demon.co.uk>
'
'  This program is free software; you can redistribute it and/or modify
'  it under the terms of the GNU General Public License as published by
'  the Free Software Foundation; either version 2 of the License, or
'  (at your option) any later version.
'
'  This program is distributed in the hope that it will be useful,
'  but WITHOUT ANY WARRANTY; without even the implied warranty of
'  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'  GNU General Public License for more details.
'
'  You should have received a copy of the GNU General Public License
'  along with this program; if not, write to the Free Software
'  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
'

' 'Game' Class

Option Explicit

' *** Class Properties **

Public OppID As Long      ' Opponents Player ID
Public Result As Single  ' Result of Game
Public Bye As Boolean     ' Game was a bye?

' *** Temporary Properties **

' Opponent number - temporarily stored for speed
Public OppNumber As Integer

' *** Class Functions and Methods **

Sub File_Write(fh As Integer)
' Writes the game to file using given file handle
  ' Start marker
  Print #fh, "GAME"
  ' Write the properties, converting to strings if nec.
  Print #fh, CStr(OppID)
  Print #fh, CStr(Result)
  Print #fh, CStr(Bye)
  Print #fh, "ENDGAME"
End Sub

Function File_Read(fh As Integer)
' Reads the game from file using given file handle
  ' Got a valid start marker?
  If ReadLine(fh) <> "GAME" Then Exit Function
  ' Read the properties, converting to numbers if nec.
  OppID = Val(ReadLine(fh))
  Result = Val(ReadLine(fh))
  Bye = CBool(ReadLine(fh))
  ' Got a valid End marker?
  If ReadLine(fh) = "ENDGAME" Then
    ' Valid end marker - return True
    File_Read = True
  Else
    ' No valid end marker - return false
    File_Read = False
  End If
End Function

Public Function ResultText() As String
' Returns the result as text
  Select Case Result
    Case 0.5: ResultText = "="
    Case 1: ResultText = "+"
    Case Else: ResultText = "-"
  End Select
End Function

Public Function OppText() As String
' Returns Opponent number or Bye
  If Bye Then
    OppText = "Bye"
  Else
    OppText = CStr(OppNumber)
  End If
End Function
