VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Section"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'  Tournament Controller, a tool for chess tournament organisers
'  Copyright (C) 1997-99 Joe Orton <joe@orton.demon.co.uk>
'
'  This program is free software; you can redistribute it and/or modify
'  it under the terms of the GNU General Public License as published by
'  the Free Software Foundation; either version 2 of the License, or
'  (at your option) any later version.
'
'  This program is distributed in the hope that it will be useful,
'  but WITHOUT ANY WARRANTY; without even the implied warranty of
'  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'  GNU General Public License for more details.
'
'  You should have received a copy of the GNU General Public License
'  along with this program; if not, write to the Free Software
'  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

' 'Section' Class

Option Explicit

' *** Class Properties **

Public SecID As Long
Attribute SecID.VB_VarUserMemId = 0
Public Name As String
Public GradSystem As Integer
Public Rounds As Integer
Public BaseNum As Integer
Public TabNumber As Integer

' *** Temporary Properties **

Public numPlayers As Integer

' *** Class Functions and Methods **

Sub File_Write(fh As Integer)
' Writes the section to file using given file handle
  ' Start marker
  Print #fh, "SECTION"
  ' Write the properties, converting to strings if nec.
  Print #fh, CStr(SecID)
  Print #fh, Name
  Print #fh, CStr(TabNumber)
  Print #fh, CStr(GradSystem)
  Print #fh, CStr(Rounds)
  Print #fh, CStr(BaseNum)
  ' End marker
  Print #fh, "ENDSECTION"
End Sub

Function File_Read(fh As Integer) As Boolean
' Reads the section from a file using given handle
' Returns True if read successful, else False
  ' Got a valid start marker?
  If ReadLine(fh) <> "SECTION" Then Exit Function
  ' Read the properties, converting to numbers if nec.
  SecID = Val(ReadLine(fh))
  Name = ReadLine(fh)
  TabNumber = Val(ReadLine(fh))
  GradSystem = Val(ReadLine(fh))
  Rounds = Val(ReadLine(fh))
  BaseNum = Val(ReadLine(fh))
  ' Got a valid End marker?
  If ReadLine(fh) = "ENDSECTION" Then
    ' Valid end marker - return True
    File_Read = True
  Else
    ' No valid end marker - return false
    File_Read = False
  End If
End Function

Sub DisplayHeaders(ByRef vObject As Variant)
' Displays the column headers on vObject
Dim numGame As Integer, XPos As Integer
  ShowCell vObject, 0, 39, 3, "Num", True, 2, vb3DFace
  ShowCell vObject, 40, 69, 5, "Surname", True, 1, vb3DFace
  ShowCell vObject, 110, 69, 5, "FName", True, 1, vb3DFace
  ShowCell vObject, 180, 99, 5, "Club", True, 1, vb3DFace
  ShowCell vObject, 280, 39, 0, "Grade", True, 1, vb3DFace
  ' Games Info
  For numGame = 1 To Rounds
    XPos = 320 + (GameResWidth + GameOppWidth) * (numGame - 1)
    ShowCell vObject, XPos, GameResWidth + GameOppWidth, 3, "R" + CStr(numGame), True, 1, vb3DFace
  Next
  XPos = XPos + GameOppWidth + GameResWidth
  ' Now show points/perf/avg
  ShowCell vObject, XPos, 39, 3, "Pnts", True, 1, vb3DFace
  ShowCell vObject, XPos + 40, 39, 5, "Perf", True, 1, vb3DFace
  ShowCell vObject, XPos + 80, 39, 5, "Avg", True, 1, vb3DFace
End Sub
Sub PrintHeaders(Mode As Integer)
' Prints the column headers on Printer
Dim numGame As Integer, XPos As Integer
  ' Titles
  Printer.ScaleMode = vbUser
  Printer.Scale (0, 0)-(GetPrintWidth(Mode), Printer.ScaleHeight)
  Printer.Print ""
  Printer.Print ""
  PrintFont Printer, "Arial", 14, True
  PrintCenter Printer, Name
  Printer.Print
  PrintFont Printer, "Arial", 10, False
  ' Headers
  PrintCell Printer, 0, 39, 3, "Num", True, 2, vbWhite
  PrintCell Printer, 40, 69, 5, "Surname", True, 1, vbWhite
  PrintCell Printer, 110, 69, 5, "FName", True, 1, vbWhite
  PrintCell Printer, 180, 99, 5, "Club", True, 1, vbWhite
  PrintCell Printer, 280, 39, 0, "Grade", True, 1, vbWhite
  ' Games Info
  If Mode = PrintFull Then
    For numGame = 1 To Rounds
      XPos = 320 + (GameResWidth + GameOppWidth) * (numGame - 1)
      PrintCell Printer, XPos, GameResWidth + GameOppWidth, 3, "R" + CStr(numGame), True, 1, vbWhite
    Next
    XPos = XPos + GameOppWidth + GameResWidth
    ' Now show points/perf/avg
    PrintCell Printer, XPos, 39, 3, "Pnts", True, 1, vbWhite
    PrintCell Printer, XPos + 40, 39, 5, "Perf", True, 1, vbWhite
    PrintCell Printer, XPos + 80, 39, 5, "Avg", True, 1, vbWhite
  End If
End Sub
Sub DisplayGrid(ByRef vObject As Variant)
' Draws gridlines on the section box
Dim YTop As Integer, YBot As Integer, numPlayer As Integer
Dim iWidth As Integer, numRound As Integer, XPos As Integer
  YTop = 0 'RowHeight
 ' vObject.Line (0, RowHeight + 1)-Step(GetWidth(), 0), vb3DFace
  YBot = HeaderHeight + (RowHeight * numPlayers)
  ' Vertical Gridlines
  vObject.Line (40, YTop)-Step(0, YBot), vb3DShadow
  vObject.Line (110, YTop)-Step(0, YBot), vb3DShadow
  vObject.Line (180, YTop)-Step(0, YBot), vb3DShadow
  vObject.Line (280, YTop)-Step(0, YBot), vb3DShadow
  vObject.Line (320, YTop)-Step(0, YBot), vb3DShadow
  ' Rounds...
  For numRound = 1 To Rounds
    XPos = 320 + (GameResWidth + GameOppWidth) * numRound
    vObject.Line (XPos, YTop)-Step(0, YBot), vb3DShadow
  Next
  vObject.Line (XPos + 40, YTop)-Step(0, YBot), vb3DShadow
  vObject.Line (XPos + 80, YTop)-Step(0, YBot), vb3DShadow
  ' Horizontal Gridlines
  iWidth = GetWidth()
  For numPlayer = 1 To numPlayers + 1
    vObject.Line (0, -1 + HeaderHeight + RowHeight * (numPlayer - 1))-Step(iWidth, 0), vb3DShadow
  Next
End Sub

Function GetHeight() As Integer
' Returns the height of the section
Dim iCount As Integer
  ' iCount= number of players in active section
  iCount = numPlayers
  ' Ensure space for 'No players in section' message is left
  If iCount = 0 Then iCount = 1
  GetHeight = HeaderHeight + (RowHeight * iCount)
End Function

Function GetWidth() As Integer
' Returns the width of the section
  GetWidth = 440 + (Rounds * (GameResWidth + GameOppWidth))
End Function

Function GetPrintWidth(Mode As Integer) As Integer
' Returns width of the section as printed
  If Mode = PrintFull Then
    ' Full listing
    GetPrintWidth = GetWidth
  Else
    ' Simple listing
    GetPrintWidth = 420
  End If
End Function
