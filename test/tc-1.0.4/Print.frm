VERSION 5.00
Begin VB.Form dlgPrint 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Print"
   ClientHeight    =   1620
   ClientLeft      =   2910
   ClientTop       =   4620
   ClientWidth     =   4950
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1620
   ScaleWidth      =   4950
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraList 
      Caption         =   "List..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1200
      Left            =   75
      TabIndex        =   5
      Top             =   60
      Width           =   2370
      Begin VB.OptionButton optListSimple 
         Caption         =   "Simple player listing (not including games)"
         Height          =   375
         Left            =   135
         TabIndex        =   7
         Top             =   660
         Width           =   2085
      End
      Begin VB.OptionButton optListFull 
         Caption         =   "Full player listing (including games)"
         Height          =   375
         Left            =   135
         TabIndex        =   6
         Top             =   210
         Value           =   -1  'True
         Width           =   2115
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   3735
      TabIndex        =   4
      Top             =   1200
      Width           =   1100
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2565
      TabIndex        =   3
      Top             =   1200
      Width           =   1100
   End
   Begin VB.Frame fraPrint 
      Caption         =   "Print..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   2625
      TabIndex        =   0
      Top             =   60
      Width           =   2220
      Begin VB.OptionButton optScopeSingle 
         Caption         =   "Only the active section"
         Height          =   255
         Left            =   135
         TabIndex        =   2
         Top             =   600
         Width           =   2000
      End
      Begin VB.OptionButton optScopeAll 
         Caption         =   "Whole tournament"
         Height          =   255
         Left            =   135
         TabIndex        =   1
         Top             =   300
         Value           =   -1  'True
         Width           =   2000
      End
   End
End

Attribute VB_Name = "dlgPrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'  Tournament Controller, a tool for chess tournament organisers
'  Copyright (C) 1997-99 Joe Orton <joe@orton.demon.co.uk>
'
'  This program is free software; you can redistribute it and/or modify
'  it under the terms of the GNU General Public License as published by
'  the Free Software Foundation; either version 2 of the License, or
'  (at your option) any later version.
'
'  This program is distributed in the hope that it will be useful,
'  but WITHOUT ANY WARRANTY; without even the implied warranty of
'  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'  GNU General Public License for more details.
'
'  You should have received a copy of the GNU General Public License
'  along with this program; if not, write to the Free Software
'  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
'
Option Explicit
' Cancelled is True if the user clicked Print
' else false
Public Cancelled As Boolean
' The mode and scope selected...
Public PrintScope As Integer
Public PrintMode As Integer

Private Sub cmdCancel_Click()
' Cancel button clicked
  Cancelled = True
  ' Hide the window
  Visible = False
End Sub

Private Sub cmdPrint_Click()
' Print button clicked
Dim numOpt As Integer
  ' Set the print mode
  ' Find the selected option box
  PrintScope = IIf(optScopeAll.Value, PrintAll, PrintSingle)
  PrintMode = IIf(optListFull.Value, PrintFull, PrintSimple)
  ' Was not cancelled
  Cancelled = False
  ' Hide the window
  Visible = False
End Sub

