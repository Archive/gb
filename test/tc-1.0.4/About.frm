VERSION 5.00
Begin VB.Form frmAbout 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "About"
   ClientHeight    =   2190
   ClientLeft      =   4020
   ClientTop       =   3180
   ClientWidth     =   4470
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2190
   ScaleWidth      =   4470
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1 'True
      Caption         =   "Close"
      Default         =   -1  'True
      Height          =   360
      Left            =   3330
      TabIndex        =   1
      Top             =   495
      Width           =   1095
   End
   Begin VB.TextBox txtInfo 
      BackColor       =   &H00C0C0C0&
      Height          =   1080
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   1035
      Width           =   4190
   End
   Begin VB.Label lblBuild 
      Alignment       =   2  'Center
      Caption         =   "Build: 0000"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   3360
      TabIndex        =   2
      Top             =   135
      Width           =   990
   End
   Begin VB.Image imgLogo 
      Height          =   930
      Left            =   30
      Picture         =   "About.frx":0000
      Top             =   45
      Width           =   3195
   End
End

Attribute VB_Name = "frmAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'  Tournament Controller, a tool for chess tournament organisers
'  Copyright (C) 1997-99 Joe Orton <joe@orton.demon.co.uk>
'
'  This program is free software; you can redistribute it and/or modify
'  it under the terms of the GNU General Public License as published by
'  the Free Software Foundation; either version 2 of the License, or
'  (at your option) any later version.
'
'  This program is distributed in the hope that it will be useful,
'  but WITHOUT ANY WARRANTY; without even the implied warranty of
'  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'  GNU General Public License for more details.
'
'  You should have received a copy of the GNU General Public License
'  along with this program; if not, write to the Free Software
'  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
'
Option Explicit

Private Sub cmdClose_Click()
  Unload frmAbout
End Sub

Private Sub Form_Load()
  txtInfo.Text = "Tournament Controller v " + TC_VERSION + vbCrLf _
    + "Copyright �1997-99 Joe Orton <joe@orton.demon.co.uk>" + vbCrLf _
    + "Tournament Controller comes with ABSOLUTELY NO WARRANTY."
End Sub

