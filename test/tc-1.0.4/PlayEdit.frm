VERSION 5.00
Begin VB.Form dlgPlayerEditor 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Player Editor"
   ClientHeight    =   1815
   ClientLeft      =   1830
   ClientTop       =   2490
   ClientWidth     =   5370
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1815
   ScaleWidth      =   5370
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox picTC 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   15
      Picture         =   "PlayEdit.frx":0000
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   1335
      Width           =   480
   End
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   4065
      TabIndex        =   12
      Top             =   1395
      Width           =   1245
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save Player"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2715
      TabIndex        =   11
      Top             =   1395
      Width           =   1275
   End
   Begin VB.TextBox txtNumber 
      BackColor       =   &H00C0C0C0&
      Height          =   300
      Left            =   3540
      Locked          =   -1  'True
      TabIndex        =   10
      Top             =   915
      Width           =   840
   End
   Begin VB.TextBox txtGrade 
      Height          =   315
      Left            =   810
      TabIndex        =   7
      Top             =   915
      Width           =   1200
   End
   Begin VB.TextBox txtClub 
      Height          =   315
      Left            =   810
      TabIndex        =   5
      Top             =   495
      Width           =   4470
   End
   Begin VB.TextBox txtFName 
      Height          =   315
      Left            =   3540
      TabIndex        =   3
      Top             =   75
      Width           =   1740
   End
   Begin VB.TextBox txtSurname 
      Height          =   315
      Left            =   810
      TabIndex        =   1
      Top             =   75
      Width           =   1740
   End
   Begin VB.Label lblInfo 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Information box"
      Height          =   465
      Left            =   540
      TabIndex        =   14
      Top             =   1320
      Width           =   2115
   End
   Begin VB.Label lblNumber 
      AutoSize        =   -1  'True
      Caption         =   "Number:"
      Height          =   195
      Left            =   2880
      TabIndex        =   9
      Top             =   975
      Width           =   600
   End
   Begin VB.Label lblGradSys 
      AutoSize        =   -1  'True
      Caption         =   "Elo"
      Height          =   195
      Left            =   2085
      TabIndex        =   8
      Top             =   975
      Width           =   225
   End
   Begin VB.Label lblGrade 
      AutoSize        =   -1  'True
      Caption         =   "Grade:"
      Height          =   195
      Left            =   270
      TabIndex        =   6
      Top             =   975
      Width           =   480
   End
   Begin VB.Label lblClub 
      AutoSize        =   -1  'True
      Caption         =   "Club:"
      Height          =   195
      Left            =   390
      TabIndex        =   4
      Top             =   540
      Width           =   360
   End
   Begin VB.Label lblFName 
      AutoSize        =   -1  'True
      Caption         =   "First Name:"
      Height          =   195
      Left            =   2685
      TabIndex        =   2
      Top             =   135
      Width           =   795
   End
   Begin VB.Label lblSurname 
      AutoSize        =   -1  'True
      Caption         =   "Surname:"
      Height          =   195
      Left            =   75
      TabIndex        =   0
      Top             =   135
      Width           =   675
   End
End

Attribute VB_Name = "dlgPlayerEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'  Tournament Controller, a tool for chess tournament organisers
'  Copyright (C) 1997-99 Joe Orton <joe@orton.demon.co.uk>
'
'  This program is free software; you can redistribute it and/or modify
'  it under the terms of the GNU General Public License as published by
'  the Free Software Foundation; either version 2 of the License, or
'  (at your option) any later version.
'
'  This program is distributed in the hope that it will be useful,
'  but WITHOUT ANY WARRANTY; without even the implied warranty of
'  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'  GNU General Public License for more details.
'
'  You should have received a copy of the GNU General Public License
'  along with this program; if not, write to the Free Software
'  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
'

Option Explicit

' The EditMode property sets which mode the dialog
' box is in. When EditMode is true, the dialog is
' in Edit Mode, if false, then in Add Mode.
Public EditMode As Boolean
' Edit Mode: The Save button closes the dialog
'    when clicked.
' Add Mode:  The Save button does not close the
'    dialog when clicked, but clears itself
'    allowing another player to be added.

' Cancelled is False if the dialog was closed
' by clicking the 'Save/Add Player' Button,
' else True
Public Cancelled As Boolean

Function InvalidData() As Boolean
' Returns true if any invalid data has been
' entered
Dim RetValue As Boolean, Message As String
  RetValue = True
  If txtSurname = "" Then
    txtSurname.SetFocus
    Message = "You must specify a surname"
  ElseIf txtFName = "" Then
    txtFName.SetFocus
    Message = "You must specify a first name"
  ElseIf Val(txtGrade) = 0 Or Val(txtGrade) > 5000 Then
    txtGrade.SetFocus
    Message = "You must specify a valid grade."
  Else
    RetValue = False
  End If
  If RetValue Then
    ' There is invalid data, show specified
    ' error message
    MsgBox Message, vbExclamation
  End If
  InvalidData = RetValue
End Function

Public Sub ClearFields()
' Clears the fields ready to enter another player
  txtSurname = ""
  txtFName = ""
  txtClub = ""
  txtGrade = ""
  txtNumber = ""
End Sub

Private Sub cmdClose_Click()
' Hide the dialog, but don't save the player
  Cancelled = True
  ' For entering another player when box redisplayed
  txtSurname.SetFocus
  ' Hide it
  Visible = False
End Sub

Private Sub cmdClose_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
' Mouse is moved over the 'Close' button
  lblInfo = "Close this window"
End Sub

Private Sub cmdSave_Click()
' 'Save/Edit Player' button clicked
' Hides the dialog box, saving the player
  ' Put the focus back to the Surname box
  ' For entering another player when box redisplayed
  If InvalidData() Then
    ' Cannot exit now
    Exit Sub
  End If
  txtSurname.SetFocus
  ' Will save the player
  Cancelled = False
  ' Hide the dialog
  Visible = False
End Sub

Private Sub cmdSave_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
' Mouse is moved over the 'Add/Save Player' button
  If EditMode Then
    ' In edit mode
    lblInfo = "Save the player to the section"
  Else
    ' In Add mode
    lblInfo = "Add the player to the section"
  End If
End Sub

Private Sub txtClub_GotFocus()
' When the 'Club' text box gets the focus
  lblInfo = "Enter the player's club here"
End Sub

Private Sub txtFName_GotFocus()
' When the 'First Name' text box gets the focus
  lblInfo = "Enter the player's first name here"
End Sub

Private Sub txtGrade_GotFocus()
' When the 'Grade' text box gets the focus
  lblInfo = "Enter the player's grade here"
End Sub

Private Sub txtNumber_GotFocus()
' When the 'Number' text box gets the focus
  lblInfo = "The player's number cannot be changed here"
End Sub

Private Sub txtSurname_GotFocus()
' When the 'Surname' text box gets the focus
  lblInfo = "Enter the player's surname here"
End Sub
