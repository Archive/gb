VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Tournament"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'  Tournament Controller, a tool for chess tournament organisers
'  Copyright (C) 1997-99 Joe Orton <joe@orton.demon.co.uk>
'
'  This program is free software; you can redistribute it and/or modify
'  it under the terms of the GNU General Public License as published by
'  the Free Software Foundation; either version 2 of the License, or
'  (at your option) any later version.
'
'  This program is distributed in the hope that it will be useful,
'  but WITHOUT ANY WARRANTY; without even the implied warranty of
'  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'  GNU General Public License for more details.
'
'  You should have received a copy of the GNU General Public License
'  along with this program; if not, write to the Free Software
'  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
'

' 'Tournament' Class

Option Explicit

' *** Class Properties **

Public Name As String             ' Tournament Name
Public Filename As String         ' Filename of Tournament
Public ShortName As String        ' Short Tournament Name
Public Changed As Boolean         ' Changed since last save?
Public Saved As Boolean           ' Ever been saved?
Public Sections As New Collection ' Sections in Tournament
Public Players As New Collection  ' All Players in Tournament

Public LastSecID As Long   ' Last used SecID
Public LastPlayID As Long  ' Last used PlayID

' *** Temporary Properties **

' ** The Active Section **
' The Section which is currently being
' displayed

' ActiveSecID is SecID of active Section
Public ActiveSecID As Long
' ActiveSection points to the active section in Sections
Public ActiveSection As Section
' ActivePlayers contains all players in current section
Public ActivePlayers As New Collection

' ** The Current Player **
' Current Player is the currently selected player
Public CurrentPlayerIndex As Integer
Public CurrentPlayer As Player
Public CurrentPlayerLast As Integer ' Last CPIndex

' *** Class Functions and Methods **

Function File_Write() As Boolean
' Writes the tournament to the file,
Dim fh As Integer, vObject As Variant
  ' If an error occurs anywhere in the writing process...
  On Error GoTo Tourn_File_Write_Error
  ' Open the file for writing with a free file handle
  fh = FreeFile
  Open Filename For Output As fh
  ' Write start marker
  Print #fh, "TCTOURN"
  ' Write tournament properties, converting to
  ' strings if necessary
  Print #fh, Name
  Print #fh, ShortName
  Print #fh, CStr(LastSecID)
  Print #fh, CStr(LastPlayID)
  ' Write the sections
  Print #fh, CStr(Sections.Count) ' number of sections
  For Each vObject In Sections
    vObject.File_Write fh
  Next
  ' Write the players (+games, which are written by the players)
  Print #fh, CStr(Players.Count) ' number of players
  For Each vObject In Players
    vObject.File_Write fh
  Next
  ' Write end marker
  Print #fh, "ENDTOURN"
  ' And close the file
  Close fh
  ' Set flags
  Saved = True
  Changed = False
  ' No errors occured, so return True
  File_Write = True
  Exit Function

Tourn_File_Write_Error:
' An error occurred somewhere in the writing process
  ' Close the file
  Close fh
  ' Return false
  File_Write = False
  Exit Function
End Function

Function File_Read() As Boolean
' Read the tournament from the file,
' Returns True if success
Dim fh As Integer, NewSection As Section, NewPlayer As Player
Dim numCount As Integer, numTotal As Integer
  ' If an error occurs anywhere in the reading process...
  On Error GoTo Tourn_File_Read_Error
  ' Open the file for reading with a free file handle
  fh = FreeFile
  Open Filename For Input As fh
  ' Write start marker
  If ReadLine(fh) <> "TCTOURN" Then
    ' No valid start marker - return false
    File_Read = False
    Close fh
    Exit Function
  End If
  ' Read tournament properties, converting from
  ' strings if necessary
  Name = ReadLine(fh)
  ShortName = ReadLine(fh)
  LastSecID = Val(ReadLine(fh))
  LastPlayID = Val(ReadLine(fh))
  ' Read the sections
  numTotal = Val(ReadLine(fh)) ' the number of sections
  Clear Sections
  ' Read in each section
  For numCount = 1 To numTotal
    ' Create a new section object
    Set NewSection = New Section
    ' Try to read the section
    If NewSection.File_Read(fh) Then
      ' Read the section ok, add it to the collection
      Sections.Add NewSection, getSecRef(NewSection.SecID)
    Else
      ' Read error, quit
      File_Read = False
      Close fh
      Exit Function
    End If
  Next
  ' Read the players (and games)
  numTotal = Val(ReadLine(fh)) ' The number of players
  For numCount = 1 To numTotal
    ' Create a new player object
    Set NewPlayer = New Player
    ' Try to read the player
    If NewPlayer.File_Read(fh) Then
      ' Player read ok, add it to the collection
      Players.Add NewPlayer, getPlayerRef(NewPlayer.PlayID)
    Else
      ' Read error, quit
      File_Read = False
      Close fh
      Exit Function
    End If
  Next
  ' Write end marker
  If ReadLine(fh) = "ENDTOURN" Then
    ' File read ok
    File_Read = True
  Else
    ' Read error
    File_Read = False
  End If
  ' And close the file
  Close fh
  ' Set flags
  Saved = True
  Changed = False
  ' No errors occured, so return True
  File_Read = True
  Exit Function

Tourn_File_Read_Error:
' An error occurred somewhere in the writing process
  ' Close the file
  Close fh
  ' Return false
  File_Read = False
  Exit Function
End Function

Sub Section_Add()
' Adds a section and displays Section Editor
Dim NewID As Long
  ' Create a default section
  NewID = Section_New()
  Changed = True
  ' Update the tabs
  Display_Tabs
  ' Switch to it
  Section_Switch NewID
  ' Edit it
  Section_Edit
End Sub

Sub Section_Edit()
' Displays the Section Editor dialog
Dim vPlayer As Variant
  With dlgSectionEditor
    ' Fill in fields
    .txtName = ActiveSection.Name
    .optGradSys(1).Value = (ActiveSection.GradSystem = BCFSystem)
    .optGradSys(2).Value = Not .optGradSys(1).Value
    .txtBaseNum = CStr(ActiveSection.BaseNum)
    .txtRounds = CStr(ActiveSection.Rounds)
    ' Show it
    .Show vbModal
  End With
  If dlgSectionEditor.Cancelled Then
    ' User selected cancel
    Exit Sub
  End If
  ' Save the section properties
  With ActiveSection
    .Name = dlgSectionEditor.txtName
    If dlgSectionEditor.optGradSys(1).Value Then
      .GradSystem = BCFSystem
    Else
      .GradSystem = EloSystem
    End If
    .BaseNum = Val(dlgSectionEditor.txtBaseNum)
    .Rounds = Val(dlgSectionEditor.txtRounds)
    ' Tournament has changed
    Changed = True
  End With
  ' Tell the players that the number of rounds has changed
  For Each vPlayer In ActivePlayers
    vPlayer.ChangeRounds ActiveSection.Rounds
  Next
  ' Update display
  Display_Tabs
  Display_Update
End Sub

Function Section_Create() As Long
' Creates a new section, returns the SecID of new section
' Section is completely blank when created
Dim NewSection As New Section, NewSecID As Long
  ' Create a section object with new ID
  NewSecID = Section_GetNewID()
  NewSection.SecID = NewSecID
  ' Assign a tab number
  NewSection.TabNumber = Sections.Count
  ' Add it to the Section collection
  Sections.Add NewSection, getSecRef(NewSecID)
  Section_Create = NewSecID
  ' Update the tabs - make sure there is one
  ' for this section
  Display_Tabs
End Function

Function getSecRef(SecID As Long) As String
    getSecRef = "SID" + CStr(SecID)
End Function

Function getPlayerRef(PlayID As Long) As String
    getPlayerRef = "PID" + CStr(PlayID)
End Function

Function Section_New() As Long
' Creates a new section and fills in default values
' Returns SecID
Dim NewID As Long
  NewID = Section_Create()
  With Section_Get(NewID)
    ' Create a section so that one can be displayed
    .Name = "New Section"
    .Rounds = 6
    .GradSystem = BCFSystem
    .BaseNum = 100
  End With
  Section_New = NewID
End Function

Function Section_GetNewID() As Long
' Returns a new SecID
  LastSecID = LastSecID + 1
  Section_GetNewID = LastSecID
End Function

Function Section_GetNewNumber() As Integer
' Returns a new Player Number for the active section
Dim Number As Integer
  Number = ActiveSection.BaseNum ' n*100
  ' First player in section must be 101, not 100, so add 1
  ' and number already in section
  Number = Number + ActivePlayers.Count + 1
  Section_GetNewNumber = Number
End Function

Sub Section_Delete()
' Deletes active section
Dim MsgValue As Integer, sMessage As String
Dim vPlayer As Variant, vSection As Variant, numTab As Integer
  If Sections.Count = 1 Then
    ' Cannot delete last section
    sMessage = "You cannot delete the last remaining section."
    MsgBox sMessage, vbExclamation
    Exit Sub
  End If
  ' Check the user really does want to delete the
  ' section and all players in it
  sMessage = "Do you really want to delete this section" + vbCrLf
  sMessage = sMessage + "(" + ActiveSection.Name + ") and all the players "
  sMessage = sMessage + "(" + CStr(ActivePlayers.Count) + ")?"
  MsgValue = MsgBox(sMessage, vbQuestion + vbYesNo)
  If MsgValue = vbYes Then
    ' Yes! Delete the section...
    ' First, remove all the players from the tournament
    For Each vPlayer In ActivePlayers
      Players.Remove getPlayerRef(vPlayer.PlayID)
    Next
    ' Must shift up tab numbers for sections which come
    ' after the section which is being deleted
    numTab = ActiveSection.TabNumber
    For Each vSection In Sections
      If vSection.TabNumber > numTab Then
        vSection.TabNumber = vSection.TabNumber - 1
      End If
    Next
    ' Now, delete the section
    Sections.Remove getSecRef(ActiveSection.SecID)
    ' Tourn has changed
    Changed = True
    ' Update the tabs
    Display_Tabs
    ' Switch to different section
    Section_Switch Sections(1).SecID
  End If
End Sub

Sub Section_GatherPlayers()
' Gets all the active players for the current section
Dim vPlayer As Variant, vSearch As Variant
Dim bInsert As Boolean
  Clear ActivePlayers
  ' Loop through all players looking for ones in
  ' the active section
  For Each vPlayer In Players
    If vPlayer.SecID = ActiveSecID Then
      ' This player is in active section
      If ActivePlayers.Count = 0 Then
        ' Active players collection empty
        ' Add the player
        ActivePlayers.Add vPlayer, getPlayerRef(vPlayer.PlayID)
      Else
        ' Insertion sort in the player
        ' Find first player (vSearch) which new player
        ' should come before
        bInsert = False
        For Each vSearch In ActivePlayers
          If vPlayer.Number < vSearch.Number Then
            ' Found earliest insertion point
            ' Insert player BEFORE found player ID
            ActivePlayers.Add vPlayer, getPlayerRef(vPlayer.PlayID), getPlayerRef(vSearch.PlayID)
            bInsert = True
            Exit For
          End If
        Next
        If bInsert = False Then
          ' Could not find earlier player, add at end
          ActivePlayers.Add vPlayer, getPlayerRef(vPlayer.PlayID)
        End If
      End If
      ' Player is now in ActivePlayers
      Player_Calculate vPlayer.PlayID
    End If
  Next
  ActiveSection.numPlayers = ActivePlayers.Count
End Sub

Sub Section_Switch(SecID As Long)
' Make section with given SecID active
  If ActiveSecID = SecID Then
    ' No need to switch to section which is already
    ' active
    Exit Sub
  End If
  ' Set ActiveSection
  ActiveSecID = SecID
  Set ActiveSection = Section_Get(ActiveSecID)
  Section_GatherPlayers
  ' Switch tab
  frmMain.tabSections.Tab = ActiveSection.TabNumber
  ' Display section
  Display_Section
  ' Load the rounds into the list box of the
  ' game entry window
  Game_LoadRounds
End Sub

Sub Section_TabSelect()
' Different tab selected - switch to relevant section
Dim vSection As Variant, TabNumber As Integer
  TabNumber = frmMain.tabSections.Tab
  ' Find section object which is being displayed
  ' on this tab
  For Each vSection In Sections
    If vSection.TabNumber = TabNumber Then
      Section_Switch vSection.SecID
      Exit For
    End If
  Next
End Sub

Function Section_Get(SecID As Long) As Section
' Returns the section given by SecID
  Set Section_Get = Sections(getSecRef(SecID))
End Function

Sub Section_Click(Y As Single)
' The section box has been clicked at y-pos Y
Dim FindIndex As Integer
  If ActivePlayers.Count = 0 Then
    ' No players to select
    Exit Sub
  End If
  FindIndex = Y - HeaderHeight
  FindIndex = Int(FindIndex / RowHeight) + 1
  CurrentPlayerIndex = FindIndex
  Section_SetCurrentPlayer
  Display_ShowCurrentPlayer
End Sub

Sub Section_SetCurrentPlayer()
  ' Set the current player... a bit hacky.
  Dim aPlayer As Variant
  If CurrentPlayerIndex = 0 Then
    ' We have nothing to do
    Exit Sub
  End If
  Set CurrentPlayer = Nothing
  For Each aPlayer In ActivePlayers
      If aPlayer.Number = (CurrentPlayerIndex + ActiveSection.BaseNum) Then
          Set CurrentPlayer = aPlayer
      End If
  Next
  If CurrentPlayer Is Nothing Then
    ' Failsafe
    MsgBox "Error setting current player... contact maintainer", vbCritical, "Tournament Controller"
    CurrentPlayerIndex = 0
  End If
End Sub

Sub Section_DisplaySetSize(ByRef vObject As Object)
' Sets the size of vObject ready for a section to be
' displayed on it
  ' Set the height and width of object to display
  ' section on.
  With vObject
    .Height = ActiveSection.GetHeight()
    .Width = ActiveSection.GetWidth()
    .Left = 0
    .Top = 0
  End With
End Sub

Sub Section_Display(ByRef vObject As Object)
' Displays the current section on vObject
Dim vPlayer As Variant
  Screen.MousePointer = vbHourglass
  ' Display the column headers
  ActiveSection.DisplayHeaders vObject
  ' Display the player information
  vObject.CurrentY = vObject.CurrentY + HeaderHeight
  CurrentPlayerLast = 0
  If ActivePlayers.Count > 0 Then
    ' The section does contain players
    For Each vPlayer In ActivePlayers
      vPlayer.Display vObject
      vObject.CurrentY = vObject.CurrentY + RowHeight
    Next
    CurrentPlayerIndex = 1
    ActiveSection.DisplayGrid vObject
  Else
    ' There are no players in the Section
    CurrentPlayerIndex = 0
    ShowCell vObject, 0, vObject.Width, 10, "There are no players in this section", False, 0, vbWhite
  End If
  Screen.MousePointer = vbNormal
  Section_SetCurrentPlayer
End Sub

Sub Section_ShowCurrentPlayer(vObject As Variant)
' Shows the current player indicator on vObject
Dim BoxColor As Integer
  ' No highlight box if no players in section
  If Players.Count = 0 Then Exit Sub
  ' Set draw mode
  vObject.DrawMode = 10
  If CurrentPlayerLast > 0 Then
    ' Erase last highlight box
    vObject.Line (41, HeaderHeight + RowHeight * (CurrentPlayerLast - 1))-Step(68, RowHeight - 2), vbHighlight, BF
  End If
  CurrentPlayerLast = CurrentPlayerIndex
  If CurrentPlayerIndex > 0 Then
    vObject.Line (41, HeaderHeight + RowHeight * (CurrentPlayerLast - 1))-Step(68, RowHeight - 2), vbHighlight, BF
  End If
  vObject.DrawMode = 13
End Sub

Sub Section_Print(SecID As Long, Mode As Integer)
' Prints the section of given SecID
Dim vPlayer As Variant
  If ActiveSecID <> SecID Then
    ' Sets it do be active
    ActiveSecID = SecID
    Set ActiveSection = Section_Get(SecID)
    ' Get the players...
    Section_GatherPlayers
  End If
  ' Print column headers
  ActiveSection.PrintHeaders Mode
  Printer.Print ""
  If ActivePlayers.Count > 0 Then
    ' The section does contain players
    For Each vPlayer In ActivePlayers
      vPlayer.doPrint Mode
      Printer.Print ""
    Next
  End If
 ' Printer.NewPage
'  Printer.CurrentY = Printer.CurrentY + HeaderHeight * 2
End Sub

Sub Section_DoSort()
' Prompts user for sort, and sorts + renumbers section
Dim SortMethod As Integer
  With dlgSectionSort
    ' Show the dialog
    .Show vbModal
    If .Cancelled Then
      ' User cancelled the operation
      Exit Sub
    End If
    ' Get the sort method from the dialog
    SortMethod = .SortMethod
  End With
  ' Sort ActivePlayers by this method
  Section_Sort SortMethod
  ' Renumber the players
  Section_Renumber
  ' Tourn has changed
  Changed = True
  ' Get the players back into ActivePlayers
  ' in the RIGHT order (ie sorted by player number)
  Section_GatherPlayers
  ' Display the section again.
  Display_Section
End Sub

Sub Section_Sort(Method As Integer)
' Sorts the players in the section
' - only changes their order in ActivePlayers
' Sorts ActivePlayers into TempPlayers, then
' copies TempPlayers back into ActivePlayers
Dim TempPlayers As New Collection
Dim vPlayer As Variant, vSearch As Variant
Dim bLessThan As Boolean, bInsert As Boolean
  ' Loop through all players looking for ones in
  ' the active section
  For Each vPlayer In ActivePlayers
    If vPlayer.SecID = ActiveSecID Then
      ' This player is in active section
      If TempPlayers.Count = 0 Then
        ' Temp players collection empty
        ' Add the player
        TempPlayers.Add vPlayer, getPlayerRef(vPlayer.PlayID)
      Else
        ' Insertion sort in the player
        ' Find first player (vSearch) which new player
        ' should come before
        bInsert = False
        For Each vSearch In TempPlayers
          ' Sort in correct method:
          bLessThan = False
          Select Case Method
            Case SortName:
              bLessThan = (vPlayer.Surname < vSearch.Surname)
            Case SortGrade:
              bLessThan = (vPlayer.Grade > vSearch.Grade)
            Case SortScore:
              If vPlayer.Points = vSearch.Points Then
                ' Same points, sort by Grade Perf
                If vPlayer.GradPerf = vSearch.GradPerf Then
                  ' Same gradperf, sort by Grad Avg
                  bLessThan = (Val(vPlayer.GradAvg) > Val(vSearch.GradAvg))
                Else
                  bLessThan = (Val(vPlayer.GradPerf) > Val(vSearch.GradPerf))
                End If
              Else
                bLessThan = (vPlayer.Points > vSearch.Points)
              End If
          End Select
          If bLessThan Then
            ' Found earliest insertion point
            ' Insert player BEFORE found player ID
            TempPlayers.Add vPlayer, getPlayerRef(vPlayer.PlayID), getPlayerRef(vSearch.PlayID)
            bInsert = True
            Exit For
          End If
        Next
        If bInsert = False Then
          ' Could not find earlier player, add at end
          TempPlayers.Add vPlayer, getPlayerRef(vPlayer.PlayID)
        End If
      End If
    End If
  Next
  ' Clear ActivePlayers
  Clear ActivePlayers
  ' Copy TempPlayers back into ActivePlayers
  For Each vPlayer In TempPlayers
    ActivePlayers.Add TempPlayers(getPlayerRef(vPlayer.PlayID)), getPlayerRef(vPlayer.PlayID)
  Next
End Sub

Sub Section_DoRenumber()
' User has asked to do a renumber
  Section_Renumber
  Section_GatherPlayers
  Display_Section
End Sub

Sub Section_RenumberFromZero()
' User has asked to do a renumber from zero
  ActiveSection.BaseNum = 0
  Section_DoRenumber
End Sub

Sub Section_Renumber()
' Renumbers the active section
Dim numPlayer As Integer, numBase As Integer
  numBase = ActiveSection.BaseNum
  For numPlayer = 1 To ActivePlayers.Count
    ActivePlayers(numPlayer).Number = numBase + numPlayer
  Next
End Sub

Function Player_GetNewID() As Long
' Returns a new SecID
  LastPlayID = LastPlayID + 1
  Player_GetNewID = LastPlayID
End Function

Function Player_Create() As Long
' Creates a new player and returns it's PlayID
Dim PlayID As Long, NewPlayer As New Player
  ' Get New PlayID:
  PlayID = Player_GetNewID()
  ' Assign PlayId to player object
  NewPlayer.PlayID = PlayID
  ' Add player object to Players collection
  Players.Add NewPlayer, getPlayerRef(PlayID)
  ' Return the PlayID
  Player_Create = PlayID
End Function

Function Player_Get(PlayID As Long) As Player
' Returns the player object of given PlayID
  On Error GoTo Player_Get_Error
  Set Player_Get = Players(getPlayerRef(PlayID))
  Exit Function
Player_Get_Error:
  Set Player_Get = Nothing
  Exit Function
End Function

Sub Player_Add()
' Displays the Player Editor dialog to add a new player
Dim PlayID As Long, bQuitLoop As Boolean
  Do
    With dlgPlayerEditor
      ' Initialize the Player Editor dialog
      .EditMode = False ' In Add mode
      .ClearFields      ' Clear all the text boxes
      ' Set the grading system label
      .lblGradSys = GetGradSysName(ActiveSection.GradSystem)
      .cmdSave.Caption = "Add Player"
      .cmdClose.Caption = "Close"
      ' Fill in the number with the next player number for
      ' the active section
      .txtNumber = CStr(Section_GetNewNumber())
      .Show vbModal
    End With
    If dlgPlayerEditor.Cancelled = True Then
      ' User did not click on 'Add Player' button
      bQuitLoop = True ' Quit the loop
    Else
      ' Create a new player
      PlayID = Player_Create()
      ' Assign players properties from Player Editor
      ' textboxes
      With Player_Get(PlayID)
        .SecID = ActiveSecID
        .Surname = dlgPlayerEditor.txtSurname
        .FName = dlgPlayerEditor.txtFName
        .Club = dlgPlayerEditor.txtClub
        .Number = Val(dlgPlayerEditor.txtNumber)
        .Grade = Val(dlgPlayerEditor.txtGrade)
        .ChangeRounds ActiveSection.Rounds
      End With
      ' Regather the players into active section
      Changed = True
      Section_GatherPlayers
      ' Redisplay the section
      Display_Update
    End If
  Loop Until bQuitLoop
End Sub

Sub Player_Delete()
' Deletes current player
Dim MsgValue As Integer, sMessage As String
  If CurrentPlayerIndex = 0 Then
    ' No Player selected
    If ActivePlayers.Count = 0 Then
      MsgBox "There are no players in this section to delete", vbInformation
    Else
      MsgBox "To delete a player, first select the player you wish to delete." _
        , vbInformation
    End If
    Exit Sub
  End If
  sMessage = "Are you sure you wish to delete this player?" + vbCrLf
  sMessage = sMessage + "(" + CurrentPlayer.FName + " " + CurrentPlayer.Surname + ")"
  ' Check that the user really does want to delete this player
  MsgValue = MsgBox(sMessage, vbQuestion + vbYesNo)
  If MsgValue = vbYes Then
    ' Delete the player from the players collection
    Players.Remove CurrentPlayer.PlayID
    ' Tournament has been changed
    Changed = True
    ' Redisplay section
    Section_GatherPlayers
    Display_Section
  End If
End Sub

Sub Player_Edit()
' Edits current player
  If CurrentPlayerIndex = 0 Then
    ' No Player selected
    If ActivePlayers.Count = 0 Then
      MsgBox "There are no players in this section to edit.", vbInformation
    Else
      MsgBox "To edit a player, first select the player you wish to edit." _
        , vbInformation
    End If
    Exit Sub
  End If
  With dlgPlayerEditor
    ' Initialize the Player Editor dialog
    .EditMode = True ' In Add mode
    ' Fill in text boxes...
    .txtSurname = CurrentPlayer.Surname
    .txtFName = CurrentPlayer.FName
    .txtClub = CurrentPlayer.Club
    .txtNumber = CurrentPlayer.Number
    .txtGrade = CurrentPlayer.Grade
    .txtNumber = CurrentPlayer.Number
    ' Set the grading system label
    .lblGradSys = GetGradSysName(ActiveSection.GradSystem)
    ' Button captions
    .cmdSave.Caption = "Save Player"
    .cmdClose.Caption = "Cancel"
    ' Show it
    .Show vbModal
  End With
  If Not dlgPlayerEditor.Cancelled Then
    ' Assign players properties from Player Editor
    ' textboxes
    With CurrentPlayer
      .SecID = ActiveSecID
      .Surname = dlgPlayerEditor.txtSurname
      .FName = dlgPlayerEditor.txtFName
      .Club = dlgPlayerEditor.txtClub
      .Number = Val(dlgPlayerEditor.txtNumber)
      .Grade = Val(dlgPlayerEditor.txtGrade)
    End With
    ' Change has been made to tourn
    Changed = True
    ' Update the display
    Display_Update
  End If
End Sub

Sub Player_Calculate(PlayID As Long)
' Calculates Points, and GradPerf for player
Dim CurPlayer As Player, OppPlayer As Player
Dim numGame As Integer, CurGame As Game
Dim Points As Single, GradTotal As Integer, GradAvg As Integer
Dim numActual As Integer, bSystem As Boolean, BCFPoints As Integer
  If ActiveSection.GradSystem = BCFSystem Then bSystem = True
  Set CurPlayer = Player_Get(PlayID)
  With CurPlayer
    For numGame = 1 To .numGames
      ' For each game played:
      Set CurGame = .Games(numGame) ' This game
      Points = Points + CurGame.Result ' Add up points
      ' Was it a bye?
      If Not CurGame.Bye Then
      ' Find opponent:
        Set OppPlayer = Player_Get(CurGame.OppID)
        If Not (OppPlayer Is Nothing) Then
          ' Found an opponent
          GradTotal = GradTotal + OppPlayer.Grade
          numActual = numActual + 1
          CurGame.OppNumber = OppPlayer.Number
          ' Calculate Grading Performance
          If bSystem Then
            ' BCF Grade Performance Calculation
            BCFPoints = BCFPoints + Player_BCFPoints(CurPlayer.Grade, OppPlayer.Grade, CurGame.Result)
          End If
        End If
      End If
    Next
    ' Set the player properties
    .Points = Points ' Total points scored
    If numActual > 0 Then
      ' Games were played...
      ' Grading average
      If GradTotal > 0 Then
        GradAvg = Int(GradTotal / numActual)
        .GradAvg = CStr(GradAvg)
      End If
      ' Grading Performance
      If bSystem Then
        ' BCF System
        If BCFPoints > 0 Then
          BCFPoints = Int(BCFPoints / numActual)
          .GradPerf = CStr(BCFPoints)
        End If
      Else
        ' Elo System
        .GradPerf = Player_EloPerf(GradAvg, Points, numActual)
      End If
    Else
      ' Not valid
      .GradPerf = " - "
      .GradAvg = " - "
    End If
  End With
End Sub

Function Player_EloPerf(GradAvg As Integer, numPoints As Single, numRounds As Integer) As String
' Calculates Elo Grading Performances
Dim iPerf As Integer
  ' Validation
  If bGotEloTable = False Then
    ' Table not loaded
    Player_EloPerf = "NoTab"
    Exit Function
  ElseIf numRounds < 4 Or numRounds > 10 Then
    ' Out of range of rounds
    Player_EloPerf = "Rnds?"
    Exit Function
  ElseIf (numPoints * 2) > 20 Or numPoints < 0 Then
    ' Out of range
    Player_EloPerf = "Pnts"
    Exit Function
  End If
  ' The Calculation
  iPerf = GradAvg + EloTable(numRounds, numPoints * 2)
  If iPerf < 600 Then
    ' A performance of less than 600 - not valid
    Player_EloPerf = "N/A"
  Else
    Player_EloPerf = CStr(iPerf)
  End If
End Function

Function Player_BCFPoints(PlayerGrade As Integer, OppGrade As Integer, Result As Single) As Integer
' Returns the number of points scored in the BCF grading
' performance calculation
Dim GradeDiff As Integer, RealOppGrade As Integer, Points As Integer
  ' Absolute grading difference
  GradeDiff = OppGrade - PlayerGrade
  If GradeDiff > 40 Then
    ' Modifier
    RealOppGrade = PlayerGrade + 40
  ElseIf GradeDiff < -40 Then
    ' Modifier
    RealOppGrade = PlayerGrade - 40
  Else
    RealOppGrade = OppGrade
  End If
  Points = RealOppGrade
  If Result = 1 Then
    ' Win
    Points = Points + 50
  ElseIf Result = 0 Then
    ' Loss
    Points = Points - 50
  End If
  Player_BCFPoints = Points
End Function
Sub Tourn_Print()
' Prompts the user for printing...
Dim vSection As Variant, CurSecID As Long
Dim bFirstPage As Boolean
  With dlgPrint
    .Show vbModal
    If .Cancelled Then Exit Sub
  End With
  ' In case it all goes wrong:
  On Error GoTo Tourn_Print_Error
  ' Display the loader with wait message
  Load frmLoader
  frmLoader.lblMsg = "Printing. Please wait..."
  frmLoader.Show
  frmLoader.Refresh
  ' Print the tournament header
  Tourn_PrintHeader
  If dlgPrint.PrintScope = PrintAll Then
    ' Print all sections
    ' Save the active section, as it will be switched
    ' from when printing others
    CurSecID = ActiveSecID
    bFirstPage = False
    For Each vSection In Sections
      If bFirstPage = True Then
        Printer.NewPage
      Else
        bFirstPage = False
      End If
      frmLoader.lblMsg = "Printing " + vSection.Name + " section..."
      frmLoader.Refresh
      Section_Print vSection.SecID, dlgPrint.PrintMode
    Next
    ' Switch back to current section
    Section_Switch CurSecID
  Else
    ' Print single section
    Section_Print ActiveSection.SecID, dlgPrint.PrintMode
  End If
  Printer.EndDoc
  Unload frmLoader
  Exit Sub
Tourn_Print_Error:
  Unload frmLoader
  MsgBox "An error occurred whilst printing:" + vbCrLf + _
    Err.Description, vbCritical + vbOKOnly
  Exit Sub
End Sub
Sub Tourn_PrintHeader()
' Prints a tournament header
  Printer.ScaleMode = vbUser
  Printer.Print ""
  Printer.Print ""
  PrintFont Printer, "Arial", 16, True
  Printer.Print ""
  PrintCenter Printer, Name
  PrintFont Printer, "Arial", 12, False
End Sub
Sub Tourn_Edit()
' Shows the Tournament Editor dialog
  With dlgTournEditor
    ' Initialize text boxes
    .txtName = Name
    .txtShortName = ShortName
    ' Show it
    .Show vbModal
    If .Cancelled Then
      ' User clicked Cancel
      Exit Sub
    End If
    ' Set the properties
    ShortName = .txtShortName
    Name = .txtName
    ' Change has been made
    Changed = True
    ' Update the window caption
    Display_Caption
  End With
End Sub

Sub Game_LoadRounds()
' Loads the rounds into the list box of the
' game entry window
Dim numRound As Integer
  With frmGameEntry.cmbRound
    .Clear
    For numRound = 1 To ActiveSection.Rounds
      .AddItem CStr(numRound)
    Next
  End With
End Sub

Sub Game_Enter()
' Called when user enters a new game
Dim PlayNum1 As Integer, PlayNum2 As Integer, Result As Single
Dim PlayID1 As Long, PlayID2 As Long, vPlayer As Variant
Dim Round As Integer, NotValid As Boolean, sMessage As String
Dim bFound1 As Boolean, bFound2 As Boolean, bByeGame As Boolean
Dim Player1 As Player, Player2 As Player
  ' Get data from the boxes
  With frmGameEntry
    Round = Val(.cmbRound)
    PlayNum1 = Val(.txtPly)
    If .txtOpp = "Bye" Then
      PlayNum2 = 0
      bByeGame = True
    Else
      PlayNum2 = Val(.txtOpp)
    End If
    If .optRes(0).Value Then Result = 0
    If .optRes(1).Value Then Result = 0.5
    If .optRes(2).Value Then Result = 1
    ' Validation
    If Round < 1 Or Round > ActiveSection.Rounds Then
      sMessage = "Enter a valid round"
      .cmbRound.SetFocus
      NotValid = True
    ElseIf PlayNum1 = 0 Then
      sMessage = "Enter a valid player number"
      .txtPly.SetFocus
      NotValid = True
    ElseIf PlayNum2 = 0 And Not bByeGame Then
      sMessage = "Enter a valid opponent number"
      .txtOpp.SetFocus
      NotValid = True
    End If
    If NotValid Then
      ' Invalid data entered
      MsgBox sMessage, vbExclamation
      Exit Sub
    End If
  End With
  ' Find Player ID's
  bFound2 = bByeGame ' If it's a bye, got player 2
  For Each vPlayer In ActivePlayers
    If vPlayer.Number = PlayNum1 Then
      ' Found Player 1
      PlayID1 = vPlayer.PlayID
      bFound1 = True
    Else
      If Not bByeGame And vPlayer.Number = PlayNum2 Then
        ' Found Player 2
        bFound2 = True
        PlayID2 = vPlayer.PlayID
      End If
    End If
  Next
  If bFound1 And bFound2 Then
    ' Found both players
    Set Player1 = Player_Get(PlayID1)
    If bByeGame Then
      ' Game was bye
      With Player1.Games(Round)
        .Result = Result
        .OppID = 0
        .Bye = True
      End With
    Else
      ' Game was not a bye
      Set Player2 = Player_Get(PlayID2)
      With Player1.Games(Round)
        .Result = Result
        .OppID = PlayID2
        .OppNumber = Player2.Number
        .Bye = False
      End With
      With Player2.Games(Round)
        .Result = 1 - Result
        .OppID = PlayID1
        .OppNumber = Player1.Number
        .Bye = False
      End With
    End If
    ' Tourn has changed
    Changed = True
    ' Focus back for next game
    frmGameEntry.txtPly.SetFocus
    ' Remember the current offsets
    Display_RememberPos
    ' Redisplay the section
    Display_Section
    ' Recall the current offsets
    ' Recall the current offsets
    Display_RecallPos
  Else
    ' Could not find one of the players
    If bFound1 = False Then
      sMessage = "Could not find player of this number."
      frmGameEntry.txtPly.SetFocus
    Else
      sMessage = "Could not find opponent of this number."
      frmGameEntry.txtOpp.SetFocus
    End If
    ' Display an error
    MsgBox sMessage, vbExclamation
  End If
End Sub
