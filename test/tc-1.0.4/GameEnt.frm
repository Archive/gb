VERSION 5.00
Begin VB.Form frmGameEntry 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Game Entry"
   ClientHeight    =   2100
   ClientLeft      =   4035
   ClientTop       =   5280
   ClientWidth     =   2730
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2100
   ScaleWidth      =   2730
   Begin VB.CheckBox chkBye 
      Caption         =   "&Bye"
      Height          =   270
      Left            =   1620
      TabIndex        =   14
      Top             =   60
      Width           =   795
   End
   Begin VB.PictureBox picTC 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   45
      Picture         =   "GameEnt.frx":0000
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   1620
      Width           =   480
   End
   Begin VB.ComboBox cmbRound 
      Height          =   315
      ItemData        =   "GameEnt.frx":0442
      Left            =   735
      List            =   "GameEnt.frx":0458
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   45
      Width           =   645
   End
   Begin VB.Frame fraMain 
      Height          =   1260
      Left            =   45
      TabIndex        =   6
      Top             =   315
      Width           =   2670
      Begin VB.CommandButton cmdHide 
         Caption         =   "Hide Window"
         Height          =   330
         Left            =   90
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   855
         Width           =   1200
      End
      Begin VB.OptionButton optRes 
         Caption         =   "&Draw"
         Height          =   195
         Index           =   1
         Left            =   930
         TabIndex        =   2
         Top             =   405
         Value           =   -1  'True
         Width           =   675
      End
      Begin VB.OptionButton optRes 
         Caption         =   "&Loss"
         Height          =   195
         Index           =   0
         Left            =   930
         TabIndex        =   1
         Top             =   195
         Width           =   675
      End
      Begin VB.OptionButton optRes 
         Caption         =   "&Win"
         Height          =   195
         Index           =   2
         Left            =   930
         TabIndex        =   3
         Top             =   630
         Width           =   675
      End
      Begin VB.CommandButton cmdEnter 
         Caption         =   "Enter Game"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1380
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   855
         Width           =   1215
      End
      Begin VB.TextBox txtPly 
         Height          =   315
         Left            =   90
         TabIndex        =   0
         Top             =   450
         Width           =   765
      End
      Begin VB.TextBox txtOpp 
         Height          =   315
         Left            =   1785
         TabIndex        =   4
         Top             =   465
         Width           =   765
      End
      Begin VB.Label lblOpp 
         AutoSize        =   -1  'True
         Caption         =   "Opponent:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1650
         TabIndex        =   10
         Top             =   195
         Width           =   900
      End
      Begin VB.Label lblPly 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Player:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   9
         Top             =   195
         Width           =   600
      End
   End
   Begin VB.Label lblInfo 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Information box"
      Height          =   465
      Left            =   570
      TabIndex        =   13
      Top             =   1620
      Width           =   2115
   End
   Begin VB.Label lblRound 
      AutoSize        =   -1  'True
      Caption         =   "Round:"
      Height          =   195
      Left            =   105
      TabIndex        =   7
      Top             =   90
      Width           =   525
   End
End

Attribute VB_Name = "frmGameEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'  Tournament Controller, a tool for chess tournament organisers
'  Copyright (C) 1997-99 Joe Orton <joe@orton.demon.co.uk>
'
'  This program is free software; you can redistribute it and/or modify
'  it under the terms of the GNU General Public License as published by
'  the Free Software Foundation; either version 2 of the License, or
'  (at your option) any later version.
'
'  This program is distributed in the hope that it will be useful,
'  but WITHOUT ANY WARRANTY; without even the implied warranty of
'  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'  GNU General Public License for more details.
'
'  You should have received a copy of the GNU General Public License
'  along with this program; if not, write to the Free Software
'  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
'
Option Explicit

Private Sub chkBye_Click()
' 'Bye' checkbox changed
  If chkBye.Value = 1 Then
    ' Game is a bye
    txtOpp.Text = "Bye"
    txtOpp.Locked = True
    txtOpp.BackColor = vbButtonFace
  Else
    ' Game is not a bye
    txtOpp.Locked = False
    txtOpp.Text = ""
    txtOpp.BackColor = vbWindowBackground
  End If
End Sub

Private Sub cmbRound_GotFocus()
' 'Round' box gets the focus
  lblInfo = "Enter the round number in which the game was played"
End Sub

Private Sub cmdEnter_Click()
' 'Enter Game' button clicked
  OpenTourn.Game_Enter
End Sub

Private Sub cmdEnter_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
' Mouse is moved over the 'Enter Game'
  lblInfo = "This will enter the game into the section"
End Sub

Private Sub cmdHide_Click()
' 'Hide Window' button clicked
  Visible = False
  frmMain.SetFocus
End Sub

Private Sub cmdHide_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
' Mouse is moved over the 'Hide Window' button
  lblInfo = "Hides the Game Entry window"
End Sub

Private Sub cmdUndo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
' Mouse is moved over the 'Undo' button
  lblInfo = "Removes the last result entered"
End Sub

Private Sub optRes_GotFocus(Index As Integer)
' One of the results options gets the focus
  lblInfo = "Select the result of the game"
End Sub

Private Sub txtOpp_GotFocus()
' 'Opponent' text box gets the focus
  lblInfo = "Enter the player number of the opponent"
End Sub


Private Sub txtPly_GotFocus()
' 'Player' text box gets the focus
  lblInfo = "Enter the number of the player."
End Sub
